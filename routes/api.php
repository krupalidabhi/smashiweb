<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
f*/
Auth::routes(['verify' => true]);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('api_register', 'Auth\RegisterController@createuser');

Route::post('login', 'Auth\LoginController@api_login');

Route::middleware('auth:api')->get('video', 'Web\HomeController@video');

Route::get('live_url', 'Web\HomeController@live_url');

Route::middleware('auth:api')->get('search', 'Web\Search\MainController@api_search');

Route::get('user_details', 'Auth\RegisterController@user_details');

Route::middleware('auth:api')->post('api_reset_password', 'Web\Profile\MainController@api_update_password')->middleware('verified');
// Route::post('api_reset_password', 'Web\Profile\MainController@api_update_password');
Route::post('user_update', 'Web\Profile\MainController@api_user_update');

Route::post('api/v2/login', 'API\UserController@login')->middleware('verified');
Route::post('new/order', 'API\UserController@generateOrder')->middleware('auth:api');
Route::delete('subscription', 'API\UserController@cancelSubscription')->middleware('auth:api');

// Route::post('api/v2/register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
Route::post('details', 'API\UserController@details');
});
Route::get('/emailverify/{token}', 'Auth\RegisterController@verifyMailToken');
Route::post('/forgotpassword', 'API\ForgotPassword@reset');

Route::get('/mobile/{provider}/login', 'API\UserController@handleMobileProviderCallback');

Route::get('/example', 'API\UserController@Testmeythod');

Route::get('/update/durations', 'API\UserController@UpdateVideoDuration');
Route::get('/update/durations/force', 'API\UserController@UpdateVideoDurationForce');
Route::get('/update/date', 'API\NovaAPIHelper@updateDateVideo');

// Route::get('/update/durations', 'API\UserController@UpdateVideoDuration');
Route::middleware('auth:api')->get('/video/categories', 'CategoriesController@SearchCatagory');
Route::get('/video/related/videos', 'CategoriesController@SearchTag');
Route::middleware('auth:api')->get('/video/related', 'CategoriesController@related_video');
Route::middleware('auth:api')->get('/video/mostViewed', 'CategoriesController@mostviewshow');

Route::post('apple/appleUser', 'AppleController@craeteuserforios');
Route::post('apple/get/appleUser', 'AppleController@getUserForIos');

Route::middleware('auth:api')->get('/video/shows', 'CategoriesController@show_video');
Route::post('/video/notifications', 'NotificationHistoryController@show_notification_History');

Route::post('/video/submit/notifications', 'NotificationHistoryController@submit_notifications');

Route::get('/send/notification', 'NotificationHistoryController@send_notifications');

Route::post('/adminpage/checkchannelstate', 'Web\HomeController@ChannelState');

Route::post('/upload/s3/video', 'API\NovaAPIHelper@UploadS3Video');

Route::post('/upload/s3/video/thumbnail', 'API\NovaAPIHelper@UploadS3VideoThumbnail');

Route::post('/upload/s3/video/image', 'API\NovaAPIHelper@UploadS3VideoImage');

Route::post('/copy/s3/video', 'API\NovaAPIHelper@CopyS3Video');

Route::post('/checkchannel', 'Web\Channel\ChannelController@checkchannel');

Route::post('/check_api', 'Web\HomeController@testFunction');

Route::middleware('auth:api')->get('checkMailStatus', 'CategoriesController@get_mail_status_user');

Route::post('api_reset_password_mobile', 'ResetPasswordApiController@api_reset_password');

Route::post('getLogFromDB', 'LogSaveInDbController@get_log_from_db')->middleware('adminAPI');
Route::get('live-video','Web\HomeController@public_live_video');

Route::post('paymentCreate','paymentController@payment_create');

// Register post notification endpoint
Route::post('/postnotification','UserNotificationController@web_post_notification_endpoint');

Route::middleware('auth:api')->get('newvideo', 'newHomeVodsController@new_video');
Route::middleware('auth:api')->post('userNotification', 'UserNotificationController@store');
//fcm token unregistred api
Route::middleware('auth:api')->post('unregistredNotification', 'UserNotificationController@destroy');

//Notification toggle API
Route::middleware('auth:api')->post('notificationStatus', 'API\UserController@Notification_toggle');

//related vods
// Route::get('relatedtest', 'CategoriesController@related_video');
// 
//[ayment API
Route::middleware('auth:api')->post('payment', 'SubscriptionsPaymentController@create');
Route::middleware('auth:api')->get('price', 'subscriptionPlanPrice@index');