<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get( '/', 'Admin\HomeController@index' );
Route::get( 'login', 'Admin\LoginController@index' );
Route::post( 'login', 'Admin\LoginController@login' );
Route::get( 'logout', 'Admin\LoginController@logout' );

Route::get( '/form', 'Admin\HomeController@form' );

Route::resource( 'admin_role', 'Admin\Access\RoleController' );
Route::resource( 'admin_user', 'Admin\Access\UserController' );

Route::resource( 'page', 'Admin\Page\MainController' );
Route::resource( 'video', 'Admin\Video\MainController' );

Route::resource( 'settings', 'Admin\Settings\MainController' );