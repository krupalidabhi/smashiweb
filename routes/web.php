<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'Web\HomeController@index')->name('home');
Route::get('/test', 'TestController@index');
Route::get('/test/facebook', 'TestController@TestYoutube');
Route::get('/latest', 'ArchiveController@index');
Route::get('/all_episodes', 'Web\AllEpisodes\MainController@index');
Route::get('/category/{slug}', 'Web\Category\MainController@index');
Route::get('/shows', 'Web\Shows\ShowsController@index');
Route::get('/shows/{slug}', 'Web\Shows\ShowsController@showvideo');
Route::get('/most_viewed', 'Web\Mostviewed\MainController@index');
Route::post('/watchedvideo', 'Web\Video\MainController@watchedvideo');

Route::get('refresh-csrf', function(){
    return csrf_token();
});
// Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function(){
	Route::match(['get'], '/adminpage', 'Web\Channel\ChannelController@indexChannel');

// });
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddlewareVodcrop'], function(){
	Route::match(['get'], '/adminpage/vodedit', 'Web\Channel\VodeditController@display_folder_s3');

});
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddlewarePlaylist'], function(){
	Route::match(['get'], '/adminpage/playlist', 'Web\Channel\PlaylistController@playlist');
});

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddlewareNova'], function(){
	Route::match(['get'], '/adminpage/updatets', 'Web\UpdateTs\MainController@updateTsMethod');
});
Route::post('/adminpage/updatechannel', 'Web\Channel\ChannelController@updatechannel');
Route::post('/adminpage/checkchannellive', 'Web\Channel\ChannelController@checkchannel');
Route::post('/adminpage/checkchannelvod', 'Web\Channel\PlaylistController@checkchannel');
Route::post('/adminpage/startchannel', 'Web\Channel\ChannelController@startchannel');
Route::post('/adminpage/stopchannel', 'Web\Channel\ChannelController@stopchannel');

Route::post('/adminpage/checkchannelstate', 'Web\HomeController@ChannelState');

Route::post('/adminpage/vodedit/edit', 'Web\Channel\VodeditController@display_folder_s3');
Route::post('/adminpage/vodedit/crop', 'Web\Channel\VodeditController@crop_json');

Route::post('/adminpage/playlist/s3_api', 'Web\Channel\PlaylistController@playlist_url_update');
Route::post('/adminpage/playlist/update', 'Web\Channel\PlaylistController@playlist_json_update');
Route::post('/adminpage/playlist/start', 'Web\Channel\PlaylistController@playlist_json_start');
Route::post('/adminpage/playlist/stop', 'Web\Channel\PlaylistController@playlist_json_stop');

Route::get('logout', 'Auth\LoginController@logout');

Route::get('/auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('/auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::group(['middleware' => 'Auth'], function () {
	Route::get('/account', 'Web\Profile\MainController@index');
	Route::post('/account/update', 'Web\Profile\MainController@update');
	Route::post('/account/update/password', 'Web\Profile\MainController@update_password');
	Route::post('/account/billing', 'Web\Profile\MainController@update_billing');

	Route::post('/account/newsletter_permation', 'Web\Profile\MainController@newsletter_permation');
});

Route::get('/contact', 'Web\Contact\MainController@index');
Route::post('/contact', 'Web\Contact\MainController@store');
Route::get('/search', 'Web\Search\MainController@index');

Route::get('/subscribe/update/{id}', 'Web\Subscription\MainController@update');
Route::get('/subscribe/{id}', 'Web\Subscription\MainController@index');
Route::post('/subscribe/response', 'Web\Subscription\MainController@store');
Route::post('/subscribe/{id}', 'Web\Subscription\MainController@get_paytabs_data');
Route::get('/subscription/cancel', 'Web\Subscription\MainController@destroy');

Route::get('/blog', 'Web\Blog\MainController@index');
Route::get('/blog/{slug}', 'Web\Blog\MainController@show');

Route::get('/video/{slug}', 'Web\Video\MainController@index');

// Route::get('/fcm/test/{title}', 'FcmController@SendVodNotification');
Route::get('/fcm/test/{title}', function($title){
	(new \App\Http\Controllers\FcmController)->SendVodNotification($title);
});

Route::any('paytabs', 'Web\Payment\MainController@PaymentReturn');

Route::get('/fcm/post', 'UserNotificationController@sendWebNotification');
Route::post('/payment/proceed', 'Web\Payment\MainController@PaymentProceed');

Route::post('/country/state/get', 'Web\Payment\MainController@getState');
Route::post('/state/city/get', 'Web\Payment\MainController@getCity');


Route::post('/subscription/cancel', 'Web\Payment\MainController@cancelSubscription');

Route::get('/paytab/test', 'Web\Payment\MainController@SyncSubscriptionPayment');
Route::get('/paytab/expire', 'Web\Payment\MainController@checkExpiredSubscription');

Route::get('/location/current/{latitude}/{longitude}', 'Location@fetch_current_location');

Route::get('/{slug}', 'Web\Page\MainController@index')->where('slug', '^(?!nova).*$');;


//Route::get('/terms', 'Web\HomeController@index');
//Route::get('/advertise', 'Web\HomeController@index');
//
//Route::get('/category/{category}', 'Web\HomeController@index');
//
//Route::get('/{article}', 'Web\HomeController@index');

Route::get('/nova/buckets/folder', 'API\NovaAPIHelper@getBucketFolders');
Route::get('/nova/buckets/folderlist', 'API\NovaAPIHelper@aws_bucketFolderListVideo');

