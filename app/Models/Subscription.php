<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Subscription
 *
 * @property int $id
 * @property string $name
 * @property int $product_id
 * @property int $pricing_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserSubscription[] $user_subscriptions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription wherePricingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription wherePrice($value)
 */
class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $fillable = ['price_sar', 'price_usd'];
    
    public function user_subscriptions() {
        return $this->hasMany(UserSubscription::class);
    }

    public function getSubscriptionPrice($sub_id){

    	return Destination::addSelect(['last_flight' => this::select('price')
		    ->whereColumn('id', $sub_id)
		    ->orderBy('desc')
		])->get();

    }
}
