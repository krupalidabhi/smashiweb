<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogSaveInDb extends Model {
	protected $table = 'logs';

	protected $fillable = [ 'id' ,'api_name','description' ,'file_Name' ,'type' ,'code' ,'line' ,'ip' , 'user_agent',];
}
