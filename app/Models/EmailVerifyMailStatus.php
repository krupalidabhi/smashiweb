<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailVerifyMailStatus extends Model {
	protected $table = 'verify_mail_sent_status';

	protected $fillable = [ 'id' ,'user_id' ,'status' ,'body' ,'error' ,'email_id' ,'time' ,];
}
