<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $fillable=['user_id','fcm_token','notification_type','expire','status','created_at'];
}
