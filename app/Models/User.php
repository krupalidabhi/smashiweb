<?php

namespace App\Models;

use Illuminate\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Support\Facades\Hash;
use App\Models\SubscriptionsPayment;
use App\Models\Subscription;
use App\Models\UserSubscription;
use Auth;
use App\Http\Controllers\Web\Payment\MainController;

/**
 * App\Models\User
 *
 * @property int                                                                                                            $id
 * @property string                                                                                                         $name
 * @property string                                                                                                         $email
 * @property string|null                                                                                                    $email_verified_at
 * @property string                                                                                                         $password
 * @property string|null                                                                                                    $remember_token
 * @property \Illuminate\Support\Carbon|null                                                                                $created_at
 * @property \Illuminate\Support\Carbon|null                                                                                $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt( $value )
 * @mixin \Eloquent
 * @property-read \App\Models\Category                                                                                      $category
 * @property string|null                                                                                                    $social_type
 * @property string|null                                                                                                    $social_id
 * @property string|null                                                                                                    $social_email
 * @property-read mixed                                                                                                     $is_social
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subscription[]                                       $subscriptions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSocialEmail( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSocialId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSocialType( $value )
 * @property-read mixed                                                                                                     $has_subscription
 * @property-read \App\Models\UserBilling $billing
 */
class User extends Authenticatable implements MustVerifyEmailContract {
	use Notifiable, HasApiTokens;

	protected $table = 'users';

	protected $appends = [ 'is_social', 'has_subscription', 'has_trial' ];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'email_verified_at',
		'password',
		'social_id',
		'social_email',
		'newsletter',
		'social_type',
		'user_agent',
		'notifications_opted',
	];
	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	protected $returnTrialResponse = [
		'id' => 0, 
		'name' => 'trial', 
		'product_id' => 0,
		'price' => 0,
		'pricing_id' => 0,
		'price_sar' => 0,
		'price_usd' => 0,
	];

	public function setPasswordAttribute( $value ) {
		$this->attributes['password'] = Hash::make( $value );
	}

	public function hasVerifiedEmailc($email){

		$checkUserD = $this::where('email', $email)->first();

		if(  !empty( $checkUserD ) && @$checkUserD->email_verified_at == null ){
			return false;
		} else {
			return true;
		}

	}

	public function category() {
		return $this->belongsTo( Category::class );
	}

	public function subscriptions() {
		return $this->belongsToMany( Subscription::class, 'users_subscription' );
	}

	public function billing() {
		return $this->hasOne( UserBilling::class );
	}

	public function getHasSubscriptionAttribute() {
		return $user_subscriptions = UserSubscription::whereUserId( Auth::id() )->where( [
			['is_active', true],
			['subscription_id', '<>', 0],
			['transaction_id', '<>', 0]
		] )->exists();

		if ( $user_subscriptions ) {
			return true;
		} else {
			return false;
		}
	}

	public function getUserType(){

		if($user = Auth::user())
		{
		    $UserType = 'login';

		    if( (new User())->getHasSubscriptionAttribute()){
				$UserType = 'subscribed';
		    } else if( (new User())->getHasTrialAttribute() ){
		    	$UserType = 'trial';
		    }

		} else {
			$UserType = 'guest';
		}

		return $UserType;
	}

	public function getHasTrialAttribute() {
		$q = UserSubscription::whereUserId( Auth::id() )->where( [
			['is_active', true],
			['subscription_id', '=', 0],
			['transaction_id', '=', 0]
		] )->first();

		$user_subscriptions = $q ? $q->toArray() : array();

		if( empty( $user_subscriptions )){
			return false;
		}

		$datetime1 = date_create(); // now
        $datetime2 = date_create( $user_subscriptions['created_at'] );

        $interval = date_diff($datetime1, $datetime2);
		$day = $interval->d;

		$dayTrialLeft = max(($user_subscriptions['valid_for'] - $day), 0);

		if( $dayTrialLeft > 0 ){
			return true;
		} else {
			return false;
		}

	}
	
	public function getSubscriptionType(){

		$user_subscriptions = UserSubscription::whereUserId( Auth::id() )->where( 'is_active', true )->first();

		if ( !empty($user_subscriptions) ) {
			
			if( $user_subscriptions->subscription_id == 0 ){
				return $this->returnTrialResponse;
			}

			return Subscription::where([ 'id' => $user_subscriptions->subscription_id])->get(['name', 'product_id'])->first();

		} else {
			return false;
		}

	}

	public function getSubscriptionDate(){
		$user_subscriptions = UserSubscription::whereUserId( Auth::id() )->where( 'is_active', true )->first();

		if ( !empty($user_subscriptions) ) {
			
			return $user_subscriptions->created_at;

		} else {
			return false;
		}
	}

	public function getSubscriptionExDate(){

		$user_subscriptions = UserSubscription::whereUserId( Auth::id() )->where( 'is_active', true )->first();

		if ( !empty($user_subscriptions) ) {

			if( $user_subscriptions->subscription_id == 0 ){
				return date('Y-m-d H:i:s', strtotime( $user_subscriptions->created_at . ' +'.$user_subscriptions->valid_for.' day'));
			}

			$user_subscriptions->created_at;

			$dailyId 	= (new MainController())->getDailyId();
			$weeklyId 	= (new MainController())->getWeeklyId();
			$monthelyId = (new MainController())->getMonthlyId();
			$annuallyId = (new MainController())->getAnnuallyId();

			$Subscription = Subscription::where([ 'id' => $user_subscriptions->subscription_id])->get(['name', 'product_id'])->first();

			if( empty($Subscription) ){
				return null;
			}

			if( $Subscription->product_id == $dailyId ){
				return date('Y-m-d H:i:s', strtotime( $user_subscriptions->created_at . ' +1 day'));
			} elseif( $Subscription->product_id == $weeklyId ){
				return date('Y-m-d H:i:s', strtotime( $user_subscriptions->created_at . ' +1 week'));
			} elseif( $Subscription->product_id == $monthelyId ){
				return date('Y-m-d H:i:s', strtotime( $user_subscriptions->created_at . ' +1 month'));
			} elseif( $Subscription->product_id == $annuallyId ){
				return date('Y-m-d H:i:s', strtotime( $user_subscriptions->created_at . ' +1 year'));
			}

		} else {
			return null;
		}

	}

	public function getIsSocialAttribute() {
		if ( isset( $this->social_id ) && isset( $this->social_type ) ) {
			return true;
		} else {
			return false;
		}
	}

	public function profile(){
        return $this->hasOne(SubscriptionsPayment::class,'user_id','id');
    }
    }
