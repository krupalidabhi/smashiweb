<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserBilling
 *
 * @property int $id
 * @property int $user_id
 * @property string $address
 * @property string $phone_number
 * @property string $city
 * @property string $state
 * @property int $country_id
 * @property string $postal_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBilling whereUserId($value)
 * @mixin \Eloquent
 */
class UserBilling extends Model {
	protected $table = 'users_billing';
}
