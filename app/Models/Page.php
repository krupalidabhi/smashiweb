<?php

namespace App\Models;

use App\Helpers\Gru;
use App\Helpers\Minion;
use App\Models\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page
 *
 * @property int                             $id
 * @property string                          $title
 * @property string                          $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereContent( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereTitle( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt( $value )
 * @mixin \Eloquent
 * @property int                             $slug_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSlugId( $value )
 * @property string $slug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSlug($value)
 */
class Page extends Model {
	protected $table = 'pages';

	protected $fillable = [ 'title', 'content', 'slug_id' ];
}
