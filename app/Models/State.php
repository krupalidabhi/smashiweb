<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	public $timestamps = false;

	protected static function getAllState($country) {
        return State::whereCountryId($country)->get();
    }
}
