<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LiveVideo
 *
 * @property int $id
 * @property string $video
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LiveVideo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LiveVideo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LiveVideo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LiveVideo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LiveVideo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LiveVideo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LiveVideo whereVideo($value)
 * @mixin \Eloquent
 */
class LiveVideo extends Model
{
    protected $table = "live_video";
}
