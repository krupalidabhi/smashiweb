<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationHistory extends Model {
	protected $table = 'notification_history';

	protected $fillable = [ 'id' ,'user_id' ,'title' ,'body' ,'color' ,'icon' ,'is_read' , 'poster_url',];
}
