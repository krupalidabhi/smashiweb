<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Slug
 *
 * @property int $id
 * @property int $type
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slug newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slug newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slug query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slug whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slug whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slug whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slug whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slug whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Slug extends Model
{
    protected $table = 'slugs';

    protected $fillable = ['type', 'slug'];
}
