<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class webPostNotification extends Model
{
    protected $table = 'web_post_notification';
    protected $fillable = ['endpoint', 'user_id', 'notification_type', 'expire', 'status'];
}
