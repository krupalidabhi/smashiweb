<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class payment_table extends Model
{
    //
    protected $table = 'payment';

    protected $fillable = ['id', 'user_id', 'initial_amount', 'valid_date', 'transaction_id', 'panding_cycle', 'completed_cycle', 'status', 'payment_status', 'total_billing_amount', 'total_billed_amount', 'billing_amount', 'billing_address', 'payment_from', 'payment_type', 'currency', 'currency_symbol', 'timestamp'];
}
