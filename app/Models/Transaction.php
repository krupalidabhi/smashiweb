<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transaction
 *
 * @property int $id
 * @property int $type
 * @property int $user_id
 * @property int $subscription_id
 * @property string $request_raw
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereRequestRaw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereUserId($value)
 * @mixin \Eloquent
 * @property int $request_type
 * @property int|null $response_status
 * @property string|null $subscription_contract_id
 * @property string|null $payment_transaction_status_code
 * @property string|null $tpay_transaction_id
 * @property string|null $next_payment_date
 * @property string|null $error_message
 * @property int|null $subscription_contract_status
 * @property string|null $msisdn
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereErrorMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereMsisdn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereNextPaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction wherePaymentTransactionStatusCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereRequestType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereResponseStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereSubscriptionContractId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereSubscriptionContractStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereTpayTransactionId($value)
 * @property string $signature
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereSignature($value)
 * @property string|null $status
 * @property string|null $response
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereStatus($value)
 */
class Transaction extends Model
{
    protected $table = 'transactions';
}
