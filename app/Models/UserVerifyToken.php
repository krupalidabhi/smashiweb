<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVerifyToken extends Model
{ 
    
    protected $table = 'user_verify_token';

    protected $fillable = ['token', 'user_email', 'user_id', 'timestamp'];
}
