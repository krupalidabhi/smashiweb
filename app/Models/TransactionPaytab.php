<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TransactionPaytab
 *
 * @property int $id
 * @property int $transaction_id
 * @property string $paytab_transaction_id
 * @property string $order_id
 * @property string $response_code
 * @property string $response_message
 * @property string $customer_name
 * @property string $customer_email
 * @property string $transaction_currency
 * @property string $customer_phone
 * @property string $last_4_digits
 * @property string $first_4_digits
 * @property string $card_brand
 * @property string $trans_date
 * @property string $pt_customer_email
 * @property string $pt_customer_password
 * @property string $pt_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereCardBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereCustomerEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereCustomerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereCustomerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereFirst4Digits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereLast4Digits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab wherePaytabTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab wherePtCustomerEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab wherePtCustomerPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab wherePtToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereResponseCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereResponseMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereTransDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereTransactionCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionPaytab whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TransactionPaytab extends Model
{
    protected $table = 'transactions_paytabs';
}
