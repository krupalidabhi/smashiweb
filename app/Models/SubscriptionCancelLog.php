<?php

namespace App\Models;

use App\Helpers\Gru;
use App\Helpers\Minion;
use App\Models\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;

class SubscriptionCancelLog extends Model
{ 
  
    protected $table = 'subscription_cancel_log';
    protected $fillable = ['user_id', 'subscription_id', 'transaction_id', 'reason'];
}
