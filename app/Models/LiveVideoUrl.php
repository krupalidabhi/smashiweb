<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LiveVideoUrl extends Model {
	protected $table = 'live_video_urls';

	protected $fillable = [ 'web_url', 'android_url', 'ios_url' ];
}
