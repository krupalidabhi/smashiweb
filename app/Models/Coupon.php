<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
	protected $fillable = [
		'title',
		'name',
		'coupon_type',
		'amount',
		'type',
		'used_count',
		'limit_count',
		'time',
		'expire',
		'status',
		'created_at',
		'updated_at'
	];
}
