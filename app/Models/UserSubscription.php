<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserSubscription
 *
 * @property int $id
 * @property int $user_id
 * @property int $subscription_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Subscription $subscription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription whereUserId($value)
 * @mixin \Eloquent
 * @property int $is_active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription whereIsActive($value)
 * @property int $transaction_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSubscription whereTransactionId($value)
 */
class UserSubscription extends Model
{
    protected $table = 'users_subscription';
    protected $fillable = ['is_active'];

    public function subscription() {
        return $this->belongsTo(Subscription::class);
    }
}
