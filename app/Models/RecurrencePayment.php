<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecurrencePayment extends Model
{
    protected $table = 'recurrence_payment';

    protected $fillable = ['user_id', 'order_id', 'payment_reference', 'pt_customer_email', 'pt_customer_password',	'pt_token',	'created_at', 'updated_at', 'pt_invoice_id', 'amount', 'currency', 'transaction_id'];
    

}
