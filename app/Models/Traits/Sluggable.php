<?php
/**
 * Created by PhpStorm.
 * User: aymanbitar
 * Date: 2019-01-27
 * Time: 21:18
 */

namespace App\Models\Traits;

use App\Helpers\Gru;
use App\Helpers\Minion;
use App\Models\Slug;

trait Sluggable {

	public function setSlugIdAttribute() {
		$title = $this->title;

		$slug = Minion::create_slug($title, Slug::class);
		$slug = Slug::create(['type' => $this->type, 'slug' => $slug ]);

		$this->attributes['slug_id'] = $slug->id;
	}
}