<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoSocialLink extends Model
{
    protected $table = 'video_social_link';
    protected $fillable = ['video_id', 'social_name', 'time', 'created_at', 'updated_at'];
}
