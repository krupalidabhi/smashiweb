<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdminRole
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AdminRole whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AdminRole whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AdminRole whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AdminRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AdminRole whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminRole query()
 */
class AdminRole extends Model
{
    protected $table = 'admin_roles';
}
