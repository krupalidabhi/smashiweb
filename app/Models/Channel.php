<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'channel';
    protected $fillable = ['name', 'popup'];

 	public static function boot()
	{
	    parent::boot();

	    static::creating(function ($channel) {
	    	$channel->name = json_encode($_REQUEST);
	        /*echo '<pre>';
	        print_r($channel);
	        die;*/
	    });
	}   
    
}