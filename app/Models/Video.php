<?php

namespace App\Models;

use App\Helpers\Gru;
use App\Helpers\Minion;
use App\Models\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Video
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property int $category_id
 * @property string|null $body
 * @property int $slug_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereSlugId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereUpdatedAt($value)
 * @property string $link
 * @property-read \App\Models\Category $category
 * @property-read \App\Models\Slug $slug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereLink($value)
 * @property string|null $raw_link
 * @property int $is_featured
 * @property-read mixed $poster
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereIsFeatured($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereRawLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Video whereSlug($value)
 */
class Video extends Model
{ 
    use \Spatie\Tags\HasTags;
    protected $table = 'videos';

    protected $appends = ['poster'];

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['duration'];

    public function slug()
    {
        return $this->belongsTo(Slug::class);
    }

    public function shows(){
        return $this->belongsTo(Shows::class);
    }

    public function getPosterAttribute()
    {
        $file_name = explode("/", $this->link);

        if (isset($file_name[5])) {
            $file_name = $file_name[5];
            return config('jarvis.aws-endpoint') . $file_name . '/Default/Thumbnails/' . $file_name . '.0000002.jpg';
        }

        return $this->link;
    }

    public function get_thumbnail($link){

        $file_name = explode("/", $link);
        $return = '';
        if (isset($file_name[5])) {
            $file_name = $file_name[5];
            $return = config('jarvis.aws-endpoint') . $file_name . '/Default/Thumbnails/' . $file_name . '.0000002.jpg';
        }

        return $return;
    }
}
