<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reschedule extends Model
{
    protected $fillable = ['title', 'time', 'timestamp', 'expire', 'status'];
    protected $casts = [
        'time' => 'datetime'
	];
}
