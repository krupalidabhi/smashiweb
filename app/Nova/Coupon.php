<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Epartment\NovaDependencyContainer\HasDependencies;
use Epartment\NovaDependencyContainer\NovaDependencyContainer;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Naif\GeneratePassword\GeneratePassword;

class Coupon extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Coupon';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Title')->creationRules('required'),

            GeneratePassword::make('Name')
                ->creationRules('required', 'string', 'min:4')
                ->updateRules('nullable', 'string', 'min:4')
                ->length(12) 
                ->excludeRules(['Symbols']),

            Select::make('Amount type', 'coupon_type')->options([
                'constant' => 'Constant (amount)',
                'dynamic'=> 'Dynamic (Percentage)'
            ])->displayUsingLabels(),

            NovaDependencyContainer::make([
                Text::make('Amount (In numbers)', 'amount')->help(
                    'Ex. 1000'
                )
            ])->dependsOn('coupon_type', 'constant'),
            NovaDependencyContainer::make([
                Number::make('Amount (In Percentage)', 'amount')->help(
                    'Ex. 30, no need to include % symbol.'
                )->min(1)->max(100)->step(1),
            ])->dependsOn('coupon_type', 'dynamic'),

            Select::make('Coupon type', 'type')->options([
                'max_use' => 'Max Use',
                'series_count'=> 'Series Count',
                'single_use' => 'Single Use'
            ])->displayUsingLabels(),

            NovaDependencyContainer::make([
                Text::make('Max Use Count', 'used_count')->help(
                    'How many times this coupon can be used.'
                )
            ])->dependsOn('type', 'max_use'),

            NovaDependencyContainer::make([
                Text::make('Add Series Upto', 'limit_count')->help(
                    'Add series to the coupon.'
                )
            ])->dependsOn('type', 'series_count'),

            Select::make('Status', 'status')->help('Active by default')->options([
                '1' => 'Active',
                '0'=> 'Inactive',
            ])->displayUsingLabels(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
