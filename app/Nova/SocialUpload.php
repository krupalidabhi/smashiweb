<?php

namespace App\Nova;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\File;
use Spatie\TagsField\Tags;
use Laravel\Nova\Http\Requests\NovaRequest;
use Epartment\NovaDependencyContainer\HasDependencies;
use Epartment\NovaDependencyContainer\NovaDependencyContainer;

class SocialUpload extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\\Models\\VideoSocialLink';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
	    'social_name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

	        Select::make('Social Name', 'social_name')->options([
                'youtube' => 'Youtube',
            ])->displayUsingLabels(),

            NovaDependencyContainer::make([
                File::make('Select Video', 'file_name')->disableDownload(),
                Text::make( 'Title' )->onlyOnForms()->sortable()->rules( 'required', 'max:255' ),
                Textarea::make( 'Description' )->onlyOnForms()->sortable()->rules( 'required', 'max:255' ),
                Text::make( 'Tags' )->onlyOnForms()->help('Add multiple by comma seperated. Ex: technology, blockchain'),
            ])->dependsOn('social_name', 'youtube'),
        ];
    }

    public static function fill(NovaRequest $request, $model)
    {


        $request->offsetUnset('Title');

        

        return parent::fill($request, $model);

    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
