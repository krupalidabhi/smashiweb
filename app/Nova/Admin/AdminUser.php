<?php

namespace App\Nova\Admin;

use App\Nova\Resource;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;
use Silvanite\NovaToolPermissions\Role;

class AdminUser extends Resource {
	/**
	 * The model the resource corresponds to.
	 *
	 * @var string
	 */
	public static $model = 'App\\Models\\AdminUser';

	/**
	 * The single value that should be used to represent the resource when being displayed.
	 *
	 * @var string
	 */
	public static $title = 'name';

	public static function label() {
		return 'Admin';
	}

	/**
	 * The logical group associated with the resource.
	 *
	 * @var string
	 */
	public static $group = 'Access';

	/**
	 * The columns that should be searched.
	 *
	 * @var array
	 */
	public static $search = [
		'id',
		'name',
		'email'
	];

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function fields( Request $request ) {
		return [
			ID::make()->sortable(),

			Text::make( 'Name' )
			    ->sortable()
			    ->rules( 'required' ),

			Text::make( 'Email' )
			    ->sortable()
			    ->rules( 'required', 'email' ),

			Password::make( 'Password' )
			        ->creationRules( [ 'required', 'min:6' ] )
			        ->hideFromIndex(),

			BelongsToMany::make( 'Roles', 'roles', Role::class ),
		];
	}

	/**
	 * Get the cards available for the request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function cards( Request $request ) {
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function filters( Request $request ) {
		return [];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function lenses( Request $request ) {
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function actions( Request $request ) {
		return [];
	}
}
