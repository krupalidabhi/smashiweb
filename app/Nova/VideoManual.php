<?php

namespace App\Nova;

use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BooleanGroup;
use Laravel\Nova\Http\Requests\NovaRequest;
use Spatie\TagsField\Tags;
use Laravel\Nova\Fields\Select;
use Codebykyle\CalculatedField\BroadcasterField;
use Codebykyle\CalculatedField\ListenerField;
use Illuminate\Support\Collection;
use Amazon\PopupField\PopupField;
use Epartment\NovaDependencyContainer\HasDependencies;
use Epartment\NovaDependencyContainer\NovaDependencyContainer;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use App\Models\Shows;

class VideoManual extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Video';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
        'link',
        'video_category',
        'body',
        'is_featured'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            // TextWithSlug::make('Title')
            //     ->sortable()
            //     ->rules('required', 'max:255')
            //     ->slug('slug'),

            BroadcasterField::make('Title')
            ->setType('string'),
            // BroadcasterField::make('Body')
            // ->setType('string'),


            Text::make('Link')
                ->creationRules('required'),

            PopupField::make('Aws'),

            Text::make('Published On'),

            Textarea::make('Body'),

            Boolean::make('Is Featured')
                ->trueValue(1)
                ->falseValue(0),

            Text::make('Views')
                ->withMeta(["value" => "0"])
                ->hideFromIndex()
                ->hideFromDetail()
                ->hideWhenUpdating(),

            Select::make('Video Category')->options([
                'تجارة وأعمال' => 'Business',
                'تكنولوجيا'=> 'Technology',
                'لايف ستايل' => 'Lifestyle'
            ])->displayUsingLabels(),

            // BelongsTo::make('Shows')->nullable(),

            BelongsTo::make('Shows', 'shows', \App\Nova\Shows::class)->nullable(),

            Tags::make('Tags'),

            Boolean::make('Under Payment')
                ->trueValue(1)
                ->falseValue(0),

            ListenerField::make('Slug','slug')
                ->calculateWith(function (Collection $values) {
                    $value=$values->values()->join('');
                    $val=str_replace(' ', '-', $value);
                    return $val;

            }),

            NovaDependencyContainer::make([
                File::make('Select Youtube Video', 'file_name')->disableDownload(),
                Text::make( 'Youtube Video Title', 'video_title' )->onlyOnForms()->sortable()->rules( 'max:255' ),
                Textarea::make( 'Youtube Video Description', 'video_description' )->onlyOnForms()->sortable()->rules( 'max:255' ),
                Text::make( 'Youtube Video Tags', 'video_tags' )->onlyOnForms()->help('Add multiple by comma seperated. Ex: technology, blockchain'),
            ])->dependsOn('youtube', true),
            
            // Boolean::make('Youtube')->hideFromDetail()->hideWhenUpdating(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\VideoCategory,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
