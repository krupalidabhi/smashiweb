<?php

namespace App\Nova;

use App\Nova\Metrics\NewUsers;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use App\Nova\Filters;

class User extends Resource {
	/**
	 * The model the resource corresponds to.
	 *
	 * @var string
	 */
	public static $model = 'App\\Models\\User';

	/**
	 * The single value that should be used to represent the resource when being displayed.
	 *
	 * @var string
	 */
	public static $title = 'name';

	public static function authorizedToCreate( Request $request ) {
		return false;
	}

	public function authorizedToUpdate( Request $request ) {
		return false;
	}

	public function authorizedToDelete( Request $request ) {
		return false;
	}

	/**
	 * The columns that should be searched.
	 *
	 * @var array
	 */
	public static $search = [
		'id',
		'name',
		'email'
	];

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function fields( Request $request ) {
		return [
			ID::make()->sortable(),

			Text::make( 'Name' )
			    ->sortable()
			    ->rules( 'required', 'max:255' ),

			Text::make( 'Email' )
			     ->sortable()
			     ->rules( 'required', 'email' ),

			Select::make('social_type')->options([
			    null => 'Web',
			    1 => 'Facebook',
			    2 => 'Google',
			    3 => 'Apple',
			])->displayUsingLabels(),

			Password::make( 'Password' )
			        ->onlyOnForms()
			        ->creationRules( 'required', 'string', 'min:6' )
			        ->updateRules( 'nullable', 'string', 'min:6' ),
		];
	}

	/**
	 * Get the cards available for the request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function cards( Request $request ) {
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function filters( Request $request ) {
		return [
	        new Filters\SocialType,
	    ];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function lenses( Request $request ) {
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function actions( Request $request ) {
		return [
			new DownloadExcel,
		];
	}
}
