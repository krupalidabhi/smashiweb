<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class VideoCategory extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {   

        if( $value == 'empty' ){
            return $query->whereNull('video_category');    
        } else{
            return $query->where('video_category', $value);
        }
        
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            'Empty' => 'empty',
            'Business' => 'تجارة وأعمال',
            'Technology' => 'تكنولوجيا',
            'Lifestyle' => 'لايف ستايل'
        ];
    }
}
