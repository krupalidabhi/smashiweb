<?php

namespace App\Providers;

use App\Models\Social;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Agent\Agent;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function () {
            $device_agent = new Agent();

            if ($device_agent->isMobile()) {
                View::share('device_agent', 'mobile');
            } else {
                View::share('device_agent', 'desktop');
            }

            $social_links = Social::all();

            View::share('social_links', $social_links);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
