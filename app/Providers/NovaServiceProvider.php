<?php

namespace App\Providers;

use App\Nova\Metrics\NewUsers;
use Infinety\MenuBuilder\MenuBuilder;
use Laravel\Nova\Nova;
use Laravel\Nova\Cards\Help;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\NovaApplicationServiceProvider;
use Mattmangoni\NovaBlogifyTool\NovaBlogifyTool;
use Silvanite\NovaToolPermissions\NovaToolPermissions;
use App\Observers\VideoObserver;
use App\Models\Video;
use App\Models\VideoSocialLink;
use App\Http\Controllers\FcmController;
use App\Models\webPostNotification;
use URL;
use Youtube;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Video::observe(VideoObserver::class);
        Video::creating(function($video){

            if( $video->youtube && !empty(@$_FILES['file_name']['tmp_name']) ){

                if( !empty( $video->tags )){
                    $tags = explode(trim($video->video_tags), ',');
                } else {
                    $tags = ['youtube'];
                }

                $video_id = Youtube::upload( $_FILES['file_name']['tmp_name'] , [
                    'title'       => $video->video_title,
                    'description' => $video->video_description,
                    'tags'        => $tags,
                ]);

                unset( $video->youtube );
                unset( $video->video_tags );
                unset( $video->video_title );
                unset( $video->video_description );
                unset( $video->youtube );
                unset( $video->file_name );

            }

            $video->duration = shell_exec("ffmpeg -i $video->link 2>&1 | grep Duration | awk '{print $2}' | tr -d ,");

            if( empty( $video->published_on ) ){
                $video->published_on = date('j/n/Y');
            }
            $Video = new Video();
            $thumbnail = $Video->get_thumbnail($video->link);

            $FcmController = new FcmController();
            $FcmController->SendVodNotification($video->title, $thumbnail,$video->link);

            $this->sendWebNotification($video->title, @$video->body, @$video->slug);

        });

        VideoSocialLink::creating(function($VideoSocialLink){

            if( $VideoSocialLink->social_name == 'youtube' ){

                if( !empty( $VideoSocialLink->tags )){
                    $tags = explode(trim($VideoSocialLink->tags), ',');
                } else {
                    $tags = ['youtube'];
                }

                $video_id = Youtube::upload( $_FILES['file_name']['tmp_name'] , [
                    'title'       => $VideoSocialLink->title,
                    'description' => $VideoSocialLink->description,
                    'tags'        => $tags,
                ]);

                $VideoSocialLink->video_id = $video_id;

            }

        });
    }

    protected function sendWebNotification($title = '', $description = '', $url = ''){

        if( empty($title) ){
            $title = 'New video uplaoded';
        }

        if( empty($description) ){
            $description = 'Check out the newly uploaded video.';
        }

        if( empty($url) ){
            $url = URL::to('/latest');
        } else {
            $url = URL::to('/video/'.$url);
        }

        $allWebNotification = webPostNotification::where('status', 1)->get()->toArray();
        

        if( !empty($allWebNotification) ){
            
            foreach ($allWebNotification as $key => $value) {

                $curl = curl_init();
                
                $dataArr = [
                    'to' => $value['endpoint'],
                    'notification' => [
                        "title" => $title,
                        "body" => $description,
                        "click_action" => $url,
                        "image" => config('app.url') . "admin/img/logo.png",
                    ],
                ];

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS =>json_encode($dataArr),
                  CURLOPT_HTTPHEADER => array(
                    "Authorization: key=".env("FCM_WEB_POST_SERVER_KEY"),
                    "Content-Type: application/json",
                    "senderId: 162364781706",
                    "gcm_sender_id: 162364781706"
                  ),
                ));

                $response = curl_exec($curl);
                curl_close($curl);
            }

            
        }
        
        return response()->json(['sucess']);
        

    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
//        Gate::define('viewNova', function ($user) {
//            return in_array($user->email, [
//                'ayman@digitalpandas.io'
//            ]);
//        });
    }

    /**
     * Get the cards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
//            new Help,
	        new NewUsers,
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
	        new NovaToolPermissions(),
            new NovaBlogifyTool(),
            new MenuBuilder(),
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
