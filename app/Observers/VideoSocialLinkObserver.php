<?php

namespace App\Observers;

use App\VideoSocialLink;

class VideoSocialLinkObserver
{
    /**
     * Handle the video social link "created" event.
     *
     * @param  \App\VideoSocialLink  $videoSocialLink
     * @return void
     */
    public function created(VideoSocialLink $videoSocialLink)
    {
        //
    }

    /**
     * Handle the video social link "updated" event.
     *
     * @param  \App\VideoSocialLink  $videoSocialLink
     * @return void
     */
    public function updated(VideoSocialLink $videoSocialLink)
    {
        //
    }

    /**
     * Handle the video social link "deleted" event.
     *
     * @param  \App\VideoSocialLink  $videoSocialLink
     * @return void
     */
    public function deleted(VideoSocialLink $videoSocialLink)
    {
        //
    }

    /**
     * Handle the video social link "restored" event.
     *
     * @param  \App\VideoSocialLink  $videoSocialLink
     * @return void
     */
    public function restored(VideoSocialLink $videoSocialLink)
    {
        //
    }

    /**
     * Handle the video social link "force deleted" event.
     *
     * @param  \App\VideoSocialLink  $videoSocialLink
     * @return void
     */
    public function forceDeleted(VideoSocialLink $videoSocialLink)
    {
        //
    }
}
