<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Aws\S3\S3Client;

class AwsList {

	private $BUCKET;


	public function __construct(){

		if(Auth::guard('admin')->check()){
			
            $this->BUCKET = config('jarvis.aws-stream-bucket');

		} else {
            return false;
        }

	}

	public function get_bucket_files_hierarchy(){

			// Instantiate the client.
            $s3 = new S3Client([
                'version' => 'latest',
                'region'  => 'eu-central-1'
            ]);
            
            $result = $s3->listObjects([
                'Bucket' => $this->BUCKET,
                'rootSize' => 1,
                // 'Delimiter' => '/',
            ]);

            try {

                return $this->CreatebucketHierarchy( $result['Contents'] );
                
            } catch (S3Exception $e) {
                return [];
            }

	}

    private function CreatebucketHierarchy($arrData){
        $returnArray = [];
        if( empty($arrData) || !is_array($arrData) ){
            return $returnArray;
        }

        $count = 0;
        $videoItem = [];
        foreach ($arrData as $key => $value) {
            
            $count = 0;
            $match = 0;
            
            // if( $value['Size'] == 0 ){

                if (strpos($value['Key'], '.mp4') == false) { 
                    // Not a video
                } 
                else { 
                    $returnArray[] = 'https://'.$this->BUCKET.'.s3.eu-central-1.amazonaws.com/' .$value['Key'];
                } 

                //A folder
                /*$match = preg_match_all('#[/]#', $value['Key'], $count);
                // $getFolder = explode('/', $value['Key'])                
                
                if( $match == 1 && $value['Size'] == 0 ){
                    $returnArray[ preg_replace('#[/]#', '', $value['Key'])] = '';
                } elseif( $match == 2 && $value['Size'] == 0 ){

                    $temp_explode = explode('/', $value['Key']);
                    $returnArray[$temp_explode[0]] = [$temp_explode[1] => ''];

                } elseif( $match == 2 && $value['Size'] > 0 ){
                    $temp_explode = explode('/', $value['Key']);
                    $videoItem[] = [ 'name' => $temp_explode[2], 'link' => $value['Key'], 'visible' => false ];

                    $returnArray[$temp_explode[0]] = [$temp_explode[1] => $videoItem ];
                    
                }*/

            // }

        }
        
        return $returnArray;

    }

}