<?php
/**
 * Created by PhpStorm.
 * User: aymanbitar
 * Date: 2018-12-25
 * Time: 13:47
 */

namespace App\Helpers;


class Gru {

	const OBJECT_TYPE_POST  = 1;
	const OBJECT_TYPE_PAGE  = 2;
	const OBJECT_TYPE_VIDEO = 3;

	const RESPONSE_STATUS_SUCCESS = 1;
	const RESPONSE_STATUS_FAILURE = 2;

	const SOCIAL_FACEBOOK = 1;
	const SOCIAL_GOOGLE   = 2;
}