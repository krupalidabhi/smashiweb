<?php

namespace App\Helpers;


use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Jarvis {

	const FINAL_LINK = 'http://staging.tpay.me/api/TPAYSubscription.svc/json/AddSubscriptionContractRequest';

	private $client;

	public $success = false;
	public $error = null;
	private $status_code = - 1;

	public function __construct() {
		$client_options                = [];
		$client_options['http_errors'] = true;

		$client_options['headers'] = [
			'Content-Type' => 'application/json',
		];

		$this->client = new Client($client_options); //GuzzleHttp\Client
	}

	public function get( $link, $form_params = [] ) {
		if ( empty( $form_params ) ) {
			$result = $this->client->get( $link, [] );
		} else {
			$result = $this->client->get(
				self::FINAL_LINK . $link, [
				'query' => $form_params
			] );
		}

		return self::response( $result );
	}

	public function post($link, $json ) {
		$result = $this->client->post(
			$link, [
			RequestOptions::BODY => $json,
		] );

		return self::response( $result );
	}

	public function response( ResponseInterface $result ) {
		self::setStatusCode( $result->getStatusCode() );

		$response = json_decode( $result->getBody() );

		if ( $response->operationStatusCode == 0) {
			$this->success = true;

			return json_decode( $result->getBody() );
		} else {
			if ( $response ) {
				$this->success = false;

				return json_decode( $result->getBody() );
			} else {
				//The return was null or empty. Something is wrong from the API Side.
			}
		}
	}

	public function getError() {
		return $this->error;
	}

	public function setError( $error ) {
		$this->error = $error;
	}

	public function getStatusCode() {
		return $this->status_code;
	}

	public function setStatusCode( $status_code ) {
		$this->status_code = $status_code;
	}
}