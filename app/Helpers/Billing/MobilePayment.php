<?php
/**
 * Created by PhpStorm.
 * User: aymanbitar
 * Date: 2019-01-27
 * Time: 14:47
 */

namespace App\Helpers\Billing;


use App\Helpers\Gru;
use App\Helpers\Jarvis;
use App\Models\Subscription;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserSubscription;
use Carbon\Carbon;

class MobilePayment {

	private $public_key;
	private $private_key;
	private $customer_account_number;
	private $msisdn;
	private $operator_code;
	private $subscription_plan_id;
	private $initial_payment_product_id;
	private $initial_payment_date;
	private $execute_initial_payment_now;
	private $recurring_payment_product_id;
	private $product_catalog_name;
	private $execute_recurring_payment_now;
	private $contract_start_date;
	private $contract_end_date;
	private $autorenew_contract;
	private $language = 1;
	private $send_verification_sms;
	private $allow_multiple_free_start_periods;
	private $header_enrichment_reference_code = '';
	private $sms_id = '';

	public function __construct() {
		$this->public_key                        = config( 'tpay.public_key' );
		$this->private_key                       = config( 'tpay.private_key' );
		$this->execute_initial_payment_now       = var_export( false, true );
		$this->execute_recurring_payment_now     = var_export( true, true );
		$this->autorenew_contract                = var_export( true, true );
		$this->send_verification_sms             = var_export( true, true );
		$this->allow_multiple_free_start_periods = var_export( true, true );

		$this->operator_code = config( 'tpay.operator_du' );
	}

	public function create_transaction( User $user, Subscription $subscription ) {
		$now = Carbon::now();

		$transaction                  = new Transaction();
		$transaction->user_id         = $user->id;
		$transaction->subscription_id = $subscription->id;
		$transaction->save();

		$this->customer_account_number      = $user->id;
		$this->msisdn                       = '00971556493006';
		$this->operator_code                = $subscription->operator;
		$this->subscription_plan_id         = $subscription->subscription_plan_id;
		$this->initial_payment_product_id   = $subscription->sku;
		$this->initial_payment_date         = $now->format( 'Y-m-d H:i:s' ) . 'Z';
		$this->recurring_payment_product_id = $subscription->sku;
		$this->product_catalog_name         = $subscription->catalog_name;

		$this->contract_start_date              = $now->format( 'Y-m-d H:i:s' ) . 'Z';
		$this->contract_end_date                = $now->addYear( 1 )->format( 'Y-m-d H:i:s' ) . 'Z';
		$this->header_enrichment_reference_code = '';
		$this->sms_id                           = '';

		$signature = self::generate_signature_digest();

		$request_body_array = [
			"signature"                     => $signature,
			"customerAccountNumber"         => (String) $this->customer_account_number,
			"msisdn"                        => $this->msisdn,
			"operatorCode"                  => $this->operator_code,
			"subscriptionPlanId"            => (int) $this->subscription_plan_id,
			"initialPaymentproductId"       => $this->initial_payment_product_id,
			"initialPaymentDate"            => $this->initial_payment_date,
			"executeInitialPaymentNow"      => $this->execute_initial_payment_now,
			"executeRecurringPaymentNow"    => $this->execute_recurring_payment_now,
			"recurringPaymentproductId"     => $this->recurring_payment_product_id,
			"productCatalogName"            => $this->product_catalog_name,
			"autoRenewContract"             => $this->autorenew_contract,
			"sendVerificationSMS"           => $this->send_verification_sms,
			"allowMultipleFreeStartPeriods" => $this->allow_multiple_free_start_periods,
			"contractStartDate"             => $this->contract_start_date,
			"contractEndDate"               => $this->contract_end_date,
			"language"                      => $this->language,
			"headerEnrichmentReferenceCode" => $this->header_enrichment_reference_code,
			"smsId"                         => $this->sms_id
		];

		$request_body = json_encode( $request_body_array );

		$transaction->request_raw = $request_body;
		$transaction->save();
		$transaction = Transaction::find( $transaction->id );

		$client  = new Jarvis();
		$request = $client->post( $request_body );

		if ( $client->success ) {
			self::handle_success( $transaction, $request );
		} else {
			self::handle_failure( $transaction, $request );
		}
	}

	private function generate_signature_digest() {
		$message =
			$this->customer_account_number .
			$this->msisdn .
			$this->operator_code .
			$this->subscription_plan_id .
			$this->initial_payment_product_id .
			$this->initial_payment_date .
			$this->execute_initial_payment_now .
			$this->recurring_payment_product_id .
			$this->product_catalog_name .
			$this->execute_recurring_payment_now .
			$this->contract_start_date .
			$this->contract_end_date .
			$this->autorenew_contract .
			$this->language .
			$this->send_verification_sms .
			$this->allow_multiple_free_start_periods .
			$this->header_enrichment_reference_code .
			$this->sms_id;

		return $this->public_key . ":" . hash_hmac( 'sha256', $message, $this->private_key );
	}

	private function handle_success( Transaction $transaction, $request ) {
		self::update_transaction( $transaction, $request );
		$transaction->response_status = Gru::RESPONSE_STATUS_SUCCESS;
		$transaction->save();

		$user_subscription                  = new UserSubscription();
		$user_subscription->transaction_id  = $transaction->id;
		$user_subscription->user_id         = $transaction->user_id;
		$user_subscription->subscription_id = $transaction->subscription_id;
		$user_subscription->save();

		return true;
	}

	private function handle_failure( Transaction $transaction, $request ) {
		self::update_transaction( $transaction, $request );
		$transaction->response_status = Gru::RESPONSE_STATUS_FAILURE;
		$transaction->save();

		return false;
	}

	private function update_transaction( Transaction $transaction, $request ) {
		$transaction->subscription_contract_id        = $request->subscriptionContractId;
		$transaction->payment_transaction_status_code = $request->paymentTransactionStatusCode;
		$transaction->tpay_transaction_id             = $request->transactionId;
		$transaction->error_message                   = $request->errorMessage;
		$transaction->subscription_contract_status    = $request->subscriptionContractStatus;
		$transaction->msisdn                          = $request->msisdn;
		$transaction->save();
	}

}