<?php
/**
 * Created by PhpStorm.
 * User: bitar
 * Date: 2019-03-10
 * Time: 15:42
 */

namespace App\Helpers\Billing;


use App\Models\Transaction;
use App\Models\TransactionPaytab;
use Illuminate\Http\Request;

class Paytabs {

	public static function create_paytab_transaction( Request $request, Transaction $transaction ) {
		$paytab_transaction                        = new TransactionPaytab();
		$paytab_transaction->transaction_id        = $transaction->id;
		$paytab_transaction->order_id              = $request->get( 'order_id' );
		$paytab_transaction->paytab_transaction_id = $request->get( 'transaction_id' );
		$paytab_transaction->response_code         = $request->get( 'response_code' );
		$paytab_transaction->response_message      = $request->get( 'response_message' );
		$paytab_transaction->customer_name         = $request->get( 'customer_name' );
		$paytab_transaction->customer_email        = $request->get( 'customer_email' );
		$paytab_transaction->transaction_currency  = $request->get( 'transaction_currency' );
		$paytab_transaction->customer_phone        = $request->get( 'customer_phone' );
		$paytab_transaction->last_4_digits         = $request->get( 'last_4_digits' );
		$paytab_transaction->first_4_digits        = $request->get( 'first_4_digits' );
		$paytab_transaction->card_brand            = $request->get( 'card_brand' );
		$paytab_transaction->trans_date            = $request->get( 'trans_date' );
		$paytab_transaction->pt_customer_email     = $request->get( 'pt_customer_email' );
		$paytab_transaction->pt_customer_password  = $request->get( 'pt_customer_password' );
		$paytab_transaction->pt_token              = $request->get( 'pt_token' );
		$paytab_transaction->save();
	}
}