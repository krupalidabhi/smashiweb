<?php

namespace App\Helpers;

use App\Models\Reservation;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Validation\Validator;
use Storage;

class Minion {

	public static function upload_image( UploadedFile $image, $location ) {
		$new_name = str_random( 25 ) . '.' . $image->getClientOriginalExtension();
		$image    = Storage::putFileAs( $location, $image, $new_name );

		return $image;
	}

	public static function return_error( $debugger = null, $message = null, $code = - 1 ) {
		$error_data["error"]             = array();
		$error_data["error"]["message"]  = $message;
		$error_data["error"]["code"]     = $code;
		$error_data["error"]["debugger"] = $debugger;

		return response( $error_data, 400 );
	}

    public static function return_success($message = null) {
        $error_data["data"]  = $message;

        return response( $error_data, 200 );
    }

	public static function create_slug( $variable, $class ) {
		$slug  = str_slug( $variable, '-', 'ar' );
		$exist = $class::where( 'slug', 'LIKE', "%$slug%" )->get();

		if ( count( $exist ) > 0 ) {
			$slug .= '-' . ( count( $exist ) + 1 );
		}

		return $slug;
	}

	public static function truncate_text( $value, $length = 1000, $remove_html = false ) {
		if ( strlen( $value ) <= $length ) {
			return $value;
		} else {
			if ( $remove_html ) {
				$value = strip_tags( $value );
			}

			return substr( $value, 0, $length ) . "...";
		}
	}

	public static function extract_image_name( $original_path ) {
		$path_array = explode( "/", $original_path );

		return $path_array[ count( $path_array ) - 1 ];
	}

	public static function extract_image_path( $original_path ) {
		$path_array = explode( "/", $original_path );

		$image_path = $path_array[2];

		for ( $i = 3; $i < count( $path_array ); $i ++ ) {
			$image_path .= "/" . $path_array[ $i ];
		}

		return $image_path;
	}

	public static function next_offset( $total, $current_count, $current_offset ) {
		if ( $current_count >= $total || $current_count + $current_offset >= $total ) {
			return - 1;
		} else {
			return $current_count + $current_offset;
		}
	}

	public static function extract_array_values( $array, $key ) {
		$values = array_map( function ( $var ) use ( $key ) {
			return $var[ $key ];
		}, $array );

		return $values;
	}

	public static function generate_access_token() {
		return hash( 'sha512', mt_rand() );
	}

	public static function word_to_slug( $title, $separator = '-' ) {
		return Str::slug( $title, $separator );
	}

	public static function human_date_format( $date, $difference = 3, $format = 'F d, Y' ) {
		$date = Carbon::createFromFormat( 'Y-m-d H:i:s', $date );

		$now = Carbon::now();

		if ( $date->diffInDays( $now ) > $difference ) {
			// return $date->format( $format );
			return false;
		} else {
			return $date->diffForHumans();
		}
	}

	public static function reformat_date( $date, $format = 'F d, Y' ) {
		$date = Carbon::createFromFormat( 'Y-m-d H:i:s', $date );

		return $date->format( $format );
	}

	public static function boolean_to_integer( $value ) {
		return isset( $value ) ? $value ? 1 : 0 : 0;
	}

	public static function object_to_array( $objects, $value ) {
		$array = array();

		foreach ( $objects as $object ) {
			array_push( $array, $object->$value );
		}

		return $array;
	}

	public static function get_excerpt( $post ) {
		if ( isset( $post->excerpt ) ) {
			return $post->excerpt;
		} else {
			return substr( strip_tags( $post->content ), 0, 210 );
		}
	}

	public static function format_transaction_ids( $ID ) {
		$ID_string = $ID . "";

		while ( strlen( $ID_string ) < 5 ) {
			$ID_string = '0' . $ID_string;
		}

		return $ID_string;
	}

	public static function arabic_date_format( $date, $difference = 3, $format = 'F d, Y' ) {
		$date = Carbon::createFromFormat( 'Y-m-d H:i:s', $date );

		$now = Carbon::now();

		$months = array(
			"Jan" => "يناير",
			"Feb" => "فبراير",
			"Mar" => "مارس",
			"Apr" => "أبريل",
			"May" => "مايو",
			"Jun" => "يونيو",
			"Jul" => "يوليو",
			"Aug" => "أغسطس",
			"Sep" => "سبتمبر",
			"Oct" => "أكتوبر",
			"Nov" => "نوفمبر",
			"Dec" => "ديسمبر"
		);
		
		// $your_date = $video->created_at->format('d F, Y'); // for example
		
		$en_month = date("M", strtotime($date));
		
		$ar_month = $months[$en_month];
		$created_date=$date->format('d '.$ar_month.' Y');
		
		return $created_date;
	}

	public static function ArabicDate($Mydate = '') {

		if( empty($Mydate) ){
			return '';
		}

        // $months = array("January" => "كانون الثاني", "February" => "شهر فبراير", "March" => "مارس", "April" => "أبريل", "May" => "مايو", "June" => "يونيو", "July" => "يوليو", "August" => "أغسطس", "September" => "سبتمبر", "October" => "أكتوبر", "November" => "شهر نوفمبر", "December" => "ديسمبر");

        $months = array("January" => "٠١", "February" => "٠٢", "March" => "٠٣", "April" => "٠٤", "May" => "٠٥", "June" => "٠٦", "July" => "٠٧", "August" => "٠٨", "September" => "٠٩", "October" => "١٠", "November" => "١١", "December" => "١٢");
        // $your_date = date('y-m-d', $timestamp); // The Current Date
        $your_date = $Mydate;
        $en_month = date("F", strtotime($your_date));
        
        $ar_month = $months[$en_month];

        $find = array ("Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri");
        $replace = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
        $ar_day_format = date('D', strtotime($Mydate)); // The Current Day
        $ar_day = str_replace($find, $replace, $ar_day_format);

        $standard				 = array("0","1","2","3","4","5","6","7","8","9");
        $eastern_arabic_symbols  = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
        $current_date = $ar_day.' '.date('d', strtotime($Mydate)).' '.$ar_month.' '.date('Y', strtotime($Mydate));

        return [
        	'week' => $ar_day,
        	'day' => str_replace($standard , $eastern_arabic_symbols , date('d', strtotime($Mydate))),
        	'month' => $ar_month,
        	'year' => str_replace($standard , $eastern_arabic_symbols , date('Y', strtotime($Mydate))),

        ];

    }

    public static function changeNumberFormat($number = 0) {

    	$standard				 = array("0","1","2","3","4","5","6","7","8","9");
        $eastern_arabic_symbols  = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");

    	return str_replace($standard , $eastern_arabic_symbols , $number);
    	// die(' oh');
    }
}