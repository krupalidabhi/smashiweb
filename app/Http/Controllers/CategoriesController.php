<?php

namespace App\Http\Controllers;
use logs;
use Illuminate\Http\Request;
use App\Models\Video;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Shows;
use App\Models\EmailVerifyMailStatus;
use App\Models\LogSaveInDb;
use Exception;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\ArchiveController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Tag;

class CategoriesController extends Controller
{
	   public function SearchTag(Request $request)
    {

 		//Slug is for a post/video
 		try{
		 		$video_category_content=[];
		 	
		 		$slug = urldecode(@$request->get('q'));
		        
		        $main_video = Video::where('id', $slug)->first();
		        Video::where('id', $slug)->increment('views'); //will increment the no. of views for video
		         
		             
		        if ($main_video) {                 
		            $tags = DB::table('videos')
		            ->rightJoin('taggables', 'videos.id', '=', 'taggables.taggable_id')
		            ->join('tags', 'tags.id', '=', 'taggables.tag_id')
		            ->where('videos.id', '=', @$main_video->id)
		            ->select('tags.name','tags.slug','tags.id')
		            ->get();

		            // return $tags;
		            if(is_array($tags)){
			            foreach($tags as $tag){ 
			              $tag_ar[]= $tag->id;
			            }
			       	 } else {
			       	 	$tag_ar = [];
			       	 }
		          
		            $videos = Video::where('id', '!=', $main_video->id)->get();
		            if(!empty($tag_ar)){
		                $tag_related=DB::table('taggables')
		                ->whereIn('tag_id', $tag_ar)
		                ->count();
		            }
		            if(!empty($tag_ar) && $tag_related>1){
		               
		                $videos = DB::table('videos')
		                ->rightJoin('taggables', 'videos.id', '=', 'taggables.taggable_id')
		                ->join('tags', 'tags.id', '=', 'taggables.tag_id')
		                ->where('videos.id', '!=', $main_video->id)
		                ->whereIn('tags.id', $tag_ar)
		                ->select('videos.*')->take(20)
		                ->get();
		            }
		            else{
		            	// die('jkknjn');
		                $videos = Video::where('id', '!=', $main_video->id)->take(20)->get(); 
		            }

		            
		            $video_category_content=[];
					foreach($videos as $row)
					{

						if( empty($row->poster_url) ){
							$poster_url = $this->get_thumbnail($row->link);
						} else {
							$poster_url = $row->poster_url;
						}

						if(empty($row->duration)){

				          $video_duration = '00:04:00.00\n';
						}
						else{
							$video_duration = @$row->duration;
						}
						$video_duration=str_replace("\\n", '', $video_duration);
						$video_duration=str_replace("\n", '', $video_duration);
						if(empty($row->created_at_arabic) && $row->created_at_arabic==false){
						 $created_at_arabic=null;
							}
							else{
								$created_at_arabic=unserialize($row->created_at_arabic);
							}
						$video_category_content[]= array
							(
								"id"=>$row->id,
								"title"=>$row->title,
								"video_link"=>$row->link,
								"video_duration"=>$video_duration,
								"poster_url" => rtrim($poster_url),
								"tags" => $row->video_tag,
								"video_category" =>$row->video_category,
								"description"=>$row->body,
								"created_at"=>date("j/n/Y", strtotime($row->created_at)),
								"created_at_arabic"=>$created_at_arabic
		                            
							);
						
					}
						$isError=false;
		                $message="جلب نتيجة الفيديو ذات الصلة بنجاح";
		                $message_en="Related video result fetch successfully";
		                $status=200;

						$return= response()->json(['search_related_video_result' => $video_category_content,
		                                        'isError' => $isError,
		                                        'message' => $message,
		                                        'message_en'=>$message_en,
		                                        'status' => $status,
		                                        

		                                        ],$status);
		        }
		        else{
		        		$isError=false;
		                $message="لم يتم العثور على فيديو ذات صلة";
		                $message_en="No related video found";
		                $status=400;
		        	$return= response()->json([	'search_related_video_result' => $video_category_content,
		                                        'isError' => $isError,
		                                        'message' => $message,
		                                        'message_en'=>$message_en,
		                                        'status' => $status,
		                                        

		                                        ],$status);
		        }
		   
		        return 	$return;

 		}
 		catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'Related Videos',
                        ]
                    );

            }

 		
        
    }


    protected function get_thumbnail($link){

		$file_name = explode("/", $link);
		$return = '';
		if (isset($file_name[5])) {
			$file_name = $file_name[5];
			$return = config('jarvis.aws-endpoint') . $file_name . '/Default/Thumbnails/' . $file_name . '.0000002.jpg';
		}

		return $return;
	}
    public function SearchCatagory( Request $request )
	{
		try{
      $validator= Validator::make($request->all(),[
        'q'=>'required',
      ]);
      if($validator->fails()){
        return response()->json([
            'isError' => true,
            'message' => $validator->errors()->first()  ,
            'message_en' =>'q is required',
            'status' => 403,
            'timestamp' => time(),

        ],403);
      }
      if($request->q =='latest'){
      		$return= $this->latest();
      }
      else{
      	$video_category_content=[];


				if($request->has('q'))
				{
						$search_query = urldecode( $request->get('q') );
						$slug= $search_query;
				        $ar_slug='menu.'.$slug; //category saved in arabic so have to covert slug into arabic words
				    	 $videos =Video::where('video_category', __($ar_slug))->orderBy('created_at', 'DESC')->get();

						$video_category_content=[];
						foreach($videos as $row)
						{

							if( empty($row->poster_url) )
							{
								$poster_url = $this->get_thumbnail($row->link);
							}
							 else {
								$poster_url = $row->poster_url;
							}

								if(empty($row->duration))
								{

						          $video_duration = '00:04:00.00\n';
								}
								else{
									$video_duration = @$row->duration;
								}
								$video_duration=str_replace("\\n", '', $video_duration);
								$video_duration=str_replace("\n", '', $video_duration);

								if(empty($row->created_at_arabic) && $row->created_at_arabic==false)
								{
							 $created_at_arabic=null;
								}
								else{
									$created_at_arabic=unserialize($row->created_at_arabic);
								}
							$video_category_content[]= array
								(
									"id"=>$row->id,
									"title"=>$row->title,
									"video_link"=>$row->link,
									"video_duration"=>$video_duration,
									"poster_url" => $poster_url,
									"video_category" =>$row->video_category,
									"description"=>$row->body,
									"created_at"=>date("j/n/Y", strtotime($row->created_at)),
									"created_at_arabic"=>$created_at_arabic

								);

						}
			   if (!empty($video_category_content))
			   {
			   				$isError=false;
			                $message="فئات نتيجة جلب بنجاح";
			                $message_en="Categories result fetch successfully";
			                $status=200;

							$return= response()->json(['search_categories_result' => $video_category_content,
			                                        'isError' => $isError,
			                                        'message' => $message,
			                                        'message_en'=>$message_en,
			                                        'status' => $status,


			                                        ],$status);
						}
						else{
							$isError=true;
			                $message="الفئات غير موجودة";
			                $message_en="Categories does not exist";
			                $status=404;

							$return= response()->json(['search_categories_result' => $video_category_content,
			                                        'isError' => $isError,
			                                        'message' => $message,
			                                        'message_en'=>$message_en,
			                                        'status' => $status,


			                                        ],$status);

						}

					}
      }
				
					return $return;

		}
		catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'Categories',
                         'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }



	}
	public function mostviewshow(Request $request){

		try{
			$mostview_content=[];
			$mostview_videos = Video::orderBy('views','desc')->get();
			
			$video_category_content=[];
			foreach($mostview_videos as $row)
			{

				if( empty($row->poster_url) ){
					$poster_url = $this->get_thumbnail($row->link);
				} else {
					$poster_url = $row->poster_url;
				}

					if(empty($row->duration)){

			          $video_duration = '00:04:00.00\n';
					}
					else{
						$video_duration = @$row->duration;
					}

				$video_duration=str_replace("\\n", '', $video_duration);
				$video_duration=str_replace("\n", '', $video_duration);


				if(empty($row->created_at_arabic) && $row->created_at_arabic==false){
				 $created_at_arabic=null;
					}
					else{
						$created_at_arabic=unserialize($row->created_at_arabic);
					}
				$mostview_content[]= array
					(
						"id"=>$row->id,
						"title"=>$row->title,
						"video_link"=>$row->link,
						"video_duration"=>$video_duration,
						"poster_url" => $poster_url,
						"video_category" =>$row->video_category,
						"views" =>$row->views,
						"description"=>$row->body,
						"created_at"=>date("j/n/Y", strtotime($row->created_at)),
						"created_at_arabic"=>$created_at_arabic

					);

			}
			// return $mostview_content;
			 if (!empty($mostview_content)) {
   				$isError=false;
                $message="فئات نتيجة جلب بنجاح";
                $message_en="Most views shows result fetch successfully";
                $status=200;

				$return= response()->json(['search_mostViewed_result' => $mostview_content,
                                        'isError' => $isError,
                                        'message' => $message,
                                        'message_en'=>$message_en,
                                        'status' => $status,


                                        ],$status);
			}
			else{
				$isError=true;
                $message="الفئات غير موجودة";
                $message_en="Most views shows  does not exist";
                $status=404;

				$return= response()->json([
                                        'isError' => $isError,
                                        'message' => $message,
                                        'message_en'=>$message_en,
                                        'status' => $status,


                                        ],$status);

			}
			return $return;

		}
		catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'mostviewshow',
                         'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }

	}
	public function shows_video(){

        $ar_slug='menu.shows';
      return  $shows = Shows::orderBy('created_at','asc')->get();

    }

    public function show_video( Request $request) {

    	try{
			$success=[];
        
        $validator = Validator::make($request->all(), [
           			 'q' => 'required',

        			]);
        if ($validator->fails()) {


		            $return= response()->json([
		                'isError' => true,
		                'message' => $validator->errors()->first(),
		                'message_en' =>'q is invalid',
		                'status' => 400,
		                'timestamp' => time(),

		            ],400);
		        }
		        else{
				        	$title=   urldecode($request->get('q'));
			        $show_video = Shows::where('title', $title)->first();
			        
			        if (!empty($show_video)) {
					$success['id']=$show_video->id;
					$success['title']=$show_video->title;
					$success['presenter_image']=$show_video->presenter_image;
					$success['presenter_logo']=$show_video->presenter_logo;
					$success['show_link']=$show_video->show_link;
					$success['body']=$show_video->body;
					$success['created_at']=date("j/n/Y", strtotime($show_video->created_at));
					$success['updated_at']=date("j/n/Y", strtotime($show_video->updated_at));


		       	$return= response()->json([
		       					'responseObject'=>$success,
				                'isError' => false,
				                'message' => 'إظهار جلب الفيديو بنجاح',
				                'message_en' =>'Show video fetch successfully',
				                'status' => 200,
				                'timestamp' => time(),

				            ],200);
				        }
				        else{
				        	$return= response()->json([
				        		'responseObject'=>$success,
				                'isError' => false,
				                'message' => 'عرض الفيديو غير موجود',
				                'message_en' =>' Show video Not found ',
				                'status' => 400,
				                'timestamp' => time(),

				            ],400);
				        }
		        }
		        return $return;

	   }
	   catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'Show',
                         'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }



       }
       public function get_mail_status_user(Request $request){
       	if($request->has('email')){
       		$details = EmailVerifyMailStatus::where('email_id',$request->get('email'))->first();

       	}
       	else{
       		$mail_status = EmailVerifyMailStatus::all();

       foreach ($mail_status as $status) {
       	# code...
       	$details[]=array(
       		'id'=>$status->id,
       		'user_id'=>$status->user_id,
       		'status'=>$status->status,
       		'body'=>$status->body,
       		'email_id'=>$status->email_id,
       		'time'=>$status->time,
       		'created_at'=>$status->created_at,

       	);
       }
       	}
       	if(!empty($details)){
       			return 	response()->json(['response'=>$details,]);
       		}
       		else{
       			return 	response()->json(['response'=>'No Data Available',]);
       		}


       }

        public function __construct()
    {
        View::share('meta_tags', 'video');
    }


    public function related_video(Request $request)
    {
        //Slug is for a post/video
        $validator = Validator::make($request->all(), [
          'q' => 'required',
      ]);
      if($validator->fails()){
        return response()->json([
            'isError' => true,
            'message' => $validator->errors()->first()  ,
            'message_en' =>'q is required',
            'status' => 403,
            'timestamp' => time(),

        ],403);
    }
       static $flag=0;
       $main_video = Video::where('slug', urldecode(str_replace(' ', '-', $request->q)))->first();
        Video::where('slug', urldecode(str_replace(' ', '-', $request->q)))->increment('views'); //will increment the no. of views for video
         
        if ($main_video) {                 
            $tags = DB::table('videos')
            ->rightJoin('taggables', 'videos.id', '=', 'taggables.taggable_id')
            ->join('tags', 'tags.id', '=', 'taggables.tag_id')
            ->where('videos.id', '=', $main_video->id)
            ->select('tags.name','tags.slug','tags.id')
            ->get();
                
            foreach($tags as $tag){ 
              $tag_ar[]= $tag->id;
            }
          
            $videos = Video::where('id', '!=', $main_video->id)
            ->get();
            if(!empty($tag_ar)){
                $tag_related=DB::table('taggables')
                ->whereIn('tag_id', $tag_ar)
                ->count();
            }
            if(!empty($tag_ar) && $tag_related>1){
               
                $videos = DB::table('videos')
                ->rightJoin('taggables', 'videos.id', '=', 'taggables.taggable_id')
                ->join('tags', 'tags.id', '=', 'taggables.tag_id')
                ->where('videos.id', '!=', $main_video->id)
                ->whereIn('tags.id', $tag_ar)
                ->select('videos.*')
                ->get();
            }
            else{
                $videos = Video::where('id', '!=', $main_video->id)
                ->get(); 
            }
            $video_category_content=[];
			foreach($videos  as $row)
			{

				if( empty($row->poster_url) ){
					$poster_url = $this->get_thumbnail($row->link);
				} else {
					$poster_url = $row->poster_url;
				}

					if(empty($row->duration)){

			          $video_duration = '00:04:00.00\n';
					}
					else{
						$video_duration = @$row->duration;
					}

				$video_duration=str_replace("\\n", '', $video_duration);
				$video_duration=str_replace("\n", '', $video_duration);


				if(empty($row->created_at_arabic) && $row->created_at_arabic==false){
				 $created_at_arabic=null;
					}
					else{
						$created_at_arabic=unserialize($row->created_at_arabic);
					}
				$related_content[]= array
					(
						"id"=>$row->id,
						"title"=>$row->title,
						"video_link"=>$row->link,
						"video_duration"=>$video_duration,
						"poster_url" => $poster_url,
						"video_category" =>$row->video_category,
						"views" =>$row->views,
						"description"=>$row->body,
						"created_at"=>date("j/n/Y", strtotime($row->created_at)),
						"created_at_arabic"=>$created_at_arabic

					);

			}
           				$isError=false;
		                $message="جلب الفيديو ذي الصلة بنجاح";
		                $message_en="Related video  fetch successfully";
		                $status=200;

           return response()->json([
           
				           	'search_related_video_result'=>$related_content, 
				           	'isError' => $isError,
				            'message' => $message,
				            'message_en'=>$message_en,
				            'status' => $status,
           ]);
        } else {
            			$isError=true;
		                $message="لم يتم العثور على فيديو ذات صلة";
		                $message_en="No related video found";
		                $status=400;
		        	return response()->json([	//'search_related_video_result' => $videos,
		                                        'isError' => $isError,
		                                        'message' => $message,
		                                        'message_en'=>$message_en,
		                                        'status' => $status,


		                                        ],$status);
        }
    }
     // use for latest Vods API
    public function latest()
    {
        //
        $ar_slug='menu.latest';
        $date = \Carbon\Carbon::today()->subDays(env("VIDEO_ARCHIVE_DAYS"));

        $archive_videos =Video::where('is_featured', 0)->orderBy('created_at', 'DESC')->take(20)->get();
        
        $video_category_content=[];
 
                    foreach($archive_videos as $row)
                    {

                        if( empty($row->poster_url) ){
         $poster_url = $this->get_thumbnail($row->link);
                        } else {
                            $poster_url = $row->poster_url;
                        }
                        if(empty($row->duration)){

                          $video_duration = '00:04:00.00\n';
                        }
                        else{
                            $video_duration = @$row->duration;
                        }
                        $video_duration=str_replace("\\n", '', $video_duration);
                        $video_duration=str_replace("\n", '', $video_duration);
                        if(empty($row->created_at_arabic) && $row->created_at_arabic==false){
                         $created_at_arabic=null;
                            }
                            else{
                                $created_at_arabic=unserialize($row->created_at_arabic);
                            }
                      $video_category_content[]= array
                            (
                                "id"=>$row->id,
                                "title"=>$row->title,
                                "video_link"=>$row->link,
                                "video_duration"=>$video_duration,
                                "poster_url" => rtrim($poster_url),
                                "tags" => $row->video_tag,
                                "video_category" =>$row->video_category,
                                "description"=>$row->body,
                                "created_at"=>date("j/n/Y", strtotime($row->created_at)),
                                "created_at_arabic"=>$created_at_arabic

                            );

                    }
                   if (!empty($video_category_content))
			   {
			   				$isError=false;
			                $message="فئات نتيجة جلب بنجاح";
			                $message_en="Categories result fetch successfully";
			                $status=200;

							return response()->json(['search_categories_result' => $video_category_content,
			                                        'isError' => $isError,
			                                        'message' => $message,
			                                        'message_en'=>$message_en,
			                                        'status' => $status,


			                                        ],$status);
						}
						else{
							$isError=true;
			                $message="الفئات غير موجودة";
			                $message_en="Categories does not exist";
			                $status=404;

							return response()->json(['search_categories_result' => $video_category_content,
			                                        'isError' => $isError,
			                                        'message' => $message,
			                                        'message_en'=>$message_en,
			                                        'status' => $status,


			                                        ],$status);

						}
    }


     
}
