<?php

namespace App\Http\Controllers;

use App\Models\SubscriptionsPayment;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Controllers\Web\Payment\MainController;

class SubscriptionsPaymentController extends Controller
{
   protected $errors=1;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'subscriptionId' => 'required |string',
            'orderId' => 'required |string',
            'mobile' => 'required',
            'billingAddress' => 'required |string',
            'billingCity' => 'required |string',
            'billingCountry' => 'required |string',
            'billingState' => 'required |string',
            'billingZipCode' => 'required |string',
            'token' => 'required',
            'planSelected' => 'required',
            'amount' => 'required |string',
            'taxAmount' => 'required |string',
            'currencyCode' => 'required |string',
        ]);
         if($validator->fails()){
             return response()->json([
                                  'isError' => true,
                                  'message' => $validator->errors()->first(),
                                  'message_en'=>'All fields are required',
                                  'status' => 403,
                                    ],403 );
         }
         else{

            $get_user_data=$this->get_user_data(urldecode(auth('api')->user()->id));

            $show_user=$this->show(auth('api')->user()->id);
            if(!empty($show_user)){
                    return  $update= $this->update($request,$get_user_data->name,$get_user_data->email,$get_user_data->id,$show_user->id);
             }
             else{
                    return $store= $this->store($request,$get_user_data->name,$get_user_data->email,$get_user_data->id);
             }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $PaymentMainController = new MainController();
        return $PaymentMainController->PaymentReturn('API');



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$username,$useremail,$user_id)
    {
            $userfields=explode( ' ', $username);
         
                $dbpayment= new SubscriptionsPayment();
                $dbpayment->user_id=$user_id;
                $dbpayment->order_id= urldecode($request->orderId);
                $dbpayment->name= $username;
                $dbpayment->first_name=  ($userfields[0])?? 'null';;
                $dbpayment->last_name= ($userfields[1])?? 'null';
                $dbpayment->email= $useremail;
                $dbpayment->mobile=urldecode($request->mobile );
                $dbpayment->billing_address= urldecode($request->billingAddress);
                $dbpayment->billing_city= urldecode($request->billingCity);
                $dbpayment->billing_country= urldecode($request->billingCountry);
                $dbpayment->billing_state= urldecode($request->billingState);
                $dbpayment->billing_zip_code= urldecode($request->billingZipCode);
                $dbpayment->coupon= (urldecode($request->coupon)) ?? 'null';
                $dbpayment->subscription_id= urldecode($request->subscriptionId);
                $dbpayment->token= urldecode($request->token);
                $dbpayment->plan_selected= urldecode($request->planSelected);
                $dbpayment->amount= urldecode($request->amount);
                $dbpayment->tax_amount= urldecode($request->taxAmount);
                $dbpayment->currency_code= urldecode($request->currencyCode);
                $dbpayment->time=Carbon::now() ;
                $dbpayment->is_active=1;
                $update= $dbpayment->save();

                if($update){
              return response()->json([
                                     'user_id'=>$user_id,
                                     'isError' => false,
                                     'message' => 'تمت الموافقة على الدفع',
                                     'message_en'=>'Payment Approved',
                                     'status' => 200,
                                     ],200 );
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubscriptionsPayment  $subscriptionsPayment
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        //
      return $show= SubscriptionsPayment::where('user_id',$user_id)->first();
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubscriptionsPayment  $subscriptionsPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(SubscriptionsPayment $subscriptionsPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubscriptionsPayment  $subscriptionsPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username,$useremail,$user_id,$id)
    {
                $userfields=explode( ' ', $username);
                $updatepayment= SubscriptionsPayment::find($id);
                $updatepayment->user_id=$user_id;
                $updatepayment->order_id= urldecode($request->orderId);
                $updatepayment->name= $username;
                $updatepayment->first_name= ($userfields[0])?? 'null';
                $updatepayment->last_name= ($userfields[1])?? 'null';
                $updatepayment->email= $useremail;
                $updatepayment->mobile=urldecode($request->mobile );
                $updatepayment->billing_address= urldecode($request->billingAddress);
                $updatepayment->billing_city= urldecode($request->billingCity);
                $updatepayment->billing_country= urldecode($request->billingCountry);
                $updatepayment->billing_state= urldecode($request->billingState);
                $updatepayment->billing_zip_code= urldecode($request->billingZipCode);
                $updatepayment->coupon= (urldecode($request->coupon))?? 'null';
                $updatepayment->subscription_id= urldecode($request->subscriptionId);
                $updatepayment->token= urldecode($request->token);
                $updatepayment->plan_selected= urldecode($request->planSelected);
                $updatepayment->amount= urldecode($request->amount);
                $updatepayment->tax_amount= urldecode($request->taxAmount);
                $updatepayment->currency_code= urldecode($request->currencyCode);
                $updatepayment->time=Carbon::now() ;
               $update= $updatepayment->save();

                if($update){
              return response()->json(['user_id'=>$user_id,
                                     'isError' => false,
                                     'message' => 'تحديث الدفع',
                                     'message_en'=>'Payment update',
                                     'status' => 200,
                                      ],200 );
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubscriptionsPayment  $subscriptionsPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubscriptionsPayment $subscriptionsPayment)
    {
        //
    }
    public function get_user_data($user_id){
       return $userdata= User::where('id',$user_id)->first();

    }
}
