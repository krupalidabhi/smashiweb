<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\SubscriptionsPayment;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;
use Laravel\Socialite\Facades\Socialite;
use App\Models\Video;
use App\Helpers\Gru;
use Log;
use Facebook;
use App\Models\LogSaveInDb;
use Exception;
use App\Http\Controllers\Web\Payment\MainController;
use App\Models\Subscription;
use App\Models\UserSubscription;
use App\Models\SubscriptionCancelLog;
use App\Models\RecurrencePayment;

class UserController extends Controller
{
public $successStatus = 200;

public function cancelSubscription(){

  $user = Auth('api')->user();

  $UserSubscription = UserSubscription::where(['user_id' => $user->id, 'is_active' => 1])->get()->first();

  if( empty($UserSubscription) ){

    return response()->json([
        'isError' => true,
        'message' => 'لم يتم العثور على اشتراك',
        'message_en' =>'No subscription found',
        'status' => 404,
        'timestamp' => time(),

    ],404);

  }

  $UserSubscription->is_active = 0;

  $RecurrencePayment = RecurrencePayment::where(['user_id' => $user->id, 'is_active' => 1])->get()->first();

  if( !empty($RecurrencePayment) ){
    $RecurrencePayment->is_active = 0;
    $RecurrencePayment->save();
  }
  $SubscriptionCancelLog = new SubscriptionCancelLog();

  if( !empty($SubscriptionCancelLog) ){
    $SubscriptionCancelLog->user_id = $user->id;
    $SubscriptionCancelLog->subscription_id = $UserSubscription->subscription_id;
    $SubscriptionCancelLog->reason = 'User want to cancel';
    $SubscriptionCancelLog->transaction_id = $UserSubscription->transaction_id;
    $SubscriptionCancelLog->save();
  }
  $UserSubscription->save();
  
  return response()->json([
    'isError' => false,
    'message' => 'نجاح',
    'message_en' =>'Success',
    'status' => 200,
    'timestamp' => time(),
  ],200);

}

public function generateOrder(Request $request){

  $validator = Validator::make($request->all(), [
      'tel_code' => 'required|max:10|min:1',
      'tel'=>'required|max:15|min:4',
      'subscription_id'=>'required|max:4',
      'email' => 'required|email|max:100',
      'city' => 'required|max:255',
      'country' => 'required|max:3',
      'state' => 'required|max:255',
      'zip_code' => 'required|max:255',
  ]);

  if ($validator->fails()) {
        
    return response()->json([
        'isError' => true,
        'message' => $validator->errors()->first()  ,
        'message_en' =>'Something went wrong',
        'status' => 403,
        'timestamp' => time(),

    ],403);

  }

  $tel_code = $request->input('tel_code');
  $tel = $request->input('tel');
  $subscription_id = $request->input('subscription_id');
  $email = $request->input('email');
  $city = $request->input('city');
  $country = $request->input('country');
  $state = $request->input('state');
  $zip_code = $request->input('zip_code');
  $billing_address = $city.', '.$state.', '.$zip_code.', '.$country;

  $MainController = new MainController();
  $order_id = $MainController->createUniqueOrderId();
  $user = Auth('api')->user();

  $exUser = explode(' ', $user->name);
  if ( !empty(@$exUser[1]) ) {
    $first_name = $exUser[0];
    $last_name = $exUser[1];
  } else {
    $first_name = $exUser[0];
    $last_name = $exUser[0];
  }

  $subscription_price = Subscription::find($subscription_id)->toArray();

  if( empty($subscription_price) ){

    return response()->json([
      'isError' => true,
      'message' => 'هناك خطأ ما',
      'message_en' =>'Something went wrong',
      'status' => 403,
      'timestamp' => time(),
    ],403);
  }

  $MainController->createOrder([
    'user_id' => $user->id,
    'order_id' => $order_id,
    'subscription_id' => $subscription_id,
    'name' => $user->name,
    'first_name' => $first_name,
    'last_name' => $last_name,
    'email' => $email,
    'mobile' => $tel,
    'billing_address' => $billing_address,
    'billing_city' => $city,
    'billing_country' => $country,
    'billing_state' => $state,
    'billing_zip_code' => $zip_code,
    'plan_selected' => $subscription_price['name'],
    'amount' => $subscription_price['price'],
    'currency_code' => 'AED',
    'is_active' => 0,
    'tel_code' => $tel_code,
  ]);

  $SubscriptionsPayment = SubscriptionsPayment::where('order_id', $order_id)->exists();

  if( $SubscriptionsPayment ){
    return response()->json([
      'responseObject' => [
        'OrderId' => $order_id,
      ],
      'isError' => false,
      'message' => 'نجاح',
      'message_en' =>'Success',
      'status' => 200,
      'timestamp' => time(),
    ],200);
  } else {

    return response()->json([
      'isError' => true,
      'message' => 'هناك خطأ ما',
      'message_en' =>'Something went wrong',
      'status' => 403,
      'timestamp' => time(),
    ],403);
  }

}

/**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        try{

            $success=[];

        $userIp=$request->ip();
        $current_timestamp = Carbon::now()->timestamp;
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|min:2|max:50',
        ]);

        if ($validator->fails()) {
                $isError=true;
                $message="تم إدخال بيانات اعتماد غير صالحة";
                $message_en="Invalid credentials entered";
                $timestamp=$current_timestamp = Carbon::now()->timestamp;
                $status=404;

            $return = response()->json([
                'isError' => true,
                'message' => $validator->errors()->first()  ,
                'message_en' =>'Invalid credentials entered',
                'status' => 403,
                'timestamp' => time(),

            ],403);

        }



        else {

            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

                try {


                $user = Auth::user();

                $getHasSubs = ( new User())->getHasSubscriptionAttribute();
                $getSubsType = ( new User())->getSubscriptionType();

                $getSubsDate = ( new User())->getSubscriptionDate();
                $getSubsExDate = ( new User())->getSubscriptionExDate();

                $paymentdata= SubscriptionsPayment::where('user_id',$user->id)->first();

                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['id'] =               $user->id;
                $success['name']=              $user->name;
                $success['email']=             $user->email;
                $success['emailVerifiedAt']=   $user->email_verified_at;
                $success['socialType']=        $user->social_type;
                $success['socialId']=          $user->social_id;
                $success['socialEmail']=       $user->social_email;
                $success['SubscriptionType']=  $getSubsType;
                $success['SubscriptionDate']=  $getSubsDate;
                $success['SubscriptionExDate']=$getSubsExDate;
                $success['createdAt']=         $user->created_at;
                $success['updatedAt']=         $user->updated_at;
                $success['newsletter']=        $user->newsletter;
                $success['isSocial']=          'false';//$user->isSocial;
                $success['hasSubscription']=   $getHasSubs;
                $success['notifications_opted']=   $user->notifications_opted;
                $isError=false;
                $message="تسجيل الدخول بنجاح";
                $message_en="Sign in successfully";
                $status=200;
                //Log::channel('apierror')->info("log in happend");
                $return = response()->json(['responseObject' => $success,
                                        'isError' => $isError,
                                        'message' => $message,
                                        'message_en'=>$message_en,
                                        'status' => $status,
                                        'length_token'=>strlen($success['token']),


                                        ], $this->successStatus);
                }

                catch (Exception $e) {
                    echo $e->getMessage();
                    die;
                }
            }
            else{
                $isError=true;
                $message="تم إدخال بيانات اعتماد غير صالحة";
                $message_en="Invalid credentials entered";
                $timestamp=$current_timestamp = Carbon::now()->timestamp;
                $status=403;
                // $isError["responseCode"]=$request->status();
                $return = response()->json([
                                        'isError' => $isError,
                                        'message' => $message,
                                        'message_en' => $message_en,
                                        'status' => $status,
                                        'timestamp' => $timestamp,
                ],403);
            }
        }
       $api_name='Login';
        Log::channel('apierror')->info("
            **************************************
                        API Name : $api_name
                        Message_En : $message_en
                        Message: $message
                        Status :$status
                        User IP: $userIp
                        Timestamp:  $current_timestamp
            **************************************
            ");
        return $return;
        }
        catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $userIp,
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'login',
                         'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }


    }


    public function handleMobileProviderCallback( Request $request, $provider ){
            $success=[];
            $validator= Validator:: make($request->all(),[
              'code'=> 'required',
            ]);
            if($validator->fails()){
              return response()->json([
                  'isError' => true,
                  'message' => $validator->errors()->first(),
                  'message_en' =>'code is requred',
                  'status' => 400,
                  'timestamp' => time(),

              ],400);
            }
        if( $provider == 'facebook' ){
            session_start();
            try {
                $fb = new Facebook\Facebook([
                  'app_id' => env('FB_app_id'), // Replace {app-id} with your app id
                  'app_secret' => env('FB_app_secret'),
                  'default_graph_version' => 'v3.2',
                ]);

                $helper = $fb->getRedirectLoginHelper();

                $accessToken = urldecode( @$request->input( 'code' ) );
                $response = $fb->get('/me?fields=id,name,email', $accessToken);
                $user = $response->getGraphUser()->asArray();
                $provider = self::get_social_type( $provider );

              $chek_email=$this->update_social_user((array)$user,$provider,$request->header('User-Agent'));
              if(!empty($chek_email)){
                $authUser= $chek_email;
              }
              else{
                $authUser = User::firstOrCreate(
                    [
                        'social_id'   => $user['id'],
                        'social_type' => $provider,
                    ], [
                        'name'         => $user['name'],
                        'social_email' => $user['email'],
                        'email'        => $user['email'],
                        'social_type'  => $provider,
                        'social_id'    => $user['id'],
                        "user_agent"   => $request->header('User-Agent'),
                        'notifications_opted'=> 1 ,
                    ]
                );
              }


                Auth::login( $authUser, true );

                $testUser = Auth::user();

                $getHasSubs = ( new User())->getHasSubscriptionAttribute();
                $getSubsType = ( new User())->getSubscriptionType();

                $getSubsDate = ( new User())->getSubscriptionDate();
                $getSubsExDate = ( new User())->getSubscriptionExDate();

                $paymentdata= SubscriptionsPayment::where('user_id',$testUser->id)->first();

                $success['token'] = $testUser->createToken('MyApp')->accessToken;
                $success['name'] = $testUser->name;
                $success['social_email'] = $testUser->social_email;

                $success['SubscriptionType']=  $getSubsType;
                $success['SubscriptionDate']=  $getSubsDate;
                $success['SubscriptionExDate']=$getSubsExDate;

                $success['social_type'] = $testUser->social_type;
                $success['social_id'] = $testUser->social_id;
                $success['isSocial']=          'true';
                $success['hasSubscription']=   $getHasSubs;
                $success['notifications_opted'] = $testUser->notifications_opted;


                return response()->json([
                    'responseObject' => $success,
                    'message'=> 'تسجيل الدخول بنجاح',
                    'message_en'=> 'Login successfully',
                    'isError' => false,
                    'status' => 200,
                ],200);


            } catch(Exception $e) {
                // abort(400);
                return $e;
            }

        }
        try{
          $googleAuthCode = urldecode( @$request->input( 'code' ) );

          if( empty($googleAuthCode) ){
              return response()->json([
                  'responseObject' => $success,
                  'message'=> 'فشل تسجيل الدخول',
                  'message_en'=> 'Login failed',
                  'isError' => true,
                  'status' => 401,
              ],200);
          }

          $accessTokenResponse= Socialite::driver( $provider )->getAccessTokenResponse($googleAuthCode);
          $accessToken=$accessTokenResponse["access_token"];
          $expiresIn=$accessTokenResponse["expires_in"];
          $idToken=$accessTokenResponse["id_token"];
          $refreshToken=isset($accessTokenResponse["refresh_token"])?$accessTokenResponse["refresh_token"]:"";
          $tokenType=$accessTokenResponse["token_type"];
          $user = Socialite::driver('google')->userFromToken($accessToken);

          $provider = self::get_social_type($provider);
          $chek_email=$this->update_social_user(json_decode(json_encode($user), true),$provider,$request->header('User-Agent'));
          if(!empty($chek_email)){
            $authUser= $chek_email;
          }
          else{
          $authUser = User::firstOrCreate(
              [
                  'social_id'   => $user->id,
                  'social_type' => $provider
              ], [
                  'name'         => $user->name,
                  'social_email' => $user->email,
                  'email'        => $user->email,
                  'social_type'  => $provider,
                  'social_id'    => $user->id,
                  'user_agent'    => $request->header('User-Agent'),
                  'notifications_opted'=> 1 ,
              ]
          );
        }

          Auth::login( $authUser, true );

          $testUser = Auth::user();

          $getHasSubs = ( new User())->getHasSubscriptionAttribute();
          $getSubsType = ( new User())->getSubscriptionType();

          $getSubsDate = ( new User())->getSubscriptionDate();
          $getSubsExDate = ( new User())->getSubscriptionExDate();

          $paymentdata= SubscriptionsPayment::where('user_id',$testUser->id)->first();
          $success['token'] = $testUser->createToken('MyApp')->accessToken;
          $success['name'] = $testUser->name;
          $success['social_email'] = $testUser->social_email;

          $success['SubscriptionType']=  $getSubsType;
          $success['SubscriptionDate']=  $getSubsDate;
          $success['SubscriptionExDate']=$getSubsExDate;
          
          $success['social_type'] = $testUser->social_type;
          $success['social_id'] = $testUser->social_id;
          $success['isSocial']=         ' true';
          $success['hasSubscription']=   $getHasSubs;
          $success['notifications_opted'] = $testUser->notifications_opted;

          $message="تسجيل الدخول بنجاح";
          $message_en="Login successfully";
          return response()->json([
              'responseObject' => $success,
              'message'=> $message,
              'message_en'=> $message_en,
              'isError' => false,
              'status' => 200,
          ],200);
        }
        catch(Exception $e) {
            // abort(400);
            return $e->getMessage();
        }



    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }


    public function UpdateVideoDuration(){

        $data = Video::whereNull('duration')->get();

        foreach( $data as $key => $value ){

            Video::where('id', $value->id)
            ->update(['duration' => shell_exec("ffmpeg -i $value->link 2>&1 | grep Duration | awk '{print $2}' | tr -d ,") ]);

        }

        die('success');
    }

    public function UpdateVideoDurationForce(){

        if( isset($_GET['video_id']) && !empty($_GET['video_id']) ){

          $data = Video::where('id', $_GET['video_id'])->first();

          //foreach( $data as $key => $value ){

              Video::where('id', $data->id)
              ->update(['duration' => shell_exec("ffmpeg -i $data->link 2>&1 | grep Duration | awk '{print $2}' | tr -d ,") ]);

          // }

        } else {

          $data = Video::all();

          foreach( $data as $key => $value ){

              Video::where('id', $value->id)
              ->update(['duration' => shell_exec("ffmpeg -i $value->link 2>&1 | grep Duration | awk '{print $2}' | tr -d ,") ]);

          }

        }

        die('success');
    }

    private function get_social_type( $provider ) {
        if ( $provider == 'facebook' ) {
            return Gru::SOCIAL_FACEBOOK;
        } elseif ( $provider == 'google' ) {
            return Gru::SOCIAL_GOOGLE;
        } else {
            abort( 404 );
        }
    }
    public function update_social_user(array $user,$provider,$agent){
     $check_email=User::where('email',$user['email'])->first();

      if (!empty($check_email)) {
        // code...
        $email_id=$check_email->id;
          $update_details = User::where('id', $email_id)
               ->update([
                   'name'         => $user['name'],
                   'social_email' => $user['email'],
                   'email'        => $user['email'],
                   'social_type'  => $provider,
                   'social_id'    => $user['id'],
                   'user_agent'    => $agent,
               ]);
               if($update_details){
                 return User::where('id',$email_id)->first();
               }



      }

    }
    public function Notification_toggle(Request $request){
      $validator=Validator::make($request->all(),[
        'statusCode'=> 'required | int',
      ]);
      if($validator->fails()){
        $return= response()->json([
                                'isError' => true,
                                'message' => $validator->errors()->first(),
                                'message_en'=>'statusCode required',
                                'status' => 403,
                                  ],403 );
      }
      else{
        $findeuser=User::where('id',auth('api')->user()->id)->first();
        if(!empty($findeuser))
        {

          $change_notification_opted = User::find($findeuser->id);
          $change_notification_opted ->notifications_opted = $request->statusCode;
          $change_notification_opted ->save();

          $Newstatus=User::where('id',auth('api')->user()->id)->first(); 
          $return = response()->json([ 'responseObject'=> $Newstatus->notifications_opted,
                                       'isError' => false,
                                       'message' => 'تم تحديث البيانات بنجاح',
                                       'message_en'=>'data Updated successfully ',
                                       'status' => 200,
                                       ],200 );
         
        }
        else{
            $return= response()->json([
                                  'isError' => true,
                                  'message' => 'المستخدم غير متوفر',
                                  'message_en'=>'User Not Available',
                                  'status' => 403,
                                    ],403 );
        }
      }
      
      return $return;
    }
}
