<?php

// namespace App\Http\Controllers\Api\Auth;
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Models\User;
use Validator;
use App\Models\LogSaveInDb;
use Exception;

class ForgotPassword extends Controller
{
    use SendsPasswordResetEmails;

    public function reset(Request $request){

        try{
                    $validator = Validator::make($request->all(), [
                    'email' => 'required|email|max:255'
                ]);

                if ($validator->fails()) {


                    return response()->json([
                        'isError' => true,
                        'message' => $validator->errors()->first(),
                        'message_en' =>'Email is invalid',
                        'status' => 400,
                        'timestamp' => time(),

                    ],400);

                }

               $email= $request->input('email');
               $user = User::where('email', '=', $email)->first();

                if(!empty($user))
                {

                    $User = new User;

                if( !$User->hasVerifiedEmailc($email) ){
                                    $isError=true;
                                    $message="لم يتم التحقق من البريد الإلكتروني";
                                    $message_en="Email is not verified";
                                    $status=403;

                                return response()->json([
                                                        'isError' => $isError,
                                                        'message' => $message,
                                                        'message_en'=>$message_en,
                                                        'status' => $status,
                                                    ],$status);

                     }
                     else{

                                   return  $this->sendResetLinkEmail($request);

                     }



            }
            else{
                                $isError=true;
                                $message="البريد الإلكتروني غير موجود";
                                $message_en="User does not exist";
                                $status=404;

                        return response()->json([
                                                'isError' => $isError,
                                                'message' => $message,
                                                'message_en'=>$message_en,
                                                'status' => $status,


                                                ],$status);

            }
       }
       catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'ForgotPassword',
                         'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }


}

    protected function sendResetLinkResponse( Request $request, $response ) {
                $isError=false;
                $message="كلمة المرور إعادة تعيين البريد المرسلة";
                $message_en="Password Reset mail sent";
                $status=200;
        return response()->json([
            'isError' => $isError,
            'message' => $message,
            'message_en'=>$message_en,
            'status' => $status,
		],$status);
    }

    protected function sendResetLinkFailedResponse( Request $request, $response ) {
                $isError=false;
                $message="فشل إعادة تعيين كلمة المرور.";
                $message_en="Password Reset failed.";
                $status=403;
        return response()->json([
                'isError' => $isError,
                'message' => $message,
                'message_en'=>$message_en,
                'status' => $status,
        ],$status);
	}

}
