<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Web\Channel\VodeditController;
use App\Helpers\Minion;
use App\Helpers\AwsList;
use App\Models\Video;
use Aws\S3\S3Client;
use Aws\MediaConvert\MediaConvertClient;
use Aws\S3\ObjectUploader;

class NovaAPIHelper extends Controller
{

    public function getBucketFolders(Request $request){

    	if(Auth::guard('admin')->check()){

    		$VodeditController = new VodeditController();
            $s3folders = $VodeditController->display_folder_s3();
            $return_arr = [];

            foreach ($s3folders as $key => $value) {
                
                $return_arr[] = [ 'folder_name' => $value, 'folder_link' => config('jarvis.aws-stream-endpoint').$value.'/' ];

            }

    		return response()->json([
                'isError' => 'false',
                'data' => $return_arr,
                'message' => 'admin logged in',
                'status' => 200,
                'timestamp' => time(),
            ],200); 

    	}

    	return response()->json([
            'isError' => true,
            'message' => 'Unothorized',
            'status' => 401,
            'timestamp' => time(),
        ],200); 
    }

    public function aws_bucketFolderListVideo(Request $request){

        if(Auth::guard('admin')->check()){
            
            $awsList = new AwsList;

            return response()->json([
                'isError' => 'false',
                'data' => array_filter( $awsList->get_bucket_files_hierarchy() ),
                'message' => 'admin logged in',
                'status' => 200,
                'timestamp' => time(),
            ],200); 

        }

        return response()->json([
            'isError' => true,
            'message' => 'Unothorized',
            'status' => 401,
            'timestamp' => time(),
        ],200); 

    }

    public function updateDateVideo(){

        $data = Video::All();

        foreach ($data as $key => $value) {
            Video::where('id', $value->id)
            ->update(['created_at_arabic' => serialize( Minion::ArabicDate( $value->created_at ) ) ]);
        }

        die('success');

    }

    public function UploadS3Video(){

        if( empty($_FILES['file']['name']) ){
            return false;
        }

        $bucket = 'smashi2019frank';

        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);

        try {
            // Upload data.
            $key = 'media-convert/'.pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME) . '/' . $_FILES['file']['name'];
            $source = fopen($_FILES['file']['tmp_name'], 'rb');

            $uploader = new ObjectUploader(
                $s3,
                $bucket,
                $key,
                $source
            );

            $result = $uploader->upload();

            if (@$result["@metadata"]["statusCode"] != '200') {
                return false;
            }
            /*$result = $s3->putObject([
                'Bucket' => $bucket,
                'Key'    => 'media-convert/'.pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME) . '/' . $_FILES['file']['name'],
                'ACL'    => 'public-read'
            ]);*/

            $s3->waitUntil('ObjectExists', array(
                'Bucket' => $bucket,
                'Key'    => 'media-convert/'.pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME) . '/' . $_FILES['file']['name']
            ));

            $client = new MediaConvertClient([
                'version' => 'latest',
                'region'  => 'eu-central-1',
                'endpoint' => 'https://usryickja.mediaconvert.eu-central-1.amazonaws.com'
            ]);

            try {
                /*$result = $client->describeEndpoints([]);
                $single_endpoint_url = $result['Endpoints'][0]['Url'];*/

            } catch (AwsException $e) {
                return $e->getMessage();
            }

            /*$client = new MediaConvertClient([
                'version' => 'latest',
                'region' => 'eu-central-1',
                'profile' => 'default',
                'endpoint' => $single_endpoint_url
            ]);*/

            $resultMedCon = $client->createJob(array (
              'JobTemplate' => 'arn:aws:mediaconvert:eu-central-1:200073705180:jobTemplates/Uploading Video smashi tv',
              // 'Queue' => 'arn:aws:mediaconvert:eu-central-1:200073705180:queues/Default',
              'UserMetadata' => 
              array (
              ),
              'Role' => 'arn:aws:iam::200073705180:role/MediaConvertRole',
              'Settings' => 
              array (
                'OutputGroups' => 
                array (
                  0 => 
                  array (
                    'Name' => 'Apple HLS',
                    'Outputs' => 
                    array (
                      0 => 
                      array (
                        'ContainerSettings' => 
                        array (
                          'Container' => 'M3U8',
                          'M3u8Settings' => 
                          array (
                            'AudioFramesPerPes' => 4,
                            'PcrControl' => 'PCR_EVERY_PES_PACKET',
                            'PmtPid' => 480,
                            'PrivateMetadataPid' => 503,
                            'ProgramNumber' => 1,
                            'PatInterval' => 0,
                            'PmtInterval' => 0,
                            'Scte35Source' => 'NONE',
                            'NielsenId3' => 'NONE',
                            'TimedMetadata' => 'NONE',
                            'VideoPid' => 481,
                            'AudioPids' => 
                            array (
                              0 => 482,
                              1 => 483,
                              2 => 484,
                              3 => 485,
                              4 => 486,
                              5 => 487,
                              6 => 488,
                              7 => 489,
                              8 => 490,
                              9 => 491,
                              10 => 492,
                            ),
                          ),
                        ),
                        'VideoDescription' => 
                        array (
                          'Width' => 1920,
                          'ScalingBehavior' => 'DEFAULT',
                          'Height' => 1080,
                          'TimecodeInsertion' => 'DISABLED',
                          'AntiAlias' => 'ENABLED',
                          'Sharpness' => 50,
                          'CodecSettings' => 
                          array (
                            'Codec' => 'H_264',
                            'H264Settings' => 
                            array (
                              'InterlaceMode' => 'PROGRESSIVE',
                              'NumberReferenceFrames' => 3,
                              'Syntax' => 'DEFAULT',
                              'Softness' => 0,
                              'GopClosedCadence' => 1,
                              'GopSize' => 90,
                              'Slices' => 1,
                              'GopBReference' => 'DISABLED',
                              'SlowPal' => 'DISABLED',
                              'SpatialAdaptiveQuantization' => 'ENABLED',
                              'TemporalAdaptiveQuantization' => 'ENABLED',
                              'FlickerAdaptiveQuantization' => 'DISABLED',
                              'EntropyEncoding' => 'CABAC',
                              'Bitrate' => 5000000,
                              'FramerateControl' => 'INITIALIZE_FROM_SOURCE',
                              'RateControlMode' => 'CBR',
                              'CodecProfile' => 'MAIN',
                              'Telecine' => 'NONE',
                              'MinIInterval' => 0,
                              'AdaptiveQuantization' => 'HIGH',
                              'CodecLevel' => 'AUTO',
                              'FieldEncoding' => 'PAFF',
                              'SceneChangeDetect' => 'ENABLED',
                              'QualityTuningLevel' => 'SINGLE_PASS',
                              'FramerateConversionAlgorithm' => 'DUPLICATE_DROP',
                              'UnregisteredSeiTimecode' => 'DISABLED',
                              'GopSizeUnits' => 'FRAMES',
                              'ParControl' => 'INITIALIZE_FROM_SOURCE',
                              'NumberBFramesBetweenReferenceFrames' => 2,
                              'RepeatPps' => 'DISABLED',
                              'DynamicSubGop' => 'STATIC',
                            ),
                          ),
                          'AfdSignaling' => 'NONE',
                          'DropFrameTimecode' => 'ENABLED',
                          'RespondToAfd' => 'NONE',
                          'ColorMetadata' => 'INSERT',
                        ),
                        'AudioDescriptions' => 
                        array (
                          0 => 
                          array (
                            'AudioTypeControl' => 'FOLLOW_INPUT',
                            'CodecSettings' => 
                            array (
                              'Codec' => 'AAC',
                              'AacSettings' => 
                              array (
                                'AudioDescriptionBroadcasterMix' => 'NORMAL',
                                'Bitrate' => 96000,
                                'RateControlMode' => 'CBR',
                                'CodecProfile' => 'LC',
                                'CodingMode' => 'CODING_MODE_2_0',
                                'RawFormat' => 'NONE',
                                'SampleRate' => 48000,
                                'Specification' => 'MPEG4',
                              ),
                            ),
                            'LanguageCodeControl' => 'FOLLOW_INPUT',
                          ),
                        ),
                        'OutputSettings' => 
                        array (
                          'HlsSettings' => 
                          array (
                            'AudioGroupId' => 'program_audio',
                            'AudioOnlyContainer' => 'AUTOMATIC',
                            'IFrameOnlyManifest' => 'EXCLUDE',
                          ),
                        ),
                        'NameModifier' => 'Output1',
                      ),
                    ),
                    'OutputGroupSettings' => 
                    array (
                      'Type' => 'HLS_GROUP_SETTINGS',
                      'HlsGroupSettings' => 
                      array (
                        'ManifestDurationFormat' => 'INTEGER',
                        'SegmentLength' => 10,
                        'TimedMetadataId3Period' => 10,
                        'CaptionLanguageSetting' => 'OMIT',
                        'Destination' => 's3://smashi2019frank/media-convert/'.pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME) .'/default/hls/',
                        'TimedMetadataId3Frame' => 'PRIV',
                        'CodecSpecification' => 'RFC_4281',
                        'OutputSelection' => 'MANIFESTS_AND_SEGMENTS',
                        'ProgramDateTimePeriod' => 600,
                        'MinSegmentLength' => 0,
                        'MinFinalSegmentLength' => 0,
                        'DirectoryStructure' => 'SINGLE_DIRECTORY',
                        'ProgramDateTime' => 'EXCLUDE',
                        'SegmentControl' => 'SINGLE_FILE',
                        'ManifestCompression' => 'NONE',
                        'ClientCache' => 'ENABLED',
                        'StreamInfResolution' => 'INCLUDE',
                      ),
                    ),
                  ),
                ),
                'AdAvailOffset' => 0,
                'Inputs' => 
                array (
                  0 => 
                  array (
                    'AudioSelectors' => 
                    array (
                      'Audio Selector 1' => 
                      array (
                        'Offset' => 0,
                        'DefaultSelection' => 'DEFAULT',
                        'ProgramSelection' => 1,
                      ),
                    ),
                    'VideoSelector' => 
                    array (
                      'ColorSpace' => 'FOLLOW',
                      'Rotate' => 'DEGREE_0',
                    ),
                    'FilterEnable' => 'AUTO',
                    'PsiControl' => 'USE_PSI',
                    'FilterStrength' => 0,
                    'DeblockFilter' => 'DISABLED',
                    'DenoiseFilter' => 'DISABLED',
                    'TimecodeSource' => 'EMBEDDED',
                    'FileInput' => 's3://smashi2019frank/'.'media-convert/'.pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME) . '/' . $_FILES['file']['name'],
                  ),
                ),
              ),
              'AccelerationSettings' => 
              array (
                'Mode' => 'DISABLED',
              ),
              'StatusUpdateInterval' => 'SECONDS_60',
              'Priority' => 0,
            ));

            // Print the URL to the object.
            return 'https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/'.pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME).'/default/hls/'.pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME)."Output1".'.m3u8';

            // return $result['@metadata']['effectiveUri'];
            
        } catch (S3Exception $e) {
            return $e->getMessage();
        }

    }

    public function UploadS3VideoImage(){

        if( empty($_FILES['file']['name']) ){
            return false;
        }

        $bucket = 'smashi2019frank';

        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);

        try {
            // Upload data.
            $result = $s3->putObject([
                'Bucket' => $bucket,
                'Key'    => 'media-convert/'.pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME) . '/Default/Thumbnails/' . pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME).'0000002.jpg',
                'Body'   => 'Hello, world!',
                'ACL'    => 'public-read'
            ]);

            // Print the URL to the object.
            return $result['ObjectURL'];
        } catch (S3Exception $e) {
            return $e->getMessage();
        }

    }

    public function CopyS3Video(){

        if( empty($_POST['file_link']) ){
            return false;
        }

        $file_link = $_POST['file_link'];

        $file_link_explode = explode('/', $file_link);

        unset( $file_link_explode[0] );
        unset( $file_link_explode[1] );

        $file_link_reverse = array_reverse($file_link_explode);
        $fileSourceUpload = '';
        unset( $file_link_explode[2] );

        $fileSourceUpload = implode('/', $file_link_explode);

        $fileExtArr = explode('.', $file_link_reverse[0]);

        $sourceBucket = 'smashistaging';
        $sourceKeyname = $fileSourceUpload;
        $targetBucket = 'smashi2019frank';

        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);

        // Copy an object.
        $returnObj = $s3->copyObject([
            'Bucket'     => $targetBucket,
            'Key'        => 'media-convert/'.$fileExtArr[0].'/'.$file_link_reverse[0],
            'CopySource' => $sourceBucket.'/'.$sourceKeyname,
        ]);

        $client = new MediaConvertClient([
          'version' => 'latest',
          'region'  => 'eu-central-1',
          'endpoint' => 'https://usryickja.mediaconvert.eu-central-1.amazonaws.com'
        ]);

        $resultMedCon = $client->createJob(array (
              'JobTemplate' => 'arn:aws:mediaconvert:eu-central-1:200073705180:jobTemplates/Uploading Video smashi tv',
              // 'Queue' => 'arn:aws:mediaconvert:eu-central-1:200073705180:queues/Default',
              'UserMetadata' => 
              array (
              ),
              'Role' => 'arn:aws:iam::200073705180:role/MediaConvertRole',
              'Settings' => 
              array (
                'OutputGroups' => 
                array (
                  0 => 
                  array (
                    'Name' => 'Apple HLS',
                    'Outputs' => 
                    array (
                      0 => 
                      array (
                        'ContainerSettings' => 
                        array (
                          'Container' => 'M3U8',
                          'M3u8Settings' => 
                          array (
                            'AudioFramesPerPes' => 4,
                            'PcrControl' => 'PCR_EVERY_PES_PACKET',
                            'PmtPid' => 480,
                            'PrivateMetadataPid' => 503,
                            'ProgramNumber' => 1,
                            'PatInterval' => 0,
                            'PmtInterval' => 0,
                            'Scte35Source' => 'NONE',
                            'NielsenId3' => 'NONE',
                            'TimedMetadata' => 'NONE',
                            'VideoPid' => 481,
                            'AudioPids' => 
                            array (
                              0 => 482,
                              1 => 483,
                              2 => 484,
                              3 => 485,
                              4 => 486,
                              5 => 487,
                              6 => 488,
                              7 => 489,
                              8 => 490,
                              9 => 491,
                              10 => 492,
                            ),
                          ),
                        ),
                        'VideoDescription' => 
                        array (
                          'Width' => 1920,
                          'ScalingBehavior' => 'DEFAULT',
                          'Height' => 1080,
                          'TimecodeInsertion' => 'DISABLED',
                          'AntiAlias' => 'ENABLED',
                          'Sharpness' => 50,
                          'CodecSettings' => 
                          array (
                            'Codec' => 'H_264',
                            'H264Settings' => 
                            array (
                              'InterlaceMode' => 'PROGRESSIVE',
                              'NumberReferenceFrames' => 3,
                              'Syntax' => 'DEFAULT',
                              'Softness' => 0,
                              'GopClosedCadence' => 1,
                              'GopSize' => 90,
                              'Slices' => 1,
                              'GopBReference' => 'DISABLED',
                              'SlowPal' => 'DISABLED',
                              'SpatialAdaptiveQuantization' => 'ENABLED',
                              'TemporalAdaptiveQuantization' => 'ENABLED',
                              'FlickerAdaptiveQuantization' => 'DISABLED',
                              'EntropyEncoding' => 'CABAC',
                              'Bitrate' => 5000000,
                              'FramerateControl' => 'INITIALIZE_FROM_SOURCE',
                              'RateControlMode' => 'CBR',
                              'CodecProfile' => 'MAIN',
                              'Telecine' => 'NONE',
                              'MinIInterval' => 0,
                              'AdaptiveQuantization' => 'HIGH',
                              'CodecLevel' => 'AUTO',
                              'FieldEncoding' => 'PAFF',
                              'SceneChangeDetect' => 'ENABLED',
                              'QualityTuningLevel' => 'SINGLE_PASS',
                              'FramerateConversionAlgorithm' => 'DUPLICATE_DROP',
                              'UnregisteredSeiTimecode' => 'DISABLED',
                              'GopSizeUnits' => 'FRAMES',
                              'ParControl' => 'INITIALIZE_FROM_SOURCE',
                              'NumberBFramesBetweenReferenceFrames' => 2,
                              'RepeatPps' => 'DISABLED',
                              'DynamicSubGop' => 'STATIC',
                            ),
                          ),
                          'AfdSignaling' => 'NONE',
                          'DropFrameTimecode' => 'ENABLED',
                          'RespondToAfd' => 'NONE',
                          'ColorMetadata' => 'INSERT',
                        ),
                        'AudioDescriptions' => 
                        array (
                          0 => 
                          array (
                            'AudioTypeControl' => 'FOLLOW_INPUT',
                            'CodecSettings' => 
                            array (
                              'Codec' => 'AAC',
                              'AacSettings' => 
                              array (
                                'AudioDescriptionBroadcasterMix' => 'NORMAL',
                                'Bitrate' => 96000,
                                'RateControlMode' => 'CBR',
                                'CodecProfile' => 'LC',
                                'CodingMode' => 'CODING_MODE_2_0',
                                'RawFormat' => 'NONE',
                                'SampleRate' => 48000,
                                'Specification' => 'MPEG4',
                              ),
                            ),
                            'LanguageCodeControl' => 'FOLLOW_INPUT',
                          ),
                        ),
                        'OutputSettings' => 
                        array (
                          'HlsSettings' => 
                          array (
                            'AudioGroupId' => 'program_audio',
                            'AudioOnlyContainer' => 'AUTOMATIC',
                            'IFrameOnlyManifest' => 'EXCLUDE',
                          ),
                        ),
                        'NameModifier' => 'Output1',
                      ),
                    ),
                    'OutputGroupSettings' => 
                    array (
                      'Type' => 'HLS_GROUP_SETTINGS',
                      'HlsGroupSettings' => 
                      array (
                        'ManifestDurationFormat' => 'INTEGER',
                        'SegmentLength' => 10,
                        'TimedMetadataId3Period' => 10,
                        'CaptionLanguageSetting' => 'OMIT',
                        'Destination' => 's3://smashi2019frank/media-convert/'. $fileExtArr[0] .'/default/hls/',
                        'TimedMetadataId3Frame' => 'PRIV',
                        'CodecSpecification' => 'RFC_4281',
                        'OutputSelection' => 'MANIFESTS_AND_SEGMENTS',
                        'ProgramDateTimePeriod' => 600,
                        'MinSegmentLength' => 0,
                        'MinFinalSegmentLength' => 0,
                        'DirectoryStructure' => 'SINGLE_DIRECTORY',
                        'ProgramDateTime' => 'EXCLUDE',
                        'SegmentControl' => 'SINGLE_FILE',
                        'ManifestCompression' => 'NONE',
                        'ClientCache' => 'ENABLED',
                        'StreamInfResolution' => 'INCLUDE',
                      ),
                    ),
                  ),
                ),
                'AdAvailOffset' => 0,
                'Inputs' => 
                array (
                  0 => 
                  array (
                    'AudioSelectors' => 
                    array (
                      'Audio Selector 1' => 
                      array (
                        'Offset' => 0,
                        'DefaultSelection' => 'DEFAULT',
                        'ProgramSelection' => 1,
                      ),
                    ),
                    'VideoSelector' => 
                    array (
                      'ColorSpace' => 'FOLLOW',
                      'Rotate' => 'DEGREE_0',
                    ),
                    'FilterEnable' => 'AUTO',
                    'PsiControl' => 'USE_PSI',
                    'FilterStrength' => 0,
                    'DeblockFilter' => 'DISABLED',
                    'DenoiseFilter' => 'DISABLED',
                    'TimecodeSource' => 'EMBEDDED',
                    'FileInput' => 's3://smashi2019frank/'.'media-convert/'.$fileExtArr[0].'/'.$file_link_reverse[0],
                  ),
                ),
              ),
              'AccelerationSettings' => 
              array (
                'Mode' => 'DISABLED',
              ),
              'StatusUpdateInterval' => 'SECONDS_60',
              'Priority' => 0,
            ));
        
        return 'https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/'.$fileExtArr[0].'/default/hls/'.$fileExtArr[0]."Output1".'.m3u8';

        return @$returnObj['@metadata']['effectiveUri'];

    }

    public function UploadS3VideoThumbnail(){

        if( empty($_GET['filename']) ){
            return 'No file found';
        }

        /*$payload = file_get_contents('php://input');
        return $payload;

        return $_REQUEST;*/
        $filename = $_GET['filename'];
        $putdata = file_get_contents("php://input");
        $request = json_decode($putdata);

        $image_parts = explode(";base64,", $putdata);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode( $image_parts[1] );

        $bucket = 'smashi2019frank';

        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);

        try {
            // Upload data.
            $result = $s3->putObject([
                'Bucket' => $bucket,
                'Key'    => 'media-convert/'.$filename.'/Default/Thumbnails/'.$filename.'.0000002.jpg',
                'Body'   => $image_base64,
                'ACL'    => 'public-read'
            ]);

            // Print the URL to the object.
            return $result['ObjectURL'];
        } catch (S3Exception $e) {
            return $e->getMessage();
        }

        /*if( empty($_FILES['file']['name']) ){
            return false;
        }

        $bucket = config('jarvis.aws-stream-bucket');

        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);

        try {
            // Upload data.
            $result = $s3->putObject([
                'Bucket' => $bucket,
                'Key'    => pathinfo( $_FILES['file']['name'], PATHINFO_FILENAME) . '/' . $_FILES['file']['name'],
                'Body'   => 'Hello, world!',
                'ACL'    => 'public-read'
            ]);

            // Print the URL to the object.
            return $result['ObjectURL'];
        } catch (S3Exception $e) {
            return $e->getMessage();
        }*/

    }
}
