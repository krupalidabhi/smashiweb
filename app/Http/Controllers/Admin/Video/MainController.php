<?php

namespace App\Http\Controllers\Admin\Video;

use App\Helpers\Minion;
use App\Models\Slug;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MainController extends Controller {

	public function __construct() {
		View::share( 'page_title', 'Videos' );
		View::share( 'sub_menu', 1 );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$videos = Video::all();

		return view( 'admin.sections.video.index', compact( 'videos' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view( 'admin.sections.video.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'title' => 'required',
			'video' => 'file|required'
		] );

		if ( $validator->fails() ) {
			return back()->withInput()->with( 'errors', $validator->errors() );
		}

		$request['slug_id'] = 0;

		$video_link = Minion::upload_image( $request->file( 'video' ), 'videos' );

		$request['link'] = $video_link;

		$video = Video::create( $request->all() );

		return redirect()->action( 'Admin\Video\MainController@edit', [ 'id' => $video->id ] )->with( 'success', 'Video created successfully!' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$video = Video::find( $id );

		return view( 'admin.sections.video.edit', compact( 'video' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		$validator = Validator::make( $request->all(), [
			'title' => 'required',
		] );

		if ( $validator->fails() ) {
			return back()->withInput()->with( 'errors', $validator->errors() );
		}

		$video = Video::find( $id );

		if ( $request->hasFile( 'video' ) ) {
			$video_link = Minion::upload_image( $request->file( 'video' ), 'videos' );

			$request['link'] = $video_link;
		}

		$video->update( $request->all() );

		return redirect()->action( 'Admin\Video\MainController@edit', [ 'id' => $video->id ] )->with( 'success', 'Video updated successfully!' );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return void
	 * @throws \Exception
	 */
	public function destroy( $id ) {
		$video = Video::find( $id );
		$video->delete();
	}
}
