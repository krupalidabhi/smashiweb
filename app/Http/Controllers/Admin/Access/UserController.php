<?php

namespace App\Http\Controllers\Admin\Access;

use App\Helpers\Minion;
use App\Models\AdminRole;
use App\Models\AdminUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin_users = AdminUser::all();
        $role = AdminRole::all();
        $page_title = 'Admin Users';
        return view('admin.sections.admin_user.index', compact('admin_users', 'page_title', 'role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin_roles = AdminRole::all()->pluck('name','id');
        $page_title = 'Admin User';
        return view('admin.sections.admin_user.create', compact('page_title', 'admin_roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);
        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }
        $username = $request->get('username');
        $password = $request->get('password');
        $role_id = $request->get('role_id');
        $slug = Minion::create_slug($username, AdminUser::class);

        $password_hashed = Hash::make($password);

        $admin_user = new AdminUser();
        $admin_user->username = $username;
        $admin_user->password = $password_hashed;
        $admin_user->role_id = $role_id;
        $admin_user->slug = $slug;

        $admin_user->save();
        return redirect()->action('Admin\Access\UserController@index')->with('success', 'Admin User added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'Admin User';
        $admin_roles = AdminRole::all()->pluck('name','id');
        $admin_user = AdminUser::all()->find($id);
        return view('admin.sections.admin_user.edit', compact('admin_user','admin_roles','page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);
        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }
        $username = $request->get('username');
        $password = $request->get('password');
        $slug = Minion::create_slug($username, AdminUser::class);

        $password_hashed = Hash::make($password);

        $admin_user = AdminUser::all()->find($id);
        $admin_user->username = $username;
        $admin_user->password = $password_hashed;
        $admin_user->slug = $slug;

        $admin_user->save();
        return redirect()->action('Admin\Access\UserController@index')->with('success', 'Admin User updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin_user = AdminUser::find($id);
        $admin_user->delete();
    }
}
