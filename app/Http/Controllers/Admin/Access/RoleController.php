<?php

namespace App\Http\Controllers\Admin\Access;

use App\Helpers\Minion;
use App\Models\AdminRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin_roles = AdminRole::all();
        $page_title = 'Admin Roles';
        return view('admin.sections.admin_role.index', compact('admin_roles', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Admin Role';
        return view('admin.sections.admin_role.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }
        $name = $request->get('name');
        $slug = Minion::create_slug($name, AdminRole::class);

        $admin_role = new AdminRole();
        $admin_role->name = $name;
        $admin_role->slug = $slug;

        $admin_role->save();
        return redirect()->action('Admin\Access\RoleController@index')->with('success', 'Admin Role added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'Admin Role';
        $admin_role = AdminRole::all()->find($id);
        return view('admin.sections.admin_role.edit', compact('admin_role','page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }
        $name = $request->get('name');
        $slug = Minion::create_slug($name, AdminRole::class);

        $admin_role = AdminRole::all()->find($id);
        $admin_role->name = $name;
        $admin_role->slug = $slug;

        $admin_role->save();
        return redirect()->action('Admin\Access\RoleController@index')->with('success', 'Admin Role updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin_role = AdminRole::find($id);
        $admin_role->delete();
    }
}
