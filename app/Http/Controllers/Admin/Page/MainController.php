<?php

namespace App\Http\Controllers\Admin\Page;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MainController extends Controller {

	public function __construct() {
		View::share( 'page_title', 'News' );
		View::share( 'sub_menu', 2 );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$pages = Page::all();

		return view( 'admin.sections.page.index', compact( 'pages' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view( 'admin.sections.page.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'title'   => 'required',
			'content' => 'required'
		] );

		if ( $validator->fails() ) {
			return back()->withInput()->withErrors( $validator->errors() );
		}

		$request['slug_id'] = 0;

		$page = Page::create( $request->all() );

		return redirect()->action( 'Admin\Page\MainController@edit', [ 'id' => $page->id ] )->with( 'success', 'Page created successfully!' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$page = Page::find( $id );

		return view( 'admin.sections.page.edit', compact( 'page' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		$validator = Validator::make( $request->all(), [
			'title'   => 'required',
			'content' => 'required'
		] );

		if ( $validator->fails() ) {
			return back()->withInput()->withErrors( $validator->errors() );
		}

		$page = Page::find($id);

		$page->update( $request->all() );

		return redirect()->action( 'Admin\Page\MainController@edit', [ 'id' => $page->id ] )->with( 'success', 'Page updated successfully!' );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return void
	 * @throws \Exception
	 */
	public function destroy( $id ) {
		$page = Page::find( $id );
		$page->delete();
	}
}
