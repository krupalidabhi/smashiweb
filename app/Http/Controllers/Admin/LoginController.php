<?php

namespace App\Http\Controllers\Admin;

use App\Models\AdminUser;
use Hash;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class LoginController extends Controller
{
	public function index() {
		return view('admin.login.index');
	}

	public function login(Request $request) {
		$username = $request->get('username');
		$password = $request->get('password');
		$remember = $request->get('remember');

		$remember = isset($remember) && $remember == "on";

		$user = AdminUser::where('email', '=', $username)->first();

		if($user && Hash::check($password, $user->password)) {
			if($remember) {
				return redirect('/' . config('jarvis.admin-path'))->withCookie(cookie(config('jarvis.session-cookie-key'), $user->id, config('jarvis.cache-time')));
			} else {
				$request->session()->put(config('jarvis.session-cookie-key'), $user->id);

				return redirect('/' . config('jarvis.admin-path'));
			}
		} else {
			$request->session()->flash('invalid', 'Invalid Credentials');
			return redirect()->back();
		}
	}

	public function logout(Request $request) {
		$request->session()->forget(config('jarvis.session-cookie-key'));
		return redirect()->action('Admin\LoginController@index')->withCookie(\Cookie::forget(config('jarvis.session-cookie-key')));
	}
}
