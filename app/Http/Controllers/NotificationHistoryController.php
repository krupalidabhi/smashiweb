<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NotificationHistory;
use Validator;
use App\Models\LogSaveInDb;
use Exception;
use App\Http\Controllers\FcmController;

class NotificationHistoryController extends Controller
{
    //
    public function show_notification_History(Request $request)
    {
            try{
                $JSON_ARR=[];
            $validator=Validator::make($request->all(),[
                    'userid'=>'required',
            ]);
            if($validator->fails()){
                return response()->json([
                'responseObject'=>$JSON_ARR,
                'isError' => true,
                'message' => $validator->errors(),
                'message_en' =>'userid is Required',
                'status' => 403,
                'timestamp' => time(),

            ],403);

            }
        if($request->has('userid')){

                $userid=$request->get('userid');

                $notification_history=NotificationHistory::where('user_id','=',$userid)->get();
                if(!empty($notification_history)){


                    $notification_history_content=[];
                    foreach ($notification_history as $notification) {
                                # code...
                        if( empty($notification->poster_url) ){
                                $poster_url = $this->get_thumbnail($notification->link);
                            } else {
                                $poster_url = $notification->poster_url;
                            }
                                $notification_history_content[]=array(
                                    'id'=>@$notification->id,
                                    'user_id'=>@$notification->user_id,
                                    'title'=>@$notification->title,
                                    'body'=>@$notification->body,
                                    'color'=>@$notification->color,
                                    'icon'=>@$notification->icon,
                                    'poster_url'=>@$poster_url,
                                    'is_read'=>@$notification->is_read,
                                    'created_at'=>@$notification->created_atupdated_at,
                                    'updated_at'=>@$notification->updated_at,
                                );

                    }
                            $JSON_ARR = array(

                        'notifaction_history'=>$notification_history_content,
                        );
                            return response()->json([
                                            'responseObject'=>$JSON_ARR,
                                            'isError' => false,
                                            'message' => 'سجل الإخطار يجلب بنجاح',
                                            'message_en' =>' Notifaction History fetch successfully',
                                            'status' => 200,
                                            'timestamp' => time(),
                            ]);
           }
        }
       }
       catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'Notifaction History',
                         'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }

    }
    protected function get_thumbnail($link){

        $file_name = explode("/", $link);
        $return = '';
        if (isset($file_name[5])) {
            $file_name = $file_name[5];
            $return = config('jarvis.aws-endpoint') . $file_name . '/Default/Thumbnails/' . $file_name . '.0000002.jpg';
        }

        return $return;
    }
    public function submit_notifications(Request $request){

        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required',
                'userid'=>'required',
                'title'=>'required',
                'body'=>'required',
                'color'=>'required',
                'icon'=>'required',
                'isread'=>'required',
                'posterurl'=>'required',


        ]);
        if($validator->fails()){
            return response()->json([
                'isError' => true,
                'message' => $validator->errors(),
                'message_en' =>'All Fields are Required',
                'status' => 403,
                'timestamp' => time(),

            ],403);
        }
        if($request->has('id') && $request->has('userid') && $request->has('title') && $request->has('body') && $request->has('color') && $request->has('icon') && $request->has('isread')&& $request->has('posterurl')){

             $request->get('id') ;
             $request->get('userid') ;
             $request->get('title') ;
             $request->get('body') ;
             $request->get('color') ;
             $request->get('icon') ;
             $request->get('isread');
             $request->get('posterurl');

            NotificationHistory::where('id', $request->get('id'))
           ->update([
               'title' => $request->get('title'),
               'body' => $request->get('body'),
               'color' => $request->get('color'),
               'icon' => $request->get('icon'),
               'is_read' => $request->get('isread'),
               'poster_url' => $request->get('posterurl'),
            ]);

           return response()->json([
            'isError' => false,
            'message' => 'تم تحديث البيانات بنجاح',
            'message_en' =>'Data updated successfully',
            'status' => 200,
            'timestamp' => time(),

          ],200);

        }
       }
       catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'submit_notifications',
                         'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

        }
    }

    public function send_notifications(Request $request){
        $FcmController = new FcmController();
        config()->set('device_token',$request->get('token'));
        $FcmController->SendVodNotification("Test", 'https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/20200825_smashi_morning_live_story+2/Default/Thumbnails/20200825_smashi_morning_live_story+2.0000002.jpg','https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/20200825_smashi_morning_live_story+2/default/hls/20200825_smashi_morning_live_story+2Output1.m3u8');
    }
}
