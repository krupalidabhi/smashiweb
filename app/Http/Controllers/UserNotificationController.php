<?php

namespace App\Http\Controllers;

use App\Models\UserNotification;
use Illuminate\Http\Request;
use Validator;
use Exception;
use App\Models\User;
use App\Models\webPostNotification;

class UserNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
      $token=  $request->bearerToken();;
      $validator=Validator::make($request->all(),[
        'fcmToken'=>'required',
      ]);

        if($validator->fails()){
          $return= response()->json([
                                  'isError' => true,
                                  'message' => $validator->errors()->first(),
                                  'message_en'=>'fcm token required',
                                  'status' => 403,
                                    ],403 );
        } else{
         $tokencheck=$this->update($request);
         if (!empty($tokencheck)) {
           // code...
           $return= $tokencheck;
         }
         else{
           $notification= new UserNotification();
           $notification->user_id= (auth('api')->user()) ? auth('api')->user()->id : null;
           $notification->fcm_token =$request->get('fcmToken');
           $notification->notification_type= 'vods';
           $notification->expire ='2020-02-19 00:00:00';
           $notification->status =1;
           $notification->save();
         $return =response()->json([
                                 'isError' => false,
                                 'message' => 'البيانات المقدمة بنجاح',
                                 'message_en'=>'data submitted successfully ',
                                 'status' => 200,
                                 ],200 );
         }


        }
        return $return;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserNotification  $userNotification
     * @return \Illuminate\Http\Response
     */
    public function show(UserNotification $userNotification)
    {
        //
        // return UserNotification::get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserNotification  $userNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(UserNotification $userNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserNotification  $userNotification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $notification= UserNotification::where('fcm_token',$request->fcmToken)->first();

        if(!empty($notification)){
          $token= $notification->fcm_token;
          $update=UserNotification::where('fcm_token',$token)
                              ->update([
                                'fcm_token'=>$request->fcmToken,
                                'user_id'=>(auth('api')->user()) ? auth('api')->user()->id : null,
                                'status'=>1,
                                'updated_at'=>date("Y-m-d h:i:s",time()),
                              ]);
            if(!empty($update)){
              return response()->json([
                                      'isError' => false,
                                      'message' => 'تم تحديث البيانات بنجاح',
                                      'message_en'=>'data Updated successfully ',
                                      'status' => 200,
                                      ],200 );
            }


        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserNotification  $userNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $validator= Validator::make($request->all(),[
        'fcmToken'=>'required',
      ]);
      if($validator->fails()){
        $return= response()->json([
                                'isError' => true,
                                'message' => $validator->errors()->first(),
                                'message_en'=>'fcm token required',
                                'status' => 403,
                                  ],403 );
      }
      else{
             $findfacmToken= UserNotification::where('fcm_token',$request->fcmToken)->first();
             if (!empty($findfacmToken)) {
               // code...
               $destory= UserNotification::find($findfacmToken->id);
               $destory->status=0;
               $destory->save();
               $return = response()->json([
                                       'isError' => false,
                                       'message' => 'تم تحديث البيانات بنجاح',
                                       'message_en'=>'data Updated successfully ',
                                       'status' => 200,
                                       ],200 );
             }
             else{
               $return = response()->json([
                                       'isError' => false,
                                       'message' => 'رمز Fcm غير صالح',
                                       'message_en'=>'Invalid Fcm Token ',
                                       'status' => 403,
                                       ],403 );
             }

      }
      return $return;




    }

    public function web_post_notification_endpoint(Request $request){

      $validator= Validator::make($request->all(),[
        'endpoint'=>'required',
      ]);

      $endpoint = $request->endpoint;

      $webPostNotification = webPostNotification::firstOrCreate(
        [
          'endpoint' => $endpoint
        ],
        [
          'endpoint' => $endpoint,
          'user_id' => (auth('web')->user()) ? auth('api')->user()->id : null,
          'status' => 1,
        ]
      );

      /*$webPostNotification->endpoint = $endpoint;
      $webPostNotification->user_id = (auth('web')->user()) ? auth('api')->user()->id : null;
      $webPostNotification->save();*/
      
      return 'sucess';
      

    }

    public function sendWebNotification($title = '', $description = '', $url = ''){

        if( empty($title) ){
            $title = 'New video uplaoded';
        }

        if( empty($description) ){
            $description = 'Check out the newly uploaded video.';
        }

        if( empty($url) ){
            $url = '/latest';
        }

        $allWebNotification = webPostNotification::where('status', 1)->get()->toArray();
        

        if( !empty($allWebNotification) ){
            
            foreach ($allWebNotification as $key => $value) {

                $curl = curl_init();
                
                $dataArr = [
                    'to' => $value['endpoint'],
                    'notification' => [
                        "title" => $title,
                        "body" => $description,
                        "click_action" => $url,
                        "image" => config('app.url') . "admin/img/logo.png",

                    ],
                ];

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS =>json_encode($dataArr),
                  CURLOPT_HTTPHEADER => array(
                    "Authorization: key=".env("FCM_WEB_POST_SERVER_KEY"),
                    "Content-Type: application/json",
                    "senderId: 162364781706",
                    "gcm_sender_id: 162364781706"
                  ),
                ));

                $response = curl_exec($curl);
                curl_close($curl);
            }

            
        }
        
        return response()->json(['sucess']);
        

    }
}
