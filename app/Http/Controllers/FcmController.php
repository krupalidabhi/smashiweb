<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Models\UserNotification;
use Lang;

class FcmController extends Controller
{
    public function SendVodNotification($title = '', $thumbnail = 'https://smashi2019frank.s3.eu-central-1.amazonaws.com/assets/image/SmashiArabic.jpg', $video = ''){

    	if( empty($title) ){
    		\Log::channel('notification_log')->info("********\n\n Failed \n\n No notification title found \n\n");
    		return response()->json(array('error'));
    	}

    	$optionBuilder = new OptionsBuilder();
    	$optionBuilder->setContentAvailable(true);
    	$optionBuilder->setMutableContent(true);
    	$optionBuilder->setPriority('high');
		$optionBuilder->setTimeToLive(60*20);

		$notificationBuilder = new PayloadNotificationBuilder($title);
		$notificationBuilder->setBody(Lang::get('messages.new_video'))->setSound('default')->setClickAction('.MainActivity');

		$dataBuilder = new PayloadDataBuilder();
		// $dataBuilder->addData([ 'data' => ['notification_type' => 'vods', 'thumbnail_url' => $thumbnail] ]);
		$dataBuilder->addData([ 'notification_type' => 'vods', 'thumbnail_url' => $thumbnail,'video_link' => $video]);
		
		$option = $optionBuilder->build();
		$notification = $notificationBuilder->build();
		$data = $dataBuilder->build();

		// You must change it to get your tokens
		// $tokens = MYDATABASE::pluck('fcm_token')->toArray();
		$tokens = UserNotification::where([
		 	['notification_type', '=', 'vods'],
		 	['status', '=', 1]
		])->whereNotNull('fcm_token')->whereNotNull('user_id')->pluck('fcm_token')->toArray();
		if(config()->has('device_token'))
		{
			$tokens = [config()->get('device_token')];
		}
		if( empty($tokens) ){
			\Log::channel('notification_log')->info("********\n\n Failed \n\n No mobile device found \n\n");
			return response()->json(array('error'));
		}
		\Log::channel('notification_log')->info("PayloadNotification: ".json_encode($notification->toArray()));
		\Log::channel('notification_log')->info("PayloadNotificationData: ".json_encode($data->toArray()));
		\Log::channel('notification_log')->info("PayloadNotification: ".print_r($tokens,true));
		$downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);


		\Log::channel('notification_log')->info("********\n\n Success: ".$downstreamResponse->numberSuccess()."\n\n Failure: ".$downstreamResponse->numberFailure()."\n\n Modification: ". $downstreamResponse->numberModification(). "\n\n TokensToDelete: ". json_encode( $downstreamResponse->tokensToDelete() ). "\n\n TokensToModify: ". json_encode($downstreamResponse->tokensToModify())."\n\n TokensToRetry: ". json_encode( $downstreamResponse->tokensToRetry() ). "\n\n ". json_encode($downstreamResponse->tokensWithError()). "\n\n *******\n" );


		$downstreamResponse->numberSuccess();
		$downstreamResponse->numberFailure();
		$downstreamResponse->numberModification();

		// return Array - you must remove all this tokens in your database
		$tokensToDelete = $downstreamResponse->tokensToDelete();
		foreach ($tokensToDelete as $key => $value) {
			UserNotification::where('fcm_token', $value)->delete();
		}

		// return Array (key : oldToken, value : new token - you must change the token in your database)
		$tokensToModify = $downstreamResponse->tokensToModify();
		foreach ($tokensToModify as $key => $value) {
			UserNotification::where('fcm_token', $key)->update(['fcm_token' => $value]);
		}

		// return Array - you should try to resend the message to the tokens in the array
		$downstreamResponse->tokensToRetry();
		// return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
		$downstreamResponse->tokensWithError();

		return response()->json(array('success'));

    }
}
