<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class Location extends Controller
{
    public function fetch_current_location( $latitude, $longitude ){

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&key=AIzaSyDnV7yAQHvD3u1FxZDh2MJiQ0j1O8hBq78');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
		    echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		$result = json_decode($result, true);

		$result_arr = $result['results'][0]['address_components'];

		$zip_code = '';

		foreach ($result_arr as $key => $value) {
			if( in_array('postal_code', $value['types']) ){
				$zip_code = $value['long_name'];
			}
		}

		return new JsonResponse( $zip_code );
  }
}
