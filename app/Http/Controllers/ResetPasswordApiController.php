<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\PasswordResets;
use App\Models\User;
use Auth;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Support\Facades\Password;
use App\Models\LogSaveInDb;
use Exception;

class ResetPasswordApiController extends Controller
{
    //
       public function api_reset_password(Request $request)
       {
              try{
                     $validator=Validator::make($request->all(),[
                     'token'=>'required',
                     'password'=>'required |min:8| max:255',
              ]);
       if ($validator->fails())
        {
              $errors=$this->validatorFailsErrors($request, $validator);
              $error = true;
              $message=$errors['message'];
              $message_en=$errors['message_en'];
              $status = 400;

              }
              else{
              if($request->has('token') and $request->has('password'))
              {

                     $token=$request->get(urldecode('token'));
                     $verify_token_mail=$this->get_id_from_Reset_table($token);
                     if(!empty($verify_token_mail))
                             {
                                   $user_table_data=User::where('email','=',$verify_token_mail)->first();

                                   if(password_verify($request->password,$user_table_data->password))
                                   {
                              $error = true;
                              $message = 'لا يمكن أن تكون كلمة المرور الجديدة هي نفس كلمة المرور القديمة';
                              $message_en ='New password can\'t be same as old password';
                              $status = 400;
                                   }
                                   else{

                                          $user_table_data->password = $request->password;
                                   $user_table_data->save();
                                   if(password_verify($request->password,$user_table_data->password)){
                                          // $verify_token_data=PasswordResets::where('email','=',$user_table_data->email)->delete();

                                     $error = false;
                                     $message = 'تم تحديث كلمة السر بنجاح';
                                     $message_en ='Password Updated Successfully';
                                     $status = 200;
                                   }
                                   else{
                                     $error = true;
                                     $message = 'تم إدخال بيانات اعتماد غير صالحة';
                                     $message_en ='Invalid credentials entered';
                                     $status = 400;
                                   }
                                   }
                             }
                            else{

                                     $error = true;
                                     $message = 'تم إدخال بيانات اعتماد غير صالحة';
                                     $message_en ='Invalid credentials entered';
                                     $status = 400;
                                   }






              }
       }
       return response()->json([
                                     'isError' => $error,
                                     'message' => $message,
                                     'message_en' =>$message_en,
                                     'status' => $status,
                                     'timestamp' => time(),

                                   ],$status);
          }
          catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'Reset password api',
                         'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }


       }
       private function validatorFailsErrors(Request $request,$validator){


       	if($validator->messages()->get('token'))
       			{
       				$message = $validator->messages()->get('token');
		            $message_en ='Token is required';
       			}
       			elseif($validator->messages()->get('password')){

		               $message = $validator->messages()->get('password');
		               $message_en ='Password is required and Password length must be at least 8 characters';

       			}
       			return array(
			                'message' => $message,
			                'message_en' =>$message_en,
       							);





       }
       private function get_id_from_Reset_table($token){

       		$verified_token='';
       		$verify_token_id='';
       		$db_token=PasswordResets::pluck('token','id');
       		foreach ($db_token as $key => $value) {
       			# code...

       			if(password_verify ( $token , $value )){
       				// return $key .'=> '.$value;
       				$verified_token=password_verify ( $token , $value );
       				  $verify_token_id=$key;
       				break;
       			}
       		}
       		$verifieddata=array(
       			'token'=>$verified_token,
       			'id'=>$verify_token_id
       		);
       		if($verifieddata['token']==1 and !empty($verifieddata['id']) and !empty($verifieddata['token']))
       		{

       			 $verify_token_data=PasswordResets::where('id','=',$verifieddata['id'])->first();
       			 $verify_token_email=$verify_token_data->email;
       			 return $verify_token_data->email;
		       }
		       else{
		       }
   }
}
