<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Helpers\Gru;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Facebook;
use App\Models\LogSaveInDb;
use Exception;
use App\Models\SubscriptionsPayment;

class AppleController extends Controller
{
    //


    public function craeteuserforios(Request $request ){
            try{
                $success=[];
                $validator = Validator::make($request->all(), [
                     'email' => 'required|email|max:255',
                     'name' => 'required',
                     'userid'=> 'required',
                     'provider'=> 'required',
                    ]);

                if ($validator->fails()) {


                    return response()->json([
                        'responseObject' => $success,
                        'isError' => true,
                        'message' => $validator->errors()->first(),
                        'message_en' =>'Email is invalid',
                        'status' => 400,
                        'timestamp' => time(),

                    ],400);

                }
              $email=$request->input('email');
              $name=$request->input('name');
              $userid=$request->input('userid');
              $provider=$request->input('provider');
              $check_user= $this->updateAppleUserIfExist($request);
              if(!empty($check_user)){
                $authUser=$check_user;
              }
              else{

                $authUser = User::firstOrCreate(
                      [
                          'social_id'   => $userid,
                          'social_type' => $provider,
                      ], [
                          'name'         => $name,
                          'social_email' => $email,
                          'email'        => $email,
                          'social_type'  => $provider,
                          'social_id'    => $userid,
                          "is_social"=> true,
                          "user_agent"=> $request->header('User-Agent'),
                      ]
                  );
              }

                Auth::login( $authUser, true );

                $appleuser = Auth::user();

                $getHasSubs = ( new User())->getHasSubscriptionAttribute();
                $getSubsType = ( new User())->getSubscriptionType();

                $getSubsDate = ( new User())->getSubscriptionDate();
                $getSubsExDate = ( new User())->getSubscriptionExDate();

                // $user = User::where('email', '=', $email)->first();
                $success['token'] = $appleuser->createToken('MyApp')->accessToken;
                $success['id'] =               $appleuser->id;
                $success['name']=              $appleuser->name;
                $success['email']=             $appleuser->email;
                $success['emailVerifiedAt']=   $appleuser->email_verified_at;
                $success['socialType']=        $appleuser->social_type;
                $success['socialId']=          $appleuser->social_id;
                $success['socialEmail']=       $appleuser->social_email;

                $success['SubscriptionType']=  $getSubsType;
                $success['SubscriptionDate']=  $getSubsDate;
                $success['SubscriptionExDate']=$getSubsExDate;
                $success['hasSubscription']=   $getHasSubs;

                $success['createdAt']=         $appleuser->created_at;
                $success['updatedAt']=         $appleuser->updated_at;
                $success['newsletter']=        $appleuser->newsletter;
                $success['isSocial']=          'true';//$user->isSocial;
                $isError=false;
                $message="تسجيل الدخول بنجاح";
                $message_en="Sign in successfully";
                $status=200;
                //Log::channel('apierror')->info("log in happend");
                return response()->json(['responseObject' => $success,
                                        'isError' => $isError,
                                        'message' => $message,
                                        'message_en'=>$message_en,
                                        'status' => $status,
                                        'length_token'=>strlen($success['token']),


                                        ],$status );
       }
       catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'craeteuserforios',
												 'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }



    }
    public function getUserForIos(Request $request){

        try{
                $success=[];
            $validator= Validator::make($request->all(),[
                'socialid'=>'required',
            ]);
            if($validator->fails()){
                return response()->json([
                            'isError' => true,
                            'message' => $validator->errors(),
                            'message_en' =>'socialid is required',
                            'status' => 400,
                            'timestamp' => time(),

                        ],400);
            }
            if($request->has('socialid')){

                $socialid = $request->get('socialid');


               $social_user= User::where('social_id','=',$socialid)->first();

               $getHasSubs = ( new User())->getHasSubscriptionAttribute();
                $getSubsType = ( new User())->getSubscriptionType();

                $getSubsDate = ( new User())->getSubscriptionDate();
                $getSubsExDate = ( new User())->getSubscriptionExDate();

               $paymentdata= SubscriptionsPayment::where('user_id',$social_user->id)->first();

               if(!empty($social_user)){
                    $success['token'] = $social_user->createToken('MyApp')->accessToken;
                    $success['id'] =               $social_user->id;
                    $success['name']=              $social_user->name;
                    $success['socialType']=        $social_user->social_type;
                    $success['socialId']=          $social_user->social_id;
                    $success['socialEmail']=       $social_user->social_email;

                    $success['SubscriptionType']=  $getSubsType;
                    $success['SubscriptionDate']=  $getSubsDate;
                    $success['SubscriptionExDate']=$getSubsExDate;
                    $success['hasSubscription']=   $getHasSubs;

                    $success['createdAt']=         $social_user->created_at;
                    $success['updatedAt']=         $social_user->updated_at;
                    $success['newsletter']=        $social_user->newsletter;
                    $success['isSocial']=          'true';//$user->isSocial;
                    $isError=false;
                    $message="تسجيل الدخول بنجاح";
                    $message_en="Sign in successfully";
                    $status=200;
                    //Log::channel('apierror')->info("log in happend");
                    if($success['socialType']==3){
                        return response()->json(['responseObject' => $success,
                                            'isError' => $isError,
                                            'message' => $message,
                                            'message_en'=>$message_en,
                                            'status' => $status,
                                             ],$status );
                    }
                    else{
                        return response()->json([
                            'responseObject' => [],
                            'isError' => true,
                            'message' => 'تم إدخال بيانات اعتماد غير صالحة',
                            'message_en' =>'Invalid credentials entered',
                            'status' => 400,
                            'timestamp' => time(),

                        ],400);
                    }

               }
               else{
                    return response()->json([
                         'responseObject' => $success,
                            'isError' => true,
                            'message' => 'تم إدخال بيانات اعتماد غير صالحة',
                            'message_en' =>'Invalid credentials entered',
                            'status' => 400,
                            'timestamp' => time(),

                        ],400);
               }
            }

       }

       catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'getUserForIos',
                         'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }

    }
    public function updateAppleUserIfExist(Request $request){
      $email=$request->input('email');
      $name=$request->input('name');
      $userid=$request->input('userid');
      $provider=$request->input('provider');
      $userdetails=User::where('email',$email)->first();
      if(!empty($userdetails)){

      $update_details = User::where('id', $userdetails->id)->update([
          'name'         => $name,
          'social_email' => $email,
          'email'        => $email,
          'social_type'  => $provider,
          'social_id'    => $userid,
          "user_agent"=> $request->header('User-Agent'),
        ]);
          return User::where('id',$userdetails->id)->first();
      }
    }
}
