<?php

namespace App\Http\Controllers;
use App\Models\LogSaveInDb;
use Illuminate\Http\Request;
use App\Models\User;
use Exception;
use Validator;

class LogSaveInDbController extends Controller
{
    //
    public function get_log_from_db(Request $request )
    {
    	try
    	{
    		$validator=Validator::make($request->all(),[
    			'email'=>'required|email'
    		]);
    		if($validator->fails()){
    			return response()->json(['message'=>$validator->errors()->first(),
    									'mesage_en'=>'Email is required.',
    									],400);

    		}
    		else{
    			return $this->get_logs($request->email);
    		}

    	}
    	catch(Exeception $e)
    	{
    		return $e->getMessage();
    	}

    }
    protected function get_logs($email)
    {

    	  $dblogs=LogSaveInDb::orderBy("id", "DESC")->get();

    	 if(!empty($dblogs))
    	 {
    	 	return  $dblogs->toArray();
    	 }
         else{
           return response()->json([
             'response'=>'No logs available',
           ]);
         }


    }
}
