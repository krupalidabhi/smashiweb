<?php

namespace App\Http\Controllers\Web\Channel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Closure;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Aws\S3\S3Client;
use Illuminate\Support\Facades\Auth;
use Aws\MediaLive\MediaLiveClient;
use Aws\S3\Exception\S3Exception;



class PlaylistController extends Controller
{
    public static function display_playlist_s3(){
     
        $bucket = config('jarvis.aws-playlist-bucket');
       
        $results_bucket='';
        // Instantiate the client.
        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);
        
        $results = $s3->getPaginator('ListObjects', [
            'Bucket' => $bucket
        ]);
            
        // Use the high-level iterators (returns ALL of your objects).
        try {
            
            foreach ($results as $result) {
          
                foreach ($result['Contents'] as $object) {
                    $temp[]=explode("/", $object['Key']); 
                    $folders = explode("/", $object['Key']);                
                    $folder_name[] = $folders[0];
                    // $vod_name[]=$folders[1];                
                }
               
            }
            
          return array_unique($folder_name);
           
        } catch (S3Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
       
    }

    public function checkchannel(){

      // if(Auth::guard('admin')->check()){

        $client = new MediaLiveClient([
          'version' => 'latest',
          'region'  => 'eu-central-1',
          'credentials' => [
              'key'    => env('MEDIA_LIVE_AWS_ACCESS_KEY_ID'),
              'secret' => env('MEDIA_LIVE_AWS_SECRET_ACCESS_KEY')
          ]
        ]);

        try {
          $result = $client->describeChannel([
              'ChannelId' => env('PLAYLIST_CHANNEL_ID'), // REQUIRED
          ])->toArray();
        } catch (Exception $e) {
          $result = [];
        }
          
          
        // echo '<pre>';
        // print_r($result);
        $ChannelState = @$result['State'];
        // die('aa');

        switch ($ChannelState) {
          case 'IDLE':
            $formField = false;
            $stopButton = true;
            $startButton = false;
            break;
          case 'STARTING':
            $formField = true;
            $stopButton = true;
            $startButton = true;
            break;
          case 'STOPPING':
            $formField = true;
            $stopButton = true;
            $startButton = true;
            break;
          case 'RUNNING':
            $formField = true;
            $stopButton = false;
            $startButton = true;
            break;
          default:
            $formField = true;
            $stopButton = true;
            $startButton = true;
            break;
        }

        return response()->json([
            'isError' => false,
            'message' => 'success',
            'responseObject' => [
              'state' => $ChannelState,
              'formField'   => $formField,
              'stopButton'  => $stopButton,
              'startButton' => $startButton,
            ],
            'status' => 200,
            'timestamp' => time(),
        ],200); 

        /*if( $ChannelState == 'IDLE' ){

          $formField = 'true';
          $stopButton = 'false';
          $startButton = 'false';

        } elseif( $ChannelState == 'STARTING' ){

          $formField = 'false';
          $stopButton = 'false';
          $startButton = 'false';

        }*/

      // }

      // abort(404);

    }

    public function playlist_json_stop(Request $request){

    $curl = curl_init();
    $ChannelId = env('PLAYLIST_CHANNEL_ID');

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/stopchannel",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\n    \"channelId\":\"$ChannelId\"\n}",
      CURLOPT_HTTPHEADER => array(
        "Accept: */*",
        "Accept-Encoding: gzip, deflate",
        "Cache-Control: no-cache",
        "Connection: keep-alive",
        "Content-Length: 29",
        "Content-Type: application/json",
        "Host: 6qff3ao4nk.execute-api.eu-central-1.amazonaws.com",
        "Postman-Token: bd06c66c-b90f-4c91-aa77-638ffff0edd9,ece8b083-717f-4fff-bb78-f9b0371b4d83",
        "User-Agent: PostmanRuntime/7.20.1",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
    
    exit;


  }
    public static function playlist_url_update(Request $request){


        $client = new Client();
  
            $s3_link=$request->post('s3_link');
            $bucket=config('jarvis.aws-playlist-bucket');
            
            $file_name = explode("/", $s3_link);
        
            $body_url='
            [
            {
                "Url": "s3://'.$bucket.'/'.$file_name[4].'"
            }
            ]';

            $client = new Client([
                'headers' => [ 'Content-Type' => 'application/json' ]
            ]);
        
            $playlist_response = $client->post('https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/playlist',
                    ['body' => $body_url]  
            );
            $playlist = var_export($playlist_response->getBody()->getContents(), true);
            return $playlist;
            exit;
        // }
    }

    public static function playlist_json_update(Request $request){
        
        $body = '[ [
    {
      "Id": "h31y7e",
      "Settings": [],
      "MediaPackageSettings": [
        {
          "ChannelId": "smashiLive"
        }
      ]
    }
  ],
 [{
    "AudioDescriptions": [
      {
        "AudioTypeControl": "FOLLOW_INPUT",
        "LanguageCodeControl": "FOLLOW_INPUT",
        "AudioSelectorName": "string",
        "Name": "audio_r4c0ba"
      }
    ],
    "CaptionDescriptions": [],
    "OutputGroups": [
      {
        "OutputGroupSettings": {
          "MediaPackageGroupSettings": {
            "Destination": {
              "DestinationRefId": "h31y7e"
            }
          }
        },
        "Name": "smashiLive",
        "Outputs": [
          {
            "OutputSettings": {
              "MediaPackageOutputSettings": {}
            },
            "OutputName": "njbr6f",
            "VideoDescriptionName": "video_1viky4",
            "AudioDescriptionNames": [
              "audio_r4c0ba"
            ],
            "CaptionDescriptionNames": []
          }
        ]
      }
    ],
    "TimecodeConfig": {
      "Source": "EMBEDDED"
    },
    "VideoDescriptions": [
      {
        "CodecSettings": {
          "H264Settings": {
            "AfdSignaling": "NONE",
            "ColorMetadata": "INSERT",
            "AdaptiveQuantization": "MEDIUM",
            "Bitrate": 900000,
            "EntropyEncoding": "CABAC",
            "FlickerAq": "ENABLED",
            "FramerateControl": "SPECIFIED",
            "FramerateNumerator": 30,
            "FramerateDenominator": 1,
            "GopBReference": "DISABLED",
            "GopClosedCadence": 1,
            "GopNumBFrames": 2,
            "GopSize": 90,
            "GopSizeUnits": "FRAMES",
            "SubgopLength": "FIXED",
            "ScanType": "PROGRESSIVE",
            "Level": "H264_LEVEL_AUTO",
            "LookAheadRateControl": "MEDIUM",
            "NumRefFrames": 1,
            "ParControl": "SPECIFIED",
            "ParNumerator": 16,
            "ParDenominator": 9,
            "Profile": "MAIN",
            "RateControlMode": "CBR",
            "Syntax": "DEFAULT",
            "SceneChangeDetect": "ENABLED",
            "SpatialAq": "ENABLED",
            "TemporalAq": "ENABLED",
            "TimecodeInsertion": "DISABLED"
          }
        },
        "Height": 1080,
        "Name": "video_1viky4",
        "RespondToAfd": "NONE",
        "Sharpness": 50,
        "ScalingBehavior": "STRETCH_TO_OUTPUT",
        "Width": 1280
      }
    ]
  }],
 [ 
    {
      "InputId": "668856",
      "InputAttachmentName": "vod",
      "InputSettings": {
        "SourceEndBehavior": "LOOP",
        "InputFilter": "AUTO",
        "FilterStrength": 1,
        "DeblockFilter": "DISABLED",
        "DenoiseFilter": "DISABLED",
        "AudioSelectors": [],
        "CaptionSelectors": []
      }
    }
  ],
 [{
    "Codec": "AVC",
    "Resolution": "HD",
    "MaximumBitrate": "MAX_20_MBPS"
  }]
]';
   
        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);
        
        $playlist_update_response = $client->post('https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/channelupdate',
                   ['body' => $body]  
        );
        // echo 'hello'; die;
        // print_r($playlist_update_response->getBody()); die;
        $playlist_up = var_export($playlist_update_response->getBody()->getContents(), true);
        // echo $playlist_up;
        // die;
        return $playlist_up;
        exit;
    }

    public static function playlist_json_start(Request $request){
        
        $curl = curl_init();
        $ChannelId = env('PLAYLIST_CHANNEL_ID');

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/startchannel",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\n    \"channelId\":\"$ChannelId\"\n}",
          CURLOPT_HTTPHEADER => array(
            "Accept: */*",
            "Accept-Encoding: gzip, deflate",
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Content-Length: 29",
            "Content-Type: application/json",
            "Host: 6qff3ao4nk.execute-api.eu-central-1.amazonaws.com",
            "Postman-Token: bd06c66c-b90f-4c91-aa77-638ffff0edd9,0051f1b2-e075-4383-a216-755166b407fc",
            "User-Agent: PostmanRuntime/7.20.1",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }

        exit;

    }
}