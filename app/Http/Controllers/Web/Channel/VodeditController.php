<?php

namespace App\Http\Controllers\Web\Channel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Closure;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;



class VodeditController extends Controller
{
     /************Creating folder in s3 ********************/

    public static function display_folder_s3(){
        
        $bucket = config('jarvis.aws-stream-bucket');

        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);
        
        $result = $s3->listObjects([
            'Bucket' => $bucket,
            'rootSize' => 1,
        ]);

        try {
            
            $folder_name = self::CreatebucketHierarchy( $result['Contents'] );
            
        } catch (S3Exception $e) {
         echo $e->getMessage() . PHP_EOL;
        }
        
        return array_unique($folder_name);
        
    }

    public static function display_folder_s3_vodedit($buck = 'smashi-proudction'){
        
        $bucket = $buck;
        $returnFolders = [];
        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);
        
        $result = $s3->listObjects([
            'Bucket' => $bucket,
            'Delimiter' => '/',
        ]);

        $resultFiles = $s3->listObjects([
            'Bucket' => $bucket,
            'rootSize' => 1,
        ]);

        try {
            
            foreach ($result['CommonPrefixes'] as $key => $value) {
              $returnFolders[] = str_replace('/', '', $value['Prefix']);
            }

            $folder_name = $returnFolders;

            $folder_name = self::CreatebucketHierarchyVoedit( $resultFiles['Contents'] );
            
        } catch (S3Exception $e) {
         echo $e->getMessage() . PHP_EOL;
        }
        
        return ($folder_name);
        
    }

    private static function CreatebucketHierarchy($arrData){
        $returnArray = [];
        if( empty($arrData) || !is_array($arrData) ){
            return $returnArray;
        }

        $count = 0;
        $videoItem = [];
        foreach ($arrData as $key => $value) {
            
            $count = 0;
            $match = 0;
            

                //A folder
                $match = preg_match_all('#[/]#', $value['Key'], $count);
                
                if( $match == 1 && $value['Size'] == 0 ){
                    // for folders
                    $returnArray[ preg_replace('#[/]#', '', $value['Key'])] = '';
                } elseif( $match == 1 && strpos($value['Key'], '.mp4') && $value['Size'] > 0 ){
                    // for files
                    $temp_explode = explode('/', $value['Key']);
                    
                    $returnArray[$temp_explode[0]] = $value['Key'];
                }

            

        }
        return $returnArray;

    }

    private static function CreatebucketHierarchyVoedit($arrData){
      $returnArray = [];
        if( empty($arrData) || !is_array($arrData) ){
          return $returnArray;
        }

        $count = 0;
        $videoItem = [];
        foreach ($arrData as $key => $value) {
            
          $count = 0;
          $match = 0;
          

              //A folder
              $match = preg_match_all('#[/]#', $value['Key'], $count);
              
              if( $match == 1 && $value['Size'] == 0 ){
                  // for folders
                  $returnArray[ preg_replace('#[/]#', '', $value['Key'])] = '';
              /*} elseif( $match == 1 && strpos($value['Key'], env('VodCroppVodFormat', '.m3u8') ) && $value['Size'] > 0 ){*/
              } elseif( $match == 1 && strpos($value['Key'], '.m3u8' ) && $value['Size'] > 0 ){
                  // for files
                  $temp_explode = explode('/', $value['Key']);
                  
                  if( empty($returnArray[$temp_explode[0]]) ){
                    $returnArray[$temp_explode[0]] = [];
                  }
                  

                  $returnArray[$temp_explode[0]][] = $value['Key'];
              }

        }
        return $returnArray;
    }

    public static function crop_json(Request $request){
        $crop_start_time=$request->post('start');
        $crop_end_time=$request->post('end');
        $s3_video_link=$request->post('s3_link');
        $crop_vod_name=$request->post('crop_name');

        // $bucket=config('jarvis.aws-stream-bucket');
        $bucket = 'smashi-proudction';
        
        $file_name = explode("/", $s3_video_link);
        
        $body='[
            [{"Mode": "DISABLED"}],[ {
                "TimecodeConfig": {
                  "Source": "ZEROBASED"
                },
                "OutputGroups": [
                  {
                    "CustomName": "test",
                    "Name": "File Group",
                    "Outputs": [
                      {
                        "ContainerSettings": {
                          "Container": "MP4",
                          "Mp4Settings": {
                            "CslgAtom": "INCLUDE",
                            "FreeSpaceBox": "EXCLUDE",
                            "MoovPlacement": "PROGRESSIVE_DOWNLOAD"
                          }
                        },
                        "VideoDescription": {
                          "Width": 1920,
                          "Height": 1080,
                          "ScalingBehavior": "DEFAULT",
                          "TimecodeInsertion": "DISABLED",
                          "AntiAlias": "ENABLED",
                          "Sharpness": 50,
                          "CodecSettings": {
                            "Codec": "H_264",
                            "H264Settings": {
                              "InterlaceMode": "PROGRESSIVE",
                              "NumberReferenceFrames": 3,
                              "Syntax": "DEFAULT",
                              "Softness": 0,
                              "GopClosedCadence": 1,
                              "GopSize": 90,
                              "Slices": 1,
                              "GopBReference": "DISABLED",
                              "SlowPal": "DISABLED",
                              "SpatialAdaptiveQuantization": "ENABLED",
                              "TemporalAdaptiveQuantization": "ENABLED",
                              "FlickerAdaptiveQuantization": "DISABLED",
                              "EntropyEncoding": "CABAC",
                              "Bitrate": 9000000,
                              "FramerateControl": "INITIALIZE_FROM_SOURCE",
                              "RateControlMode": "CBR",
                              "CodecProfile": "MAIN",
                              "Telecine": "NONE",
                              "MinIInterval": 0,
                              "AdaptiveQuantization": "HIGH",
                              "CodecLevel": "AUTO",
                              "FieldEncoding": "PAFF",
                              "SceneChangeDetect": "ENABLED",
                              "QualityTuningLevel": "SINGLE_PASS",
                              "FramerateConversionAlgorithm": "DUPLICATE_DROP",
                              "UnregisteredSeiTimecode": "DISABLED",
                              "GopSizeUnits": "FRAMES",
                              "ParControl": "INITIALIZE_FROM_SOURCE",
                              "NumberBFramesBetweenReferenceFrames": 2,
                              "RepeatPps": "DISABLED",
                              "DynamicSubGop": "STATIC"
                            }
                          },
                          "AfdSignaling": "NONE",
                          "DropFrameTimecode": "ENABLED",
                          "RespondToAfd": "NONE",
                          "ColorMetadata": "INSERT"
                        },
                        "AudioDescriptions": [
                          {
                            "AudioTypeControl": "FOLLOW_INPUT",
                            "CodecSettings": {
                              "Codec": "AAC",
                              "AacSettings": {
                                "AudioDescriptionBroadcasterMix": "NORMAL",
                                "Bitrate": 96000,
                                "RateControlMode": "CBR",
                                "CodecProfile": "LC",
                                "CodingMode": "CODING_MODE_2_0",
                                "RawFormat": "NONE",
                                "SampleRate": 48000,
                                "Specification": "MPEG4"
                              }
                            },
                            "LanguageCodeControl": "FOLLOW_INPUT"
                          }
                        ],
                        "Extension": ".mp4",
                        "NameModifier": "_'.$crop_vod_name.'"
                      }
                    ],
                    "OutputGroupSettings": {https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/startchannel
                      "Type": "FILE_GROUP_SETTINGS",
                      "FileGroupSettings": {
                        "Destination": "s3://'.$bucket.'/'.$file_name[4].'/cropped_video/"
                      }
                    }
                  }
                ],
                "AdAvailOffset": 0,
                "Inputs": [
                  {
                    "InputClippings": [
                      {
                        "EndTimecode": "'.$crop_end_time.'",
                        "StartTimecode": "'.$crop_start_time.'"
                      }
                    ],
                    "AudioSelectors": {
                      "Audio Selector 1": {
                        "Offset": 0,
                        "DefaultSelection": "DEFAULT",
                        "ProgramSelection": 1
                      }
                    },
                    "VideoSelector": {
                      "ColorSpace": "FOLLOW",
                      "Rotate": "AUTO"
                    },
                    "FilterEnable": "AUTO",
                    "PsiControl": "USE_PSI",
                    "FilterStrength": 0,
                    "DeblockFilter": "DISABLED",
                    "DenoiseFilter": "DISABLED",
                    "TimecodeSource": "ZEROBASED",
                    "FileInput": "s3://'.$bucket.'/'.$file_name[4].'/'.$file_name[5].'"
                  }
                ]
              }],
            [{}]]';

        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);
    
        $crop_response = $client->post('https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/mediaconverter/video-clipping',
            ['body' => $body]  
        );
        $crop = var_export($crop_response->getBody()->getContents(), true);
        return 'https://s3.console.aws.amazon.com/s3/buckets/'.$bucket.'/'.$file_name[4].'/cropped_video/?region=eu-central-1&tab=overview';
    
    }
}