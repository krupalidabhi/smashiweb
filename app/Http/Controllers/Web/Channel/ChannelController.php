<?php

namespace App\Http\Controllers\Web\Channel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Closure;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Aws\S3\S3Client;
use Aws\MediaLive\MediaLiveClient;
use Illuminate\Support\Facades\Auth;
use Aws\S3\Exception\S3Exception;



class ChannelController extends Controller
{

    public function checkchannel(){

      // if(Auth::guard('admin')->check()){

        $client = new MediaLiveClient([
          'version' => 'latest',
          'region'  => 'eu-central-1',
          'credentials' => [
              'key'    => env('MEDIA_LIVE_AWS_ACCESS_KEY_ID'),
              'secret' => env('MEDIA_LIVE_AWS_SECRET_ACCESS_KEY')
          ]
        ]);

        try {
          $result = $client->describeChannel([
              'ChannelId' => env('LIVE_CHANNEL_ID'), // REQUIRED
          ])->toArray();
        } catch (Exception $e) {
          $result = [];
        }
          
          
        // echo '<pre>';
        // print_r($result);
        $ChannelState = @$result['State'];
        // die('aa');

        switch ($ChannelState) {
          case 'IDLE':
            $formField = false;
            $stopButton = true;
            $startButton = false;
            break;
          case 'STARTING':
            $formField = true;
            $stopButton = true;
            $startButton = true;
            break;
          case 'STOPPING':
            $formField = true;
            $stopButton = true;
            $startButton = true;
            break;
          case 'RUNNING':
            $formField = true;
            $stopButton = false;
            $startButton = true;
            break;
          default:
            $formField = true;
            $stopButton = true;
            $startButton = true;
            break;
        }

        return response()->json([
            'isError' => false,
            'message' => 'success',
            'responseObject' => [
              'state' => $ChannelState,
              'formField'   => $formField,
              'stopButton'  => $stopButton,
              'startButton' => $startButton,
            ],
            'status' => 200,
            'timestamp' => time(),
        ],200); 

        /*if( $ChannelState == 'IDLE' ){

          $formField = 'true';
          $stopButton = 'false';
          $startButton = 'false';

        } elseif( $ChannelState == 'STARTING' ){

          $formField = 'false';
          $stopButton = 'false';
          $startButton = 'false';

        }*/

      // }

      // abort(404);

    }

    public function IndexChannel(){

      if(Auth::guard('admin')->check()){

        $client = new MediaLiveClient([
          'version' => 'latest',
          'region'  => 'eu-central-1',
          'credentials' => [
              'key'    => env('MEDIA_LIVE_AWS_ACCESS_KEY_ID'),
              'secret' => env('MEDIA_LIVE_AWS_SECRET_ACCESS_KEY')
          ]
        ]);

        // $ListChannels = $client->listChannels([])->toArray();

        // $client = new Client();
    
        // $status_response = $client->post('https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/describeChannel');

        /*echo '<pre>';
        print_r($ListChannels);
        die;*/

        try {
          $result = $client->describeChannel([
              'ChannelId' => env('LIVE_CHANNEL_ID'), // REQUIRED
          ])->toArray();
        } catch (Exception $e) {
          $result = [];
        }
          
          
        // echo '<pre>';
        // print_r($result);
        $ChannelState = @$result['State'];
        // die('aa');

        switch ($ChannelState) {
          case 'IDLE':
            $formField = '';
            $stopButton = 'disabled';
            $startButton = '';
            break;
          case 'STARTING':
            $formField = 'disabled';
            $stopButton = 'disabled';
            $startButton = 'disabled';
            break;
          case 'STOPPING':
            $formField = 'disabled';
            $stopButton = 'disabled';
            $startButton = 'disabled';
            break;
          case 'RUNNING':
            $formField = 'disabled';
            $stopButton = '';
            $startButton = 'disabled';
            break;
          default:
            $formField = 'disabled';
            $stopButton = 'disabled';
            $startButton = 'disabled';
            break;
        }

        return view('admin.pages.channel', compact('formField', 'stopButton', 'startButton') );
      }

      abort(404);

    }

    public function updatechannel(Request $request)
    {   
      
      $yt_url=$request->post('yt_url');
      $yt_stream=$request->post('yt_stream');
      $folder_name_exist=$request->post('folder_name');
      $folder_ar = array();
      
      $filename = @$request->post('filename');
      /*$aws_height = @$request->post('aws_height');
      $aws_width = @$request->post('aws_width');
      $codec = @$request->post('codec');
      $resolution = @$request->post('resolution');*/
      $filename = ( !empty($filename) ) ? $filename : $folder_name_exist;
      /*$aws_height = ( !empty($aws_height) ) ? $aws_height : 720;
      $aws_width = ( !empty($aws_width) ) ? $aws_width : 1080;
      $codec = ( !empty($codec) ) ? $codec : 'MPEG2';
      $resolution = ( !empty($resolution) ) ? $resolution : 'SD';*/


    /********** List of bucket folder  ***************/
      $bucket = 'smashistaging';

      // Instantiate the client.
      $s3 = new S3Client([
          'version' => 'latest',
          'region'  => 'eu-central-1'
      ]);
      
      
      // Use the high-level iterators (returns ALL of your objects).
   
      try {
        $results = $s3->getPaginator('ListObjects', [
            'Bucket' => $bucket
        ]);
      foreach ($results as $result) {
          
          foreach ($result['Contents'] as $object) {

                $folder_name = explode("/", $object['Key']);                
                        if (isset($folder_name[0]) && !in_array($folder_name[0], $folder_ar)) {

                          $folder_ar[] = $folder_name[0];
                            
                        }
            }
        }
        
      } catch (S3Exception $e) {
        echo $e->getMessage() . PHP_EOL;
      }
      if(in_array($folder_name_exist, $folder_ar)){

        return "FOLD_EXTS";
        die;
      }
      else{
      


      /*******List of bucket folder END  ************/             
        
        $client = new MediaLiveClient([
          'version' => 'latest',
          'region'  => 'eu-central-1',
          'credentials' => [
              'key'    => env('MEDIA_LIVE_AWS_ACCESS_KEY_ID'),
              'secret' => env('MEDIA_LIVE_AWS_SECRET_ACCESS_KEY')
          ]
        ]);


        return $result = $client->updateChannel(

          [
            'Name' => 'livechannel',
            'ChannelId' => 7774334,
            'Arn' => 'arn:aws:medialive:eu-central-1:200073705180:channel:7774334',
            'InputAttachments' => 
            [
              [
                'InputId' => '7319898',
                'InputAttachmentName' => 'smashitv',
                'InputSettings' => 
                [
                  'SourceEndBehavior' => 'CONTINUE',
                  'InputFilter' => 'AUTO',
                  'FilterStrength' => 1,
                  'DeblockFilter' => 'DISABLED',
                  'DenoiseFilter' => 'DISABLED',
                  'AudioSelectors' => [],
                  'CaptionSelectors' => [],
                ],
              ],
            ],
            'Destinations' => 
            [
              [
                'Id' => '0tx339',
                'Settings' => [],
                'MediaPackageSettings' => 
                [
                  [
                    'ChannelId' => 'smashiLive',
                  ],
                ],
              ],
              [
                'Id' => 'yymiba',
                'Settings' => 
                [
                  [
                    'Url' => $yt_url,
                    'StreamName' => $yt_stream,
                  ],
                ],
                'MediaPackageSettings' => [],
              ],
              [
                'Id' => '4jpcg',
                'Settings' => 
                [
                  [
                    'Url' => 's3ssl://smashi-proudction/'.$folder_name_exist.'/',
                  ],
                ],
                'MediaPackageSettings' => [],
              ],
            ],
            'EncoderSettings' => 
            [
              'AudioDescriptions' => 
              [
                [
                  'AudioTypeControl' => 'FOLLOW_INPUT',
                  "AudioSelectorName"=> "string",
                  'LanguageCodeControl' => 'FOLLOW_INPUT',
                  'Name' => 'audio_n3mp7t',
                ],
                [
                  'AudioTypeControl' => 'FOLLOW_INPUT',
                  "AudioSelectorName"=> "string",
                  'LanguageCodeControl' => 'FOLLOW_INPUT',
                  'Name' => 'audio_6qr1li',
                ],
                [
                  'AudioTypeControl' => 'FOLLOW_INPUT',
                  "AudioSelectorName"=> "string",
                  'LanguageCodeControl' => 'FOLLOW_INPUT',
                  'Name' => 'audio_6s3fvq',
                ],
              ],
              'CaptionDescriptions' => [],
              'OutputGroups' => 
              [
                [
                  'OutputGroupSettings' => 
                  [
                    'MediaPackageGroupSettings' => 
                    [
                      'Destination' => 
                      [
                        'DestinationRefId' => '0tx339',
                      ],
                    ],
                  ],
                  'Name' => 'smashiLive',
                  'Outputs' => 
                  [
                    [
                      'OutputSettings' => 
                      [
                        'MediaPackageOutputSettings' => [],
                      ],
                      'OutputName' => 'pijr',
                      'VideoDescriptionName' => 'video_zymr8h',
                      'AudioDescriptionNames' => 
                      [
                        'audio_n3mp7t',
                      ],
                      'CaptionDescriptionNames' => [],
                    ],
                  ],
                ],
                [
                  'OutputGroupSettings' => 
                  [
                    'RtmpGroupSettings' => 
                    [
                      'AuthenticationScheme' => 'COMMON',
                      'CacheLength' => 30,
                      'RestartDelay' => 15,
                      'CacheFullBehavior' => 'DISCONNECT_IMMEDIATELY',
                      'CaptionData' => 'ALL',
                      'InputLossAction' => 'EMIT_OUTPUT',
                    ],
                  ],
                  'Name' => 'Restream',
                  'Outputs' => 
                  [
                    [
                      'OutputSettings' => 
                      [
                        'RtmpOutputSettings' => 
                        [
                          'Destination' => 
                          [
                            'DestinationRefId' => 'yymiba',
                          ],
                          'ConnectionRetryInterval' => 2,
                          'NumRetries' => 10,
                          'CertificateMode' => 'VERIFY_AUTHENTICITY',
                        ],
                      ],
                      'OutputName' => 'yymiba',
                      'VideoDescriptionName' => 'video_uxsbi',
                      'AudioDescriptionNames' => 
                      [
                        'audio_6qr1li',
                      ],
                      'CaptionDescriptionNames' => [],
                    ],
                  ],
                ],
                [
                  'OutputGroupSettings' => 
                  [
                    'ArchiveGroupSettings' => 
                    [
                      'Destination' => 
                      [
                        'DestinationRefId' => '4jpcg',
                      ],
                      // 'RolloverInterval' => 20000,
                      /* create multiple chunk for single ts file*/
                      'RolloverInterval' => 300,
                    ],
                  ],
                  'Name' => 'ArchiveVideo',
                  'Outputs' => 
                  [
                    [
                      'OutputSettings' => 
                      [
                        'ArchiveOutputSettings' => 
                        [
                          'NameModifier' => $filename,
                          'Extension' => '.ts',
                          'ContainerSettings' => 
                          [
                            'M2tsSettings' => 
                            [
                              'CcDescriptor' => 'DISABLED',
                              'Ebif' => 'NONE',
                              'NielsenId3Behavior' => 'NO_PASSTHROUGH',
                              'ProgramNum' => 1,
                              'PatInterval' => 100,
                              'PmtInterval' => 100,
                              'PcrControl' => 'PCR_EVERY_PES_PACKET',
                              'PcrPeriod' => 40,
                              'TimedMetadataBehavior' => 'NO_PASSTHROUGH',
                              'BufferModel' => 'MULTIPLEX',
                              'RateMode' => 'CBR',
                              'AudioBufferModel' => 'ATSC',
                              'AudioStreamType' => 'DVB',
                              'AudioFramesPerPes' => 2,
                              'SegmentationStyle' => 'MAINTAIN_CADENCE',
                              'SegmentationMarkers' => 'NONE',
                              'EbpPlacement' => 'VIDEO_AND_AUDIO_PIDS',
                              'EbpAudioInterval' => 'VIDEO_INTERVAL',
                              'EsRateInPes' => 'EXCLUDE',
                              'Arib' => 'DISABLED',
                              'AribCaptionsPidControl' => 'AUTO',
                              'AbsentInputAudioBehavior' => 'ENCODE_SILENCE',
                              'PmtPid' => '480',
                              'VideoPid' => '481',
                              'AudioPids' => '482-498',
                              'DvbTeletextPid' => '499',
                              'DvbSubPids' => '460-479',
                              'Scte27Pids' => '450-459',
                              'Scte35Pid' => '500',
                              'Scte35Control' => 'NONE',
                              'Klv' => 'NONE',
                              'KlvDataPids' => '501',
                              'TimedMetadataPid' => '502',
                              'EtvPlatformPid' => '504',
                              'EtvSignalPid' => '505',
                              'AribCaptionsPid' => '507',
                            ],
                          ],
                        ],
                      ],
                      'OutputName' => 'nh6ifb',
                      'VideoDescriptionName' => 'video_t9khsu',
                      'AudioDescriptionNames' => 
                      [
                        'audio_6s3fvq',
                      ],
                      'CaptionDescriptionNames' => [],
                    ],
                  ],
                ],
              ],
              'TimecodeConfig' => 
              [
                'Source' => 'EMBEDDED',
              ],
              'VideoDescriptions' => 
              [
                [
                  'CodecSettings' => 
                  [
                    'H264Settings' => 
                    [
                      'AfdSignaling' => 'NONE',
                      'ColorMetadata' => 'INSERT',
                      'AdaptiveQuantization' => 'MEDIUM',
                      'Bitrate' => 3000000,
                      'EntropyEncoding' => 'CABAC',
                      'FlickerAq' => 'ENABLED',
                      'FramerateControl' => 'SPECIFIED',
                      'FramerateNumerator' => 30,
                      'FramerateDenominator' => 1,
                      'GopBReference' => 'DISABLED',
                      'GopClosedCadence' => 1,
                      'GopNumBFrames' => 2,
                      'GopSize' => 90,
                      'GopSizeUnits' => 'FRAMES',
                      'SubgopLength' => 'FIXED',
                      'ScanType' => 'PROGRESSIVE',
                      'Level' => 'H264_LEVEL_AUTO',
                      'LookAheadRateControl' => 'MEDIUM',
                      'NumRefFrames' => 1,
                      'ParControl' => 'SPECIFIED',
                      'ParNumerator' => 16,
                      'ParDenominator' => 9,
                      'Profile' => 'MAIN',
                      'RateControlMode' => 'CBR',
                      'Syntax' => 'DEFAULT',
                      'SceneChangeDetect' => 'ENABLED',
                      'SpatialAq' => 'ENABLED',
                      'TemporalAq' => 'ENABLED',
                      'TimecodeInsertion' => 'DISABLED',
                    ],
                  ],
                  'Height' => 1080,
                  'Name' => 'video_zymr8h',
                  'RespondToAfd' => 'NONE',
                  'Sharpness' => 50,
                  'ScalingBehavior' => 'STRETCH_TO_OUTPUT',
                  'Width' => 1280,
                ],
                [
                  'CodecSettings' => 
                  [
                    'H264Settings' => 
                    [
                      'AfdSignaling' => 'NONE',
                      'ColorMetadata' => 'INSERT',
                      'AdaptiveQuantization' => 'MEDIUM',
                      'Bitrate' => 3000000,
                      'EntropyEncoding' => 'CABAC',
                      'FlickerAq' => 'ENABLED',
                      'FramerateControl' => 'SPECIFIED',
                      'FramerateNumerator' => 30,
                      'FramerateDenominator' => 1,
                      'GopBReference' => 'DISABLED',
                      'GopClosedCadence' => 1,
                      'GopNumBFrames' => 2,
                      'GopSize' => 90,
                      'GopSizeUnits' => 'FRAMES',
                      'SubgopLength' => 'FIXED',
                      'ScanType' => 'PROGRESSIVE',
                      'Level' => 'H264_LEVEL_AUTO',
                      'LookAheadRateControl' => 'MEDIUM',
                      'NumRefFrames' => 1,
                      'ParControl' => 'SPECIFIED',
                      'ParNumerator' => 16,
                      'ParDenominator' => 9,
                      'Profile' => 'MAIN',
                      'RateControlMode' => 'CBR',
                      'Syntax' => 'DEFAULT',
                      'SceneChangeDetect' => 'ENABLED',
                      'SpatialAq' => 'ENABLED',
                      'TemporalAq' => 'ENABLED',
                      'TimecodeInsertion' => 'DISABLED',
                    ],
                  ],
                  'Height' => 720,
                  'Name' => 'video_uxsbi',
                  'RespondToAfd' => 'NONE',
                  'Sharpness' => 50,
                  'ScalingBehavior' => 'STRETCH_TO_OUTPUT',
                  'Width' => 1280,
                ],
                [
                  'CodecSettings' => 
                  [
                    'H264Settings' => 
                    [
                      'AfdSignaling' => 'NONE',
                      'ColorMetadata' => 'INSERT',
                      'AdaptiveQuantization' => 'MEDIUM',
                      'Bitrate' => 3000000,
                      'EntropyEncoding' => 'CABAC',
                      'FlickerAq' => 'ENABLED',
                      'FramerateControl' => 'SPECIFIED',
                      'FramerateNumerator' => 30,
                      'FramerateDenominator' => 1,
                      'GopBReference' => 'DISABLED',
                      'GopClosedCadence' => 1,
                      'GopNumBFrames' => 2,
                      'GopSize' => 90,
                      'GopSizeUnits' => 'FRAMES',
                      'SubgopLength' => 'FIXED',
                      'ScanType' => 'PROGRESSIVE',
                      'Level' => 'H264_LEVEL_AUTO',
                      'LookAheadRateControl' => 'MEDIUM',
                      'NumRefFrames' => 1,
                      'ParControl' => 'SPECIFIED',
                      'ParNumerator' => 16,
                      'ParDenominator' => 9,
                      'Profile' => 'MAIN',
                      'RateControlMode' => 'CBR',
                      'Syntax' => 'DEFAULT',
                      'SceneChangeDetect' => 'ENABLED',
                      'SpatialAq' => 'ENABLED',
                      'TemporalAq' => 'ENABLED',
                      'TimecodeInsertion' => 'DISABLED',
                    ],
                  ],
                  'Height' => 1080,
                  'Name' => 'video_t9khsu',
                  'RespondToAfd' => 'NONE',
                  'Sharpness' => 50,
                  'ScalingBehavior' => 'STRETCH_TO_OUTPUT',
                  'Width' => 1280,
                ],
              ],
            ],
            'RoleArn' => 'arn:aws:iam::200073705180:role/MediaLiveAccessRole',
            'InputSpecification' => 
            [
              'Codec' => 'AVC',
              'Resolution' => 'HD',
              'MaximumBitrate' => 'MAX_20_MBPS',
            ],
            'LogLevel' => 'ERROR',
            'Tags' => [],
            'ChannelClass' => 'SINGLE_PIPELINE',
            'PipelineDetails' => [],
          ]

        );

         exit;
      }  

    }

/************start channel ******************** */
  public function startchannel(Request $request){

    /*$client = new MediaLiveClient([
      'version' => 'latest',
      'region'  => 'eu-central-1',
      'credentials' => [
        'key'    => env('MEDIA_LIVE_AWS_ACCESS_KEY_ID'),
        'secret' => env('MEDIA_LIVE_AWS_SECRET_ACCESS_KEY')
      ]
    ]);


    return $promise = $client->startChannelAsync([
      "ChannelId" => env('LIVE_CHANNEL_ID'),
    ]);*/

    $curl = curl_init();
    $channelId = env('LIVE_CHANNEL_ID');
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/startchannel",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\n    \"channelId\":\"$channelId\"\n}",
      CURLOPT_HTTPHEADER => array(
        "Accept: */*",
        "Accept-Encoding: gzip, deflate",
        "Cache-Control: no-cache",
        "Connection: keep-alive",
        "Content-Length: 29",
        "Content-Type: application/json",
        "Host: 6qff3ao4nk.execute-api.eu-central-1.amazonaws.com",
        "Postman-Token: bd06c66c-b90f-4c91-aa77-638ffff0edd9,0051f1b2-e075-4383-a216-755166b407fc",
        "User-Agent: PostmanRuntime/7.20.1",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }

      /*$start=$request->post('start');
      $client = new Client();
    
      $start_response = $client->post('https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/startchannel');
      $start_resp = var_export($start_response->getBody()->getContents(), true);
    return $start_resp;*/
    exit;

  }   

  /************stop channel ******************** */
  public function stopchannel(Request $request){

    /*$client = new MediaLiveClient([
      'version' => 'latest',
      'region'  => 'eu-central-1',
      'credentials' => [
        'key'    => env('MEDIA_LIVE_AWS_ACCESS_KEY_ID'),
        'secret' => env('MEDIA_LIVE_AWS_SECRET_ACCESS_KEY')
      ]
    ]);

    return $result = $client->stopChannel([
      "ChannelId" => env('LIVE_CHANNEL_ID'),
    ]);*/

    $curl = curl_init();
    $channelId = env('LIVE_CHANNEL_ID');
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/stopchannel",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\n    \"channelId\":\"$channelId\"\n}",
      CURLOPT_HTTPHEADER => array(
        "Accept: */*",
        "Accept-Encoding: gzip, deflate",
        "Cache-Control: no-cache",
        "Connection: keep-alive",
        "Content-Length: 29",
        "Content-Type: application/json",
        "Host: 6qff3ao4nk.execute-api.eu-central-1.amazonaws.com",
        "Postman-Token: bd06c66c-b90f-4c91-aa77-638ffff0edd9,ece8b083-717f-4fff-bb78-f9b0371b4d83",
        "User-Agent: PostmanRuntime/7.20.1",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "Error";
    } else {
      echo $response;
    }
    /*$start=$request->post('stop');
    $client = new Client();
  
    $stop_response = $client->post('https://6qff3ao4nk.execute-api.eu-central-1.amazonaws.com/prod/stopchannel');
    $stop_resp = var_export($stop_response->getBody()->getContents(), true);
    return $stop_resp;*/
    exit;


  }
  /************Video  Editing ******************** */
  // public function vodedit(Request $request){

  //   return view('admin.pages.vodedit');
  // }
  


  

  public function channelView(){

    return view('admin.pages.novachannel');

  }


}