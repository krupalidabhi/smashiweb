<?php

namespace App\Http\Controllers\Web\UpdateTs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Closure;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use File;

class MainController extends Controller
{
	private $bucket = 'smashi-proudction';
	private $body = '';
  private $dir = 'm3u8';

    public function updateTsMethod(){

        $returnFolders = [];
        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-central-1'
        ]);
        
        $result = $s3->listObjects([
            'Bucket' => $this->bucket,
            'Delimiter' => '/',
        ]);

        $resultFiles = $s3->listObjects([
            'Bucket' => $this->bucket,
            'rootSize' => 1,
        ]);

        try {
            
            foreach ($result['CommonPrefixes'] as $key => $value) {
              $returnFolders[] = str_replace('/', '', $value['Prefix']);
            }

            $folder_name = $returnFolders;

            $folder_name = self::CreatebucketHierarchyVoedit( $resultFiles['Contents'] );
            
        } catch (S3Exception $e) {
         echo $e->getMessage() . PHP_EOL;
        }
        
        if( !empty($folder_name) ){
	        foreach ($folder_name as $index => $tsFile) {
	        	$tsList = '';
	        	$secondsArr = [];
	        	$this->body = "#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-MEDIA-SEQUENCE:0\n#EXT-X-ALLOW-CACHE:YES\n";
	        	foreach ($tsFile as $kk => $FilePath) {

              $FileNameArr = explode('/', $FilePath);
	        		/*if(File::exists($this->dir. '/' .$FileNameArr[0].'/'.$FileNameArr[0].'.m3u8')) {
                continue;
              }*/

	        		$link = 'https://s3.eu-central-1.amazonaws.com/'.$this->bucket.'/'.$FilePath;
	        		

	        		$time = shell_exec("ffmpeg -i $link 2>&1 | grep Duration | awk '{print $2}' | tr -d ,");

	        		$duration = explode(":", $time);
  				    $seconds = ($duration[0] * 3600) + ($duration[1] * 60) + round($duration[2]);   
  				    $minutes = $seconds/60;
  				    $real_minutes = floor($minutes);
  				    $real_seconds = round(($minutes-$real_minutes)*60);
  				    $length = $real_minutes.':'.$real_seconds;

  				    
  				    $secondsArr[] = $seconds;
  				    

  				    $tsList .= "#EXTINF:".$seconds.",\n".$FileNameArr[1]."\n";
	        		
	        		
	        	}

	        	$this->body .= "#EXT-X-TARGETDURATION:".array_sum($secondsArr)."\n ".$tsList."#EXT-X-ENDLIST";
            $this->makeDirectory($this->dir);
				    $this->makeDirectory($this->dir.'/'.$FileNameArr[0]);

	        	// File::put($this->dir. '/' .$FileNameArr[0].'/'.$FileNameArr[0].'.m3u8', $this->body);
            $result = $s3->putObject([
                'Bucket' => $this->bucket,
                'Key'    => $FileNameArr[0].'/'.$FileNameArr[0].'.m3u8',
                'Body'   => $this->body,
                'ACL'    => 'public-read'
            ]);

	        }
	    }
        
        return response()->json(['sucess']);

    }

    /**
     * Create a directory.
     *
     * @param  string  $path
     * @param  int     $mode
     * @param  bool    $recursive
     * @param  bool    $force
     * @return bool
     */
    private function makeDirectory($path, $mode = 0777, $recursive = false, $force = false)
    { 
      if(File::exists($path)) {
        return true;
      }

      if ($force)
      {
          return @mkdir($path, $mode, $recursive);
      }
      else
      {
          return mkdir($path, $mode, $recursive);
      }
    }

    private static function CreatebucketHierarchyVoedit($arrData){
      $returnArray = [];
      $deleteArray = [];
        if( empty($arrData) || !is_array($arrData) ){
          return $returnArray;
        }

        $count = 0;
        $videoItem = [];

        /*echo '<pre>';
        print_r($arrData);
        die;*/

        foreach ($arrData as $key => $value) {
            
          $count = 0;
          $match = 0;
          

              //A folder
              $match = preg_match_all('#[/]#', $value['Key'], $count);
              
              if( $match == 1 && $value['Size'] == 0 ){
                  // for folders
                  $returnArray[ preg_replace('#[/]#', '', $value['Key'])] = '';
              /*} elseif( $match == 1 && strpos($value['Key'], env('VodCroppVodFormat', '.m3u8') ) && $value['Size'] > 0 ){*/
              } elseif( $match == 1 && strpos($value['Key'], '.ts' ) && $value['Size'] > 0 ){
                  // for files
                  $temp_explode = explode('/', $value['Key']);
                  
                  if( empty($returnArray[$temp_explode[0]]) ){
                    $returnArray[$temp_explode[0]] = [];
                  }
                  

                  $returnArray[$temp_explode[0]][] = $value['Key'];
              } elseif( $match == 1 && strpos($value['Key'], '.m3u8' ) && $value['Size'] > 0 ){
              		$temp_explode = explode('/', $value['Key']);
                  
					// if( empty($deleteArray[$temp_explode[0]]) ){
						$deleteArray[] = $temp_explode[0];
						// array_unique($deleteArray);
					// }
              }

        }

        $deleteArray = array_unique($deleteArray);
        foreach ($deleteArray as $key => $value) {
        	unset($returnArray[$value]);
        }
        return array_filter( $returnArray );
    }
}
