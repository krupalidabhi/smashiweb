<?php

namespace App\Http\Controllers\Web\Payment;

use Auth;
use Mail;
use App\Models\City;
use App\Models\User;
use App\Models\State;
use App\Models\Transaction;
use App\Models\Subscription;
use Illuminate\Http\Request;
use App\Models\payment_table;
use App\Models\UserSubscription;
use App\Models\RecurrencePayment;
use App\Http\Controllers\Controller;
use App\Models\SubscriptionsPayment;
use App\Models\SubscriptionCancelLog;
use App\Http\Controllers\Web\Payment\Paytab;
use App\Models\Country;

class MainController extends Controller
{

    protected $dailySubId       = 29;
    protected $WeeklySubId      = 30;
    protected $MonthlySubId     = 31;
    protected $AnnuallySubId    = 32;

    function getDailyId(){
        return $this->dailySubId;
    }

    function getWeeklyId(){
        return $this->WeeklySubId;
    }

    function getMonthlyId(){
        return $this->MonthlySubId;
    }

    function getAnnuallyId(){
        return $this->AnnuallySubId;
    }

	function TestPaytabs(){

        $pt = new Paytab();
        
        $pt->paytabs(env('PAYTABS_MERCHANT_EMAIL'), env('PAYTABS_SECRET_KEY'));
        
        $result = $pt->create_tokenization_payment(array(
            'cc_first_name' => "Vishal",
            'cc_last_name' => "Yadav",
            // 'cc_phone_number' => "091",
            'phone_number' => "9896618966",
            'email' => "vishal.yadav@oodlestechnologies.com",
            
            //Customer's Shipping Address (All fields are mandatory)
            'address_shipping' => "Juffair bahrain",
            'city_shipping' => "manama",
            'state_shipping' => "manama",
            'postal_code_shipping' => "00973",
            'country_shipping' => "BHR",
            
            'amount' => "93.00",
            'currency' => "AED",
            'title' => "John Doe",               // Customer's Name on the invoice
            'product_name' => "John Doe",
            
            "order_id" => "1231231",
         
            "customer_email" => "vishal.yadav@oodlestechnologies.com",
            "pt_token" => "chYwr9fyYLaeVJdKerbjUqfguuropLEB",
            "pt_customer_email" => "vishal.yadav@oodlestechnologies.com",
            "pt_customer_password" => "fWm4faTUjg",
            "billing_shipping_details" => "no",
            //Website Information
            
        ));

        echo "FOLLOWING IS THE RESPONSE: <br />";
        print_r ($result);
        die;
    }

    function PaymentProceed(Request $request){

        $this->validate($request,[
            'tel_code' => 'required|max:10|min:1',
            'tel'=>'required|max:15|min:4',
            'subscription_id'=>'required|max:4',
            'email' => 'required|email|max:100',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
            'state' => 'required|max:255',
            'zip_code' => 'required|max:255',
        ]);

        $tel_code = $request->input('tel_code');
        $tel = $request->input('tel');
        $subscription_id = $request->input('subscription_id');
        $email = $request->input('email');
        // $country = $request->input('country');
        // $state = $request->input('state');
        // $city = $request->input('city');
        $country = Country::whereId($request->country)->first();
        $state = State::whereId($request->state)->first();
        $city = City::whereId($request->city)->first();
        $zip_code = $request->input('zip_code');
        $billing_address = $city.', '.$state.', '.$zip_code.', '.$country;
        $pt = new Paytab();
        
        $subscription_price = Subscription::find($subscription_id)->toArray();

        $user = Auth::user();
        $title = $user->name;

        $exUser = explode(' ', $user->name);
        if ( !empty(@$exUser[1]) ) {
            $first_name = $exUser[0];
            $last_name = $exUser[1];

        } else {
            $first_name = $exUser[0];
            $last_name = $exUser[0];
        }

        $order_id = $this->createUniqueOrderId();

        

        $pt->paytabs(env('PAYTABS_MERCHANT_EMAIL'), env('PAYTABS_SECRET_KEY'));

        $result = $pt->create_pay_page(array(
            "merchant_email" => env('PAYTABS_MERCHANT_EMAIL'),
            "secret_key" => env('PAYTABS_SECRET_KEY'),
            "site_url" => env('PAYTABS_SITE_URL'),
            'return_url' => env('PAYTABS_RETURN_URL'),
             "title" => "JohnDoe And Co.",
            'cc_first_name' => $first_name,
            'cc_last_name' => $last_name,
            'cc_phone_number' => $tel_code,
            'phone_number' => $tel,
            'email' => 'testdev301@gmail.com',
            "products_per_title" => "Subscription ".$subscription_price['name'],
            'unit_price' => $subscription_price['price'],
            'quantity' => "1",
            "other_charges" => "0",
            'amount' => '10.00',
            'discount'=>"0",
            'currency' => "BHD", 
             "reference_no" => $order_id,
            "ip_customer" =>"1.1.1.0",
            "ip_merchant" =>"1.1.1.0",
            'billing_address' => $billing_address,
            "city" => "Manama",
            "state" => "Capital",
            "postal_code" => "97300",
            "country" => "BHR",
            "shipping_first_name" => "John",
            "shipping_last_name" => "Doe",
            'address_shipping' => $billing_address,
            'city_shipping' => "manama",
            'state_shipping' => "manama",
            'postal_code_shipping' => "97300",
            'country_shipping' => "BHR",
            "msg_lang" => "en",     
            
            "cms_with_version" => "1",
            "paypage_info" => "1",
            "is_tokenization" => "TRUE",
            "is_existing_customer" => "FALSE"
        ));
       if($result->response_code == 4012){
        return redirect($result->payment_url);
        }
        abort(404);
        // \Log::channel('notification_log')->info("********\n\n Failed \n\n ". json_encode($result) ." \n\n");
        
       

        // return redirect()->route('home');

    }

    public function createOrder($data = []){
        if( empty($data) ){
            return false;
        }
        
        $SubscriptionsPayment = new SubscriptionsPayment();
        foreach ($data as $key => $value) {
            $SubscriptionsPayment->$key = $value;
        }

        // SubscriptionsPayment::insert($data);
        $SubscriptionsPayment->save();
        return true;
    }

    public function createUniqueOrderId(){

        $number = mt_rand(1000000000, 9999999999); // better than rand()

        // call the same function if the barcode exists already
        if ($this->OrderNumberExists($number)) {
            return $this->createUniqueOrderId();
        }

        // otherwise, it's valid and can be used
        return $number;

    }
    
    private function OrderNumberExists($oderId){

        return SubscriptionsPayment::where('order_id', $oderId)->exists();

    }

    private function alreadyExistsRecurrence( $refId = 0 ){

        return $RP = RecurrencePayment::where(['payment_reference' => $refId])->exists();

    }

    public function PaymentReturn($call = 'web'){
        \Log::channel('notification_log')->info(json_encode($_REQUEST));
        if( !isset($_REQUEST['payment_reference']) ){

            if( $call == 'API' ){
                return response()->json([
                    'isError' => true,
                    'message' => 'Something went wrong',
                    'message_en'=>'Something went wrong',
                    'status' => 403,
                ],403 );
            } else {
                return redirect()->route('home')->withInput()->with('payment_errors', 'Transaction Failed: Invalid Payment');
            }
        }

        $payment_table = new payment_table();

        // $payment_table->payment_reference = $_REQUEST['payment_reference'];

        $payment_reference      = $_REQUEST['payment_reference'];
        $pt_customer_email      = @$_REQUEST['pt_customer_email'];
        $pt_customer_password   = @$_REQUEST['pt_customer_password'];
        $pt_token               = @$_REQUEST['pt_token'];

        if( $this->alreadyExistsRecurrence( $payment_reference ) ){

            if( $call == 'API' ){
                return response()->json([
                    'isError' => true,
                    'message' => 'Payment Already exists',
                    'message_en'=>'Payment Already exists',
                    'status' => 403,
                ],403 );
            } else {
                return redirect()->route('home')->withInput()->with('payment_errors', 'Transaction Failed: Invalid Payment');
            }
        }

        $user = Auth::user();

        if( !empty(@$user->id) ){
            $userId = $user->id;
        } else {
            $userId = 0;
        }
        $payment_table->user_id = $userId;
        

        $pt = new Paytab();
        $pt->paytabs(env('PAYTABS_MERCHANT_EMAIL'), env('PAYTABS_SECRET_KEY'));
        $VerifyData = $pt->verify_payment($payment_reference);

        $payment_table->panding_cycle = 0;
        $payment_table->completed_cycle = 1;

        if ($VerifyData->response_code == 100) {
            
            $payment_table->initial_amount = $this->GetOrderAmount($VerifyData->reference_no);
            $payment_table->valid_date = $this->getValidDate($VerifyData->reference_no);
            $payment_table->transaction_id = $VerifyData->transaction_id;

            if( $this->verifyAmount($VerifyData->reference_no, $VerifyData->amount) )
            {
                $payment_table->status = 1;
                $payment_table->payment_status = $VerifyData->response_code;
                $payment_table->total_billing_amount = $VerifyData->amount;

                RecurrencePayment::where('user_id', $userId)->update(['is_active' => 0]);

                $RecurrencePayment = new RecurrencePayment();
                $RecurrencePayment->user_id = $userId;
                $RecurrencePayment->order_id = $VerifyData->reference_no;
                $RecurrencePayment->payment_reference = $payment_reference;
                $RecurrencePayment->pt_customer_email = $pt_customer_email;
                $RecurrencePayment->pt_customer_password = $pt_customer_password;
                $RecurrencePayment->pt_token = $pt_token;
                $RecurrencePayment->pt_invoice_id = $VerifyData->pt_invoice_id;
                $RecurrencePayment->amount = $VerifyData->amount;
                $RecurrencePayment->currency = $VerifyData->currency;
                $RecurrencePayment->transaction_id = $VerifyData->transaction_id;
                $RecurrencePayment->is_active = true;
                $RecurrencePayment->save();

                $Transaction = new Transaction();
                $Transaction->user_id = $userId;
                $Transaction->subscription_id = $this->getSubscriptioId($VerifyData->reference_no);
                $Transaction->type = 1;
                $Transaction->save();

                UserSubscription::where(['user_id'=> $userId, 'is_active' => 1])->update(['is_active' => 0]);

                $UserSubscription = new UserSubscription();
                $UserSubscription->user_id = $userId;
                $UserSubscription->subscription_id = $this->getSubscriptioId($VerifyData->reference_no);
                $UserSubscription->transaction_id = $VerifyData->transaction_id;
                $UserSubscription->is_active = 1;
                $UserSubscription->save();

                SubscriptionsPayment::where('order_id', $VerifyData->reference_no)->update(['is_active' => 1]);
                
            } else {

                $payment_table->status = 0;
                $payment_table->payment_status = $VerifyData->response_code;
                $payment_table->total_billing_amount = $VerifyData->amount;

            }

        } else {

            $payment_table->status = 1;
            $payment_table->payment_status = $VerifyData->response_code;
            $payment_table->total_billing_amount = @$VerifyData->amount;
            // Do something for decliened payments

        }

        $payment_table->payment_from = 'paytabs';
        $payment_table->payment_type = 'subscription';
        $payment_table->currency = 'AED';
        $payment_table->currency_symbol = '$';
        $payment_table->save();
        if( $call == 'API' ){
            return response()->json([
                'isError' => true,
                'message' => 'Payment Complete',
                'message_en'=>'Payment Complete',
                'status' => 200,
            ],200 );
        } else {
            return redirect()->route('home');
        }
    }

    protected function getSubscriptioId($orderId = 0){

        $SP = SubscriptionsPayment::where(['order_id' => $orderId])->first()->toArray();
        return $SP['subscription_id'];
    }

    protected function verifyAmount( $order_id, $amount){

        return $SP = SubscriptionsPayment::where(['order_id' => $order_id, 'amount' => $amount])->exists();

    }

    protected function GetOrderAmount( $order_id){
        $SP = SubscriptionsPayment::where(['order_id' => $order_id])->first()->toArray();
        return $SP['amount'];
    }

    protected function getValidDate($reference){

        $SP = SubscriptionsPayment::where(['order_id' => $reference])->first()->toArray();
        
        $subscription = Subscription::find($SP['subscription_id'])->toArray();

        if( empty($subscription) || empty($reference) || empty($SP)  ){
            return null;
        }

        if( $subscription['product_id'] == $this->dailySubId ){
            $date = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($SP['created_at'])));
        } else if( $subscription['product_id'] == $this->WeeklySubId ){
            $date = date('Y-m-d H:i:s', strtotime('+1 week', strtotime($SP['created_at'])));
        } else if( $subscription['product_id'] == $this->MonthlySubId ){
            $date = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($SP['created_at'])));
        } else if( $subscription['product_id'] == $this->AnnuallySubId ){
            $date = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($SP['created_at'])));
        } else {
            $date = null;
        }


        return $date;
        
    }

    public function SyncSubscriptionPayment(){

        $RecurrencePayment = RecurrencePayment::where('is_active', 1)->get()->toArray();
        
        if( !empty( $RecurrencePayment ) ){
            foreach ($RecurrencePayment as $key => $value) {

                $SubscriptionsPayment = SubscriptionsPayment::where(['order_id' => $value['order_id'], 'is_active' => 1 ])->first()->toArray();

                if( empty($SubscriptionsPayment) ){
                    continue;
                }

                $subscriptionData = Subscription::find($SubscriptionsPayment['subscription_id'])->toArray();

                $datetime1 = date_create(); // now
                $datetime2 = date_create( $SubscriptionsPayment['created_at'] );

                $interval = date_diff($datetime1, $datetime2);

                if( $subscriptionData['product_id'] == $this->dailySubId ){
                    // create Daily payments

                    if( $interval->d == 1 ){
                        
                        $this->makeRecurencePayment($subscriptionData, $SubscriptionsPayment, $value);
                    }
                    
                    
                } else if( $subscriptionData['product_id'] == $this->WeeklySubId ){

                    if( $interval->m == 1 ){
                        
                        $this->makeRecurencePayment($subscriptionData, $SubscriptionsPayment, $value);
                    }

                } else if( $subscriptionData['product_id'] == $this->MonthlySubId ){

                    if( $interval->y == 1 ){
                        $this->makeRecurencePayment($subscriptionData, $SubscriptionsPayment, $value);
                    }

                } else if( $subscriptionData['product_id'] == $this->AnnuallySubId ){
                    if( $interval->d == 1 ){
                        
                        $this->makeRecurencePayment($subscriptionData, $SubscriptionsPayment, $value);
                    }
                }

                return true;
            }
        }
     
    }

    private function makeRecurencePayment($subscriptionData, $SubscriptionsPayment, $RecurrencePayment){
        
        $order_id = $this->createUniqueOrderId();

        $this->createOrder([
            'user_id' => $SubscriptionsPayment['user_id'],
            'order_id' => $order_id,
            'subscription_id' => $subscriptionData['id'],
            'name' => $SubscriptionsPayment['name'],
            'first_name' => $SubscriptionsPayment['first_name'],
            'last_name' => $SubscriptionsPayment['last_name'],
            'email' => $SubscriptionsPayment['email'],
            'mobile' => $SubscriptionsPayment['mobile'],
            'billing_address' => $SubscriptionsPayment['billing_address'],
            'billing_city' => $SubscriptionsPayment['billing_city'],
            'billing_country' => $SubscriptionsPayment['billing_country'],
            'billing_state' => $SubscriptionsPayment['billing_state'],
            'billing_zip_code' => $SubscriptionsPayment['billing_zip_code'],
            'plan_selected' => $subscriptionData['name'],
            'amount' => $SubscriptionsPayment['amount'],
            'currency_code' => $SubscriptionsPayment['currency_code'],
            'is_active' => 0,
            'tel_code' => $SubscriptionsPayment['tel_code'],
        ]);

        $pt = new Paytab();
        $pt->paytabs(env('PAYTABS_MERCHANT_EMAIL'), env('PAYTABS_SECRET_KEY'));

        $result = $pt->create_tokenization_payment(array(
            'cc_first_name' => $SubscriptionsPayment['first_name'],
            'cc_last_name' => $SubscriptionsPayment['last_name'],
            'cc_phone_number' => $SubscriptionsPayment['tel_code'],
            'phone_number' => $SubscriptionsPayment['mobile'],
            'email' => $SubscriptionsPayment["email"],
            
            //Customer's Billing Address (All fields are mandatory)
            //When the country is selected as USA or CANADA, the state field should contain a String of 2 characters containing the ISO state code otherwise the payments may be rejected. 
            //For other countries, the state can be a string of up to 32 characters.
            /*'billing_address' => "manama bahrain",
            'city' => "manama",
            'state' => "manama",
            'postal_code' => "00973",
            'country' => "BHR",*/
            
            //Customer's Shipping Address (All fields are mandatory)
            'address_shipping' => $SubscriptionsPayment['billing_address'],
            'city_shipping' => $SubscriptionsPayment['billing_city'],
            'state_shipping' => $SubscriptionsPayment['billing_state'],
            'postal_code_shipping' => $SubscriptionsPayment['billing_zip_code'],
            'country_shipping' => $SubscriptionsPayment['billing_country'],
           
           //Product Information
            // "products_per_title" => "Product1",   //Product title of the product. If multiple products then add “||” separator
            // 'quantity' => "1",                                    //Quantity of products. If multiple products then add “||” separator
            // 'unit_price' => "2",                                  //Unit price of the product. If multiple products then add “||” separator.
            // "other_charges" => "91.00",                                     //Additional charges. e.g.: shipping charges, taxes, VAT, etc.
            
            'amount' => $SubscriptionsPayment['amount'],
            'currency' => $SubscriptionsPayment['currency_code'],
            //Invoice Information
            'title' => "Subscription " .$subscriptionData['name'],               // Customer's Name on the invoice
            'product_name' => $subscriptionData['name'],
            // "msg_lang" => "en",                 //Language of the PayPage to be created. Invalid or blank entries will default to English.(Englsh/Arabic)
            // "reference_no" => "1231231",        //Invoice reference number in your system
            "order_id" => $order_id,
         //     "ip_customer" =>"1.1.1.0",
            // "ip_merchant" =>"1.1.1.0",
            "customer_email" => $SubscriptionsPayment['email'],
            "pt_token" => $RecurrencePayment['pt_token'],
            "pt_customer_email" => $RecurrencePayment['pt_customer_email'],
            "pt_customer_password" => $RecurrencePayment['pt_customer_password'],
            "billing_shipping_details" => "no",
            //Website Information
            
        ));   

        if( !empty( $result->transaction_id ) && $result->result == 'Approved' ){
            SubscriptionsPayment::where('user_id', $SubscriptionsPayment['user_id'])->update(['is_active' => 0]);
            SubscriptionsPayment::where('order_id', $order_id)->update(['is_active' => 1]);
            RecurrencePayment::where('user_id', $SubscriptionsPayment['user_id'])->update(['order_id'=> $order_id]);
        }
        return true;
    }

    public function checkExpiredSubscription(){

        // Start of expire trials code


        $UserSubscription = UserSubscription::all()->filter(function($item) {
            $now = date_create();
            $create_at = $item->created_at;
            $interval = date_diff($now, $create_at);
            $day = $interval->d;
            // return $item;
            $dayTrialLeft = max(($item->valid_for - $day), 0);
            $item->day = $dayTrialLeft;
            if( ( $dayTrialLeft <= 0 ) && ($item->subscription_id == 0) && ($item->is_active == 1) ){
                return $item;
            }
        })->toArray();

        if( !empty($UserSubscription) ){

            foreach ($UserSubscription as $key => $value) {
                UserSubscription::find($value['id'])->update([
                    'is_active' => 0,
                ]);

                $this->sendExpiryMail($value['user_id']);

            }

        }

        return true;

    }

    function sendExpiryMail( $user_id ){

        $User = User::where('id',$user_id)->first();

        if( empty( $User ) ){
            return false;
        }

        $data = array('username'=> $User->name, 'baseUrl' => \URL::to('/') );
        Mail::send('mail.trial-expire', $data, function($message) use($User) {
            $message->to($User->email, 'SmashiTV')->subject
            ('SmashiTV | 7 days trial expired');
            $message->from('hello@smashi.tv','SmashiTV');
        });

    }

    public function cancelSubscription(){

      $user = Auth::user();

      if( empty($user) ){
        return response()->json([
            'isError' => true,
            'message' => 'لم يتم العثور على اشتراك',
            'message_en' =>'No user found',
            'status' => 404,
            'timestamp' => time(),
        ],404);
      }

      $UserSubscription = UserSubscription::where(['user_id' => $user->id, 'is_active' => 1])->get()->first();

      if( empty($UserSubscription) ){

        return response()->json([
            'isError' => true,
            'message' => 'لم يتم العثور على اشتراك',
            'message_en' =>'No subscription found',
            'status' => 404,
            'timestamp' => time(),
        ],404);

      }

      $UserSubscription->is_active = 0;

      $RecurrencePayment = RecurrencePayment::where(['user_id' => $user->id, 'is_active' => 1])->get()->first();

      if( !empty($RecurrencePayment) ){
        $RecurrencePayment->is_active = 0;
        $RecurrencePayment->save();
      }
      $SubscriptionCancelLog = new SubscriptionCancelLog();

      if( !empty($SubscriptionCancelLog) ){
        $SubscriptionCancelLog->user_id = $user->id;
        $SubscriptionCancelLog->subscription_id = $UserSubscription->subscription_id;
        $SubscriptionCancelLog->reason = 'User want to cancel';
        $SubscriptionCancelLog->transaction_id = $UserSubscription->transaction_id;
        $SubscriptionCancelLog->save();
      }
      $UserSubscription->save();
      
      $data = array('username'=> $user->name, 'baseUrl' => \URL::to('/') );
        Mail::send('mail.dynamic-mail', $data, function($message) use($user) {
            $message->to($user->email, 'SmashiTV')->subject
            ('SmashiTV | Subscription canceled');
            $message->from('hello@smashi.tv','SmashiTV');
        });

      return response()->json([
        'isError' => false,
        'message' => 'نجاح',
        'message_en' =>'Success',
        'status' => 200,
        'timestamp' => time(),
      ],200);

    }

    public function getState(Request $request)
    {
        if(isset($request->country) && !empty($request->country)) {
            $stateData = [];
            $data = State::getAllState($request->country);
            foreach($data as $key => $value) {
                $stateData[$key] = ["id" => $value->id, "text" => $value->name];
            }
            return $stateData;
        }
    }

    public function getCity(Request $request)
    {
        if(isset($request->state) && !empty($request->state)) {
            $cityData = [];
            $data = City::getAllCity($request->state);
            foreach($data as $key => $value) {
                $cityData[$key] = ["id" => $value->id, "text" => $value->name];
            }
            return $cityData;
        }
    }
}