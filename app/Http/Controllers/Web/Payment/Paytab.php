<?php

namespace App\Http\Controllers\Web\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Paytab extends Controller
{
    private $merchant_email;
    private $secret_key;

    function __construct(){
    	define("AUTHENTICATION", "https://www.paytabs.com/apiv2/validate_secret_key");
	    define("PAYPAGE_URL", "https://www.paytabs.com/apiv2/create_pay_page");
        define("TOKKEN_PAY_URL", "https://www.paytabs.com/apiv3/tokenized_transaction_prepare");
	    define("VERIFY_URL", "https://www.paytabs.com/apiv2/verify_payment");
    }

    function paytabs($merchant_email, $secret_key) {
        $this->merchant_email = $merchant_email;
        $this->secret_key = $secret_key;
    }
    
    function authentication(){
        $obj = json_decode($this->runPost(AUTHENTICATION, array("merchant_email"=> $this->merchant_email, "secret_key"=>  $this->secret_key)),TRUE);
		
		if($obj->response_code == "4000"){
          return TRUE;
        }
        return FALSE;
		
    }
    
    function get_client_ip() {
        
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    function get_server_ip(){
        echo '<pre>';
        print_r($_SERVER);
        die;
        return $_SERVER['SERVER_ADDR'];
    }

    function create_pay_page($values) {
        $values['merchant_email'] = $this->merchant_email;
        $values['secret_key'] = $this->secret_key;
        /*$values['ip_customer'] = $this->get_client_ip();
        $values['ip_merchant'] = $this->get_server_ip();*/
        $values['ip_customer'] =  $this->get_client_ip();
        $values['ip_merchant'] = env('SERVER_IP', gethostbyname( parse_url( env('APP_URL') , PHP_URL_HOST) ));

        // echo '<pre>';
        // print_r($values);
        // die;

        return json_decode($this->runPost(PAYPAGE_URL, $values));
    }
    
    
    
    function verify_payment($payment_reference){
        $values['merchant_email'] = $this->merchant_email;
        $values['secret_key'] = $this->secret_key;
        $values['payment_reference'] = $payment_reference;
        return json_decode($this->runPost(VERIFY_URL, $values));
    }
    
    function runPost($url, $fields) {
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        $fields_string = rtrim($fields_string, '&');
        $ch = curl_init();
        $ip = $_SERVER['REMOTE_ADDR'];

        $ip_address = array(
            "REMOTE_ADDR" => $ip,
            "HTTP_X_FORWARDED_FOR" => $ip
        );
		curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, false);
          curl_setopt($ch, CURLOPT_TIMEOUT, 30);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($ch, CURLOPT_VERBOSE, true);
		/*
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $ip_address);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, 1);
		*/
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }

    public function create_tokenization_payment($values){

        $values['merchant_email'] = $this->merchant_email;
        $values['secret_key'] = $this->secret_key;
        
        return json_decode($this->runPost(TOKKEN_PAY_URL, $values));

    }

}
