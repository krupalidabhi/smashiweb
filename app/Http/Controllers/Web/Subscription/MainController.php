<?php

namespace App\Http\Controllers\Web\Subscription;

use App\Helpers\Billing\MobilePayment;
use App\Helpers\Billing\Paytabs;
use App\Helpers\Gru;
use App\Helpers\Jarvis;
use App\Models\Subscription;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserSubscription;
use Carbon\Carbon;
use DateTime;
use Doctrine\DBAL\Schema\Table;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Jenssegers\Agent\Agent;
use tpayLibs\src\_class_tpay\TransactionApi;

class MainController extends Controller {

	private $public_key = '';
	private $private_key = '';
	private $device_agent = '';
	private $paytabs_secret_key = '';
	private $paytabs_merchant_id = '';

	public function __construct() {
		$this->public_key          = env( 'TPAY_PUBLIC_KEY' );
		$this->private_key         = env( 'TPAY_PRIVATE_KEY' );
		$this->paytabs_merchant_id = env( 'PAYTABS_MERCHANT_ID' );
		$this->paytabs_secret_key  = env( 'PAYTABS_SECRET_KEY' );
		$this->device_agent        = new Agent();
	
	
	
	}

	public function index( Request $request, $subscription ) {
		$user_subscription = UserSubscription::where( 'user_id', Auth::id() )
		                                     ->where( 'subscription_id', $subscription )
		                                     ->where( 'is_active', true )
		                                     ->first();

		if ( $user_subscription ) {
			return redirect()->back()->with( 'error', 'You are already subscribed' );
		} else {
			$subscription = Subscription::find( $subscription );

			if ( $subscription ) {

				$epoch     = Carbon::createFromDate( 1970, 1, 1 );
				$date_time = Carbon::now();

				$message   = $date_time->diffInDays( $epoch ) . '.' . $date_time->format( 'H:i:s' );
				$signature = $this->public_key . ':' . hash_hmac( 'sha256', $message, $this->private_key );

				$transaction                  = new Transaction();
				$transaction->user_id         = Auth::id();
				$transaction->subscription_id = $subscription->id;
				$transaction->signature       = $signature;
				$transaction->save();

				$consent_dictionary = [
					'subscriptionProductId'            => $subscription->product_id,
					'subscriptionProductPricingPlanId' => $subscription->pricing_id,
					'customerAccountNumber'            => Auth::id() . '-' . $transaction->id,
					'language'                         => '2',
					'redirectUrl'                      => Url::action( 'Web\Subscription\MainController@store' )
				];

				return view( 'web.pages.profile.subscription.index', compact( 'consent_dictionary', 'signature', 'message' ) );
			} else {
				abort( 404 );
			}
		}
	}

	public function store( Request $request ) {
		if ( $request->has( 'signature' ) ) {

			$status = $request->get( 'status' );

			if ( $status == 'success' ) {
				$signature = $request->get( 'signature' );
				$customer  = explode( "-", $request->get( 'customerAccountNumber' ) );

				$user        = User::find( $customer[0] );
				$transaction = Transaction::find( $customer[1] );

				if ( $user && $transaction ) {
					$new_transaction                  = new Transaction();
					$new_transaction->user_id         = $user->id;
					$new_transaction->subscription_id = $transaction->subscription_id;
					$new_transaction->type            = 2;
					$new_transaction->signature       = $signature;
					$new_transaction->status          = $status;
					$new_transaction->response        = json_encode( $request->all() );
					$new_transaction->save();

					$user_subscription                  = new UserSubscription();
					$user_subscription->user_id         = Auth::id();
					$user_subscription->subscription_id = $transaction->subscription_id;
					$user_subscription->transaction_id  = $new_transaction->id;
					$user_subscription->is_active       = true;
					$user_subscription->save();

					return redirect( '/account' );
				} else {
					abort(404);
				}

			} else {
//				$new_transaction                  = new Transaction();
//				$new_transaction->user_id         = $user->id;
//				$new_transaction->subscription_id = $transaction->subscription_id;
//				$new_transaction->type            = 2;
//				$new_transaction->signature       = $signature;
//				$new_transaction->status          = $status;
//				$new_transaction->response        = json_encode( $request->all() );
//				$new_transaction->save();

				return redirect()->back()->with('errors', 'Transaction Failed' . json_encode( $request->all() ));
			}
		} elseif($request->has('secure_sign')) {
			$transaction = Transaction::find($request->get('order_id'));

			if($transaction) {
				$new_transaction                  = new Transaction();
				$new_transaction->user_id         = $transaction->user_id;
				$new_transaction->subscription_id = $transaction->subscription_id;
				$new_transaction->type            = 2;
				$new_transaction->signature       = $request->get('secure_sign');
				$new_transaction->status          = $request->get('response_code');
				$new_transaction->response        = json_encode( $request->all() );
				$new_transaction->save();

				Paytabs::create_paytab_transaction($request, $new_transaction);

				$user_subscription                  = new UserSubscription();
				$user_subscription->user_id         = Auth::id();
				$user_subscription->subscription_id = $transaction->subscription_id;
				$user_subscription->transaction_id  = $new_transaction->id;
				$user_subscription->is_active       = true;
				$user_subscription->save();

				return redirect( '/account' )->with('success', 'تم الاشتراك بنجاح');
			} else {
				abort(404);
			}
		} else {
			abort( 404 );
		}
	}

	public function update( $id ) {
		$delete_subscription = $this->delete();

		if ( $delete_subscription ) {
			return $this->subscribe( $id );
		} else {
			return redirect()->action( 'Web\Profile\MainController@index' )->with( 'success', 'Subscription cancellation failed' );
		}
	}

	public function destroy( Request $request ) {
		if ( $this->delete() ) {
			return redirect()->action( 'Web\Profile\MainController@index' )->with( 'success', 'Subscription cancelled Successfully' );
		} else {
			return redirect()->action( 'Web\Profile\MainController@index' )->with( 'success', 'Subscription cancellation failed' );
		}
	}

	private function delete() {
		$user_subscription = UserSubscription::whereIsActive( true )->first();

		if ( $user_subscription ) {
			$transaction              = Transaction::find( $user_subscription->transaction_id );
			$response                 = json_decode( $transaction->response, true );
			$subscription_contract_id = $response['subscriptionContractId'];
			$signature                = $this->public_key . ":" . hash_hmac( "sha256", $subscription_contract_id, $this->private_key );
			$request_data             = [
				'signature'              => $signature,
				'subscriptionContractId' => $subscription_contract_id
			];

			$jarvis   = new Jarvis();
			$response = $jarvis->post( 'http://staging.tpay.me/api/TPAYSubscription.svc/json/CancelSubscriptionContractRequest', json_encode( $request_data ) );

			if ( $jarvis->success ) {
				$user_subscription->is_active = false;
				$user_subscription->save();

				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	private function subscribe( $subscription ) {
		$subscription = Subscription::find( $subscription );

		if ( $subscription ) {

			$epoch     = Carbon::createFromDate( 1970, 1, 1 );
			$date_time = Carbon::now();

			$message   = $date_time->diffInDays( $epoch ) . '.' . $date_time->format( 'H:i:s' );
			$signature = $this->public_key . ':' . hash_hmac( 'sha256', $message, $this->private_key );

			$transaction                  = new Transaction();
			$transaction->user_id         = Auth::id();
			$transaction->subscription_id = $subscription->id;
			$transaction->signature       = $signature;
			$transaction->save();

			$consent_dictionary = [
				'subscriptionProductId'            => $subscription->product_id,
				'subscriptionProductPricingPlanId' => $subscription->pricing_id,
				'customerAccountNumber'            => Auth::id() . '-' . $transaction->id,
				'language'                         => '2',
				'redirectUrl'                      => Url::action( 'Web\Subscription\MainController@store' )
			];

			return view( 'web.pages.profile.subscription.index', compact( 'consent_dictionary', 'signature', 'message' ) );
		} else {
			abort( 404 );
		}
	}

	public function get_paytabs_data( $subscription ) {
		$subscription = Subscription::find( $subscription );

		if ( $subscription ) {
			$name       = Auth::user()->name;
			$name_parts = explode( " ", $name );

			$transaction                  = new Transaction();
			$transaction->user_id         = Auth::id();
			$transaction->subscription_id = $subscription->id;
			$transaction->save();

			$array                                        = [];
			$array['settings']['secret_key']              = $this->paytabs_secret_key;
			//$array['settings']['secret_key']              = 'CL1YngD00P5Z3Fttz5Cvm5BR6Q8UDU2gG3e3jS3jH8jKOMdnRQTeVv52ldWZ8YfO2uRlmVF33YORK1fCS9RNEXQZaMYjLd7NHZ94';
			$array['settings']['merchant_id']             = $this->paytabs_merchant_id;
			//$array['settings']['merchant_id']             = 10036326;
			$array['settings']['url_redirect']            = Url::to( '/subscribe/response' );
			$array['settings']['amount']                  = $subscription->price;
			$array['settings']['title']                   = $name . ' ' . $subscription->name;
			$array['settings']['currency']                = 'درهم';
			$array['settings']['product_names']           = $subscription->name;
			$array['settings']['order_id']                = $transaction->id;
			$array['settings']['display_customer_info']   = 1;
			$array['settings']['display_billing_fields']  = 1;
			$array['settings']['display_shipping_fields'] = 0;
			$array['settings']['language']                = 'en';
			$array['settings']['redirect_on_reject']      = 1;


			$array['customer_info']['first_name']    = $name_parts[0];
			$array['customer_info']['last_name']     = $name_parts[ count( $name_parts ) - 1 ];
			$array['customer_info']['email_address'] = Auth::user()->email;

			return json_encode( $array );
		} else {
			abort( 404 );
		}
	}
}
