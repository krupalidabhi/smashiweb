<?php

namespace App\Http\Controllers\Web\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Mattmangoni\NovaBlogifyTool\Models\Image;
use Mattmangoni\NovaBlogifyTool\Models\Post;

class MainController extends Controller
{
    public function index() {
        $posts = Post::orderBy('created_at', 'DESC')->get();

        foreach ($posts as $post) {
            $image = Image::find($post->image_id);
            $post->image = '/storage/' . $image->filename;
        }


        return view('web.pages.blog.index', compact('posts'));
    }

    public function show(Request $request, $slug) {
        View::share('meta_tags', 'blog');
        $post = Post::where('slug', $slug)->first();

        $image = Image::find($post->image_id);
        $post->image = '/storage/' . $image->filename;

        return view('web.pages.blog.show', compact('post'));
    }
}
