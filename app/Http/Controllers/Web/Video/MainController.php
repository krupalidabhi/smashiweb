<?php

namespace App\Http\Controllers\Web\Video;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Video;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    /**
     * MainController constructor.
     */
    
    public function __construct()
    {
        View::share('meta_tags', 'video');
    }

    public function index(Request $request, $slug)
    {
        //Slug is for a post/video
        static $flag=0;
        $main_video = Video::where('slug', $slug)->first();
        Video::where('slug', $slug)->increment('views'); //will increment the no. of views for video
         
        $UserType = (new User())->getUserType();

        if ($main_video) {                 
            $tags = DB::table('videos')
            ->rightJoin('taggables', 'videos.id', '=', 'taggables.taggable_id')
            ->join('tags', 'tags.id', '=', 'taggables.tag_id')
            ->where('videos.id', '=', $main_video->id)
            ->select('tags.name','tags.slug','tags.id')
            ->get();
                
            foreach($tags as $tag){ 
              $tag_ar[]= $tag->id;
            }

            
            ///////single blade.php line 79 
            /*@if($tags->count() > 0) 
            <div class="row">
                <div class="col-12 text-right">
                    <div class="video-tags">
                   <?php foreach($tags as $tag){ 
                       $tag_arr = json_decode( $tag->name);
                       foreach($tag_arr as $key => $value){
                            if(strlen($value) != mb_strlen($value, 'utf-8')){
                                $val_arr[]=$value;
                            }
                       }                       
                    }  
                    if(!empty($val_arr)){ 
                        echo '<p>'.implode(", ",$val_arr).'</p>';
                    } ?>
                    </div>
                </div>
            </div>
            @endif        */       
            // $tag_ar=array();
          
            $videos = Video::where('id', '!=', $main_video->id)
            ->paginate(4);
            if(!empty($tag_ar)){
                $tag_related=DB::table('taggables')
                ->whereIn('tag_id', $tag_ar)
                ->count();
            }
            if(!empty($tag_ar) && $tag_related>1){
               
                $videos = DB::table('videos')
                ->rightJoin('taggables', 'videos.id', '=', 'taggables.taggable_id')
                ->join('tags', 'tags.id', '=', 'taggables.tag_id')
                ->where('videos.id', '!=', $main_video->id)
                ->whereIn('tags.id', $tag_ar)
                ->select('videos.*')
                ->paginate(4);
            }
            else{
                $videos = Video::where('id', '!=', $main_video->id)
                ->paginate(4); 
            }
           
            if ($request->ajax()) {
                $view = view('web.partials.single-video-card',compact('videos'))->render();
                return response()->json(['html'=>$view]);
                
            }
           
            // return view('web.pages.video.single',compact('main_video', 'videos'));
            return view('web.pages.video.single', compact('main_video', 'videos', 'tags', 'UserType'));
        } else {
            abort(404);
        }
    }

    public function watchedvideo(Request $request)
    {   
		// echo $request->post('watchedvideo_id');
        $existed_val=array();
         $existed_val=Auth::user()->watchedvideos;
        
         $new_val=$request->post('watchedvideo_id');
        if( $existed_val != 0 ){
            $temp=array();
          echo  $temp=Auth::user()->watchedvideos;
            // array_push($temp,$new_val);
        }
        // else{
        //     Auth::user()->watchedvideos = $new_val;
        // }
         Auth::user()->save();
        		
		print_r(Auth::user()->toSql());
                
    }


}
