<?php

namespace App\Http\Controllers\Web\Page;

use App\Helpers\Gru;
use App\Models\Page;
use App\Models\Slug;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class MainController extends Controller
{

    public function __construct()
    {
        View::share('meta_tags', 'page');
    }

    public function index(Request $request, $slug)
    {

        $page = Page::where('slug', $slug)->first();

        if ($page) {
            return view('web.pages.generic.index', compact('page'));
        } else {
            abort(404);
        }
    }
}
