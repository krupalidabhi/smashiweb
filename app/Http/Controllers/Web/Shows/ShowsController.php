<?php

namespace App\Http\Controllers\Web\Shows;

use App\Models\Shows;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowsController extends Controller {

	public function index( Request $request ) {
        $ar_slug='menu.shows';
        $shows = Shows::orderBy('created_at','asc')->get(); 
        return view('web.pages.shows.shows-details',compact('ar_slug','shows'));
    }

    public function showvideo( Request $request, $slug ) {
        // $ar_slug='menu.shows';

        $main_video = Shows::where('slug', $slug)->first();

        if( empty($main_video) ){
            return abort(404);
        }

        $show_video = Shows::where('slug', $slug)->first();

        if( empty( $show_video->show_link ) ){
            $videoLink = Video::where('shows_id', $main_video->id)->orderBy('created_at', 'DESC')->get()->first();
            if( empty($videoLink) ){
                abort(404);
            }

            $count = Video::where('shows_id', $main_video->id)->count();

            $show_video->show_link = @$videoLink->link;
            $show_video->id = @$videoLink->id;
            $archive_videos = Video::where('shows_id', $main_video->id)->offset(1)->limit(($count-1))->orderBy('created_at', 'DESC')->get();
        } else {
            $archive_videos = Video::where('shows_id', $main_video->id)->orderBy('created_at', 'DESC')->get();
        }


        // $main_video = Video::where('slug', $slug)->first();
        return view('web.pages.shows.shows-video',compact('show_video', 'archive_videos', 'main_video'));

    }
}