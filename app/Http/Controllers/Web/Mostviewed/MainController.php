<?php

namespace App\Http\Controllers\Web\Mostviewed;

use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    /**
     * MainController constructor.
     */
 
    
    public function index(Request $request)
    {
        //Slug is for a post/video
        $ar_slug='menu.mostviewed';
        $archive_videos = Video::orderBy('views','desc')->paginate(15); 

        if ($request->ajax()) {
            $view = view('web.partials.archive-video-card',compact('archive_videos'))->render();
            return response()->json(['html'=>$view]);
        }

       return view('web.pages.archive.single',compact('archive_videos','ar_slug'));
        
    }
}