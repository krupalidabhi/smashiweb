<?php

namespace App\Http\Controllers\Web;

use App\Models\LiveVideo;
use App\Models\LiveVideoUrl;
use App\Models\Video;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\Channel\ChannelController;
use App\Http\Controllers\Web\Channel\PlaylistController;
use Validator;
use App\Models\LogSaveInDb;
use Exception;
use Auth;

class HomeController extends Controller
{
    public function index() {

    	$live_video = LiveVideo::first();

		$UserType = (new User())->getUserType();

    	$date = \Carbon\Carbon::today()->subDays(env("VIDEO_ARCHIVE_DAYS"));
    	$frontEndChannel = $this->ChannelState();
    	$archive_videos =Video::take(6)->where('is_featured', 0)->orderBy('created_at', 'DESC')->get();

    	$LiveVideoUrl = LiveVideoUrl::where('id', 1)->pluck('web_url')->first();

    	return view('web.pages.index', compact('live_video','archive_videos', 'frontEndChannel', 'LiveVideoUrl', 'UserType'));
    }

	public function ChannelState(){
		$frontEndChannel = 'NOLIVE';
    	$ChannelController = new ChannelController;
        $channelState = (array) $ChannelController->checkchannel();
        $LiveVideoUrl = LiveVideoUrl::where('id', 1)->pluck('web_url')->first();

        $ChannelStateIs = $channelState['original']['responseObject']['state'];

		if( $ChannelStateIs == 'RUNNING' ){

			$chnlState = shell_exec("ffprobe -v quiet -print_format json -show_streams ".$LiveVideoUrl);
			$chnlStateArr = json_decode( $chnlState, true );

			if( is_array(@$chnlStateArr['streams']) ){

				foreach ($chnlStateArr['streams'] as $key => $value) {

					if( array_key_exists('duration', $value) ){
						$frontEndChannel = 'NOLIVE';
						break;
					} else {
						$frontEndChannel = 'LIVE';
					}

				}

			}

		} else {

			// commented code because playlist channel does't mean we are live
			/*$PlaylistController = new PlaylistController;
	        $PlaylistControllerState = (array) $PlaylistController->checkchannel();
	        $PlaylistControllerStateIs = $PlaylistControllerState['original']['responseObject']['state'];

	        echo '<pre>';
	        print_r($PlaylistControllerStateIs);
	        die;

	        if( $PlaylistControllerStateIs == 'RUNNING' ){
	        	$frontEndChannel = 'LIVE';
	        } else {
	        	$frontEndChannel = 'NOLIVE';
	        }*/

	        $frontEndChannel = 'LIVE';

		}
		return $frontEndChannel;
	}

	public function testFunction(){

		$LiveVideoUrl = LiveVideoUrl::where('id', 1)->pluck('web_url')->first();

		$chnlState = shell_exec("ffprobe -v quiet -print_format json -show_streams ".$LiveVideoUrl);
		return json_decode( $chnlState, true );

	}

	protected function get_thumbnail($link){

		$file_name = explode("/", $link);
		$return = '';
		if (isset($file_name[5])) {
			$file_name = $file_name[5];
			$return = config('jarvis.aws-endpoint') . $file_name . '/Default/Thumbnails/' . $file_name . '.0000002.jpg';
		}

		return $return;
	}


	public function video(Request $request)
 {
	try{
				$JSON_ARR=[];
			$live_video = LiveVideo::first();

		    $videos = Video::where('is_featured', 0)->orderBy('created_at', 'DESC')->take(20)->get();
		    if(!empty($videos)){
		    	$video_file_content=[];
					foreach($videos as $row)
					{

							if( empty($row->poster_url) ){
								$poster_url = $this->get_thumbnail($row->link);
							} else {
								$poster_url = $row->poster_url;
							}
							if(empty($row->duration)){

					          $video_duration = '00:04:00.00\n';
							}
							else{
								$video_duration = @$row->duration;
							}
						$video_duration=str_replace("\\n", '', $video_duration);
						$video_duration=str_replace("\n", '', $video_duration);


								if(empty($row->created_at_arabic) && $row->created_at_arabic==false){
									 $created_at_arabic=null;
								}
								else{
									$created_at_arabic=unserialize($row->created_at_arabic);
								}

								if(empty($row->body) && $row->body==null){
									$description="";

								}else{
									$description=$row->body;
								}
						$video_file_content[]= array
							(
								"id"=>$row->id,
								"title" =>$row->title,
								"video_link"=>$row->link,
								"video_duration"=> $video_duration,
								// "poster_url"=> $row->poster_url,
								"poster_url" => $poster_url,
								"description"=>$description,
								"created_at"=>date("j/n/Y", strtotime($row->created_at)),
								"created_at_arabic"=>$created_at_arabic

							);
				    }

				    $JSON_ARR[] = array(
						'live_video_url'=>$this->live_video_urls( $type='web_url'),
						'video_details'=>$video_file_content,
            'isError' => false,
            'message' => 'يتم جلب جميع مقاطع الفيديو بنجاح',
            'message_en' =>' All videos fetch successfully',
            'status' => 200,
            'timestamp' => date("Y-m-d h:i:sa",time()),
						);

						print json_encode($JSON_ARR);

			}
			else{
				return response()->json([
	                'responseObject'=>$JSON_ARR,
	                'isError' => true,
	                'message' => 'لا يتم جلب جميع مقاطع الفيديو بنجاح',
	                'message_en' =>' All videos not fetch successfully',
	                'status' => 400,
	                'timestamp' => time(),
	            ],400);
			}
	   }
	   catch(\Exception $exception)
        {

        LogSaveInDb::insert(
	        [
	        	'description' =>$exception->getTraceAsString(),
				'file_Name' => $exception->getFile(),
				'type' =>  $exception->getMessage(),
				'code' => $exception->getCode(),
				'line' => $exception->getLine(),
				'ip' => $request->ip(),
				'user_agent' => $request->header('User-Agent'),
				'api_name' => 'Videos',
				'created_at'=> date("Y-m-d h:i:s",time()),
	        ]
	    );

            }


 }

	public function live_url( )
	{

		$live_video = LiveVideo::first();
		$live_video_content[]= array
			(
				"title"=>'live video',
				"id"=>$live_video->id,
				"video"=>$live_video->video
			);
			print json_encode($live_video_content);


	}


	protected function  live_video_urls( $type='')
	{
		
		$live_video = LiveVideoUrl::first($type);
		$live_video_content["web_url"]=@$live_video->web_url;

		$isError=false;
		$message="تسجيل الدخول بنجاح";
		$message_en="Sign in successfully";
		$status=200;

		return		$live_video_content["web_url"];

	}
	public function public_live_video(){
  		$live_url= $this->live_video_urls( $type='web_url');
  		if(!empty($live_url)){
		$return= response()->json(['liveUrl'=>$live_url,
			'isError'=>false,
            'message'=>"جلب الفيديو المباشر بنجاح",
            'message_en'=>"live video fetch successfully",
            'status'=>200,
		],200);
  		}
  		else{
			$return= response()->json([
					'isError'=>true,
			    'message'=>"لا يتم جلب الفيديو المباشر بنجاح",
			    'message_en'=>"live video not fetch successfully",
			    'status'=>400,
			],400);
  		}
  		return $return;


	}







}
