<?php

namespace App\Http\Controllers\Web\Search;

use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LogSaveInDb;
use Exception;

class MainController extends Controller {

	public function index( Request $request ) {

		if( empty(trim($request->get('q'))) ){
			return view('web.pages.search.no-search-results');
		}

		if($request->has('q')) {
			$search_query = $this->escape_like( $request->get('q') );

			//search for video title and category
			$videos_basic = Video::where('title', 'LIKE', '%'. $search_query . '%')
				->orWhere('video_category', 'LIKE', '%'. $search_query . '%')
				->get();
			//search for video tags
			$videos_tags = Video::withAnyTagsOfAnyType([$search_query])
			 ->get();

			$videos = $videos_basic->merge($videos_tags);
			// $videos = $videos_basic;
			if($videos->count() > 0) {
				$total_videos = $videos->count();

				$videos = $videos->chunk(3);

				return view('web.pages.search.search-results', compact('total_videos', 'videos'));
			} else {
				return view('web.pages.search.no-search-results');
			}
		} else {
			abort(404);
		}
	}

	protected function escape_like($string)
    {
        $search = array('%', '_');
        $replace   = array('\%', '\_');
        return str_replace($search, $replace, $string);
    }

	protected function get_thumbnail($link){

		$file_name = explode("/", $link);
		$return = '';
		if (isset($file_name[5])) {
			$file_name = $file_name[5];
			$return = config('jarvis.aws-endpoint') . $file_name . '/Default/Thumbnails/' . $file_name . '.0000002.jpg';
		}

		return $return;
	}

	public function api_search( Request $request )
	{
		try{
				if($request->has('q')) {
				$search_query = $request->get('q');

				$videos = Video::where('title', 'LIKE', '%'. $search_query . '%')->get();
				if(!empty($videos)){
					$video_file_content=[];
					foreach($videos as $row)
					{

						if( empty($row->poster_url) ){
							$poster_url = $this->get_thumbnail($row->link);
						} else {
							$poster_url = $row->poster_url;
						}
						if(empty($row->duration)){

									$video_duration = '00:04:00.00\n';
						}
						else{
							$video_duration = @$row->duration;
						}
						if(empty($row->created_at_arabic) && $row->created_at_arabic==false){
						 $created_at_arabic=null;
							}
							else{
								$created_at_arabic=unserialize($row->created_at_arabic);
							}
						$video_duration=str_replace("\\n", '', $video_duration);
						$video_duration=str_replace("\n", '', $video_duration);
						$video_file_content[]= array
							(
								"id"=>$row->id,
								"title"=>$row->title,
								"video_link"=>$row->link,
								"video_duration"=>$video_duration,
								// "poster_url"=>$row->poster_url,
								"poster_url" => $poster_url,
								"description"=>$row->body,
								"created_at"=>date("j/n/Y", strtotime($row->created_at)),
								"created_at_arabic"=>$created_at_arabic

							);

					}

					$JSON_ARR = array(
						'search_video_details'=>$video_file_content
						);
						return response()->json([
							'search_video_details'=>$video_file_content,
							'isError' => false,
							'message' => 'يتم جلب جميع مقاطع الفيديو بنجاح',
							'message_en' =>' All videos fetch successfully',
							'status' => 200,
							'timestamp' => date("Y-m-d h:i:sa",time()),
						],200);
				}
				else{
					return response()->json([
						'search_video_details'=>$video_file_content,
						'isError' => true,
						'message' => 'لا يتم جلب جميع مقاطع الفيديو بنجاح',
						'message_en' =>' All videos not fetch successfully',
						'status' => 400,
						'timestamp' =>date("Y-m-d h:i:sa",time()),
					],200);
				}



				//if($videos->count() > 0) {
				//	$total_videos = $videos->count();

				//	$videos = $videos->chunk(3);

				//	return view('web.pages.search.search-results', compact('total_videos', 'videos'));
				//} else {
				//	return view('web.pages.search.no-search-results');
				//}
			}
			else{
				return response()->json([
				'isError' => true,
				'message' => 'مطلوب ف',
				'message_en' =>' q is required',
				'status' => 400,
				'timestamp' => date("Y-m-d h:i:sa",time()),
			],400);
			}
	   }
	   catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'Search',
												 'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

        }

	}

}
