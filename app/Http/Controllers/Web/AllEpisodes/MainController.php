<?php

namespace App\Http\Controllers\Web\AllEpisodes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Video;

class MainController extends Controller
{
  public function index(Request $request)
  {
      //Slug is for a post/video
      $ar_slug='menu.all_episodes';
      $archive_videos = Video::whereNotNull('id')->orderBy('id', 'desc')->paginate(15);

      if ($request->ajax()) {
          $view = view('web.partials.archive-video-card',compact('archive_videos'))->render();
          return response()->json(['html'=>$view]);
      }

     return view('web.pages.archive.single',compact('archive_videos','ar_slug'));

  }
}
