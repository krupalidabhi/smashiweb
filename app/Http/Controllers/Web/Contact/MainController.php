<?php

namespace App\Http\Controllers\Web\Contact;

use App\Models\Contact;
use App\Notifications\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    public function index() {
    	return view('web.pages.contact.index');
    }

    public function store(Request $request) {

    	$validator = Validator::make($request->all(), [
    		'name' => 'required',
		    'email' => 'required|email',
		    'message' => 'required'
	    ]);

    	if($validator->fails()) {
    		return back()->withInput()->with('errors', $validator->errors());
	    }

        if( strpos($request->get('name'), 'http') == 'FALSE' ){

        return trans('messages.contact_success');
      }

		$contact_us = Contact::create($request->all());

    	$contact_us = Contact::find($contact_us->id);

        Notification::route('mail', 'support@smashi.tv')->notify(new ContactUs($contact_us));

    	if($contact_us) {
    		return trans('messages.contact_success');
	    }
    }
}
