<?php

namespace App\Http\Controllers\Web\Category;

use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slug)
    {
        
        $ar_slug='menu.'.$slug; //category saved in arabic so have to covert slug into arabic words
    	$archive_videos =Video::where('video_category', __($ar_slug))->orderBy('created_at', 'DESC')->paginate(12);
         
       if ($request->ajax()) {
            $view = view('web.partials.archive-video-card',compact('archive_videos'))->render();
           return response()->json(['html'=>$view]);
        }
       return view('web.pages.archive.single',compact('archive_videos','ar_slug'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
