<?php

namespace App\Http\Controllers\Web\Profile;
use App\Models\User;
use App\Helpers\Minion;
use App\Models\Country;
use App\Models\Subscription;
use App\Models\UserBilling;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\LogSaveInDb;
use Exception;
use App\Models\UserSubscription;

class MainController extends Controller {
	public function index( Request $request ) {
		$subscriptions = Subscription::all();
		$dayTrialLeft = null;
		if(Auth::user()->has_trial > 0){

			$UserSubscription = UserSubscription::where( 'user_id', Auth::user()->id)->get()->first()->toArray();

			$datetime1 = date_create(); // now
            $datetime2 = date_create( $UserSubscription['created_at'] );

            $interval = date_diff($datetime1, $datetime2);
			$day = $interval->d;

			$dayTrialLeft = max(($UserSubscription['valid_for'] - $day), 0);
			
		}

		$countries = Country::pluck( 'name', 'id' );

		$path = storage_path() . "/json/country-list-3.json";

		$country_json_list = json_decode(file_get_contents($path), true);
		$countrySelectArray = [];
		if (!empty($country_json_list['countryDetails'])) {
			foreach ($country_json_list['countryDetails'] as $key => $value) {
				$countrySelectArray[$value['alpha']] = $value['name'];
			}
		}

		asort($countrySelectArray);
		
		return view( 'web.pages.profile.index', compact( 'subscriptions', 'countries', 'countrySelectArray', 'dayTrialLeft' ) );
	}



	public function newsletter_permation(Request $request)
	{
		$newsletter_per=0;
		$user = Auth::user();
		if($request->post('newsletter')== 'true')
		{
			$newsletter_per=1;

			$this->subscribe_user_mailchimp(['email' => $user->email, 'name' => $user->name ], 'subscribed');

		} else {

			$this->subscribe_user_mailchimp(['email' => $user->email, 'name' => $user->name ], 'unsubscribed');

		}

		Auth::user()->newsletter = $newsletter_per;
		echo Auth::user()->save();

		Auth::user()->toSql();
	}

	private function subscribe_user_mailchimp(array $data, $status){

		// Register the user in mailchimp subscription list
		$apiKey = env('MAILCHIMP_API');
        $listID = env('MAILCHIMP_LIST');

        // MailChimp API URL
        $memberID = md5(strtolower( $data['email'] ));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

        $fname_array = explode(' ', $data['name']);

        $fname = @$fname_array[0];
        $lname = @$fname_array[1];

        $merge_fields = array(
        	'FNAME'     => $fname,
        );

        if( !empty($lname) ){
        	$merge_fields['LNAME'] = $lname;
        }

        // member information
        $json = json_encode([
            'email_address' => $data['email'],
            'status'        => $status,
            'merge_fields'  => $merge_fields,
        ]);

        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);

        return true;

	}

	public function update( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'name' => 'required'
		] );

		if ( $validator->fails() ) {
			return redirect()->back()->withInput()->with( 'errors', $validator->errors() );
		}

		$user = Auth::user();
		$user->update( $request->all() );

		return redirect()->back()->with( 'update-success', 'Profile updated successfully' );
	}

	public function update_password( Request $request ) {



		$validator = Validator::make( $request->all(), [
			'current-password' => 'required',
			'password'         => 'required',
			'confirm-password' => 'required',
		] );


		if ( $validator->fails() ) {
			return redirect()->back()->withInput()->with( 'errors', $validator->errors() );
		}

		$old_password = $request->get( 'current-password' );

		if ( Hash::check( $old_password, Auth::user()->getAuthPassword() ) ) {
			Auth::user()->password = $request->get( 'password' );
			Auth::user()->save();

			return Minion::return_success( 'Password changed successfully!' );
		} else {
			return Minion::return_error( '', 'Invalid Current Password' );
		}
	}

	public function update_billing( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'full_address',
			'phone_number',
			'city',
			'state',
			'country',
			'postal_code'
		] );

		if ( $validator->fails() ) {
			return Minion::return_error( null, $validator->errors() );
		}

		$user_billing = UserBilling::where( 'user_id', Auth::id() )->first();

		if ( ! $user_billing ) {
			$user_billing          = new UserBilling();
			$user_billing->user_id = Auth::id();
		}

		$user_billing->address      = $request->get( 'full_address' );
		$user_billing->phone_number = $request->get( 'phone_number' );
		$user_billing->city         = $request->get( 'city' );
		$user_billing->state        = $request->get( 'state' );
		$user_billing->country_id   = $request->get( 'country' );
		$user_billing->postal_code  = $request->get( 'postal_code' );
		$user_billing->save();


	}


	public function api_update_password( Request $request ) {

		try{
					$validator = Validator::make( $request->all(), [
						'current-password' => 'required',
						'password'         => 'required',
					] );
					$old_password = $request->get( 'current-password' );

					$newPassword= $request->get( 'password' );
					$email=$request->get( 'email' );
					$user_id = User::select('id')->where('email', $email)->first();


					if(!empty($user_id))
					{

						$user = User::find($user_id)->first();

						$hasher = app('hash');
						if ($hasher->check($old_password, $user->password))
						{

						$user->password = $request->get( 'password' );
						$user->save();
						$message = "تم تغيير كلمة مرورك بنجاح!";
						$message_en="Your password was successfully changed";
						$isError=false;
						$status=200;
						$current_date_time = Carbon::now()->toDateTimeString();
						return response()->json(['message'=>$message,
						'message_en'=>$message_en,
						'isError' => $isError,
						'status' => $status,
						'timestamp'=>$current_date_time,
						],200);
						// print json_encode($JSON_ARR);


					}
					else
					{
						// $JSON_ARR = array(
						// 'response'=>"your password has been not changed successfully",
						// 'change_password'=>0
						// );
						$message = "كلمة سر خاطئة";
						$message_en="Wrong password";
						$isError=true;
						$status=403;
						$change_password=0;
						$current_date_time = Carbon::now()->toDateTimeString();
						return response()->json(['message'=>$message,
						'message_en'=>$message_en,
						'isError' => $isError,
						'status' => $status,
						'change_password'=>$change_password,
						'timestamp'=>$current_date_time,
						],403);
						// print json_encode($JSON_ARR);


					}
				}
				else
				{
					// $JSON_ARR = array(
					// 	'response'=>"this user does not exist",
					// 	'change_password'=>0
					// 	);
						$message = "هذا المستخدم غير موجود";
						$message_en="This user does not exist";
						$isError=true;
						$status=401;
						$change_password=0;
						$current_date_time = Carbon::now()->toDateTimeString();
						return response()->json(['message'=>$message,
						'message_en'=>$message_en,
						'isError' => $isError,
						'status' => $status,
						'change_password'=>$change_password,
						'timestamp'=>$current_date_time,
						],401);
						// print json_encode($JSON_ARR);

				}


	   }
	   catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'api_update_password',
												 'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }

	}

	public function api_user_update( Request $request ) {
		$name = $request->get( 'name' );
		$id = $request->get( 'user_id' );


		if(!empty($name)  and !empty($id) )
		{

			$user_id = User::select('id')->where('id', $id)->first();
			if(!empty($user_id))
			{


				$user = User::find($user_id)->first();
				$user->name = $name;
				$user->save();
				$JSON_ARR = array(
				'response'=>"your profile has changed successfully!",
				'change_profile'=>1
				);
				print json_encode($JSON_ARR);

			}
			else
			{
				$JSON_ARR = array(
				'response'=>"This user does not exist",
				'change_profile'=>0
				);
				print json_encode($JSON_ARR);

			}

		}
		else
		{
			$JSON_ARR = array(
						'response'=>"Not valid data",
						'change_profile'=>0
						);
					print json_encode($JSON_ARR);
		}


		//if ( $validator->fails() ) {
		//	return redirect()->back()->withInput()->with( 'errors', $validator->errors() );
		//}

		//$user = Auth::user();
		//$user->update( $request->all() );

		///return redirect()->back()->with( 'update-success', 'Profile updated successfully' );
	}



}
