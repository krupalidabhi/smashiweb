<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Gru;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Facebook;

class LoginController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/account';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'guest' )->except( 'logout' );
	}

	public function showLoginForm() {
		return view( 'web.pages.access.login' );
	}

    public function logout() {
		Auth::logout();

		return redirect( '/' );
	}

	public function authenticated(Request $request, $user)
  {

      if (!$user->email_verified_at) {

          auth()->logout();

		   return $request->expectsJson()
            ? response()->json(['message' => 'Email is not verified', 'errors' => ['email'=> [" لم يتم التحقق من البريد الإلكتروني "]]], 422)
            : redirect()->guest(route('login'));

					// return redirect()->back()
          // ->withInput($request->only($this->username(), 'remember'))
          // ->withErrors([
          //     $this->username() => 'Not verified email',
          // ]);
					//return redirect()->back()->withErrors(['email', 'Email is not verify']);
          // return back()->with('email', 'You need to confirm your account. We have sent you an activation code, please check your email.');
      }
      return redirect()->intended($this->redirectPath());
  }

	public function redirectToProvider( $provider ) {

		if( $provider == 'facebook' ){
			session_start();
			$fb = new Facebook\Facebook([
	          'app_id' => env('FB_app_id'), // Replace {app-id} with your app id
	          'app_secret' => env('FB_app_secret'),
	          'default_graph_version' => 'v3.2',
	        ]);

	        $helper = $fb->getRedirectLoginHelper();

	        $permissions = ['email']; // Optional permissions
	        $loginUrl = $helper->getLoginUrl('https://'.$_SERVER["HTTP_HOST"].env('FB_redirect'), $permissions);

	        return redirect()->away( $loginUrl );

		}

		return Socialite::driver( $provider )->redirect();
	}

	public function handleProviderCallback( $provider ) {

		if( $provider == 'facebook' ){
			session_start();
			try {
				$fb = new Facebook\Facebook([
		          'app_id' => env('FB_app_id'), // Replace {app-id} with your app id
		          'app_secret' => env('FB_app_secret'),
		          'default_graph_version' => 'v3.2',
		        ]);

		        $helper = $fb->getRedirectLoginHelper();

		        if (isset($_GET['state'])) {
				    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
				}

		        $accessToken = $helper->getAccessToken();

		        // echo $accessToken;
		        // die;
		        $response = $fb->get('/me?fields=id,name,email', $accessToken);
	        	$user = $response->getGraphUser()->asArray();
	        	$provider = self::get_social_type( $provider );

	        	$authUser = User::firstOrCreate(
					[
						'email' => $user['email'],
					], [
						'name'         => $user['name'],
						'social_email' => $user['email'],
						'social_type'  => $provider,
						'social_id'    => $user['id']
					]
				);

				Auth::login( $authUser, true );

				return redirect('/account/');
			} catch(Exception $e) {
				abort(400);
			}

		}

		$user = Socialite::driver( $provider )->stateless()->user();

		$provider = self::get_social_type( $provider );

		$authUser = User::firstOrCreate(
			[
				'email' => $user->email,
			], [
				'name'         => $user->name,
				'social_email' => $user->email,
				'email' => $user->email,
				'social_type'  => $provider,
				'social_id'    => $user->id
			]
		);

		Auth::login( $authUser, true );

		return redirect( $this->redirectTo );
	}

	public function findOrCreateUser( $user, $provider ) {
		$provider = self::get_social_type( $provider );

		$authUser = User::where( 'social_id', $user->id )
		                ->where( 'social_type', $provider )->first();
		if ( $authUser ) {
			return $authUser;
		}

		return User::create(
			[
				'name'        => $user->name,
				'email'       => $user->email,
				'social_type' => $provider,
				'social_id'   => $user->id
			] );
	}

	private function get_social_type( $provider ) {
		if ( $provider == 'facebook' ) {
			return Gru::SOCIAL_FACEBOOK;
		} elseif ( $provider == 'google' ) {
			return Gru::SOCIAL_GOOGLE;
		} else {
			abort( 404 );
		}
	}

public function api_login(Request $request)
	{
	//$validate=	$this->validateLogin($request);

		$email=$request['email'];
		$password=$request['password'];
		if(!empty($email) )
		{
			if(!filter_var($email, FILTER_VALIDATE_EMAIL))
			{
				$JSON_ARR[] = array(
				'response'=>"Not valid email",
				'is_login'=>"NO"
				);
				print json_encode($JSON_ARR);
				die();
			}
		}
		if(empty($password))
		{

				$JSON_ARR[] = array(
				'response'=>"Password is required",
				'is_login'=>"NO"
				);
				print json_encode($JSON_ARR);
				die();
		}

		if ($this->attemptLogin($request)) {
			$user = $this->guard()->user();
			//$user->generateToken();

			//$request= response()->json([
			//	'data' => $user,
			//]);

			//print(response()->json($user, 200));

				$JSON_ARR[] = array(
				'response'=>$user,
				'is_login'=>"YES"
				);

			print json_encode($JSON_ARR);
			die();

		}
		else
		{

			 //print( response()->json(['error' => 'UnAuthorised'], 401));

			$JSON_ARR[] = array(
				'response'=>'UnAuthorised',
				'is_login'=>"NO"
				);

			print json_encode($JSON_ARR);
			die();

		}
		///print_r( $this->sendFailedLoginResponse($request));
	}







}
