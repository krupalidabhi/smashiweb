<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserVerifyToken;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\verifyEmail;
use Illuminate\Support\Str;
use Mail;
use App\Models\EmailVerifyMailStatus;
use DB;
use App\Models\LogSaveInDb;
use Exception;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Support\Facades\Password;
use Illuminate\Contracts\Routing\ResponseFactory;
use Hash;
use App\Models\UserSubscription;
use App\Rules\ValidRecaptcha;

class RegisterController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/login';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'guest' );
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 *
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator( array $data ) {
		return Validator::make( $data, [
			'name'     => 'required|string|max:255',
			'email'    => 'required|string|email|unique:users|max:255',
			'password' => 'required|string|min:6|confirmed',
			'g-recaptcha-response'=> ['required', new ValidRecaptcha],
		],[
			'g-recaptcha-response.required' => 'مطلوب المصادقة البشرية',
		] );
	}

	private function subscribe_user_mailchimp($data){

		// Register the user in mailchimp subscription list
		$apiKey = env('MAILCHIMP_API');
        $listID = env('MAILCHIMP_LIST');

        // MailChimp API URL
        $memberID = md5(strtolower( $data['email'] ));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

        $fname_array = explode(' ', $data['name']);

        $fname = @$fname_array[0];
        $lname = @$fname_array[1];

        $merge_fields = array(
        	'FNAME'     => $fname,
        );

        if( !empty($lname) ){
        	$merge_fields['LNAME'] = $lname;
        }

        // member information
        $json = json_encode([
            'email_address' => $data['email'],
            'status'        => 'subscribed',
            'merge_fields'  => $merge_fields,
        ]);

        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);

        return true;

	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 *
	 * @return \App\Models\User
	 */
	protected function create( array $data, Request $request ) {

		if( @$data['newsletter'] == 1 ){
			$this->subscribe_user_mailchimp($data);
	    }else{
	        // Do nothing
	    }

		$user = User::create( [
			'name'     => $data['name'],
			'email'    => $data['email'],
			'password' => $data['password'],
			'newsletter'=> ( empty(@$data['newsletter'])) ? 0 : $data['newsletter'] ,
			'notifications_opted' => ( empty(@$data['notifications_opted'])) ? 0 : (int)$data['notifications_opted'] ,
			'user_agent'    => $request->header('User-Agent'),
			'notifications_opted'    => ( empty(@$data['notificationsOpted'])) ? 0 : $data['notificationsOpted'],
		] );

		$users = User::where(['email'=>$user->email])->orderBy('id')->first();

    	//email verification
		// return $users;

		$thisUser = User::findOrFail($users->id);

		$UserVerifyToken = new UserVerifyToken();
		$verifyToken = Str::random();
		$UserVerifyToken->token = $verifyToken;
		$UserVerifyToken->user_email = $data['email'];
		$UserVerifyToken->user_id = $users->id;
		// ISO 8601 timestamp
		$UserVerifyToken->timestamp = strtotime( date("c") );
		$UserVerifyToken->save();

		$this->sendEmail($thisUser, $verifyToken);

		return $user;
	}

	public function showRegistrationForm() {
		return view( 'web.pages.access.register' );
	}

	public function register( Request $request ) {

		$this->validator( $request->all() )->validate();

		event( new Registered( $user = $this->create( $request->all(), $request ) ) );

		return $this->registered( $request, $user )
			?: redirect( $this->redirectPath() );
	}



	public function createuser( Request $request )
	{
	try{
			$email=$request->input('email');
			$password=$request->input('password');
			$name=$request->input('name');

			if(!empty($password) and !empty($email) and !empty($name))
			{

				if(!filter_var($email, FILTER_VALIDATE_EMAIL))
				{
				$message ="بريد الكتروني غير صالح";
				$message_en="Invalid email";
				$isError=true;
				$status=403;
				return response()->json(['message'=>$message,
				'message_en'=>$message_en,
				'isError' => $isError,
				'status' => $status,
				],403);
				}
				else{

				}


				$user = User::where('email', '=', $email)->first();

				if(!empty($user))
				{
					if(empty($user->email_verified_at)){
						return $this->resend_mail_if_user_allready_exist($request);
					}
					$message= "اسم المستخدم موجود بالفعل";
					$message_en="Username already exists";
					$isError=true;
					$status=409;
					return response()->json(['message'=>$message,
					'message_en'=>$message_en,
					'isError' => $isError,
					'status' => $status,

					],409);

				}
				else{
					event(new Registered($user = $this->create($request->all(),$request)));
				}



				if($user){
		            try {
					// $users = User::all();

					$message = "أهلا بك! لقد قمت بالتسجيل بنجاح";
					$message_en="welcome! You have successfully registered";
					$user_details=$user;
					$isError=false;
					$status=200;
					return response()->json(['message'=>$message,
					'message_en'=>$message_en,
					'user_details'=>$user_details,
					'isError' => $isError,
					'status' => $status,

					],200);
					}

					catch (Exception $e) {
						echo $e->getMessage();
						die;
					}
				// print json_encode($JSON_ARR);
				}

				else
				{
					$message = "كلمة المرور خاطئة";
					$message_en="Wrong password";
					$isError=true;
					$status=403;
					return response()->json(['message'=>$message,
					'message_en'=>$message_en,
					'isError' => $isError,
					'status' => $status,

					],403);

					// print json_encode($JSON_ARR);

				}




			}
			else
			{
				$message = "بيانات خاطئة";
				$isError=true;
					$status=401;
					return response()->json(['message'=>$message,
						'message_en'=>'Wrong data',
					'isError' => $isError,
					'status' => $status,

					],401);

			}

	   }
	   catch(\Exception $exception)
        {

        LogSaveInDb::insert(
                        ['description' =>$exception->getTraceAsString(),
                         'file_Name' => $exception->getFile(),
                         'type' =>  $exception->getMessage(),
                         'code' => $exception->getCode(),
                         'line' => $exception->getLine(),
                         'ip' => $request->ip(),
                         'user_agent' => $request->header('User-Agent'),
                         'api_name' => 'Register',
												 'created_at'=> date("Y-m-d h:i:s",time()),
                        ]
                    );

            }


	}

	public function sendEmail($thisUser, $verifyToken)
    {
        $sent= Mail::to($thisUser['email'])->send(new verifyEmail($thisUser, $verifyToken));
        if( count(Mail::failures()) > 0 ) {



		   foreach(Mail::failures() as $email_address) {
		       $body[]=$email_address;
		    }


		     $data = array(
			    array('user_id'=>$thisUser['id'],
			    	  'email_id'=> $thisUser['email'],
			    	  'status'=>'Not sent',
			    	  'body'=>$body,
			    	  'error'=>true,
			    	  'time'=>date("Y-m-d h:i:sa",time()),
			    	),
			);
			EmailVerifyMailStatus::insert($data);

		} else {
		    $data = array(
			    array('user_id'=>$thisUser['id'],
			    	  'email_id'=> $thisUser['email'],
			    	  'status'=>'Sent',
			    	  'body'=>'',
			    	  'error'=>false,
			    	  'time'=>date("Y-m-d h:i:sa",time()),
			    	),
			);
			EmailVerifyMailStatus::insert($data);
		}


    }

	public function user_details(Request $request)
	{
		$user_id=$request->input('user_id');
		$user = User::where('id', '=', $user_id)->first();
		if(!empty($user))
		{
			//echo "====".$user['email'];
			//print_r($user);
		$message = $user;
		$isError=true;
			$status=401;
			return response()->json(['message'=>$message,
			'isError' => $isError,
			'status' => $status,

			],401);


		}
		else
		{
			$message = "المستخدم غير موجود";
			$isError=true;
			$status=401;
			return response()->json(['message'=>$message,
			'isError' => $isError,
			'status' => $status,

			],401);
		}


	}


	public function verifyMailToken($token, Request $request){

		if( !empty($token) ){
			// In minutes
			$expire_time = env('EMAIL_VERIFY_TIME', 1440);
			$UserVerifyToken = UserVerifyToken::where('token', $token)->first();
			$curretnTime = strtotime( date('c') );

			$time_spend_m = ( $curretnTime - $UserVerifyToken->timestamp ) / 60;

			if( $time_spend_m <= $expire_time ){

				$user = User::where('email', '=', $UserVerifyToken->user_email)->first();
				// $user->email_verified_at = date('Y-m-d h:i:s');

				$user->save();

				$user->markEmailAsVerified();
				$status_mail = " تم التحقق من البريد الإلكتروني الخاص بك. يمكنك الآن تسجيل الدخول.";

				$validator = Validator::make($request->all(), []);


		        $cookie_name = "message";
				setcookie($cookie_name, $status_mail, time() + (86400 * 1), "/");

				// Subscribe the user for 7 day trial period
				$UserSubscription = new UserSubscription();
				$UserSubscription->user_id = $user->id;
				$UserSubscription->subscription_id = 0;
				$UserSubscription->transaction_id = 0;
				$UserSubscription->is_active = 1;
				$UserSubscription->valid_for = 7;
				$UserSubscription->save();

				// Send mail
				$data = array('username'=> $user->name);
				Mail::send('mail.trial', $data, function($message) use($user) {
					$message->to($user->email, 'SmashiTV')->subject
					('SmashiTV | 7 days Trial');
					$message->from('hello@smashi.tv','SmashiTV');
				});

				return redirect( '/login' );

				return view( 'web.pages.access.login' )->with(['errors' => $validator->errors(), 'status_mail' => $status_mail]);


				// event(new Verified($user));
				return response()->json([
					'message'=>'Email verified',
					'isError' => false,
					'status' => 200,

				],200);
			}
		}

		return response()->json([
			'message'=>'Email not verified',
			'isError' => true,
			'status' => 401,

		],200);
	}

	protected function resend_mail_if_user_allready_exist(Request $request)
	{
		 try { $user= User::where('email','=',$request->email)->first();

			 $email=$request->input('email');
			 $password=Hash::make($request->input('password'));
			 $name=$request->input('name');
			 $newsletter= @$request->input('newsletter');
			 $notifications_opted= @$request->input('notificationsOpted');

			 $authUser=User::where('id', $user->id)
									 ->update(['name' =>$name ,
												 'email' => $email,
												 'password' => $password,
												 'newsletter' => (empty($newsletter)) ? 0 : (int)$newsletter,
												 'notifications_opted' => (empty($notifications_opted)) ? 0 :(int)$notifications_opted,
											 ]);
					$users = User::all();
		        	$users = User::where(['email'=>$user->email])->orderBy('id')->first();


					$thisUser = User::findOrFail($users->id);

					$UserVerifyToken = new UserVerifyToken();
					$verifyToken = Str::random();
					$UserVerifyToken->token = $verifyToken;
					$UserVerifyToken->user_email = $user->email;
					$UserVerifyToken->user_id = $users->id;
					// ISO 8601 timestamp
					// echo "string";
					// return DB::table('password_resets')->where('email','=',$user->email)->get();

					$UserVerifyToken->timestamp = strtotime( date("c") );
					$UserVerifyToken->save();

					$this->sendEmail($thisUser, $verifyToken);
					$message = "أهلا بك! لقد قمت بالتسجيل بنجاح";
					$message_en="welcome! You have successfully registered";
					$user_details=$user;
					$isError=false;
					$status=200;



					return response()->json(['message'=>$message,
					'message_en'=>$message_en,
					'user_details'=>$user= User::where('email','=',$request->email)->first(),
					'isError' => $isError,
					'status' => $status,

					],200);
					}

					catch(\Exception $exception)
		        {

		        LogSaveInDb::insert(
		                        ['description' =>$exception->getTraceAsString(),
		                         'file_Name' => $exception->getFile(),
		                         'type' =>  $exception->getMessage(),
		                         'code' => $exception->getCode(),
		                         'line' => $exception->getLine(),
		                         'ip' => $request->ip(),
		                         'user_agent' => $request->header('User-Agent'),
		                         'api_name' => 'Register',
														 'created_at'=> date("Y-m-d h:i:s",time()),
		                        ]
		                    );

		            }
	}


}
