<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subscription;

class subscriptionPlanPrice extends Controller
{
    //
    public function index(){

    	$bd_allPlan = Subscription::all();

        // array_push($bd_allPlan, 'price_ab');
        
        foreach ($bd_allPlan as $Plans) {
            # code...
            
            $all_plan[]=array(
                "id"=> $Plans->id,
                "name"=> $Plans->name,
                "name_ab"=>$this->planInAr($Plans->name),
                "price"=> $Plans->price,
                "product_id"=> $Plans->product_id,
                "pricing_id"=> $Plans->pricing_id,
                "created_at"=>  date( $Plans->created_at),
                "updated_at"=> date($Plans->updated_at),
                "sku"=> $Plans->sku,
                "price_sar"=> $Plans->price_sar,
                "price_usd"=> $Plans->price_usd
            );
        }

    	return response()->json([
			'plans'=> $all_plan,
			'isError' => false,
            'message' => 'جلب جميع الخطط بنجاح',
            'message_en'=>'all plans fetch successfully ',
            'status' => 200,
    	],200);
    }
    private function planInAr($plan){
        if(ucfirst($plan)=='Daily'){
                // $Plans->name_ab='اليومي';
                return 'اليومي';
            }
            elseif(ucfirst($plan)=='Weekly'){
                // $Plans->name_ab='أسبوعي';
                return 'أسبوعي';
            }
            elseif(ucfirst($plan)=='Monthly'){
                // $Plans->name_ab='شهريا';
                return 'شهريا';
            }
            elseif(ucfirst($plan)=='Annually'){
                // $Plans->name_ab='سنويا';
                return 'سنويا';
            }
    }

}
