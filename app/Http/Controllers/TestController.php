<?php

namespace App\Http\Controllers;


use App\Models\Contact;
use App\Notifications\ContactUs;
use Illuminate\Support\Facades\Notification;
use Youtube;
use Facebook\FacebookRequest;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Facebook\FacebookSDKException;
use Facebook;

class TestController extends Controller {
	public function index() {
//	    $contact_us = Contact::find(1);
//        Notification::route('mail', 'support@smashi.tv')->notify(new ContactUs($contact_us));

    }

    public function TestYoutube(){

        try {
            $fb = new Facebook\Facebook([
              'app_id' => env('FB_APP_ID'), // Replace {app-id} with your app id
              'app_secret' => env('FB_APP_SECRET'),
              'default_graph_version' => 'v3.2',
            ]);
          // Returns a `Facebook\FacebookResponse` object
          $response = $fb->post(
            '/708628499168910/videos',
            array (
              'source' => '/home/vishal/Downloads/videoplayback.mp4',
            ),
            'EAAITgGRzeTwBAMHwb1IyQOQCOvEgKhdGOBwOUZBwvudGkn9gM55I7vp5o4IpAbeiYzG6mhiUm8JAIPfJoAlUjULcxK7VGqBJoLP5Omas4TnqTcWEU9DZAh99eBT2I4kXdeOZBOC9fYDNkCO3kqAEKA0x0OrLwtmQLS4FD6ZAgwZDZD'
          );
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }
        $graphNode = $response->getGraphNode();

        echo '<pre>';
        print_r($graphNode);
        die;

    	/*$video = Youtube::upload('https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/20200204_Smashi_MorningShow_Step_Social/default/hls/20200204_Smashi_MorningShow_Step_SocialOutput1.m3u8', [
            'title'       => 'Morning news',
            'description' => '31 March Morning news',
            'tags'        => ['api', 'youtube'],
        ]);
         
        echo $video->getVideoId();
        die;*/

    }
}
