<?php

namespace App\Http\Middleware;

use Closure;
// use Response;
use Illuminate\Http\Response;
use App\Models\LiveVideo;
use App\Models\Video;
use GuzzleHttp\Client;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use GuzzleHttp\Psr7\Response;


class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {  
        
        if(Auth::guard('admin')->check()){
            return new Response(view('admin.pages.channel'));
        }

        abort(404);
        /*$verfied_id = array('abhinav.sharma@oodlestechnologies.com','test@gmail.com','');
        $no_id= count($verfied_id);
        for($i=0; $i<$no_id; $i++){

            if ($request->user()->email == $verfied_id[$i] ){
                
               return new Response(view('admin.pages.channel'));
            }

        }
        
        return $next($request);*/
    }


}
