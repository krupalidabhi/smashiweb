<?php

namespace App\Http\Middleware\Nova;

use Closure;

class Authenticate extends \Laravel\Nova\Http\Middleware\Authenticate
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure                 $next
	 * @param array                     $guards
	 *
	 * @return mixed
	 * @throws \Illuminate\Auth\AuthenticationException
	 */
	public function handle($request, Closure $next, ...$guards)
	{
		return parent::handle($request, $next, 'admin');
	}
}
