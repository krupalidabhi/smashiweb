<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class CheckEmailVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $User = new User;
       if(!empty($_REQUEST['email'])) {

        if( !$User->hasVerifiedEmailc($_REQUEST['email']) ){

            return response()->json([
                'message_en'=> 'Email is not verified',
                'message'=> 'لم يتم التحقق من البريد الإلكتروني',
                'isError' => true,
                'status' => 403,

            ],403);

        }
        return $next($request);
       }
       else{
       return response()->json([
                'message'=> 'تم إدخال بيانات اعتماد غير صالحة',
                'message_en'=> 'Invalid credentials entered',
                'isError' => true,
                'status' => 403,

            ],403);

       }


    }
}
