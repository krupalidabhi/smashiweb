<?php

namespace App\Http\Middleware;

use App\Models\AdminUser;
use Closure;
// use Illuminate\Support\Facades\Cookie;

class adminCheckForApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   private $user = null;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty($_REQUEST['email'])) {
            $adminuser=$this->get_user_profile($_REQUEST['email']);
            if(!empty($adminuser)){
                 return $next($request);
            }
            else{
                return response()->json([
                'message'=> 'تم إدخال بيانات اعتماد غير صالحة',
                'message_en'=> 'Invalid credentials entered',
                'isError' => true,
                'status' => 403,
            
            ],403);
            }
     
    }
    }

    private function get_user_profile($email) {
        return AdminUser::where('email', '=', $email)->first();
    }
}
