<?php

namespace App\Http\Middleware;

use App\Models\AdminUser;
use Closure;
use Illuminate\Support\Facades\Cookie;

class AdminAuthentication
{
	private $user = null;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $session_exist = $request->session()->has(config('jarvis.session-cookie-key'));
	    $cookie_exist = Cookie::has(config('jarvis.cache-prefix'));

	    if($request->getPathInfo() == '/login' && ($session_exist || $cookie_exist)) {
		    return redirect('/');
	    }

		if($session_exist) {
			$user_id = $request->session()->get(config('jarvis.session-cookie-key'));
			$this->user = self::get_user_profile($user_id);

			if(!is_null($this->user)) {
				$request->attributes->add(['user' => $this->user]);
                view()->share('__user', $this->user);
				return $next($request);
			} else {
				$request->session()->forget(config('jarvis.session-cookie-key'));
				return redirect()->action('Admin\LoginController@index');
			}
		} elseif($cookie_exist) {
			$user_id = Cookie::get(config('jarvis.session-cookie-key'));
			$this->user = self::get_user_profile($user_id);

			if(!is_null($this->user)) {
				$request->attributes->add(['user' => $this->user]);
                view()->share('__user', $this->user);
				return $next($request);
			} else {
				return redirect()->action('Admin\LoginController@index')->withCookie(Cookie::forget(config('jarvis.session-cookie-key')));
			}
		} else {
			if($request->getPathInfo() == '/'. config('jarvis.admin-path') .'/login') {
				return $next($request);
			} else {
				return redirect()->action('Admin\LoginController@index');
			}
		}
    }

	private function get_user_profile($user_id) {
		return AdminUser::where('id', '=', $user_id)->first();
	}
}
