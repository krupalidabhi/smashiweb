<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Web\Payment\MainController;

class ExpireTheSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set inactive to expired subscriptions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $paymentController = new checkExpiredSubscription();
        $paymentController->checkExpiredSubscription();
        return true;
    }
}
