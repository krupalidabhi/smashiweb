<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Subscription;

class HourlyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currience:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync currience data in table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscription = Subscription::all();

        if( !empty($subscription) ){
            foreach ($subscription as $key => $value) {
                $subscr = Subscription::find($value->id);
                $subscr->price_sar = $this->currencyConverter('AED', 'SAR', $value->price);
                $subscr->price_usd = $this->currencyConverter('AED', 'USD', $value->price);
                $subscr->save();
            }
        }

    }

    function currencyConverter($fromCurrency,$toCurrency,$amount) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://free.currconv.com/api/v7/convert?q=".$fromCurrency."_".$toCurrency."&compact=ultra&apiKey=1596754d05bb653de4b7",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode( $response, true );
        return ( $response[$fromCurrency."_".$toCurrency] ) * $amount;
    }
}
