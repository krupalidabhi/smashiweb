<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Web\Payment\MainController;

class SyncSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manage subscribed user payment data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $PaymentController = new MainController();
        $PaymentController->SyncSubscriptionPayment();
        return true;
    }
}
