let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack ostbuild steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.sass('resources/assets/sass/style-ar.scss', 'public/assets/css/compiled.css')
    .sass('resources/assets/sass/admin.scss', 'public/admin/css/admin.css')
    .js('resources/assets/js/main.js', 'public/assets/js/compiled.js');

mix.styles([
    'node_modules/bootstrap-v4-rtl/dist/css/bootstrap.min.css',
    'node_modules/bootstrap-select-v4/dist/css/bootstrap-select.min.css',
    'public/vendor/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css',
    'public/vendor/fontawesome/css/all.min.css',
    'node_modules/video.js/dist/video-js.min.css',
    'public/assets/css/compiled.css'
],  'public/assets/css/style-ar.css');
//
mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
    'node_modules/bootstrap-select-v4/dist/js/bootstrap-select.min.js',
    'node_modules/jquery-validation/dist/jquery.validate.min.js',
    'public/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    'public/vendor/scrollreveal/scrollreveal.min.js',
    'node_modules/video.js/dist/video.min.js',
    'node_modules/@videojs/http-streaming/dist/videojs-http-streaming.js',
    'public/vendor/dacast/player.js',
    // 'public/vendor/paytabs/paytabse.js',
    // 'node_modules/videojs-hls-quality-selector/dist/videojs-hls-quality-selector.min.js',
    'public/assets/js/compiled.js'
], 'public/assets/js/main.js');

mix.scripts([
    'public/admin/js/vendors.bundle.js',
    'public/admin/js/scripts.bundle.js',
    'public/admin/js/html-table.js',
    'public/admin/js/bootstrap-select.js',
    'public/admin/js/bootstrap-datepicker.js',
    'public/admin/js/calendar-basic.js',
    'public/admin/js/summernote.js',
    'public/admin/js/bootstrap-datetimepicker.js',
    'public/vendors/sweetalert2/sweetalert2.all.min.js',
    'public/admin/js/dashboard.js',
    'public/admin/js/admin.js'
], 'public/admin/js/admin_main.js');

