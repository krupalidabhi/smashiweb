-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: smashitv
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_events`
--

DROP TABLE IF EXISTS `action_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_id` int(10) unsigned NOT NULL,
  `target_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` int(10) unsigned DEFAULT NULL,
  `fields` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'running',
  `exception` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `action_events_actionable_type_actionable_id_index` (`actionable_type`,`actionable_id`),
  KEY `action_events_batch_id_model_type_model_id_index` (`batch_id`,`model_type`,`model_id`),
  KEY `action_events_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_events`
--

LOCK TABLES `action_events` WRITE;
/*!40000 ALTER TABLE `action_events` DISABLE KEYS */;
INSERT INTO `action_events` VALUES (1,'8d1c63d7-c33a-449a-a84f-eee4e1314e43',2,'Update','App\\Models\\Page',3,'App\\Models\\Page',3,'App\\Models\\Page',3,'','finished','','2019-03-02 12:45:36','2019-03-02 12:45:36'),(2,'8d1c66c7-aa46-445e-9cd4-3c81a112aa54',2,'Update','App\\Models\\Page',3,'App\\Models\\Page',3,'App\\Models\\Page',3,'','finished','','2019-03-02 12:53:49','2019-03-02 12:53:49'),(3,'8d1c6713-c1e2-4de2-8b6e-62b7b14b6323',2,'Update','App\\Models\\Page',3,'App\\Models\\Page',3,'App\\Models\\Page',3,'','finished','','2019-03-02 12:54:39','2019-03-02 12:54:39'),(4,'8d1c68af-f4b3-470d-8b58-4a804e639b9a',2,'Update','App\\Models\\Page',3,'App\\Models\\Page',3,'App\\Models\\Page',3,'','finished','','2019-03-02 12:59:09','2019-03-02 12:59:09'),(5,'8d1c6a6b-5ce6-49c6-a53c-d86d54b64f6b',2,'Update','App\\Models\\Page',3,'App\\Models\\Page',3,'App\\Models\\Page',3,'','finished','','2019-03-02 13:04:00','2019-03-02 13:04:00'),(6,'8d1c6d72-4773-4e93-93d7-b97a4f378519',2,'Update','App\\Models\\Page',4,'App\\Models\\Page',4,'App\\Models\\Page',4,'','finished','','2019-03-02 13:12:28','2019-03-02 13:12:28'),(7,'8d1c6e25-14d6-4a88-9814-47ce2e1d66c0',2,'Update','App\\Models\\Page',1,'App\\Models\\Page',1,'App\\Models\\Page',1,'','finished','','2019-03-02 13:14:25','2019-03-02 13:14:25'),(8,'8d1c8cde-080d-49ec-ba6a-6c37667913ea',2,'Update','App\\Models\\Page',3,'App\\Models\\Page',3,'App\\Models\\Page',3,'','finished','','2019-03-02 14:40:19','2019-03-02 14:40:19'),(9,'8d1c8d45-4b94-4f70-bb98-a0883ce25129',2,'Update','App\\Models\\Page',3,'App\\Models\\Page',3,'App\\Models\\Page',3,'','finished','','2019-03-02 14:41:27','2019-03-02 14:41:27'),(10,'8d1ddab3-7450-4208-9de0-2e8d1172e179',2,'Update','App\\Models\\Page',1,'App\\Models\\Page',1,'App\\Models\\Page',1,'','finished','','2019-03-03 06:13:47','2019-03-03 06:13:47'),(11,'8d1ddac6-cadb-44e1-b4b7-bd40e217275e',2,'Update','App\\Models\\Page',4,'App\\Models\\Page',4,'App\\Models\\Page',4,'','finished','','2019-03-03 06:14:00','2019-03-03 06:14:00'),(12,'8d1ddc5e-b24b-4a19-8a34-8ff1b06b3437',2,'Update','App\\Models\\Page',4,'App\\Models\\Page',4,'App\\Models\\Page',4,'','finished','','2019-03-03 06:18:27','2019-03-03 06:18:27'),(13,'8d1dde7d-d4ff-4b6c-be90-6ec8627e062f',2,'Update','App\\Models\\Page',1,'App\\Models\\Page',1,'App\\Models\\Page',1,'','finished','','2019-03-03 06:24:23','2019-03-03 06:24:23'),(14,'8d1ddf2c-349a-41b6-bf4a-c588c841f34d',2,'Update','App\\Models\\Page',1,'App\\Models\\Page',1,'App\\Models\\Page',1,'','finished','','2019-03-03 06:26:17','2019-03-03 06:26:17'),(15,'8d1de255-4684-4424-8a5b-789225825f48',2,'Delete','App\\Models\\Page',2,'App\\Models\\Page',2,'App\\Models\\Page',2,'','finished','','2019-03-03 06:35:08','2019-03-03 06:35:08'),(16,'8d1ea06d-1afe-42a9-b279-afce9bf00bc9',2,'Delete','App\\Models\\Video',3,'App\\Models\\Video',3,'App\\Models\\Video',3,'','finished','','2019-03-03 15:26:40','2019-03-03 15:26:40'),(17,'8d1ea071-255c-4b14-a398-9cdd7aa2842c',2,'Delete','App\\Models\\Video',2,'App\\Models\\Video',2,'App\\Models\\Video',2,'','finished','','2019-03-03 15:26:43','2019-03-03 15:26:43'),(18,'8d1ea075-2bf3-476c-abaf-a641dd1dac0a',2,'Delete','App\\Models\\Video',1,'App\\Models\\Video',1,'App\\Models\\Video',1,'','finished','','2019-03-03 15:26:45','2019-03-03 15:26:45'),(19,'8d1eaadf-6887-4169-8eb3-4aed95104e9a',2,'Update','App\\Models\\Video',5,'App\\Models\\Video',5,'App\\Models\\Video',5,'','finished','','2019-03-03 15:55:53','2019-03-03 15:55:53'),(20,'8d1eac27-479f-4dd8-a4a8-df481dd0e067',2,'Update','App\\Models\\Video',6,'App\\Models\\Video',6,'App\\Models\\Video',6,'','finished','','2019-03-03 15:59:27','2019-03-03 15:59:27'),(21,'8d1eae12-4475-4302-8d1b-85ce29e3eee3',2,'Update','App\\Models\\Video',8,'App\\Models\\Video',8,'App\\Models\\Video',8,'','finished','','2019-03-03 16:04:49','2019-03-03 16:04:49'),(22,'8d1eae2d-85f9-4659-854f-0c27fc65ef57',2,'Update','App\\Models\\Video',7,'App\\Models\\Video',7,'App\\Models\\Video',7,'','finished','','2019-03-03 16:05:07','2019-03-03 16:05:07'),(23,'8d1eae43-8fbc-4fe5-893e-f1ffa9aff6c7',2,'Update','App\\Models\\Video',6,'App\\Models\\Video',6,'App\\Models\\Video',6,'','finished','','2019-03-03 16:05:21','2019-03-03 16:05:21'),(24,'8d1eae5b-5c00-46ca-a513-8af833b0a28d',2,'Update','App\\Models\\Video',5,'App\\Models\\Video',5,'App\\Models\\Video',5,'','finished','','2019-03-03 16:05:37','2019-03-03 16:05:37'),(25,'8d1eb4fe-9855-453c-861d-29f11737a047',2,'Update','App\\Models\\Page',4,'App\\Models\\Page',4,'App\\Models\\Page',4,'','finished','','2019-03-03 16:24:11','2019-03-03 16:24:11'),(26,'8d1eb537-0538-469b-8c38-7db6cbd3c50f',2,'Update','App\\Models\\Page',4,'App\\Models\\Page',4,'App\\Models\\Page',4,'','finished','','2019-03-03 16:24:48','2019-03-03 16:24:48'),(27,'8d1eb710-b226-4c8e-b865-30b5338885a6',2,'Update','App\\Models\\Page',3,'App\\Models\\Page',3,'App\\Models\\Page',3,'','finished','','2019-03-03 16:29:58','2019-03-03 16:29:58'),(28,'8d1fd3d8-e735-4df0-bf25-30211412779a',2,'Update','App\\Models\\Video',4,'App\\Models\\Video',4,'App\\Models\\Video',4,'','finished','','2019-03-04 05:46:17','2019-03-04 05:46:17'),(29,'8d242914-17a0-4efd-a2dc-2c4953c1cc8e',2,'Update','App\\Models\\Subscription',1,'App\\Models\\Subscription',1,'App\\Models\\Subscription',1,'','finished','','2019-03-06 09:27:55','2019-03-06 09:27:55'),(30,'8d242917-627e-460b-933f-0c714de94c54',2,'Update','App\\Models\\Subscription',1,'App\\Models\\Subscription',1,'App\\Models\\Subscription',1,'','finished','','2019-03-06 09:27:57','2019-03-06 09:27:57'),(31,'8d24292c-04f2-4c49-b98d-5da6d0add93b',2,'Update','App\\Models\\Subscription',2,'App\\Models\\Subscription',2,'App\\Models\\Subscription',2,'','finished','','2019-03-06 09:28:10','2019-03-06 09:28:10'),(32,'8d242937-4dd6-4435-b991-e515c59365c0',2,'Update','App\\Models\\Subscription',3,'App\\Models\\Subscription',3,'App\\Models\\Subscription',3,'','finished','','2019-03-06 09:28:18','2019-03-06 09:28:18'),(33,'8d2454ac-1886-4c14-bcf3-58e0e5a2b802',2,'Update','App\\Models\\Page',3,'App\\Models\\Page',3,'App\\Models\\Page',3,'','finished','','2019-03-06 11:29:49','2019-03-06 11:29:49'),(34,'8d245582-7109-4210-954e-bc0fbb969135',2,'Update','App\\Models\\Page',1,'App\\Models\\Page',1,'App\\Models\\Page',1,'','finished','','2019-03-06 11:32:09','2019-03-06 11:32:09'),(35,'8d245742-ec2e-4424-acd5-d400c2967b23',2,'Update','App\\Models\\Page',4,'App\\Models\\Page',4,'App\\Models\\Page',4,'','finished','','2019-03-06 11:37:03','2019-03-06 11:37:03'),(36,'8d2c799b-3a21-42b3-b09b-510b24983239',2,'Update','App\\Models\\Video',6,'App\\Models\\Video',6,'App\\Models\\Video',6,'','finished','','2019-03-10 12:39:42','2019-03-10 12:39:42'),(37,'8d2c7a29-7d37-4320-b05b-a53cd820373b',2,'Update','App\\Models\\Video',5,'App\\Models\\Video',5,'App\\Models\\Video',5,'','finished','','2019-03-10 12:41:16','2019-03-10 12:41:16'),(38,'8d2c7a88-5acc-4b15-8bca-1de8bafa0e7e',2,'Update','App\\Models\\Video',6,'App\\Models\\Video',6,'App\\Models\\Video',6,'','finished','','2019-03-10 12:42:18','2019-03-10 12:42:18'),(39,'8d2c7ac1-8d13-419c-a4af-573ac82f6eea',2,'Update','App\\Models\\Video',9,'App\\Models\\Video',9,'App\\Models\\Video',9,'','finished','','2019-03-10 12:42:55','2019-03-10 12:42:55'),(40,'8d2c7af5-c36f-41bf-943d-8a9d870cafb0',2,'Update','App\\Models\\Video',8,'App\\Models\\Video',8,'App\\Models\\Video',8,'','finished','','2019-03-10 12:43:30','2019-03-10 12:43:30'),(41,'8d2c7b31-a997-4e7b-8fa9-4c0729551d4d',2,'Update','App\\Models\\Video',7,'App\\Models\\Video',7,'App\\Models\\Video',7,'','finished','','2019-03-10 12:44:09','2019-03-10 12:44:09'),(42,'8d2c7d21-fe2c-435d-8f80-0ac4c4375aba',2,'Update','App\\Models\\Video',6,'App\\Models\\Video',6,'App\\Models\\Video',6,'','finished','','2019-03-10 12:49:34','2019-03-10 12:49:34'),(43,'8d2c7d67-4f41-425a-b470-1fee571fd69e',2,'Update','App\\Models\\Video',4,'App\\Models\\Video',4,'App\\Models\\Video',4,'','finished','','2019-03-10 12:50:20','2019-03-10 12:50:20'),(44,'8d2dde31-6d08-46e3-b261-707190f5b1b8',2,'Update','App\\Models\\Video',5,'App\\Models\\Video',5,'App\\Models\\Video',5,'','finished','','2019-03-11 05:16:48','2019-03-11 05:16:48'),(45,'8d2ddf06-34c0-42a8-af2a-450ec22ef20b',2,'Update','App\\Models\\Video',9,'App\\Models\\Video',9,'App\\Models\\Video',9,'','finished','','2019-03-11 05:19:07','2019-03-11 05:19:07'),(46,'8d2ddf34-b9c0-4d93-9cea-7edb517ce748',2,'Update','App\\Models\\Video',5,'App\\Models\\Video',5,'App\\Models\\Video',5,'','finished','','2019-03-11 05:19:38','2019-03-11 05:19:38'),(47,'8d2ddf6f-a60b-496a-aad5-1e770543ff40',2,'Update','App\\Models\\Video',4,'App\\Models\\Video',4,'App\\Models\\Video',4,'','finished','','2019-03-11 05:20:16','2019-03-11 05:20:16'),(48,'8d2ddfd4-ee6f-40de-b605-9ccabadf8733',2,'Update','App\\Models\\Video',6,'App\\Models\\Video',6,'App\\Models\\Video',6,'','finished','','2019-03-11 05:21:23','2019-03-11 05:21:23'),(49,'8d2de027-e7c1-425e-bd89-514528de96fb',2,'Update','App\\Models\\Video',7,'App\\Models\\Video',7,'App\\Models\\Video',7,'','finished','','2019-03-11 05:22:17','2019-03-11 05:22:17'),(50,'8d2de0cb-3b79-41b9-98c3-ebc31bb7fc30',2,'Update','App\\Models\\Video',8,'App\\Models\\Video',8,'App\\Models\\Video',8,'','finished','','2019-03-11 05:24:04','2019-03-11 05:24:04');
/*!40000 ALTER TABLE `action_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_role`
--

DROP TABLE IF EXISTS `admin_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user_role`
--

LOCK TABLES `admin_user_role` WRITE;
/*!40000 ALTER TABLE `admin_user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'Ayman Bitar','ayman@digitalpandas.io','$2y$10$psMN9W9t89xs1nNaK0Z.RO1tmfqTfEGN660MGFJuh27R6MeIlVr46','ayman-bitar',NULL,'2019-02-28 09:05:44','2019-02-28 09:05:44'),(2,'Giles Wright','giles@weareaugustus.com','$2y$10$CcxS5W8m36DCzuV1VqZHUeKHfmUxQDQpZ9vq9g0urz.HjF4bkUjjO','giles-wright',NULL,'2019-02-28 09:05:44','2019-02-28 09:05:44');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'General','general',NULL,'2019-02-28 09:30:20','2019-02-28 09:30:20');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_us` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_us`
--

LOCK TABLES `contact_us` WRITE;
/*!40000 ALTER TABLE `contact_us` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Afghanistan','AF'),(2,'Åland Islands','AX'),(3,'Albania','AL'),(4,'Algeria','DZ'),(5,'American Samoa','AS'),(6,'Andorra','AD'),(7,'Angola','AO'),(8,'Anguilla','AI'),(9,'Antarctica','AQ'),(10,'Antigua and Barbuda','AG'),(11,'Argentina','AR'),(12,'Armenia','AM'),(13,'Aruba','AW'),(14,'Australia','AU'),(15,'Austria','AT'),(16,'Azerbaijan','AZ'),(17,'Bahamas','BS'),(18,'Bahrain','BH'),(19,'Bangladesh','BD'),(20,'Barbados','BB'),(21,'Belarus','BY'),(22,'Belgium','BE'),(23,'Belize','BZ'),(24,'Benin','BJ'),(25,'Bermuda','BM'),(26,'Bhutan','BT'),(27,'Bolivia, Plurinational State of','BO'),(28,'Bonaire, Sint Eustatius and Saba','BQ'),(29,'Bosnia and Herzegovina','BA'),(30,'Botswana','BW'),(31,'Bouvet Island','BV'),(32,'Brazil','BR'),(33,'British Indian Ocean Territory','IO'),(34,'Brunei Darussalam','BN'),(35,'Bulgaria','BG'),(36,'Burkina Faso','BF'),(37,'Burundi','BI'),(38,'Cambodia','KH'),(39,'Cameroon','CM'),(40,'Canada','CA'),(41,'Cape Verde','CV'),(42,'Cayman Islands','KY'),(43,'Central African Republic','CF'),(44,'Chad','TD'),(45,'Chile','CL'),(46,'China','CN'),(47,'Christmas Island','CX'),(48,'Cocos (Keeling) Islands','CC'),(49,'Colombia','CO'),(50,'Comoros','KM'),(51,'Congo','CG'),(52,'Congo, the Democratic Republic of the','CD'),(53,'Cook Islands','CK'),(54,'Costa Rica','CR'),(55,'Côte d\'Ivoire','CI'),(56,'Croatia','HR'),(57,'Cuba','CU'),(58,'Curaçao','CW'),(59,'Cyprus','CY'),(60,'Czech Republic','CZ'),(61,'Denmark','DK'),(62,'Djibouti','DJ'),(63,'Dominica','DM'),(64,'Dominican Republic','DO'),(65,'Ecuador','EC'),(66,'Egypt','EG'),(67,'El Salvador','SV'),(68,'Equatorial Guinea','GQ'),(69,'Eritrea','ER'),(70,'Estonia','EE'),(71,'Ethiopia','ET'),(72,'Falkland Islands (Malvinas)','FK'),(73,'Faroe Islands','FO'),(74,'Fiji','FJ'),(75,'Finland','FI'),(76,'France','FR'),(77,'French Guiana','GF'),(78,'French Polynesia','PF'),(79,'French Southern Territories','TF'),(80,'Gabon','GA'),(81,'Gambia','GM'),(82,'Georgia','GE'),(83,'Germany','DE'),(84,'Ghana','GH'),(85,'Gibraltar','GI'),(86,'Greece','GR'),(87,'Greenland','GL'),(88,'Grenada','GD'),(89,'Guadeloupe','GP'),(90,'Guam','GU'),(91,'Guatemala','GT'),(92,'Guernsey','GG'),(93,'Guinea','GN'),(94,'Guinea-Bissau','GW'),(95,'Guyana','GY'),(96,'Haiti','HT'),(97,'Heard Island and McDonald Mcdonald Islands','HM'),(98,'Holy See (Vatican City State)','VA'),(99,'Honduras','HN'),(100,'Hong Kong','HK'),(101,'Hungary','HU'),(102,'Iceland','IS'),(103,'India','IN'),(104,'Indonesia','ID'),(105,'Iran, Islamic Republic of','IR'),(106,'Iraq','IQ'),(107,'Ireland','IE'),(108,'Isle of Man','IM'),(109,'Italy','IT'),(110,'Jamaica','JM'),(111,'Japan','JP'),(112,'Jersey','JE'),(113,'Jordan','JO'),(114,'Kazakhstan','KZ'),(115,'Kenya','KE'),(116,'Kiribati','KI'),(117,'Korea, Democratic People\'s Republic of','KP'),(118,'Korea, Republic of','KR'),(119,'Kuwait','KW'),(120,'Kyrgyzstan','KG'),(121,'Lao People\'s Democratic Republic','LA'),(122,'Latvia','LV'),(123,'Lebanon','LB'),(124,'Lesotho','LS'),(125,'Liberia','LR'),(126,'Libya','LY'),(127,'Liechtenstein','LI'),(128,'Lithuania','LT'),(129,'Luxembourg','LU'),(130,'Macao','MO'),(131,'Macedonia, the Former Yugoslav Republic of','MK'),(132,'Madagascar','MG'),(133,'Malawi','MW'),(134,'Malaysia','MY'),(135,'Maldives','MV'),(136,'Mali','ML'),(137,'Malta','MT'),(138,'Marshall Islands','MH'),(139,'Martinique','MQ'),(140,'Mauritania','MR'),(141,'Mauritius','MU'),(142,'Mayotte','YT'),(143,'Mexico','MX'),(144,'Micronesia, Federated States of','FM'),(145,'Moldova, Republic of','MD'),(146,'Monaco','MC'),(147,'Mongolia','MN'),(148,'Montenegro','ME'),(149,'Montserrat','MS'),(150,'Morocco','MA'),(151,'Mozambique','MZ'),(152,'Myanmar','MM'),(153,'Namibia','NA'),(154,'Nauru','NR'),(155,'Nepal','NP'),(156,'Netherlands','NL'),(157,'New Caledonia','NC'),(158,'New Zealand','NZ'),(159,'Nicaragua','NI'),(160,'Niger','NE'),(161,'Nigeria','NG'),(162,'Niue','NU'),(163,'Norfolk Island','NF'),(164,'Northern Mariana Islands','MP'),(165,'Norway','NO'),(166,'Oman','OM'),(167,'Pakistan','PK'),(168,'Palau','PW'),(169,'Palestine, State of','PS'),(170,'Panama','PA'),(171,'Papua New Guinea','PG'),(172,'Paraguay','PY'),(173,'Peru','PE'),(174,'Philippines','PH'),(175,'Pitcairn','PN'),(176,'Poland','PL'),(177,'Portugal','PT'),(178,'Puerto Rico','PR'),(179,'Qatar','QA'),(180,'Réunion','RE'),(181,'Romania','RO'),(182,'Russian Federation','RU'),(183,'Rwanda','RW'),(184,'Saint Barthélemy','BL'),(185,'Saint Helena, Ascension and Tristan da Cunha','SH'),(186,'Saint Kitts and Nevis','KN'),(187,'Saint Lucia','LC'),(188,'Saint Martin (French part)','MF'),(189,'Saint Pierre and Miquelon','PM'),(190,'Saint Vincent and the Grenadines','VC'),(191,'Samoa','WS'),(192,'San Marino','SM'),(193,'Sao Tome and Principe','ST'),(194,'Saudi Arabia','SA'),(195,'Senegal','SN'),(196,'Serbia','RS'),(197,'Seychelles','SC'),(198,'Sierra Leone','SL'),(199,'Singapore','SG'),(200,'Sint Maarten (Dutch part)','SX'),(201,'Slovakia','SK'),(202,'Slovenia','SI'),(203,'Solomon Islands','SB'),(204,'Somalia','SO'),(205,'South Africa','ZA'),(206,'South Georgia and the South Sandwich Islands','GS'),(207,'South Sudan','SS'),(208,'Spain','ES'),(209,'Sri Lanka','LK'),(210,'Sudan','SD'),(211,'Suriname','SR'),(212,'Svalbard and Jan Mayen','SJ'),(213,'Swaziland','SZ'),(214,'Sweden','SE'),(215,'Switzerland','CH'),(216,'Syrian Arab Republic','SY'),(217,'Taiwan','TW'),(218,'Tajikistan','TJ'),(219,'Tanzania, United Republic of','TZ'),(220,'Thailand','TH'),(221,'Timor-Leste','TL'),(222,'Togo','TG'),(223,'Tokelau','TK'),(224,'Tonga','TO'),(225,'Trinidad and Tobago','TT'),(226,'Tunisia','TN'),(227,'Turkey','TR'),(228,'Turkmenistan','TM'),(229,'Turks and Caicos Islands','TC'),(230,'Tuvalu','TV'),(231,'Uganda','UG'),(232,'Ukraine','UA'),(233,'United Arab Emirates','AE'),(234,'United Kingdom','GB'),(235,'United States','US'),(236,'United States Minor Outlying Islands','UM'),(237,'Uruguay','UY'),(238,'Uzbekistan','UZ'),(239,'Vanuatu','VU'),(240,'Venezuela, Bolivarian Republic of','VE'),(241,'Viet Nam','VN'),(242,'Virgin Islands, British','VG'),(243,'Virgin Islands, U.S.','VI'),(244,'Wallis and Futuna','WF'),(245,'Western Sahara','EH'),(246,'Yemen','YE'),(247,'Zambia','ZM'),(248,'Zimbabwe','ZW');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `images_title_unique` (`title`),
  UNIQUE KEY `images_filename_unique` (`filename`),
  UNIQUE KEY `images_thumbnail_unique` (`thumbnail`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'fff.png','aRXzLCBd_1551346245qIQrshqUNECGAW4dFky5d0kcBCzdm032.png','aRXzLCBd_1551346245qIQrshqUNECGAW4dFky5d0kcBCzdm032_thumb.png','0.00MB','2019-02-28 09:30:45','2019-02-28 09:30:45');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` json DEFAULT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `classes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,2,'شروط الاستخدام','/terms',NULL,NULL,'_self',NULL,1,1,NULL,'2019-02-28 09:07:31','2019-03-02 12:48:11'),(3,2,'سياسة الخصوصية','/privacy',NULL,NULL,'_self',NULL,2,1,NULL,'2019-02-28 09:07:57','2019-03-02 09:50:15'),(6,2,'عن SMASHI TV','/about',NULL,NULL,'_self',NULL,3,1,NULL,'2019-03-02 09:48:29','2019-03-02 09:50:31'),(8,2,'تواصل معنا','/contact',NULL,NULL,'_self',NULL,7,1,NULL,'2019-03-03 06:35:50','2019-03-03 06:35:50');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`),
  UNIQUE KEY `menus_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'Header menu','header-menu','2019-02-28 09:06:01','2019-02-28 09:06:01'),(2,'Footer menu','footer-menu','2019-02-28 09:06:04','2019-02-28 09:06:04');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2017_01_10_075854_create_admin_tables',1),(2,'2017_05_28_115649_create_gates_table',1),(3,'2018_01_01_000000_create_action_events_table',1),(4,'2018_10_16_090557_create_settings_tables',1),(5,'2018_12_22_214452_create_initial_tables',1),(6,'2018_12_22_220659_create_videos_table',1),(7,'2018_12_25_112318_create_contact_us_table',1),(8,'2018_12_25_114435_create_pages_table',1),(9,'2019_01_27_141420_create_subscriptions_table',1),(10,'2019_02_27_102533_create_menus_table',1),(11,'2019_02_28_092707_add_slug_to_category',2),(20,'2019_03_03_125332_drop_subscriptions_table',3),(21,'2019_03_03_125457_update_subscriptions_table',3),(22,'2019_03_10_072310_create_users_billing_table',3),(23,'2019_03_10_113809_create_transaction_paytabs_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'عن SMASHI TV','<p style=\"text-align: right;\">هي أول قناة إعلامية اجتماعية تخاطب العالم العربي عن كل ما يتعلق بالتجارة والأعمال.&nbsp;</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\">تجارة وأعمال. ثقافة. تقنية.&nbsp;</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\">رؤيتنا:</p>\n<p style=\"text-align: right;\">&nbsp;شبكة بث رقمي مباشر لكل ما يتعلّق بالتجارة والإقتصاد من قلب العالم العربي، وتشمل الإصدارات والخدمات والتقنيات الأكثر ابتكاراً في تغيير حياتنا.</p>\n<p style=\"text-align: right;\">&nbsp;</p>','about','2019-02-28 09:05:44','2019-03-06 11:32:09'),(3,'شروط الاستخدام','<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">إن الغرض من هذا المنشور هو إعداد الشروط والأحكام الخاصة بالقناة الرقميةSMASHI&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">بنود الخدمة</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">هذا الاقرار لشروط الاستخدام (\"اتفاقية\") هي بينك كمستخدم وSMASHI TV Inc. (والتي يشار إليها بـ \" &ldquo;SMASHI TV أو \"نحن\").&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">تنظم هذه الاتفاقية استعمالك لخدمات (\" SMASHI TV Service\") التي تسمح للمستخدم بالوصول إلى محتوى معين حسب الطلب ومحتوى البث المباشر (\"SMASHI TV Content\") لفترة محدودة بدون رسوم، بالإضافة إلى إمكانية الدخول إلى محتوى القناة بشكل دائم مع خدمات الاشتراك الخاصة بنا. تتضمن خدمة SMASHI TV الموقع ومحتوى منصّة TV SMASHI وتطبيقات SMASHI TV.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">عند استخدام خدمة منصّة TV SMASHI، من خلال عرض محتوى منصّة TV SMASHI ،او من خلال إكمال عملية التسجيل لخدمة SMASHI TV أو إجراء عملية شراء أو الوصول إلى موقع SMASHI TV (\"الموقع الالكتروني\") أو تنزيل أحد تطبيقات الهاتف المحمول (\"SMASHI TV APPS \") فأنت تقر بأن (1) أنك قرأت وفهمت ووافقت على الالتزام بهذه البنود، (٢) أنك في السن القانوني الذي يؤهلك على لإقرار ببنود SMASHI TV و (3) لديك السلطة على الإقرار هذه الاتفاقية بشكل شخصي.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">يشير مصطلح \"أنت\" إلى هويتك التي تمثلك ككيان شخصي وقانوني، كما هو معمول به اذا تم تحديدك كمستخدم عند تسجيلك على موقع الويب.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">إذا لم توافق على الالتزام بالاتفاقية، فقد لا تستطيع الوصول أو استخدام هذا الموقع SMASHI.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;وإذا كنت مشتركًا في قناة SMASHI لمدة محددة (\"المدة التجريبية\") فستتجدد البنود تلقائيا&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">وستستمر الخدمات بالتجدد إلى إذا قررت أن تلغي جدمة التجديد التلقائي أو رفضت تجديد الخدمة الخاصة بك.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">الرجاء أن تعي ببنود هذه الاتفاقية جيدا أدناه، فهي تنص على فهم مطالبنا ومطالبكم بما في ذلك عدم تعريضك الى مطالب ارشدت مسبقا قبل تاريخ هذه الاتفاقية.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">في حال حصول خلافات بينك وبين خدمة SMASHI TV فيمكنك فض النزاع بشكل فردي وليس كمدّعي أو ممثل في دعوى جماعية. وأنت تتخلى عن حقك برفع قضية في محكمة القانون وحضور هيئة محلفين. أي خلاف بينك وبين خدمتنا سيتم فكّه حسب قوانين بلد إقامتك.&nbsp;&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">أي نزاع أو مطالبة متعلقة بطريقة استخدامك للموقع ستخضع ويتم تفسيرها بموجب احكام ولوائح وأنظمة القانون بما في ذلك ودون تحديد قوانين موقع الخدمة الذي يمكنك من الوصول إلى الخدمات.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">تسجيل الحساب والخدمات&nbsp;</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">عند زيارتك للموقع لأول مرة، سيتمكن لك الوصول إلى محتوى قناة SMASHI TV</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;مجاناً لفترة مدتها ٧ أيام، ومن بعد ٧ أيام سيتم خفض وتخصيص المحتويات التي يمكنك الوصول إليها.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;سيتعين عليك إنشاء حساب (\"Account\") للحصول على خدمة منصّة TV SMASHI لمدة ٢٤ ساعة في اليوم، مع إمكانية الوصول إلى جميع محتويات المنصّة بالإضافة إلى مقاطع الفيديو المخزّنة. والاشتراك (&ldquo;Subscription&rdquo;) برسوم لمدة يوم أو الاشتراك أسبوعيا أو شهريا.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">لإنشاء حساب ، يجب عليك استخدام عنوان بريد إلكتروني صالح. أنت تقر وتضمن أن جميع المعلومات التي تقدمها صحيحة.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">أنت وحدك مسؤول عن الحفاظ على أمان حسابك وعن الأنشطة التي تحدث من خلاله، يجب عليك إبلاغنا على الفور إذا كنت تشك في أي دخول غير مصرح به لحسابك او حدوث نشاطات غريبه لم تصدر منك في حسابك.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">لا يحق لك إنشاء حساب إذا كنت أصغر من 13 عامًا. وإذا أنشأت حساب، فإنك تقر بأن عمرك على لا يقل عن 13 عامًا، وإلى الاعمار التي لم تتجاوز السن القانوني، فإن ذلك يتطلب إذن من أحد الوالدين أو الوصي بإنشاء حساب واستخدام خدمات منصّة SMASHI TV.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">الخصوصية&nbsp;</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">توضح سياسات الخصوصية في قناة SMASHI TV كيفية تعاملنا مع بياناتك الشخصية وكيفية حماية خصوصيتك عندما تستخدم خدماتنا.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">رسوم اشتراك&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">رسوم الاشتراك لمنصّة SMASHI TV:</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">ستكون مسؤولاً عن دفع الرسوم التي تسري على اشتراكك المحدد في خدمة منصّة TV SMASHI (\"رسم الاشتراك\")، الذي تحدده اثناء إنشاء حسابك وتحديد نوع الاشتراك. يمكنك اختيار اشتراك شهري مقابل ١ درهم إماراتي لليوم، وللاشتراك الأسبوعي ٤ درهم اماراتي لليوم، وللاشتراك اليومي ٢ درهم اماراتي.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">باستثناء ما هو منصوص عليه في هذه البنود، فإن جميع رسوم الاشتراك في خدمة منصّة TV SMASHI غير قابلة للاسترداد. لن يكون هناك أي عقد بينك وبين SMASHI TV لخدمة SMASHI TV حتى تصلك الموافقة على طلبك من SMASHI TV عن طريق رسالة بريد إلكتروني تأكيدية أو رسالة SMS / MMS أو وسائل اتصال مناسبة أخرى.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">لدى منصّة TV SMASHI الحق لتغيير رسوم الاشتراك في أي وقت وتغيير طرق الدفع الخاصة بها، إما عبر نشر التحديث على موقع TV SMASHI أو إرسال المعلومات عبر البريد الإلكتروني الخاص بك.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">أنت مسؤول عن الحصول على خدمة الإنترنت وجهاز متوافق متصل بالإنترنت لتنزيل محتوى TV SMASHI. وتعتمد جودة تجربة المشاهدة على عدد من العوامل، بما في ذلك جهاز العرض الخاص بك وجودة اتصالك بالإنترنت.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">الدفع&nbsp;</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">يجب عليك اختيار طريقة دفع صالحة لإجراء عملية شراء. يجب أن تستخدم بطاقة ائتمان صالحة (فيزا أو ماستركارد أو أي مصدر آخر مقبول من قِبلنا) (\"مزود الدفع\") كشرط للتسجيل في خدمات منصّة TV SMASHI. يحكم اتفاق مزود الدفع الخاص بك استخدام حساب بطاقة ائتمان معينة، ويجب عليك الرجوع إلى تلك الاتفاقية وليس هذه الاتفاقية لتحديد حقوقك والتزاماتك. ومن خلال تزويد منصّة TV SMASHI برقم حساب بطاقة الائتمان والمعلومات المرتبطة بالدفع، فأنت توافق على أن SMASHI TV مرخص له إرسال فاتورة حسابك على الفور لجميع الرسوم والأجور المستحقة والدفع إلى SMASHI TV بموجب هذه الاتفاقية وأنه لا يلزم أي إشعار أو موافقة إضافية. أنت توافق على إبلاغ SMASHI TV على الفور بأي تغيير في عنوان إرسال الفواتير أو حساب بطاقة الائتمان المستخدم للدفع بموجب هذه الاتفاقية. تحتفظ منصّة TV SMASHI بالحق في أي وقت بتغيير أسعارها وطرق إعداد الفواتير، إما فور نشرها على الموقع أو عن طريق البريد الإلكتروني إليك.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">سيتم فرض رسوم على طريقة الدفع الخاصة بك في بداية كل فترة اشتراك. أما إذا عرضت عليك فترة تجريبية مجانية، فسيتم فرض رسوم على طريقة الدفع في نهاية الفترة التجريبية المجانية عند بداية مدة اشتراك أخرى ما لم تلغِ الاشتراك قبل نهاية تلك الفترة. لإيقاف أو إلغاء اشتراكك، قم بزيارة إعدادات حسابك. سيتم تطبيق التغييرات على فترات الاشتراك التالية بعد الفترة التجريبية.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">تستخدم منصّة TV SMASHI&nbsp; بيتابس \"PayTabs\" كمزود خدمات تابع لجهة خارجية لخدمات الدفع على سبيل المثال&nbsp; (قبول البطاقة والخدمات التابعة لذلك) (\"مزود دفع خارجي\") لدفع الاشتراك. من خلال إجراء أو قبول أي مدفوعات على موقع SMASHI TV ، فإنك توافق على الالتزام بسياسة خصوصية PayTabs في:&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\"><a href=\"https://www.paytabs.com/ar/privacy-policy/\">https://www.paytabs.com/ar/privacy-policy/</a> وبموجب ذلك انت توافق لـ PayTabs و لمنصّة SMASHI TV بمشاركة أي معلومات أو تعليمات دفع تقدمها إلى الحد الأدنى المطلوب لاستكمال معاملاتك. عن طريق إجراء أو استلام المدفوعات على موقع SMASHI TV، أنت توافق أيضًا على الالتزام باتفاقية خدمات PayTabs على: </span><a href=\"https://www.paytabs.com/ar/terms-of-use/\"><span style=\"font-size: 18.6667px;\">https://www.paytabs.com/ar/terms-of-use/</span></a></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">يحق لنا تطبيق الضرائب، بما في ذلك ضريبة القيمة المضافة (VAT) على أي رسوم. كما أن الأسعار وشروط الشراء الأخرى قابلة للتغيير. بالنسبة إلى دفعات اشتراكك عبر الجوّال، نستخدم خدمات الدفع الخاصة بشركة: (</span><span style=\"font-size: 18.6667px;\"><a href=\"https://tpay.com/en\">https://tpay.com/en</a>)</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">نحن لا نقوم بمعالجة أو تسجيل أو صيانة معلومات حساب موفر خدمة الجوال الخاص بك. لمزيد من المعلومات حول كيفية التعامل مع مدفوعات الجوال، أو لفهم أمن البيانات والخصوصية التي توفرها هذه المعلومات، يرجى الرجوع إلى: <a href=\"https://tpay.com/en/terms\"><span style=\"font-size: 18.6667px;\">https://tpay.com/en/terms</span></a>.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">جميع المشتريات، بما في ذلك رسوم الشحن، نهائية وغير قابلة للاسترداد.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">التجديد التلقائي: سيستمر اشتراكك لأجل غير مسمى حتى يتم إنهاؤه وفقاً لهذه البنود. بعد فترة الاشتراك الأولي، ومرة أخرى بعد أي فترة اشتراك لاحقة، سيبدأ اشتراكك تلقائيًا في اليوم الأول بعد نهاية الفترة المشتركة ويستمر لفترة إضافية تعادل فترة الاشتراك السابقة، بالسعر الساري حينئذٍ على مثل هذه الاشتراكات.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">أنت توافق على أن حسابك سيخضع لميزة التجديد التلقائي هذه ما لم تلغ اشتراكك عن طريق تسجيل الدخول والانتقال إلى صفحة \"الحساب\" على </span><span style=\"font-size: 18.6667px;\"><a href=\"https://SMASHI.TV/account\">https://SMASHI.TV/account</a> ، وتحديد زر \"إلغاء الاشتراك\" أو إلغاء المدفوعات من خلال معالج الدفع لطرف ثالث. إذا كنت لا ترغب في تجديد حسابك تلقائيًا، أو إذا كنت ترغب في تغيير اشتراكك أو إنهائه، يرجى تسجيل الدخول والانتقال إلى صفحة الحساب على </span><a href=\"https://SMASHI.TV/account\"><span style=\"font-size: 18.6667px;\">https://SMASHI.TV/account</span></a><span style=\"font-size: 18.6667px;\">&nbsp; وإلغاء المدفوعات من خلال الطرف الثالث معالج الدفع.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">إذا قمت بإلغاء اشتراكك، فيمكنك الاستفادة من اشتراكك حتى نهاية مدته الحالية. ولن يتم تجديد اشتراكك بعد انتهاء المدة. ولن تكون مؤهلاً لاسترداد أي جزء من رسوم الاشتراك المدفوعة في فترة الاشتراك الحالية.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">عن طريق الاشتراك، فإنك تسمح لمنصّة TV SMASHI لشحن مزود الدفع الخاص بك الآن، ومرة أخرى في بداية أي فترة اشتراك لاحقة.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">عند تجديد اشتراكك، إذا لم يتلقى SMASHI TV دفعة من مزود الدفع، (١) فإنك توافق على دفع جميع المبالغ المستحقة على حسابك عند الطلب، و / أو (2) توافق على أنه يجوز لـ SMASHI TV إما إنهاء أو تعليق اشتراكك ومواصلة محاولة فرض رسوم على مزود الدفع الخاص بك، إلى أن يتم استلام الدفعة (عند استلام الدفع، سيتم تفعيل حسابك ولأغراض التجديد التلقائي، ستبدأ فترة التزامك الجديد اعتبارًا من استلام الدفعة في اليوم).</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">خدمات الطرف الثالث</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">مواقع الويب والتطبيقات والإعلانات الخاصة بالطرف الثالث: قد يحتوي موقع SMASHI TV على روابط لمواقع طرف ثالث (\"مواقع الطرف الثالث\") وتطبيقات (\"تطبيقات الطرف الثالث\") وإعلانات لأطراف ثالثة (\"إعلانات الطرف الثالث \"). عندما تنقر على رابط إلى موقع تابع لجهة خارجية أو تطبيق تابع لجهة خارجية أو إعلان من طرف ثالث، قد لا نحذرك من رغبتك في ترك موقع ويب SMASHI TV وتخضع للبنود والشروط (بما في ذلك سياسات الخصوصية) الخاصة بـ موقع أو وجهة أخرى. لا تخضع مواقع ويب الطرف الثالث وتطبيقات الطرف الثالث وإعلانات الجهات الخارجية لسيطرة قناة SMASHI TV. SMASHI TV غير مسؤول عن أي مواقع طرف ثالث أو تطبيقات الطرف الثالث أو إعلانات الطرف الثالث. توفّر منصّة TV SMASHI هذه المواقع الإلكترونية للجهات الخارجية وتطبيقات الطرف الثالث وإعلانات الجهات الخارجية فقط كوسيلة راحة ولا تراجع أو توافق أو تراقب أو تصادق أو تضمن أو تقدم أي إقرارات فيما يتعلق بمواقع ويب الطرف الثالث أو الجهات الخارجية للتطبيقات أو إعلانات الجهات الخارجية أو منتجاتها أو خدماتها. أنت تستخدم جميع الروابط في مواقع الويب الخاصة بطرف ثالث وتطبيقات الطرف الثالث وإعلانات الجهات الخارجية على مسؤوليتك الخاصة. عندما تغادر موقع SMASHI TV، فشروطنا وسياساتنا لا تعد تسري. يجب عليك مراجعة البنود والسياسات المعمول بها، بما في ذلك ممارسات الخصوصية وتجميع البيانات، من أي مواقع ويب خاصة بطرف ثالث أو تطبيقات الطرف الثالث، ويجب إجراء أي تحقيق تشعر أنه ضروري أو مناسب قبل الشروع في أي معاملة مع أي طرف ثالث.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;متاجر التطبيقات: أنت تقر وتوافق على أن توفر SMASHI TV Apps والخدمات يعتمد على الطرف الثالث الذي حصلت منه على ترخيص SMASHI TV Apps، على سبيل المثال: (\"Apple App Store or Google Play (&ldquo;App Store كما أنك تقر بأن الاتفاقية هي بينك وبين SMASHI TV وليس مع App Store.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">إن SMASHI TV، وليس متجر App Store، هو المسؤول الوحيد عن موقع SMASHI TV وخدمات SMASHI TV، بما في ذلك تطبيقات SMASHI TV، ومحتوياتها، والصيانة، وخدمات الدعم، والضمان لذلك، ومعالجة أي مطالبات متعلقة بها (على سبيل المثال المنتج والمسؤولية القانونية أو الامتثال القانوني أو انتهاك الملكية الفكرية). لاستخدام SMASHI TV Apps، يجب أن يكون لديك إمكانية الوصول إلى شبكة لاسلكية، وأنت توافق على دفع جميع الرسوم المرتبطة بهذا الوصول. أنت توافق أيضًا على دفع جميع الرسوم (إن وجدت) التي يتقاضاها متجر التطبيقات في اتصال مع موقع SMASHI TV وخدمات SMASHI TV، بما في ذلك تطبيقات SMASHI TV. أنت توافق على الامتثال، والترخيص الخاص بك لاستخدام SMASHI TV Apps مشروط بالتوافق مع جميع بنود اتفاقية الطرف الثالث المعمول بها (مثل شروط وسياسات متجر التطبيقات) عند استخدام موقع SMASHI TV وخدمات SMASHI TV ، بما في ذلك تطبيقات SMASHI TV.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">التراخيص والملكية الفكرية&nbsp;</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">ترخيص SMASHI TV Apps: وفقًا لالتزامك بالبنود، يمنحك SMASHI TV ترخيصًا محدودًا وغير حصري وغير قابل للتحويل ويتيح لك تنزيل وتثبيت واستخدام نسخة من SMASHI TV Apps على هاتف واحد أو جهاز الكمبيوتر الذي تملكه أو تتحكم فيه وتستخدم من خلاله تطبيقات SMASHI TV Apps فقط لأغراضك الشخصية.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">وعلاوة على ذلك، فما يتعلق بأية تطبيقات لمنصّة TV&nbsp; SMASHI يتم الوصول إليها من خلال متجر Apple App Store أو تنزيلها من (\"App Store Sourced Applications \")، فلن تستخدم إلا App Store Sourced Application (1) على منتج يحمل علامة تجارية من Apple يقوم بتشغيل نظام iOS أو tvOS (نظام التشغيل الخاص بشركة Apple) و (2) كما هو مسموح به في \"بنود الاستخدام\" المنصوص عليها في شروط خدمة Apple App Store.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;أنت تدرك أن منصّة TV SMASHI قد تطلب منك قبول التحديثات على خدمات TV SMASHI التي قمت بتثبيتها على جهاز الكمبيوتر الخاص بك أو SMASHI TV Apps.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">ترخيص محتوى قناة :SMASHI وفقًا لشروط هذه الاتفاقية ، لديك الحق في إجراء بث و / أو تنزيل (إذا تم تقديمه) لأغراض الترفيه الشخصية الخاصة بك: (1) محتوى SMASHI TV الذي يمكنك الوصول إليه لفترة محدودة ، مجانًا؛ و (2) محتوى SMASHI TV الذي يمكنك الوصول إليه بعد قيامك بشراء اشتراك.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">لا يحق لك إعادة بيع المحتوى أو التنزيلات، أو استخدام أي محتوى تلفزيوني من نوع SMASHI لأي غرض تجاري، أو إعادة توزيع أو إعادة إرسال أي محتوى تلفزيوني من نوع SMASHI، أو إجراء أي محتوى تلفزيوني أو عرضه بشكل علني، أو إنشاء أعمال مشتقة من أي محتوى تلفزيوني من نوع SMASHI.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">خدمة SMASHI TV: تخضع لشروط هذه البنود، يمنحك SMASHI TV الإذن لاستخدام خدمة TV SMASHI، بغرض شراء وعرض محتوى SMASHI للترفيه الشخصي الخاص بك. جميع الحقوق غير الممنوحة بشكل صريح من تلفزيون SMASHI محفوظة.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">المحتوى الخاص بك: إلى الحد الذي ترسل فيه أي محتوى إلى قناة SMASHI، فإنك تمنح TV SMASHI حقًا دائمًا غير قابل للإلغاء، على مستوى العالم، وغير حصري، وخالي من حقوق الاستخدام والترخيص لاستخدامه ونسخه وتكييفه ونقله وتوزيعه وترخيصه وعرضه في جميع الوسائط لأي غرض كان.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">الملكية: بغض النظر عن أي شيء يخالف ذلك هنا، فإنك تقر وتوافق على أنك لن تملك أي ملكية أو حقوق تملك أخرى في حسابك، وأنك تقر وتوافق أيضًا على أن جميع الحقوق في حسابك مملوكة للأبد لصالح قناة SMASHI.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">سياسة الاستخدام المقبول</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">عند استخدام خدمة SMASHI TV ، لن يحق لك القيام بما يلي؛</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; سحب أو إعادة إنتاج أو إعادة توزيع أو بيع أو إنشاء أعمال مشتقة أو تفكيك أو اعادة هندسة خدمة قناة SMASHI أو تخزين شفرة المصدر الخاصة بالموقع</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; محاولة التحايل على أي اجراءات تقنية (بما في ذلك إدارة الحقوق الرقمية) التي قد نستخدمها</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; اتخاذ أي إجراءات للتدخل أو إلحاق الضرر بخدمة TV SMASHI</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; استخدم خدمة SMASHI TV لنقل المحتوى الضار وغير القانوني والمنتهك وغير المقبول</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; ارسال الرسائل المزعجة (سبام) او أي مضايقات لـ SMASH TV او مستخدمين آخرين</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; القيام بأي ممارسات احتيالية أو خادعة أو مضللة</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; جمع معلومات شخصية عن مستخدمي TV SMASHI الآخرين</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; المشاركة في أي أنشطة تجارية باستثناء ما نسمح به</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; الانخراط في أي أنشطة غير مشروعة</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">البنود&nbsp;</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">تسري هذه البنود عند استخدامك لأول مرة خدمة SMASHI TV. يجوز لك إنهاء هذه الاتفاقية في أي وقت عن طريق حذف حسابك (حساباتك). نحتفظ بالحق في إنهاء حسابك أو تقييد الوصول إلى حسابك لأي سبب، بما في ذلك خرق هذه البنود.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\"><strong>إخلاء مسؤولية</strong>&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">استخدامك لخدمة منصّة TV SMASHI ، بما في ذلك تطبيقات SMASHI TV ، وأي محتوى في قناة SMASHI يكون على مسؤوليتك الخاصة. يوفر منصّة TV SMASHI خدمة محتوى SMASHI على الأساس الذي يجعله \"كما هو\" و \"كما هو متاح\" ، دون أي ضمانات من أي نوع ، سواء صريحة أو ضمنية.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">تخلي SMASHI TV مسؤوليتها عن جميع الضمانات، سواء كانت صريحة أو ضمنية، بما في ذلك على سبيل المثال لا الحصر، الضمانات الضمنية للرواج ، والملائمة لغرض معين ، وعدم الانتهاك.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">بدون تقييد لما سبق، لا يقدم SMASHI TV أي تعهدات أو ضمانات بخصوص:</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; توفر خدمة SMASHI TV أو تطبيقات SMASHI TV&nbsp; لسلطة قضائية معينة.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; توفر خدمة SMASHI TV أو SMASHI TV Apps لأي جهاز أو نظام تشغيل أو متصفح محدد.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; الدعم المستمر لميزة خاصة من خدمة SMASHI TV أو تطبيقات SMASHI TV.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; جودة المشاهدة لأي محتوى في SMASHI.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; لا يضمن SMASH TV إمكانية استخدام خدمة TV SMASH أو تطبيقات SMASHI TV أو عرض أي محتوى تلفزيوني من نوع SMASHI دون انقطاع أو خالي من الأخطاء.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; أي محتوى من محتويات SMASHI TV ومدى ملائمة أي من محتويات SMASHI TV لأي شريحة من الجهور. جميع الآراء والبيانات التي يعبر عنها أو في محتوى التلفزيون SMASHI (أو المواد ذات الصلة) تعود للأشخاص المشاركين في إنتاج محتوى التلفزيون SMASHI، وليس TV SMASHI.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; استمرارية توفر أي من محتويات TV SMASHI.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">حدود المسؤولية</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">إلى أقصى حد يسمح به القانون ، لا يكون SMASHI TV ، ، والتابع لها ، والمديرون، والمسؤولون ، والموظفون ، والممثلون ، والمستشارون ، والوكلاء ، والموردون ، والشركاء مسؤولين عن : (1) أي اضرار مباشرة أو غير مباشرة أو عرضية أو خاصة أو تبعية أو تعويضات نموذجية، بما في ذلك الأضرار الناجمة عن فقدان الأرباح أو الشهرة أو الاستخدام أو البيانات أو غيرها من الخسائر غير الملموسة ؛ الأضرار المتعلقة بأي نزاع بينك وبين مستخدم آخر من قنوات SMASHI TV ؛ أو (3) أضرار تزيد قيمتها عن 500 درهم إماراتي أو المبالغ التي دفعتها إلى تلفزيون SMASHI على مدار 12 شهرًا قبل تقديم مطالبتك. تنطبق القيود السابقة على جميع المطالبات، سواء كانت مستندة على الضمان أو العقد أو الضرر أو أي نظرية قانونية أخرى، سواء تم إبلاغ منصّة SMASHI بإمكانية حدوث مثل هذا الضرر، وما إذا كان التصحيح المنصوص عليه في هذه الوثيقة قد فشل في تحقيق غرضه الأساسي.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">قد لا تسمح بعض السلطات القضائية بإقصاء بعض الضمانات أو استبعاد أو تحديد المسؤولية كما هو موضح أعلاه، لذلك قد لا تنطبق هذه القيود أعلاه عليك.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">التعويض</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">أنت ملزم بتعويضTV&nbsp; SMASHI وكل التابعين له من المديرين والمسؤولين والموظفين والممثلين والمستشارين والوكلاء والموردين والشركاء من أي مسؤولية أو مطالبة أو أضرار أو خسائر أو تكاليف (بما في ذلك أتعاب المحاماة ) الناشئة عن: (١) استخدامك غير المصرح به لأي نوع محتوى من قناة SMASHI ؛ (٢) استخدامك أو عدم القدرة على استخدام خدمة قناة SMASHI TV أو تطبيق SMASHI TV Apps أو موقع website SMASHI TV&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">(٣) خرق هذه البنود أو انتهاك أي قانون أو حق طرف ثالث؛ وعند أي محتوى تقوم بتحميله أو إرساله إلينا. التحكيم الملزم والقانون الحاكم</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">الأحكام الملزمة والقانون الساري&nbsp;</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">الاحكام الإلزامية مع قناةTV&nbsp; SMASHI: أنت توافق بجانب SMASHI TV على أن أي نزاع بينك وبين قناة SMASHI ناجم عن هذه البنود أو يتعلق بها أو باستعمالك لخدمة SMASHI TV يجب أن يتم حلها بواسطة BINDING ARBITRATION التي تديرها JAMS. إذا وافقت على التحكيم مع الشركة، فأنت توافق على أنك لن تشارك أو تسأل عن استرداد الأموال النقدية أو أي نوع آخر من الإغفال في أي دعوى قضائية مقدمة ضد الشركات التي تدعي أنها ترفع دعاوى جماعية، و / أو تعارض ضدك.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">عوضا عن ذلك، من خلال الموافقة على التحكيم، قد تكون لديك دعاوى ضد الشركة في إجراء التحكيم الفردي. إذا نجحت في هذه الدعاوى، فسيتم منحك مكافأة مالية أو اعفاء آخر من قِبل أحد المحكمين. أنت تقر بأنك قد أخطرت بأنك قد تتشاور مع محام في اتخاذ قرار لقبول هذه الاتفاقية، بما في ذلك اتفاق التحكيم هذا.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">تنازل عن مطالبات الدعاوى الجماعية: بالموافقة على التحكيم في مطالبتك ضد منصّة SMASHI ، أنت (١) تتخلى عن حقك في الذهاب إلى المحكمة وان يكون لديك دعاوى تحاكمها بواسطة قضاة أو هيئة محلفين (باستثناء المسائل التي قد يتم اتخاذها لمحكمة الدعاوي الصغيرة) و (٢) أنت تتخلى عن حقك في المشاركة بدعوى جماعية أو اجراء جماعي آخر.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">الحق في رفع الدعاوى الصغيرة: الاستثناء الوحيد لخصوصية التحكيم هو أنه لديك الحق في تقديم مطالبة فردية في محكمة مطالبات صغيرة ذات ولاية قضائية مختصة. ولكن سواء اخترت التحكيم أو محكمة المطالبات الصغيرة، لا يجوز لك تحت أي ظرف من الظروف أن تبدأ أو تحافظ على أي إجراء جماعي أو تحكيم صنف أو إجراء تمثيلي آخر أو إجراء آخر</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">قواعد التحكيم: ينظم قانون التحكيم الفيدرالي تفسير وتطبيق اتفاق التحكيم هذا. تخضع إجراءات التحكيم لقواعد التحكيم الشامل أو قواعد التحكيم المبسطة التي تعتمد عليها JAMS ، بناءً على مقدار الثمن المتنازع عليه وسياسة JAMS بشأن تحكيم المستهلكين عملاً ببنود ما قبل النزاع. لديك الحق في جلسة استماع عادلة أمام محكم محايد وإتاحة الفرصة للمشاركة في عملية اختيار المحكم. يكون للمحكِّم الحق في منح جميع التعويضات التي قد تمنحها المحكمة.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">إجراءات المحكمة: أي دعوى على تطبيق اتفاق التحكيم هذا، بما في ذلك أي إجراءات لتأكيد أو تعديل أو إلغاء قرار التحكيم، يمكن أن تبدأ في أي محكمة قضائية مختصة. في حالة كون اتفاق التحكيم هذا لأي سبب غير قابل للتنفيذ، فإن أي دعوى (باستثناء الدعاوى القضائية للمطالبات الصغيرة) يجب أن تبدأ فقط في المحاكم الفيدرالية أو محاكم الولاية الموجودة في مدينة دبي المالية العالمية (DIFC). أنت بموجب هذا توافق بشكل لا رجعة فيه على اختصاص تلك المحاكم لمثل هذه الاجراءات، وإلى الحد الذي يسمح به القانون، كل من قناة SMASHI وأنت تتنازلون عن أي حق لمحاكمتك من قبل JURY في مثل هذا الإجراء.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">حق 30 يومًا في إلغاء الاشتراك: يحق لك إلغاء الاشتراك في بنود اتفاقية التحكيم هذه عن طريق إرسال إشعار خطي من قرارك بالانسحاب إلى العنوان التالي: </span><span style=\"font-size: 18.6667px;\"><a href=\"mailto:hello@smashi.tv\">hello@SMASHI.TV</a> ، في غضون 30 يومًا من تاريخ أول ظهور للموضوع إلى اتفاق التحكيم هذا. يجب أن يتضمن إشعارك اسمك وعنوانك، واسم مستخدم TV SMASHI (إن وجد) ، وعنوان البريد الإلكتروني الذي استخدمته لإعداد حساب TV SMASHI (إذا كان لديك واحد) ، وبيان واضح لا لبس فيه أنك تريد إلغاء الاشتراك في اتفاقيه هذا التحكيم. في حالة إلغاء الاشتراك في اتفاقية التحكيم هذه، ستظل جميع الأجزاء الأخرى من هذه الاتفاقية تنطبق عليك. لا يؤثر إلغاء الاشتراك في اتفاقية التحكيم هذه على أي اتفاقات تحكيم أخرى قد تكون لديك حاليًا، أو قد تدخلها في المستقبل معنا.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">القانون الحاكم: يجوز لك استخدام الخدمة فقط وفقًا لبنود وشروط هذه الاتفاقية وجميع القوانين والقواعد واللوائح المعمول بها، بما في ذلك على سبيل المثال لا الحصر قوانين موقع الخدمة التي يمكنك من خلالها الدخول إلى الخدمة.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><strong><span style=\"font-size: 18.6667px;\">شروط عامة&nbsp;</span></strong></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">عدم التنازل: إن فشل قناة SMASHI في ممارسة أو تطبيق أي بند من هذه البنود لن يشكل تنازلاً عن أي الشرط.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">احكام قابلة للفصل: إذا تم اعتبار أي بند من هذه الاتفاقية غير صالح أو غير قابل للتنفيذ، فسيتم تفسير هذا المصطلح بطريقة تتفق مع القانون المعمول به ليعكس، قدر الإمكان، النوايا الأصلية للأطراف ، وستبقى البنود المتبقية سارية المفعول ونافذة تماما.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">التفسير: يتم توفير العناوين للتيسير ولا يجوز استخدامها لتفسير شروط هذه الاتفاقية.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">التنازل: هذه الاتفاقية ملزمة للطرفين وخلفائهم، ورثتهم، والمتنازل إليهم ممن يسمح لهم بذلك. هذه الاتفاقية غير قابلة للتحويل أو النقل من قبلك دون موافقتنا الخطية المسبقة.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">العلاقات: الأطراف هم مستقلون متعهدون إلى بعضهم البعض.&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">لا يقصد بهذه الاتفاقية أي وكالة أو شراكة أو مشروع مشترك أو علاقة بين الموظف أو صاحب العمل أو صاحب الامتياز.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">لا أطراف ثالثة: لا يجوز لأطراف ثالثة أن يكون لها أي حقوق بموجب هذه البنود، باستثناء أنه يجوز للأطراف المعفاة إنفاذ حقوق التعويض.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">الاضطرار: لن يكون SMASHI TV مسؤولاً عن أي فشل أو تأخير في تنفيذ التزاماته بموجب هذه الاتفاقية التي قد تضطر، بشكل مباشر أو غير مباشر، للخضوع لظروف خارجة عن السيطرة المعقولة، بما في ذلك، على سبيل المثال لا الحصر قضاء الله، والكوارث الطبيعية، والحروب واضطرابات مدنية أو عسكرية وتخريب والاضراب والأوبئة وأعمال الشغب وانقطاع التيار الكهربائي وفشل الكمبيوتر وفقدان أو عطل في المرافق أو النقل (الأجهزة أو البرامج) أو خدمة الاتصالات الهاتفية والحوادث والنزاعات العمالية وأعمال السلطة المدنية أو العسكرية وإجراءات حكومية أو عدم القدرة على الحصول على العمل أو المواد أو المعدات أو النقل.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">التوافق مع الولايات المتحدة للتصدير: لا يحق لك استخدام SMASHI TV إذا: (١) أنك شخصياً تخضع لعقوبات الحكومة الأمريكية أو (2) أنك موجود في بلد يخضع لعقوبات الحكومة الأمريكية بحيث أن استخدامك لخدمة TV SMASHI سوف تنتهك القانون الأمريكي. أنت تقر وتضمن أنك لا تخضع لهذه العقوبات.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;الوصول إلى التطبيق وتنزيله من iTunes: ينطبق ما يلي على أي تطبيق من تطبيقات App Store تم الوصول إليه من خلال Apple App Store أو تنزيله:</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; أنت تقر وتوافق على أن (١) الاتفاقية مبرمة بينك وبين SMASHI TV فقط ، وليس Apple ، و(٢) SMASHI TV&nbsp; وليس Apple وحدها المسؤولة عن تطبيق App Store Sourced ومحتواه. يجب أن يتوافق استخدامك لتطبيق App Store Sourced مع بنود خدمة App Store.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; تقر بأن Apple ليس عليها أي التزام على الإطلاق بتقديم أي خدمات صيانة ودعم فيما يتعلق بتطبيق App Store Sourced.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; في حالة حدوث أي إخفاق في تطبيق App Store Sourced للتوافق مع أي ضمان ساري، فيمكنك إخطار Apple، وستقوم Apple بإعادة سعر الشراء لتطبيق App Store Sourced إليك وإلى أقصى حد يسمح به القانون المعمول به، Apple لن تكون ملزمة بضمان آخر من أي نوع فيما يتعلق بتطبيق App Store Sourced. وفيما بين منصّة SMASHI و Apple ، فإن أي مطالبات أو خسائر أو التزامات أو أضرار أو تكاليف أو مصروفات أخرى عائدة إلى أي إخفاق في الالتزام بأي ضمان ستكون مسئولية شركة SMASHI TV وحدها.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; بين SMASHI و Apple ، تقر أنت بأن Apple لا تتحمل مسؤولية معالجة أي مطالبات لديك أو أي مطالبات من أي طرف ثالث تتعلق بتطبيق App Store Sourced واستخدام تطبيق App Store Sourced. بما في ذلك، على سبيل المثال لا الحصر: (1) مطالبات مسؤولية المنتج. (٢) أي مطالبة تفيد بأن تطبيق App Store Sourced Application لا يتوافق مع أي متطلبات قانونية أو تنظيمية سارية&nbsp; و (٣) المطالبات الناشئة بموجب حماية المستهلك أو التشريعات المماثلة.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; تقر أنت و SMASHI TV أنه في حالة وجود أي مطالبة من طرف ثالث بأن تطبيق App Store Sourced&nbsp; واستخدام تطبيق App Store Sourced Application ينتهك حقوق الملكية الفكرية الخاصة بطرف ثالث ، كما هو الحال بين قناة SMASHI و Apple ، فإن SMASHI TV ، وليس Apple ، ستكون وحدها المسؤولة عن التحقيق والدفاع والتسوية وإبراء ذمة أي انتهاك لحقوق الملكية الفكرية إلى الحد الذي تنص عليه الاتفاقية.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; تقر أنت و SMASHI TV وتوافقان على أن Apple وشركات Apple التابعة هي طرف ثالث مستفيد من الاتفاقية فيما يتعلق بترخيصك من تطبيق App Store Sourced، وأنه بمجرد قبولك لشروط وأحكام الاتفاقية، فإن لدى Apple الحق (وسوف يعتبر أنه قد قبل الحق) لتنفيذ الاتفاقية المتعلقة بترخيصك من تطبيق App Store Sourced ضدك كمستفيد من الطرف الثالث.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; بدون الحد من أي بنود أخرى في الاتفاقية، يجب عليك الالتزام بجميع بنود اتفاقية الطرف الثالث المعمول بها عند استخدام تطبيق App Store Sourced Application.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">ملاحظة: أنت توافق على استلام إشعارات عبر عنوان البريد الإلكتروني الذي استخدمته لإنشاء حساب أو عبر موقع SMASHI TV.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">الاتفاقية الكاملة: تتضمن هذه الاتفاقية الاتفاقيات والسياسات التالية بالرجوع إليها:</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&bull; سياسة الخصوصية</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">باستثناء ما هو مذكور أعلاه، فإن الروابط المؤدية إلى صفحات على موقع SMASHI TV أو أي مواد تابعة لطرف ثالث هي للتيسير فقط ولا تشكل جزءًا من هذه الاتفاقية.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">تحدد هذه الاتفاقية التفاهم الكامل بينك وبين منصّة TV&nbsp; SMASHI فيما يتعلق باستخدامك لخدمة SMASHI TV&nbsp; وتحل محل جميع الاتفاقيات السابقة المتعلقة بها. قد يتم تعديل هذه الاتفاقية من قبلنا فقط. سيتم نشر أي تغييرات على موقع SMASHI TV وسيتم تطبيقه بأثر رجعي.</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">اتصل بنا:</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;إذا كان لديك أي أسئلة، يرجى الاتصال بنا على : <a href=\"mailto:support@smashi.tv\">support@SMASHITV.com</a></span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">لجميع الأسئلة الأخرى المتعلقة بهذه البنود، يرجى الاتصال بنا على:</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">SMASHI TV FZ LLC</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">IN5 Media, Dubai Production City, 800-2465, Dubai, UAE</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><a href=\"mailto:support@smashi.tv\">support@SMASHI.TV</a></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\"><span style=\"font-size: 18.6667px;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"text-align: right;\">&nbsp;</p>','terms','2019-02-28 09:05:44','2019-03-06 11:29:49'),(4,'سياسة الخصوصية','<p style=\"text-align: right;\"><strong>سياسة خصوصية TV SMASHI</strong></p>\n<p style=\"text-align: right;\">نظرة عامة</p>\n<p style=\"text-align: right;\">هذه سياسة الخصوصية الخاصة بـ SMASHI TV FZ LLC.&nbsp;</p>\n<p style=\"text-align: right;\">توفر منصّة TV SMASHI خدمات SMASHI TV التي تسمح للمستهلكين من أمثالك بالوصول إلى محتوى منصّة TV SMASHI ومشاهدته. كما يمكن الوصول إلى محتوى منصّة TV SMASHI من خلال تطبيق SMASHI TV للهواتف المحمولة.</p>\n<p style=\"text-align: right;\">أي شروط لم يتم تعريفها هنا يمكن ايجادها في شروط الخدمة المتوفرة على <a href=\"https://smashi.tv/terms.\">https://smashi.tv/terms</a></p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\">البيانات التي نجمعها: \"البيانات الشخصية\" تعني البيانات التي تسمح لأي شخص بتحديد هويتك أو الاتصال بك، بما في ذلك -على سبيل المثال- اسمك وعنوانك ورقم هاتفك وعنوان بريدك الإلكتروني، بالإضافة إلى أي معلومات أخرى غير عامة عنك ترتبط أو تتعلق بأي من البيانات السابقة.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\">\"البيانات المجهولة\" تعني البيانات غير المرتبطة ببياناتك الشخصية؛ لا تسمح البيانات المجهولة، من تلقاء نفسها، بتحديد هوية الافراد. نقوم بجمع البيانات الشخصية والبيانات المجهولة، كما هو موضح أدناه.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>خصوصية حسابك&nbsp;</strong></p>\n<p style=\"text-align: right;\">أنت وحدك المسؤول عن (1) الحفاظ على أمان حسابك (2) وجميع الأنشطة التي تحدث في حسابك. يجب عليك إبلاغنا على الفور إذا كنت تشك في أي وصول غير مصرح به إلى أو استخدام حسابك. إذا نسيت معلومات تسجيل الدخول الخاصة بحسابك، فيمكنك أن تطلب إرسالها إلى عنوان البريد الإلكتروني المرتبط بحسابك.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>المعلومات التي نجمعها</strong></p>\n<p style=\"text-align: right;\">يجوز لـ SMASHI TV ، والبائعين الذين يعملون نيابة عن SMASHI TV ، بجمع بيانات شخصية منك وعنك على النحو التالي:</p>\n<p style=\"text-align: right;\">المعلومات المتعلقة بالحساب: لإنشاء حساب، يجب عليك تقديم معلومات معينة، بما في ذلك البيانات الشخصية مثل عنوان بريدك الإلكتروني وكلمة المرور. قد نسمح لك بتقديم معلومات إضافية بما في ذلك الصورة الرمزية. يمكنك تقديم معلومات أخرى عن نفسك فيما يتعلق بالاستطلاعات والمسابقات والعروض الخاصة واستفسارات دعم العملاء والاتصالات الأخرى معنا.</p>\n<p style=\"text-align: right;\">معلومات الاستخدام: نقوم بتجميع معلومات حول استخدامك لخدمة SMASHI TV، بما في ذلك الوصول إلى محتوى منصّة TV SMASHI ومشاهدته وتحميله.</p>\n<p style=\"text-align: right;\">المعلومات التي يتم جمعها تلقائيًا: نقوم بجمع معلومات حول استخدامك لمواقعنا على الويب وتطبيقات SMASHI TV. قد يشمل ذلك الأنشطة الخاصة بك وعنوان IP الخاص بك ونوع المستعرض وموفر خدمة الإنترنت (ISP) والصفحات المرجعية / المخرجة ونظام التشغيل أو نوع الجهاز وطوابع التاريخ / الوقت والبيانات الوصفية ذات الصلة. كيف نرد على إشارات عدم التتبع: لا نرد حاليًا على إشارات \"عدم التتبع\" أو غير ذلك من الآليات التي قد تمكِّن المستهلكين من تعطيل التتبع على موقعنا الإلكتروني. Flash LSOs. عندما ننشر مقاطع فيديو، قد تستخدم الجهات الخارجية كائنات مشتركة محلية، تُعرف باسم \"ملفات تعريف الارتباط\"، لتخزين تفضيلاتك للتحكم في مستوى الصوت أو لتخصيص بعض ميزات الفيديو.</p>\n<p style=\"text-align: right;\">الأجهزة النقالة والخدمات: إذا كنت تستخدم خدماتنا على جهازك المحمول، بما في ذلك من خلال تطبيقنا الرقمي TV SMASHI، فقد نجمع رقم تعريف الجهاز الخاص. قد نقوم أيضًا بجمع معلومات غير شخصية من جهازك المحمول إذا قمت بتنزيل تطبيق SMASHI TV الخاص بنا. تستخدم هذه المعلومات بشكل عام لمساعدتنا في تقديم المعلومات الأكثر صلة بك. تتضمن الأمثلة على المعلومات التي يمكن جمعها واستخدامها كيفية استخدامك لتطبيق SMASHI TV، ومعلومات حول نوع الجهاز الذي تستخدمه. بالإضافة إلى ذلك، في حالة تعطل تطبيق SMASHI TV على جهازك المحمول، سوف نتلقى معلومات حول نوع الطراز الخاص ببرنامج هاتفك ومعلومات حول ناقل الخدمة. مما يسمح لنا بتحديد وإصلاح الأخطاء وتحسين أداء تطبيقاتنا على التلفزيون SMASHI). يتم إرسال هذه المعلومات إلينا كمعلومات مجمعة ولا يمكن تتبعها لأي فرد ولا يمكن استخدامها لتحديد هوية الفرد.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>جمع بيانات الطرف الثالث</strong></p>\n<p style=\"text-align: right;\">أنظمة الطرف الثالث: عندما تستخدم تطبيقًا يتم تشغيله على نظام أساسي تابع لجهة خارجية (على سبيل المثال، iOS، اتصالات E-Vision، إلخ.)، يمكن لمزود هذا النظام الأساسي للجهات الخارجية جمع بيانات شخصية أو معلومات أخرى عنك وعن استخدامك من تطبيقنا لمنصّة TV SMASHI.</p>\n<p style=\"text-align: right;\">التحليل والإعلان: يجوز لنا استخدام خدمات تحليلات الجهات الخارجية لجمع بيانات حول استخدام خدماتنا. قد نسمح لشركات إعلانية تابعة لطرف ثالث (بما في ذلك وكالات الإعلان والشبكات و بائعو خدمات التوصيل) بجمع البيانات المتعلقة بالإعلانات التي قد يضعونها على مواقعنا الإلكترونية.</p>\n<p style=\"text-align: right;\">قد يستخدم مزودو التحليلات وشركات الإعلانات التابعة للجهات الخارجية ملفات تعريف الارتباط لتتبع أنشطتك. قد تكون بعض شركات الإعلان عضو في مبادرة شبكات الإعلانات&nbsp;&nbsp;</p>\n<p style=\"text-align: right;\">لعدم المتابعة بنشر إعلانات محدّدة الأهداف من الشركات الأعضاء.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\">كيف نستخدم معلوماتك</p>\n<p style=\"text-align: right;\">قد تستخدم منصّة TV SMASHI المعلومات التي نجمعها عنك من أجل:</p>\n<p style=\"text-align: right;\">&bull; تلبية طلباتك وتقديم محتوى المنصّة وغيرها من الخدمات.</p>\n<p style=\"text-align: right;\">&bull; خدمة دعم العملاء.</p>\n<p style=\"text-align: right;\">&bull; تحليل كيفية استخدام خدماتنا.&nbsp;</p>\n<p style=\"text-align: right;\">&bull; التواصل معك.&nbsp;</p>\n<p style=\"text-align: right;\">&bull; التحقق من أهليتك لإجراء عمليات الشراء والمشاركة في العروض.</p>\n<p style=\"text-align: right;\">&bull; جمع أي ديون، ومنع الاحتيال، وحماية سلامة أنظمتنا.</p>\n<p style=\"text-align: right;\">&bull; فرض الحقوق القانونية الخاصة بنا.&nbsp;</p>\n<p style=\"text-align: right;\">&bull; أداء أي عملية ورد وصفها في حال تجميع المعلومات، عدى ذلك ندير الأعمال الخاصة بنا.</p>\n<p style=\"text-align: right;\">الإفصاح عن معلوماتك إلى طرف ثالث:</p>\n<p style=\"text-align: right;\">قد تكشف منصّة TV SMASHI عن معلومات عنك كما يلي:</p>\n<p style=\"text-align: right;\">مقدمو الخدمة المعتمدون: قد نكشف معلوماتك لمزودي الخدمة المشاركين في تشغيل أعمالنا. وهذا يشمل مقدمي خدمات الدفع وموفري خدمة البريد الإلكتروني وشبكات توصيل المحتوى وموفري خدمات التخزين السحابية وشركات التحليلات والمتخصصين مثل المحاسبين والمحامين. ويطلب من مقدمي الخدمة عدم استخدام بياناتك الشخصية إلا عند توفير الخدمات التي تطلبها منصّة SMASHI.&nbsp;</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\">الحالات القانونية: الأوضاع القانونية: قد نكشف عن معلوماتك عندما نؤمن بحسن نية أن (1) الإفشاء مطلوب بموجب أمر تفتيش أو أمر استدعاء (سواء كان جنائيًا أو مدنيًا) أو طلب تحقيق مدني أو أمر محكمة أو طلب تعاون من جهة القانون أو وكالة حكومية أخرى؛ (2) الإفصاح مناسب وضروري للإبلاغ عن جريمة مشبوهة (بما في ذلك أي حالة يشتبه استغلال الأطفال فيها) أو لمنع وقوع أي ضرر جسدي لفرد أو ممتلكات؛ (3) الإفصاح مناسب وضروري لإنفاذ حقوقنا؛ و (4) في حالة كان الإفصاح مطلوب بموجب القانون. لا يعتبر أي شيء في سياسة الخصوصية هذه إنشاءً لأي حق لجهة خارجية، ونحن نحتفظ بالحق في الاعتراض على أي طلبات من جهات خارجية للحصول على معلومات في الظروف المناسبة.</p>\n<p style=\"text-align: right;\">الشركات التابعة والمعاملات التجارية: قد نكشف عن معلوماتك لشركاتنا الأم فيما يتعلق بتشغيل أعمالنا. في حال اشتراك SMASHI TV أو الشركات الأم في عملية انتقال الأعمال التي تنطوي على بيع أو دمج أو سحب قناة SMASHI كما ينطبق على الشركات الأم كذلك، فقد نكشف عن أي معلومات عنك إلى الجهات المملوكة. ستظل المعلومات المنقولة خاضعة لأحكام سياسة الخصوصية هذه، بصيغتها الحديثة.</p>\n<p style=\"text-align: right;\">للجمهور: يجوز لنا إنشاء سجلات بيانات مجهولة المصدر من البيانات الشخصية. قد نكشف علنًا عن بيانات مجهولة المصدر وإحصائيات مستخدم مجمّعة ومعلومات أخرى.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>رسائل البريد الإلكتروني</strong></p>\n<p style=\"text-align: right;\">عند إنشاء حساب، قد تتلقى بعض رسائل في بريدك الإلكتروني:</p>\n<p style=\"text-align: right;\">رسائل البريد الإلكتروني الخاصة بالمعاملات: عند تسجيلك لأول مرة مع SMASHI TV ، قد نرسل لك رسالة بريد إلكتروني ترحيبية تقدم معلومات حول خدمة SMASHI TV وحسابك. قد نرسل لك أيضًا من وقت لآخر رسائل إلكترونية أخرى تتعلق بحالة حسابك. لا يجوز لك الانسحاب من تلقي رسائل ومعاملات البريد الإلكتروني.</p>\n<p style=\"text-align: right;\">النشرات الإخبارية:</p>\n<p style=\"text-align: right;\">قد نرسل إليك رسائل إخبارية ورسائل إلكترونية أخرى حول منتجات أو خدمات أو عروض تلفزيون SMASHI الجديدة أو القادمة. إذا قررت أنك لم تعد ترغب في تلقي هذه الرسائل الإلكترونية، فيمكنك إلغاء الاشتراك وفقًا للتعليمات الواردة في كل رسالة إلكترونية أو في إعدادات حسابك.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>إجراءات أمنية&nbsp;</strong></p>\n<p style=\"text-align: right;\">نحن نستخدم إجراءات أمنية لحماية الخسارة وسوء الاستخدام والتغيير غير المصرح به للمعلومات وجعلها تحت سيطرتنا. ومع ذلك، يرجى العلم أننا لا نستطيع أن نضمن أن إجراءاتنا الأمنية ستمنع حدوث اضطرابات أو وصول غير مصرح به.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>خيارات الخصوصية الخاصة بك</strong></p>\n<p style=\"text-align: right;\">يمكنك اختيار عدم تزويدنا بمعلومات معينة. إذا أخفقت في تزويدنا بمعلومات معينة، فقد لا تتمكن من استخدام خدمات معينة يتم نقديمها من قبلنا.</p>\n<p style=\"text-align: right;\">يمكنك تغيير معلومات حسابك أو إغلاق حسابك في أي وقت عن طريق تسجيل الدخول إلى حسابك واختيار إعدادات الحساب. إذا أغلقت حسابك، لن تتمكن من مشاهدة محتوى منصّة TV SMASHI الذي كان متاحًا لك من خلال اشتراكك في خدمات منصّة TV SMASHI. يجوز لنا الحفاظ على معلومات حسابك (١) لفترة من الوقت في حالة الحذف غير المقصود أو في حالة تغيير رأيك،(2) وحيث نؤمن بحسن نية أن الحفظ مطلوب بموجب القانون أو ضروريًا لفرض حقوقنا.</p>\n<p style=\"text-align: right;\">يمكنك إيقاف جميع المعلومات عن طريق تطبيق SMASHI TV بإلغاء تثبيت تطبيق SMASHI TV. يجوز لك استخدام عمليات إلغاء التثبيت القياسية التي قد تكون متوفرة كجزء من جهازك المحمول أو عبر سوق تطبيقات الهاتف المحمول أو الشبكة.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>استعمال خدماتنا من خارج الشرق الاوسط</strong></p>\n<p style=\"text-align: right;\">تهدف سياسة الخصوصية هذه إلى تغطية مجموعة من المعلومات عن أو عبر خدماتنا من سكان الشرق الأوسط. إذا كنت تستخدم خدماتنا من خارج الشرق الأوسط، يرجى الانتباه إلى أن معلوماتك قد يتم نقلها وتخزينها ومعالجتها في الشرق الأوسط حيث توجد خوادمنا ويتم تشغيل قاعدة البيانات المركزية الخاصة بنا. قد لا تكون قوانين حماية البيانات والخصوصية في الشرق الأوسط شاملة مثل تلك الموجودة في بلدك. من خلال استخدام خدماتنا، فإنك تدرك أنه قد يتم نقل معلوماتك إلى منشآتنا والجهات الخارجية التي نشارك معها كما هو موضح في سياسة الخصوصية هذه، وأنت توافق على هذا النقل.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>خصوصية الأطفال&nbsp;</strong></p>\n<p style=\"text-align: right;\">المستهلكون: من أجل إنشاء حساب مع SMASHI TV ، يجب أن يكون عمر الفرد على الأقل 13 سنة. كما يجب أن يحصل الأطفال الذين تتراوح أعمارهم من 13 عامًا إلى 18 عامًا على موافقة ولي الأمر أو الوصي على استخدام خدمة SMASHI TV.</p>\n<p style=\"text-align: right;\">إذا كنت تعتقد أنه قد يكون لدينا أي بيانات شخصية من طفل أقل من 13 عامًا ، فالرجاء الاتصال بنا على <a href=\"mailto:support@smashi.tv\">support@SMASHI.TV</a></p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>خدمات الطرف الثالث</strong></p>\n<p style=\"text-align: right;\">قد نقدم روابط لمواقع وتطبيقات الطرف الثالث. يرجى الانتباه إلى أن شروط سياسة الخصوصية هذه لا تنطبق على هذه المواقع أو التطبيقات الخارجية، أو على أي مجموعة من بياناتك الشخصية بعد النقر على روابط لهذه المواقع الخارجية. ليس لدينا سيطرة على مثل هذه الخدمات، وبالتالي نحيلك إلى سياسات الخصوصية الخاصة بهم للبيانات الشخصية أو غيرها من المعلومات حول ممارسات الخصوصية الخاصة بكل منهم. الامر يعود إليك في فتح الروابط التي نضعها أو النقر على كل مواقع الطرف الثالث. وأن نضعها فهذا لا يعني موافقتنا على الطرف الثالث نفسه أو منتجاته أو محتواه أو مواقعه.&nbsp;</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>الدفع وبطاقات الائتمان&nbsp;</strong></p>\n<p style=\"text-align: right;\">للدفع عبر الإنترنت من اشتراكك، نستخدم خدمات الدفع الخاصة بـ PayTabs، Inc. (https://www.paytabs.com/ar/). نحن لا نقوم بمعالجة أو تسجيل أو صيانة معلومات بطاقة الائتمان أو الحساب البنكي الخاص بك. ولمزيد من المعلومات حول كيفية التعامل مع المدفوعات، أو لفهم أمن البيانات والخصوصية الممنوحة لهذه المعلومات، يرجى الرجوع إلى <a href=\"https://www.paytabs.com/ar/privacy-policy/\">https://www.paytabs.com/ar/privacy-policy/</a></p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\">أما بالنسبة إلى عمليات دفع اشتراكك عبر الجوّال، نستخدم خدمات الدفع الخاصة بشركة T-Pay، Inc. (https://tpay.com/en). نحن لا نقوم بمعالجة أو تسجيل أو صيانة معلومات حساب موفر خدمة الجوال الخاص بك. للمزيد من المعلومات حول كيفية التعامل مع مدفوعات الجوال، أو لفهم أمن البيانات والخصوصية التي توفرها هذه المعلومات، يرجى الرجوع إلى <a href=\"https://tpay.com/en/terms-of-use\">https://tpay.com/en/terms-of-use</a>.</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>التحديثات</strong></p>\n<p style=\"text-align: right;\">قد نقوم بتحديث سياسة الخصوصية هذه من وقت لآخر من خلال نشر سياسة الخصوصية المحدثة على موقع SMASHI TV. سنقوم بإخطارك بأي تغييرات تطرأ على سياسة الخصوصية الخاصة بنا عن طريق نشر سياسة الخصوصية الجديدة على الموقع www.SMASHI .TV./privacy وسنقوم بتغيير تاريخ \"آخر تحديث\" أعلاه. كما يمثل استمرار استخدامك لخدماتنا موافقتك على سياسة الخصوصية المحدثة في حال تم تحديثها مستقبلا.&nbsp;</p>\n<p style=\"text-align: right;\">&nbsp;</p>\n<p style=\"text-align: right;\"><strong>تواصل معنا&nbsp;</strong></p>\n<p style=\"text-align: right;\">إذا كان لديك أي أسئلة أو استفسارات حول سياسة الخصوصية هذه ، يرجى الاتصال بنا على:</p>\n<p style=\"text-align: right;\">SMASHI TV FZ LLC</p>\n<p style=\"text-align: right;\">IN5 Media, Dubai Production City, 800-2465, Dubai, UAE</p>\n<p style=\"text-align: right;\"><a href=\"mailto:support@SMASHI.TV\">support@SMASHI.TV</a></p>\n<p style=\"text-align: right;\">&nbsp;</p>','privacy','2019-02-28 09:05:44','2019-03-06 11:37:03');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_tag`
--

DROP TABLE IF EXISTS `post_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_tag`
--

LOCK TABLES `post_tag` WRITE;
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `scheduled_for` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_title_unique` (`title`),
  UNIQUE KEY `posts_slug_unique` (`slug`),
  KEY `posts_user_id_index` (`user_id`),
  KEY `posts_image_id_index` (`image_id`),
  KEY `posts_category_id_index` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,1,1,1,'SmashiTV Release','smashitv-release','The summary off smashitv release','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas feugiat consequat quam sit amet blandit. Proin id ligula et dui condimentum gravida. Duis ac erat vel odio congue scelerisque at quis odio. In mattis efficitur lectus eget commodo. Ut elementum, felis ac cursus dictum, tellus nisi posuere velit, ac posuere nisi quam sit amet nisi. Donec ullamcorper commodo hendrerit. Vestibulum at sapien eu sem vehicula hendrerit sit amet nec lectus. Proin urna dolor, venenatis egestas elementum sit amet, vulputate at ipsum.\n\nSuspendisse ut dui id orci semper placerat vel id nibh. Vivamus non volutpat elit, at tempor magna. Donec dictum nibh ut ligula tempor, eu condimentum felis placerat. Aliquam suscipit faucibus iaculis. Cras sit amet diam sed purus cursus vestibulum. Nunc faucibus libero ut nibh scelerisque euismod. Sed quis mauris felis. Vivamus turpis mi, tincidunt quis metus at, cursus molestie nisi. Mauris eu dapibus est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec maximus tellus augue, non gravida nibh maximus eget. Ut sit amet tortor cursus velit tristique feugiat. Proin scelerisque dolor nec nulla egestas, quis egestas est rutrum. Integer tristique sed arcu sit amet accumsan.',0,1,'2019-02-28 09:30:45',NULL,'2019-02-28 09:30:45','2019-02-28 09:30:45');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `role_id` int(10) unsigned NOT NULL,
  `permission_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`,`permission_slug`),
  CONSTRAINT `role_permission_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `admin_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pricing_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriptions`
--

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
INSERT INTO `subscriptions` VALUES (1,'Daily',5,29,64,'2019-03-10 12:05:34','2019-03-10 12:05:34'),(2,'Weekly',12,30,65,'2019-03-10 12:05:34','2019-03-10 12:05:34'),(3,'Monthly',25,31,66,'2019-03-10 12:05:34','2019-03-10 12:05:34');
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `tagged_count` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `subscription_id` int(10) unsigned NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Request, 2 = Response',
  `signature` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_user_id_foreign` (`user_id`),
  KEY `transactions_subscription_id_foreign` (`subscription_id`),
  CONSTRAINT `transactions_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `subscriptions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,1,2,1,NULL,NULL,NULL,'2019-03-10 12:05:38','2019-03-10 12:05:38'),(2,1,2,1,NULL,NULL,NULL,'2019-03-10 12:10:24','2019-03-10 12:10:24'),(3,1,1,1,NULL,NULL,NULL,'2019-03-10 13:31:34','2019-03-10 13:31:34'),(4,3,1,1,NULL,NULL,NULL,'2019-03-10 13:31:50','2019-03-10 13:31:50'),(5,1,2,1,NULL,NULL,NULL,'2019-03-10 13:57:30','2019-03-10 13:57:30'),(6,3,2,1,'9v4DQdY2eGwJvm1WcV78:63e641e5c2d1afabe371674cb94fb3a89ae4916ce88e807636b32cd32ba4e3ef',NULL,NULL,'2019-03-11 08:43:59','2019-03-11 08:43:59');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions_paytabs`
--

DROP TABLE IF EXISTS `transactions_paytabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions_paytabs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) unsigned NOT NULL,
  `paytab_transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_4_digits` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_4_digits` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trans_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pt_customer_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pt_customer_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pt_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_paytabs_transaction_id_foreign` (`transaction_id`),
  CONSTRAINT `transactions_paytabs_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions_paytabs`
--

LOCK TABLES `transactions_paytabs` WRITE;
/*!40000 ALTER TABLE `transactions_paytabs` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions_paytabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Blog Post','a.bitar@gmail.com',NULL,'$2y$10$l2Qcn1v5H/b3K/vLSlKfRefZOaN8pXmXmSPh14TFcgAnKbVVE2duu','bZ3zUDR0ga8gyVkekwbzDCrtysumaaEi6s02mWROIgmSWTGYSfxsuLkIIN9r',NULL,NULL,NULL,'2019-02-28 09:25:08','2019-02-28 09:25:08'),(2,'Ayman Bitar',NULL,NULL,NULL,'NT6LaAWtaZekVwEDxn8p4oHsMZXvDiHSGlcLh9Cur5DRji6GnVeEFXbZ0lLp','2','117197342521936757647',NULL,'2019-02-28 09:31:48','2019-02-28 09:31:48'),(3,'Giles Wright',NULL,NULL,NULL,'zUFs1AeJqLdG8Zmxn2kIJv4xaDywwAqPewiVbSxEBa6FqAcnfqduyTzclMIg','2','117849139564047057656',NULL,'2019-02-28 09:33:34','2019-02-28 09:33:34'),(4,'Richard FitzGerald',NULL,NULL,NULL,'n4NH7UTWBJfhmGBaYtAebuiBZKn2XaaGajNaTzQA15pnvT8JArv6kA3LG3Lo','1','10156962848826866',NULL,'2019-03-04 05:36:57','2019-03-04 05:36:57'),(5,'Chaitanya Sinha',NULL,NULL,NULL,'tBgbWRCFNbK7d40wpM3Zd55eYnlaouTR6qnhmkfTpUvOoHISGyBDfaiB1iSm','2','100273896409842769804',NULL,'2019-03-04 06:01:27','2019-03-04 06:01:27'),(6,'Prem Lobo','plobo@paytabs.com',NULL,'$2y$10$C9poaNJrTWzIAxJXg7Mdtuc0X9LuP0WFpwg5Zl3cP5LDIaEqOXQ4S',NULL,NULL,NULL,NULL,'2019-03-05 10:28:33','2019-03-05 10:28:33');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_billing`
--

DROP TABLE IF EXISTS `users_billing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_billing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_billing_user_id_foreign` (`user_id`),
  KEY `users_billing_country_id_foreign` (`country_id`),
  CONSTRAINT `users_billing_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_billing_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_billing`
--

LOCK TABLES `users_billing` WRITE;
/*!40000 ALTER TABLE `users_billing` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_billing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_subscriptions`
--

DROP TABLE IF EXISTS `users_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `subscription_id` int(10) unsigned NOT NULL,
  `transaction_id` int(10) unsigned NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_subscriptions_user_id_foreign` (`user_id`),
  KEY `users_subscriptions_subscription_id_foreign` (`subscription_id`),
  KEY `users_subscriptions_transaction_id_foreign` (`transaction_id`),
  CONSTRAINT `users_subscriptions_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `subscriptions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_subscriptions_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_subscriptions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_subscriptions`
--

LOCK TABLES `users_subscriptions` WRITE;
/*!40000 ALTER TABLE `users_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `raw_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (4,'رامز يوسف - Thayarah World','https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/Thayarah_Live_show/Default/HLS/Thayarah_Live_show.m3u8',NULL,'رامز يوسف يخبرنا عن تجربته في التوصّل إلى أفكار محتوى محلّية مناسبة للوصول إلى الشباب العربي خارج مصر، وعلى ما يبدو الآن، السعودية هي المكان الأمثل لمحتوى يتناسب مع الشباب العربي.',0,'ramz-ywsf-thayarah-world','2019-03-03 15:29:26','2019-03-11 05:20:16'),(5,'خالد شربتلي - Desert Technology','https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/Desert_Technology_Live_show/Default/HLS/Desert_Technology_Live_show.m3u8',NULL,'عبر التطور التكنولوجي في مجال الطاقة الشمسية، أصبحت ألواح الطاقة الشمسية أرخص وسيلة لتوليد الطاقة على المدى الطويل، وخصوصاً في بعض الدول الأفريقية حيث تستخدم ألواح الطاقة الشمسية لتخزين الطاقة في بطاريات، وأيضاً كبنك لتخزين النقود بأمان!',0,'khald-shrbtly-desert-technology','2019-03-03 15:55:24','2019-03-11 05:19:38'),(6,'ناجي غزيري -Nestle','https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/Nestle_Live_show/Default/HLS/Nestle_Live_show.m3u8',NULL,'يطلعنا ناجي غزيري من شركة نستله على أهمية خلق التواصل بين المحتوى وجمهوره، وأن تعريب المحتوى والاخلاص للغة والسرعة في مواكبة التطورات هو سرّ نجاح الشركة على مواقع التواصل الجتماعي.',0,'najy-ghzyry-nestle','2019-03-03 15:59:14','2019-03-11 05:21:23'),(7,'نادين مزهر - Sarwa','https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/Sarwa_Live_show/Default/HLS/Sarwa_Live_show.m3u8',NULL,'نادين من ثروة تصف لنا نظام عمل الشركة وأهمية فتح المجال للجميع للاستثمار بمبالغ ضئيلة لتأمين مستقبلهم والحصول على عائدات على استثماراتهم.',0,'nadyn-mzhr-sarwa','2019-03-03 16:00:01','2019-03-11 05:22:17'),(8,'وليد فزع - Wamda Capital','https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/Wamda_Capita_Live_show/Default/HLS/Wamda_Capita_Live_show.m3u8',NULL,'جهودنا تكمن في مساعدة شركات الشرق الأوسط على الوصول إلى مستوى إقليمي وعالمي. وهي أمنية كل شركة ناشئة.',0,'wlyd-fza-wamda-capital','2019-03-03 16:00:39','2019-03-11 05:24:04'),(9,'فيصل حمّود - Dubai Internet City','https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/Dubai_Internet_City_Live_show/Default/HLS/Dubai_Internet_City_Live_show.m3u8',NULL,'لدى القطاع التعليمي دور هام في تأهيل شباب العالم العربي لتقوية مجال ريادة الأعمال في الإقليم، وحين ننجح في تقوية هذا القطاع، سنتوقّف عن الاستعانة بمصادر خارجية وسنمكّن الشباب العربي من استخدام قدراتهم وخبراتهم.',0,'fysl-hm-wd-dubai-internet-city','2019-03-03 16:02:43','2019-03-11 05:19:07');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos_comments`
--

DROP TABLE IF EXISTS `videos_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `videos_comments_video_id_foreign` (`video_id`),
  CONSTRAINT `videos_comments_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos_comments`
--

LOCK TABLES `videos_comments` WRITE;
/*!40000 ALTER TABLE `videos_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos_likes`
--

DROP TABLE IF EXISTS `videos_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `enum` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '1 = Like, 2 = Dislike',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `videos_likes_video_id_foreign` (`video_id`),
  CONSTRAINT `videos_likes_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos_likes`
--

LOCK TABLES `videos_likes` WRITE;
/*!40000 ALTER TABLE `videos_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos_likes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-11 21:06:28
