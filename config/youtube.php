<?php

return [

    /**
     * Client ID.
     */
    'client_id' => '162364781706-uf0v2rj1dg18bnk4ltnk55cn009nejco.apps.googleusercontent.com',

    /**
     * Client Secret.
     */
    'client_secret' => 'VFc2ieXgd87SLr7DgfCxsjei',

    /**
     * Scopes.
     */
    'scopes' => [
        'https://www.googleapis.com/auth/youtube',
        'https://www.googleapis.com/auth/youtube.upload',
        'https://www.googleapis.com/auth/youtube.readonly'
    ],

    /**
     * Route URI's
     */
    'routes' => [

        /** 
         * Determine if the Routes should be disabled.
         * Note: We recommend this to be set to "false" immediately after authentication.
         */
        'enabled' => env('SETUP_YOUTUBE_ACCOUNT', false),

        /**
         * The prefix for the below URI's
         */
        'prefix' => 'youtube',

        /**
         * Redirect URI
         */
        'redirect_uri' => 'callback',

        /**
         * The autentication URI
         */
        'authentication_uri' => 'auth',

        /**
         * The redirect back URI
         */
        'redirect_back_uri' => '/',

    ]

];
