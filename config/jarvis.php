<?php

return [

    'project-title'         => 'Smashi TV - ',

    'site-title'            => 'Digital Pandas - Dashboard',

    'admin-site-title'      => 'Digital Pandas Dashboard',

    'cache-key'             => 'dp-dash-',

    'admin-path'            => 'dp-admin',

    'cache-time'            => 1439,

    'cache-prefix'          => 'dp-',

    'session-cookie-key'    => 'dp-',

    'admin-email'           => 'tech@digitalpandas.io',

//	'aws-endpoint'          => 'https://d3qmiuxavltxfz.cloudfront.net/',
    'aws-endpoint'          => 'https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/',
    'aws-stream-endpoint'   => 'https://s3.eu-central-1.amazonaws.com/smashistaging/',
    'aws-stream-bucket'     => 'smashistaging',
    'aws-playlist-bucket'   => 'smashi-playlist',
    'aws-playlist-endpoint' => 'https://s3.eu-central-1.amazonaws.com/smashi-playlist/'

];