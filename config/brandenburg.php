<?php

return [
    /**
     * User model class name.
     */
    'userModel' => env('USER_MODEL', 'App\Models\AdminUser'),

    /**
     * Configure Brandenburg to not register its migrations.
     */
    'ignoreMigrations' => false,
];
