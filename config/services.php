<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
//	    'client_id'     => env('FACEBOOK_ID'),
//	    'client_secret' => env('FACEBOOK_SECRET'),
//	    'redirect'      => env('FACEBOOK_URL'),
	    'client_id'     => '436831383921155',
	    'client_secret' => '534da2e7f6d9876107b738e5f6bb6784',
	    'redirect'      => '/auth/facebook/callback',
    ],

    'google' => [
	    'client_id'     => env("GOOGLE_client_id", ""),
	    'client_secret' => env("GOOGLE_client_secret", ""),
	    'redirect'      => env("GOOGLE_redirect", ""),
    ],

];
