<?php

return [

    /**
     * Nova User resource tool class.
     */
    'userResource' => 'App\Nova\Admin\AdminUser',

    /**
     * The group associated with the resource
     */
    'roleResourceGroup' => 'Access',
];
