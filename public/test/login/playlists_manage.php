<div class="card">
    <div class="row">
        <div class="col-sm-12">
               <div class="col-sm-9"> 
				<button data-toggle="modal" data-target="#mymodal" data-id="<?php echo base_url() . 'admin/view_modal/playlist_add';?>" id="menu" class="btn btn-sm btn-primary waves-effect waves-light"><span class="btn-label"><i class="fa fa-plus"></i></span><?php echo 'Add playlist'; ?></button>
				</div>
				
				<div class="col-sm-3"> 
				<a href="<?php echo base_url() . 'admin/manage_related_playlist';?>">
				<button class="btn btn-sm btn-primary waves-effect waves-light"> 
				<span class="btn-label"><i class="fa fa-plus"></i>
				
				
				
				<?php echo 'MANAGE RELATIONSHIPS'; ?>
				</span></button>
                </a>
				</div>
				
				<br>
                <br>
                <table id="datatable" class="table table-striped">
                    <thead>
                        <tr>
                            
							<th><?php echo 'Channel'; ?></th>	
						   <th><?php echo tr_wd('name'); ?></th>
                            <!-- <th><?php //echo tr_wd('slug'); ?></th>-->
                           
                            <th><?php echo 'Created'; ?></th>
							<th><?php echo tr_wd('status'); ?></th>
                            <th></th>
							<th><?php echo tr_wd('option'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $sl = 1; foreach ($playlists as $row_plist):?>
                        <tr id='row_<?php echo $row_plist['id'];?>'>
                            <td><?php echo $row_plist['description'];?></td>
                            <td><strong><?php echo $row_plist['name'];?></strong></td>
                          <!--  <td><?php //echo $row_plist['slug'];?></td> -->
                            
                            
							<td><?php echo  date("F d, Y", strtotime($row_plist['created_date']));?></td>
							
							<td>
                                <?php
                                    if($row_plist['status']=='1'){
                                        echo '<span class="label label-primary label-xs">Published</span>';
                                    }
                                    else{
                                        echo '<span class="label label-warning label-mini">Unublished</span>';
                                    }
                                ?>
                            </td>                                    
                            
							<td>
							<div class="btn-group">
                    <button type="button" class="btn btn-white btn-sm dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></button>
                    <ul class="dropdown-menu" role="menu">
                      
					  <li><a  href="<?php echo base_url() . 'admin/playlist_category/'.$row_plist['id']?>"><?php echo 'Categories'; ?></a></li>
					  
			<li><a  href="<?php echo base_url() . 'admin/videos/'.$row_plist['id']?>"><?php echo 'Manage Video'; ?></a>
					  </li>		  
					 
                    </ul>
                  </div>

							</td>
							
							<td>
                                <div class="btn-group m-b-20"> <a data-toggle="modal" data-target="#mymodal" data-id="<?php echo base_url() . 'admin/view_modal/playlists_edit/'. $row_plist['id'];?>" id="menu" title="<?php echo tr_wd('edit'); ?>" class="btn btn-icon"><i class="fa fa-pencil"></i></a> <a title="<?php echo tr_wd('delete'); ?>" class="btn btn-icon" onclick="delete_row(<?php echo " 'playlists' ".','.$row_plist['id'];?>)" class="delete"><i class="fa fa-remove"></i></a> </div>
                            </td>
                        
						</tr>
                        
						<?php endforeach;?>
                    </tbody>
                </table>
        </div>
        <!-- end col-12 -->
    </div>
</div>

    
<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/parsleyjs/dist/parsley.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('form').parsley();
    });
</script>