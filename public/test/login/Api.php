<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    var $session_user;
    function __construct() 
	{
        parent::__construct();
		$this->load->model('Api_model');
    }

   
	public function index()
    {
      
    }
	
   
	public function GetMovie()
	{
		
		$videolist=	$this->Api_model->get_video_list();
		//print_r($lideolist);
		$video_content=[];
		$number=0;
		foreach($videolist as $row_video )
		{
		    $number++;
			$videofile_list=$this->Api_model->get_video_file($row_video['videos_id']);
			
			
			//print_r($videofile_list);
			//exit;
			
			
				$video_file_content=[];
				
				if(!empty($videofile_list))
				{	
					foreach($videofile_list as $row_video_file)
					{
						$video_file_content[]= array
						(
							"video_file_id"=>$row_video_file['video_file_id'],
							"file_source" =>$row_video_file['file_source'],
							"video_url"=>$row_video_file['file_url']
						);
					}
				}
				
				
				$trailers=$this->Api_model->get_video_trailers_file($row_video['videos_id']);
				
				$video_trailers_file_content=[];
				if(!empty($trailers))
				{	
					foreach($trailers as $row_trailers)
					{
						$video_trailers_file_content[]= array
						(
							"trailer_file_id"=>$row_trailers['trailer_file_id'],
							"file_source" =>$row_trailers['file_source'],
							"video_url"=>$row_trailers['file_url']
							
						);
					}
				}
			
			$video_content[]= array(
					 "videos_id" => $row_video['videos_id'],
					 "title" => $row_video['title'],
					 "description"=> $row_video['description'],
					 "release"=>$row_video['release'],
					 "video_thumb_path"=> base_url().'uploads/video_thumb/'.$row_video['videos_id'].".jpg",
					 "poster_image_path"=>base_url().'uploads/poster_image/'.$row_video['videos_id'].".jpg",
					"video_file_details"=>$video_file_content,
					"video_trailers_details"=>$video_trailers_file_content
					);





					
    }

		$video_cat[]= array(
					 "category" =>'Featured Movies',
					"video_content"=>$video_content
					);	
		
		
		$JSON_ARR = array(
				'video_details'=>$video_cat
				);

				print json_encode($JSON_ARR);
	}
	
	
	public function video_search($search_string="")
	{
		$video_search=$this->Api_model->video_search($search_string);
		
	if(!empty($video_search))	
	{	
		$video_content=[];
		foreach($video_search as $row_video)
		{
			$videofile_list=$this->Api_model->get_video_file($row_video['videos_id']);
			$video_file_content=[];
			
			if(!empty($videofile_list))
			{	
				foreach($videofile_list as $row_video_file)
				{
					$video_file_content[]= array
					(
						"video_file_id"=>$row_video_file['video_file_id'],
						"file_source" =>$row_video_file['file_source'],
						"video_url"=>$row_video_file['file_url'],
						"duration"=> $row_video_file['duration']
					);
				}
			}
			
			
			$adtiming_list=$this->Api_model->get_ad_timing($row_video['videos_id']);
			$ad_time_content=[];
			if(!empty($adtiming_list))
			{	
				foreach($adtiming_list as $ad_row )
				{
					$ad_time_content[]= array
					(
						"id"=>$ad_row['id'],
						"add_time"=>$ad_row['add_time']
					);
				}
			}
			
			$video_content[]= array(
					 "videos_id" => $row_video['videos_id'],
					 "title" => $row_video['title'],
					 "description"=> $row_video['description'],
					 "release"=>$row_video['release'],
					 "video_thumb_path"=> base_url().'uploads/video_thumb/'.$row_video['videos_id'].".jpg",
					 "poster_image_path"=>base_url().'uploads/poster_image/'.$row_video['videos_id'].".jpg",
					"video_file_details"=>$video_file_content,
					"ad_timing"=>$ad_time_content
					);	
			
			
			
			
			
			
		}
		
		$JSON_ARR = array(
				'video_search_details'=>$video_content
				);

				print json_encode($JSON_ARR);
		
		
		
	}	
	else
	{
		$video_content[]= array(
					 "Error" => 'There is not record'
					);	
		$JSON_ARR = array(
				'video_search_details'=>$video_content
				);
				print json_encode($JSON_ARR);
	}		
		

	}
		
		
		
		
	public function opt_signin()	
	{		
			$gen_code=$this->input->post('generated_code');
			if(	isset($gen_code)  )
			{	
				$result_signin= $this->Api_model->signin_by_gen_code($gen_code);
				
				//print_r($result_signin);
				//exit;
				if(!empty($result_signin))
				{
					$JSON_ARR[]=array(
							'user_id'=>intval($result_signin[0]['user_id']),
							'name'=>$result_signin[0]['name'],
							'username'=>$result_signin[0]['username'],
							'email'=>$result_signin[0]['email'],
							'join_date'=>$result_signin[0]['join_date'],
							'is_login'=>"YES"		
							);		
				
							
					print json_encode($JSON_ARR);
				}
				else
				{
						$JSON_ARR[]=array(
							'user_id'=>"",
							'is_login'=>"NO"		
							);		
					print json_encode($JSON_ARR);	
				}

			}
			else
			{
					$JSON_ARR = array(
									'response'=>"Wrong data"
									);
					print json_encode($JSON_ARR);
			}

	}
	
	
	
	public function user_signin()	
	{		
			$username=	$this->input->post('username');
			$password=$this->input->post('password');
			if(	isset($username) &&  isset($password)  )
			{	
				$result_signin= $this->Api_model->user_signin();
				
				//print_r($result_signin);
				
				if(!empty($result_signin))
				{
					$JSON_ARR[]=array(
							'user_id'=>intval($result_signin[0]['user_id']),
							'name'=>$result_signin[0]['name'],
							'username'=>$result_signin[0]['username'],
							'email'=>$result_signin[0]['email'],
							'join_date'=>$result_signin[0]['join_date'],
							'is_login'=>"YES"		
							);		
				
							
					print json_encode($JSON_ARR);
				}
				else
				{
						$JSON_ARR[]=array(
							'user_id'=>"",
							'username'=>"",
							'is_login'=>"NO"		
							);		
					print json_encode($JSON_ARR);	
				}

			}
			else
			{
					$JSON_ARR = array(
									'response'=>"Wrong data"
									);
					print json_encode($JSON_ARR);
			}

	
	
	
	}
	
	
	
	
	
	
	
	

		public function post_favourites()
		{
			//$device_id=$this->input->post('device_id');
			$video_id=$this->input->post('video_id');
			$user_id=$this->input->post('user_id');
			if(	!empty($video_id) and !empty($user_id) )
			{
				$favourite_flag= $this->Api_model->check_favourite();
				if($favourite_flag>0)
				{
					$JSON_ARR = array(
					'response'=>"This record already exists"
					);
					print json_encode($JSON_ARR);			
					exit;
				}	
				else
				{
						$data=array(
							"videos_id"=>$video_id,
							"user_id"=>$user_id
						);	
						
						$sth=$this->Api_model->save_favourite($data);
						if($sth)
						{
							$JSON_ARR = array(
							'response'=>"Added to favourites!!"
							);
							print json_encode($JSON_ARR);
							exit;
						}
					
				}		
				
			}
			else
			{
				$JSON_ARR = array(
							'response'=>"Wrong data"
							);
				print json_encode($JSON_ARR);	
			}	
			
			
			
			
		}
	
	
	
	public function get_favourites()
	{
		
		
		$video_id=$this->input->post('video_id');
			$user_id=$this->input->post('user_id');
			if(	!empty($video_id) and !empty($user_id) )
			{
				
				$favourites_list=$this->Api_model->get_favourites();

			if(!empty($favourites_list))
			{	
				$video_content=[];
				foreach($favourites_list as $favourite)
				{
					
					
					$videofile_list=$this->Api_model->get_video_file($favourite['videos_id']);
					$video_file_content=[];
					if(!empty($videofile_list))
					{	
						foreach($videofile_list as $row_video_file)
						{
							$video_file_content[]= array
							(
								"video_file_id"=>$row_video_file['video_file_id'],
								"file_source" =>$row_video_file['file_source'],
								"video_url"=>$row_video_file['file_url'],
								"duration"=> $row_video_file['duration']
							);
						}
					}
				
					$video_content[]= array(
					 "videos_id" => $favourite['videos_id'],
					 "title" => $favourite['title'],
					 "description"=> $favourite['description'],
					 "release"=>$favourite['release'],
					 "video_thumb_path"=> base_url().'uploads/video_thumb/'.$favourite['videos_id'].".jpg",
					 "poster_image_path"=>base_url().'uploads/poster_image/'.$favourite['videos_id'].".jpg",
					"video_file_details"=>$video_file_content,
					
					);	
				
				}
				
				
				
				$JSON_ARR = array(
					'video_search_details'=>$video_content
					);

					print json_encode($JSON_ARR);
		
		
		
				}	
				else
				{
					$video_content[]= array(
								 "Error" => 'There is not record in this favourites'
								);	
					$JSON_ARR = array(
							'video_search_details'=>$video_content
							);
							print json_encode($JSON_ARR);
				}		
		
			
			
			
			}
			else
			{
				$JSON_ARR = array(
							'response'=>"Wrong data"
							);
				print json_encode($JSON_ARR);		
				
			}	
		
		
	}

	
	
	
	public function remove_favourites()
	{
		$video_id=$this->input->post('video_id');
		$user_id=$this->input->post('user_id');
		if(	!empty($video_id) and !empty($user_id) )
		{
			$favourite_flag= $this->Api_model->check_favourite();
				if($favourite_flag>0)
				{
				
					$sth = $this->db->delete('favourite_video_relation', array('videos_id' => $video_id,'user_id'=>$user_id)); 
					if($sth)
					{
						$JSON_ARR = array(
							'response'=>"Removed from favourites!!"
							);
						print json_encode($JSON_ARR);
					}	
				
				}	
				else
				{
					$JSON_ARR = array(
					'response'=>"This record does not exists in this favourites"
					);
					print json_encode($JSON_ARR);			
					exit;
				}	
			
		}
		else
		{
			$JSON_ARR = array(
							'response'=>"Wrong data"
							);
			print json_encode($JSON_ARR);
		
		}
		
		
		
	}
	
	
	public function advertisement()
	{
	   $result_adv= $this->Api_model->GetAdvertisement();
	   if(!empty($result_adv))
	   {
		$content=[];
		foreach($result_adv as $row)
		{
			$content[]= array(
			"adsURL"=>$row['ads_url'],
			);	
			
	   
		}
	   
			$JSON_ARR = array(
			'advertisement'=>$content
			);
			print json_encode($JSON_ARR);
	   
	   
	   }
	   else
	   {
	    	$JSON_ARR = array(
			'response'=>"advertisement string not found"
			);
			print json_encode($JSON_ARR);   
	       
	   }
	    
	}
	
	public function signup()	
	{	
			if(	isset($_REQUEST['email']) && isset($_REQUEST['username']) && isset($_REQUEST['password'])  )
			{
				$email=$_REQUEST['email'];
				if(!filter_var($email, FILTER_VALIDATE_EMAIL)) 
				{
				 $JSON_ARR[] = array(
				 'response'=>"Not valid email"
				 );

							print json_encode($JSON_ARR);
							die();
							
				}


				$sql = "SELECT *  FROM user where 	username ='".trim($_REQUEST['username'])."' or email='".trim($_REQUEST['email'])."'";
				$res = $this->db->query($sql);
				if ($res->num_rows() > 0) 
				{
					//$row = $res->result_array();
					//return $row;
					$JSON_ARR[] = array(
							'response'=>"Username already exist"
							);
					print json_encode($JSON_ARR);
					
				}
				else
				{
					$data=array(
					"email"=>$_REQUEST['email'],
					"password"=>md5($_REQUEST['password']),
					"username"=>trim($_REQUEST['username']),
					 "status"=>1,					
					 "role"=>'subscriber'
					);
			
						$signup_id= $this->Api_model->signup($data);
						if($signup_id){
							$JSON_ARR[] = array(
									'response'=>"Welcome! you have signed up successfully...",
									'user_id'=>$signup_id
									);

							print json_encode($JSON_ARR);
								}
				
				}

			}
			else
			{
					$JSON_ARR[] = array(
									'response'=>"Wrong data"
									);
							print json_encode($JSON_ARR);
				
			}
		
	}	
	
	public function check_subscription()	
	{
		$user_id=$this->input->post('user_id');
		$videos_id=$this->input->post('videos_id');	
		if(	isset($user_id) && isset($videos_id) )
		{	
			$result= $this->Api_model->validate_subscription();
			if(!empty($result))
			{	
				$JSON_ARR = array(
				'response'=>"This User has already subscribed"
				);
				print json_encode($JSON_ARR);
			
			}
			else
			{
			$JSON_ARR = array(
				'response'=>"Subscription not found"
				);
				print json_encode($JSON_ARR);
			}	
		
		}
		else
		{
			$JSON_ARR[] = array(
				'response'=>"Wrong data"
				);
			print json_encode($JSON_ARR);
			
		}		
				
	}
/////////////////////////////////////////////////////////////////////////////	 
   

/*
   public function index_old() {
        redirect(base_url('account/change_password'));
    }
    
	
	
	public function get_gallery()
	{
			$username=$this->input->post('username');
			//$username='ratnesh@yahoo.com';
			if(!empty($username))
			{	
			$user_details=$this->Api_model->get_userID($username);
			//echo "=====".$user_details->users_id;
			$user_id=$user_details->users_id;
			$user_details=$this->Api_model->get_gallery($user_id);
			foreach( $user_details as $row)
			{
			//print_r($row);
			
			if(!empty($row['image']))
			{	
					$content[]= array(
					 "id" => $row['id'],
					 "type" => 'Image',
					 "title" =>$row['image'],
					 "path"=>base_url().'uploads/gallery_image/'.$row['image'],
					 "duration"=>$row['video_duration']
				   );
			}		
			if(!empty($row['video']))
			{	
					$content[]= array(
					 "id" => $row['id'],
					 "type" => 'Video',
					 "title" => $row['video'],
					 "path"=> base_url().'uploads/gallery_video/'. $row['video'],
					 "duration"=>$row['video_duration']
					 
				   );
					
			}
			}
			
			$JSON_ARR = array(
				'media_details'=>$content

				);

				print json_encode($JSON_ARR);

			
			}
		
	}
	
	
	
	
	public function login()
	{
		$data['notif'] = $this->auth_model->Authentification();
		if(empty($data['notif']))
		{
			$content=array('error'=>'Success','login'=>'1','user_id'=>$this->session->userdata['logged_in']['users_id']);	
		}
		else
		{
			
			//$notif['message'] = 'Username or password incorrect !';
            //$notif['type'] = 'danger';
			
			$content=array('error'=>$data['notif']['message'],'login'=>'0');
		}	
		$JSON_ARR = array(
				'valid_login'=>$content
				);

				print json_encode($JSON_ARR);
	}
   
   public function get_section($user_id=0,$schedule="")
   {
	   if ( $user_id<=0) 
	   {
           $JSON_ARR = array(
				'response'=>"Please login first"
				);

		    print json_encode($JSON_ARR);
       }
	   else
	   {
		  $content=array();
		  $section_data= $this->Section_model->get_section_app($user_id,$schedule); 
		  
		  //echo $this->db->last_query();
		  
		  foreach($section_data as $row)
		  {
			  $input = array("Video", "Image");
			  $arr_gallery_id= explode(',',$row['gallery_id']);
			  for($ii=0;$ii<count($arr_gallery_id);$ii++)
			  {
				  $gallery_details=$this->Gallery_model->get_gallerydetails($arr_gallery_id[$ii]);
	
				

			
				
				if($gallery_details[0]['display_type']=='Video')
				{
					$content[]= array(
					 "id" => $gallery_details[0]['id'],
					 "title"=>$gallery_details[0]['title'],
					 "type" => 'Video',
					 "path"=> base_url().'uploads/gallery_video/'. $gallery_details[0]['video'],
					 "duration"=>$gallery_details[0]['video_duration']
					 
				   );
					
				}	
				elseif($gallery_details[0]['display_type']=='Image')
				{
					$content[]= array(
					 "id" => $gallery_details[0]['id'],
					 "title"=>$gallery_details[0]['title'],
					 "type" => 'Image',
					 "path"=>base_url().'uploads/gallery_image/'.$gallery_details[0]['image'],
					 "duration"=>$gallery_details[0]['video_duration']
				   );
				
			    }
			
			  
			  }
			  
			  	  $section_content[]= array(
					 "sectionid"=>$row['id'],
					 "schedule"=>$row['schedule'],
					 "horizontal" => $row['H_size'],
					 
					 "vertical" => $row['V_size'],
					 "actions" => $row['on_off'],
					 "gallery"=>$content
				   );
			  
			  
			  //$this->Gallery_model->get_gallerydetails();
			  
			  //print_r($row);
			  
		  }	
		  print json_encode($section_content); 
	   }	   
	   
	   
   }
   
   public function login_old()
	{
		$data['notif'] = $this->auth_model->Authentification();
		if(empty($data['notif']))
		{
			$content=array('login'=>'1');	
		}
		else
		{
			$content=array('login'=>'0');
		}	
		$JSON_ARR = array(
				'valid_login'=>$content
				);

				print json_encode($JSON_ARR);

		
		//print_r($data['notif']);
	
	}
   
   
    public function chk_update()
	{
		$update_data = $this->Api_model->get_data_update();
		$JSON_ARR = array(
				'is_change'=>$update_data[0]['reaccess_flag']
				);
		$sql = "update reaccess set reaccess_flag=0 ";
		$this->db->query($sql);
		print json_encode($JSON_ARR);		
	}
   

  */  
    
}
