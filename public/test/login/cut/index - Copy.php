<?php
require('../db.php');
session_start();
$query = "SELECT * FROM video_list where id='".$_GET['id']."'";
		$result = mysqli_query($con,$query) or die(mysql_error());
		$rows =  mysqli_fetch_array($result);
		$video_name=$rows['original_video'];
		$id=$rows['id'];

?>


<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Video capture</title>
	
	<link href="http://vjs.zencdn.net/4.12.2/video-js.css" rel="stylesheet">
	
	<script src="../lib/video-js/video.min.js"></script>
	
	<!--RangeSlider Pluging-->
	<script src="../src/rangeslider.js"></script>
	<link href="../build/rangeslider.min.css" rel="stylesheet">
	
    <!--Demo CSS-->
	<link href="../demo.css" rel="stylesheet">
	
	
	
    <script>
			
        //var defaultVideo = "../video/7j.mp4";
		
		var defaultVideo = '../upload/'+'<?php echo $video_name ?>';
        Date.prototype.Format = function (fmt) {
            var o = {
                "h+": this.getUTCHours(),                   //小时
                "m+": this.getUTCMinutes(),                 //分
                "s+": this.getUTCSeconds(),                 //秒
            };
            for (var k in o) {
                if (new RegExp("(" + k + ")").test(fmt)){
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
                }
            }
            return fmt;
        }

        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }

        var currentSegmentBeginTime = new Date(0);

        window.onload = function () {
            var buttonAddSegment = document.querySelectorAll('.addSegment')[0];
            var buttonDoCut = document.querySelectorAll('.doCut')[0];
            var video = document.querySelectorAll('video')[0];
            var width = 960;
            var height = 540;

            buttonDoCut.disabled = true;

            var file = getQueryString("file");
            video.src = file?file:defaultVideo;

            video.width = width;
            video.height = height;
            video.controls = true;
            video.autoplay = true;
            video.loop = true;
            video.muted = true;
			
            buttonAddSegment.onclick = function(){
                var time = video.currentTime;
                var currentVideoTime = new Date(time*1000);
                console.log(currentVideoTime.Format("hh:mm:ss"));

                if(currentSegmentBeginTime.getTime() == 0){
                    var segmentslist = document.getElementById('segmentsList');

                    var segment = "<li class=\"segmentItem\"><div class=\"timestamp\">-ss ";
                    segment += currentVideoTime.Format("hh:mm:ss");
                    segment += "<div></li>";

                    segmentsList.innerHTML += segment;

                    currentSegmentBeginTime = currentVideoTime;
                    buttonAddSegment.innerText = "End of tag";
                    buttonDoCut.disabled = true;
                }
                else{
                    if(currentVideoTime > currentSegmentBeginTime){
                        var segments = document.querySelectorAll('.segmentItem');
                        var liSegment = segments[segments.length - 1];

                        var timeSpan = new Date(currentVideoTime - currentSegmentBeginTime);
                        liSegment.innerText = liSegment.innerText.substr(0, liSegment.innerText.length - 1) + " -t ";
                        liSegment.innerText += timeSpan.Format(" hh:mm:ss");

                        currentSegmentBeginTime = new Date(0);
                        buttonAddSegment.innerText = "Add fragment";
                        buttonDoCut.disabled = false;
                    }
                    else{
                        alert("The end time must be greater than the start time!");
                    }
                }
            };

            buttonDoCut.onclick = function(){
                //cut.php?segments[]=-ss 00:00:10 -t 00:00:10&segments[]=-ss 00:00:50 -t 00:00:20

                var jumpUrl = "cut.php?video=";
                var file = getQueryString("file");
                jumpUrl += file?file:defaultVideo;
                jumpUrl += "&";
                var segments = document.querySelectorAll('.segmentItem');

                for (var i=0; i<segments.length; i++){
                    jumpUrl += "segments[]=";
                    jumpUrl += segments[i].innerText;
                    jumpUrl += "&id="+<?php echo $id; ?>;
                }
                window.location.href = jumpUrl;
            };
			
			
			video_play();
        };

    </script>

<script>
		
		//Example of options ={hidden:false,locked:true,panel:false}
		
	function video_play()
	{	
		var options ={hidden:false},
			mplayer=videojs("vid1");
			mplayer.rangeslider(options);
     
	}
	 
		function playBetween(){
			var start,end;
			start = document.getElementById('Start').value;
			end = document.getElementById('End').value;
			mplayer.playBetween(start,end);
		}
        function loopBetween() {
            var start = document.getElementById('Start').value;
            var end = document.getElementById('End').value;
            mplayer.loopBetween(start, end);
        }
		function getValues(){
			var values = mplayer.getValueSlider();
            console.log(values);
			document.getElementById('Start').value=videojs.round(values.start,2);
			document.getElementById('End').value=videojs.round(values.end,2);
		}
		
</script>	


</head>

<body>
<h1>Video capture</h1>
<video id="vid1" >Only supports H264 format MP4</video>

<div id="fieldSelection">
		<legend>Select a section of the video</legend>
		<div class="button" onClick="playBetween()">Play Between</div>
        <div class="button" onClick="loopBetween()">Loop Between</div>
		<div class="button" onClick="getValues()">Get Arrow Values</div><br/>
		Start (seconds):<input type="text" value="2" id="Start">
		End (seconds): <input type="text" value="5" id="End">
	</div>





<ul id="segmentsList"></ul>
<button class="addSegment">Add fragment</button>
<button class="doCut">Start cutting</button>


</body>
</html>
