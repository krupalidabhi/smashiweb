<?php
require('db.php');

session_start();
if(!isset($_SESSION["username"])){
header("Location: login.php");
exit(); }

?>
    <!-- CSS-->
    <!--summernote CSS -->
    <link href="assets/plugins/summernote/dist/summernote.css" rel="stylesheet" />
    <!-- CSS-->
    <!--summernote CSS -->

    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />

    <!-- main CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <!-- linear icon CSS -->
    <link rel="stylesheet" href="assets/css/addons/linearicons/style.css">
    <!--sweet alert2 CSS -->
    <link href="assets/plugins/swal2/sweetalert2.min.css" rel="stylesheet">
    <!--Jquery JS -->
    <script src="assets/js/jquery-2.1.4.min.js"></script>

<div class="card">
    <div class="row">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-md-3">
            <a href="upload.html" class="btn btn-sm btn-primary waves-effect waves-light"><span class="btn-label"><i class="fa fa-plus"></i></span><?php echo 'Add Video'; ?></a> <br>
          <br>
          </div>
          <div class="col-md-9">
			
			</div>
          </div>
        </div>
        
          <table class="table table-striped" id="datatable">
            <thead>
              <tr>
                <th>#</th>
               <!-- <th>###</th> -->
                <th width="30%" >Original Video</th>
                <th width="30%">Cut Video</th>
                <th width="30%">Edited Video</th>
              </tr>
            </thead>
            
			
			
			<tbody>
        <?php      
		
		$query = "SELECT * FROM video_list order by id ";
		$result = mysqli_query($con,$query) or die(mysql_error());
		$cc=1;
		while($rows =  mysqli_fetch_array($result))
		{	
		
		//print_r($rows);
		
		?>
				<tr>
                <td><?php echo $cc; ?></td>
				<td>
				<?php echo $rows['original_video']; ?>
				<br>
				<video height="100px" width="140px" src="upload/<?php echo $rows['original_video']?>" controls>
				  Your browser does not support the video tag.
				</video>
				<br>
				<a href="cut/demo.php?id=<?php echo $rows['id']?>"><strong>Cut Video </strong> </a>				
				</td>
                
				
				<td>
				<?php  
				if($rows['cut_video']!='')
				{
				?>
				<video height="100px" width="140px" src="upload/<?php echo $rows['cut_video']?>" controls>
				  Your browser does not support the video tag.
				</video>
				<?php
				}
				?>
				<td>
				
				<?php
				if($rows['edited_video']!="")
				{
				?>
				<video height="100px" width="140px" src="upload/<?php echo $rows['edited_video']?>" controls>
				  Your browser does not support the video tag.
				</video>
				<?php
				}
				?>
				
				</td>
				
              </tr>
           <?php
		}
		   
		   
		   ?> 
			
			</tbody>
          </table>
       
          <!-- <div class="text-center"><h2>No video found..</h2></div> -->
       

      </div>
      <!-- end col-12 --> 
    </div>
    <!-- end row --> 
</div>



