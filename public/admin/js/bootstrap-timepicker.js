//== Class definition

var BootstrapTimepicker = function () {

    //== Private functions
    var demos = function () {
        // minimum setup
        $('.m_timepicker').timepicker();

    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

jQuery(document).ready(function() {
    BootstrapTimepicker.init();
});