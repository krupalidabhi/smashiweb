//== Class definition

var BootstrapDatetimepicker = function () {
    
    //== Private functions
    var demos = function () {
        // minimal setup
        $('.m_datetimepicker').datetimepicker({
            todayHighlight: true,
            autoclose: true
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

jQuery(document).ready(function() {    
    BootstrapDatetimepicker.init();
});