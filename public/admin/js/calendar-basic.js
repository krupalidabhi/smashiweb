var CalendarBasic = function() {

    return {
        //main function to initiate the module
        init: function() {
            var m_calendar_element = $(".m_calendar");
            if(m_calendar_element.length > 0) {
                var data = m_calendar_element.data('events');

                m_calendar_element.fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listWeek'
                    },
                    eventLimit: true, // allow "more" link when too many events
                    navLinks: true,
                    events: data,

                    eventRender: function(event, element) {
                        if (element.hasClass('fc-day-grid-event')) {
                            element.data('content', event.description);
                            element.data('placement', 'top');
                            mApp.initPopover(element);
                        } else if (element.hasClass('fc-time-grid-event')) {
                            element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
                        } else if (element.find('.fc-list-item-title').lenght !== 0) {
                            element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
                        }
                    }
                });
            }
        }
    };
}();

jQuery(document).ready(function() {
    CalendarBasic.init();
});