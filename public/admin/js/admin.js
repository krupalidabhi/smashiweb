let Admin = function () {

    let delete_form = function () {
        $('.delete-btn').click(function (e) {
            e.preventDefault();

            let href = $(this).attr('href');
            let this_row = $(this).parent('tr');

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {

                    // result.dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                    $.ajax({
                        type: 'delete',
                        url: href,
                        success: function () {
                            swal(
                                'Deleted!',
                                '',
                                'success'
                            );

                            location.reload();
                        },
                        error: function () {
                            swal(
                                'Something Wrong Happened',
                                '',
                                'error'
                            )
                        }
                    });
                } else if (result.dismiss === 'cancel') {
                    swal(
                        'Cancelled',
                        '',
                        'error'
                    )
                }
            });
        });
    };

    let texteditor = function() {
        tinymce.init({
            selector: 'textarea',
            height: 500,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
            toolbar: 'undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat',

            content_css: [
                '//fonts.googleapis.com/css?family=Cairo:300,400',
                '//www.tinymce.com/css/codepen.min.css']
        });
    };

    return {
        //main function to initiate the module
        init: function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            delete_form();
            texteditor();
        }
    };

}();

jQuery(document).ready(function () {
    Admin.init();
});