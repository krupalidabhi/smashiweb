/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
__webpack_require__(2);
module.exports = __webpack_require__(3);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

var SmashiTV = function () {
    var is_rtl = false;
    var auto_played = false;
    var myPlayer;
    var subscription_id = void 0;

    var language_flag = function language_flag() {
        is_rtl = $("body").hasClass("rtl");
    };

    var mobile_toggle = function mobile_toggle() {
        $("#toggle-mobile-menu").click(function (e) {
            e.preventDefault();

            var mobile_menu = $("#mobile-menu");
            var menu_toggle = $(this);

            if (mobile_menu.hasClass("closed")) {
                // change toggle
                menu_toggle.find("i").removeClass("far fa-bars");
                menu_toggle.find("i").addClass("fal fa-times");

                // open it
                mobile_menu.slideDown("slow");
                mobile_menu.removeClass("closed");
                mobile_menu.addClass("open");
            } else {
                // change toggle
                menu_toggle.find("i").removeClass("fal fa-times");
                menu_toggle.find("i").addClass("far fa-bars");

                // open it
                mobile_menu.slideUp("slow");
                mobile_menu.removeClass("open");
                mobile_menu.addClass("closed");
            }
        });
    };

    var close_overlay = function close_overlay() {
        $(".close-overlay-btn").click(function (e) {
            e.preventDefault();

            var overlay = $("#" + $(this).attr("data-overlay"));

            if (overlay.hasClass("open")) {
                overlay.fadeOut("slow");
                overlay.removeClass("open");
                overlay.addClass("closed");
            }
        });
    };

    var open_overlay = function open_overlay() {
        $(".open-overlay-btn").click(function (e) {
            e.preventDefault();

            var overlay = $("#" + $(this).attr("data-overlay"));

            if (overlay.hasClass("closed")) {
                overlay.fadeIn("slow");
                overlay.removeClass("closed");
                overlay.addClass("open");
            }
        });
    };

    var datepicker = function datepicker() {
        $('.datepicker').datepicker();
    };

    var facebook_share = function facebook_share() {
        $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
            FB.init({
                appId: '',
                version: 'v2.7' // or v2.1, v2.2, v2.3, ...
            });
        });

        $(".facebook-share-btn").click(function (e) {
            e.preventDefault();
            var url = $(this).attr("data-href");
            FB.ui({
                method: 'share',
                href: url
            }, function (response) {});
        });
    };

    var linkedin_share = function linkedin_share() {
        $(".linkedin-share-btn").click(function (e) {
            e.preventDefault();

            var share_comment = $(this).attr("data-share");

            var share_data = {
                "comment": share_comment,
                "visibility": {
                    "code": "anyone"
                }
            };

            $.ajax({
                url: "https://api.linkedin.com/v1/people/~/shares?format=json",
                type: "POST",
                beforeSend: function beforeSend(request) {
                    request.setRequestHeader("Content-Type", "application/json");
                    request.setRequestHeader("x-li-format", "json");
                },
                data: share_data,
                success: function success(output) {
                    console.log("success");
                },
                error: function error(output) {
                    console.log("error");
                }
            });
        });
    };

    var animate_reveal = function animate_reveal() {
        var config = {
            viewFactor: 0.15,
            duration: 2000,
            distance: "0px",
            scale: 0.8,
            easing: 'ease',
            mobile: true
        };

        window.sr = ScrollReveal(config);

        sr.reveal('.animate-element');
    };

    var form_validation = function form_validation() {
        $("#edit-profile-form").validate({
            messages: {
                "name": "الرجاء تحديد الإسم الكامل"
            },
            submitHandler: function submitHandler(form) {
                form.submit();
            }
        });

        $("#forgot-password-form").validate({
            messages: {
                "email": "الرجاء تحديد البريد الإلكتروني"
            },
            submitHandler: function submitHandler(form) {
                form.submit();
            }
        });

        $("#reset-password-form-form").validate({
            rules: {
                "email": {
                    required: true,
                    email: true
                },
                "password_confirmation": {
                    required: true,
                    equalTo: "#reset-password"
                }
            },
            messages: {
                "password": "الرجاء تحديد كلمة السر",
                "password_confirmation": {
                    required: "الرجاء تحديد كلمة السر",
                    equalTo: "رجاء إدخال نفس القيمة"
                },
                "email": {
                    required: "الرجاء تحديد البريد الإلكتروني",
                    email: "الرجاء إدخال بريد الإلكتروني صحيح"
                }
            },
            submitHandler: function submitHandler(form) {
                form.submit();
            }
        });

        $("#change-password-form").validate({
            rules: {
                "current-password": "required",
                "password": "required",
                "confirm-password": {
                    required: true,
                    equalTo: "#password"
                }
            },
            messages: {
                "confirm-password": "الرجاء تحديد كلمة السر",
                "password": "الرجاء تحديد كلمة السر",
                "password_confirmation": {
                    required: "الرجاء تحديد كلمة السر",
                    equalTo: "رجاء إدخال نفس القيمة"
                }
            },
            submitHandler: function submitHandler(form) {
                var response_container = $(form).parents('.tab-pane').find('.response-container');
                var submit_btn = $(form).find("input[type=submit]");

                $.ajax({
                    method: $(form).attr("method"),
                    url: $(form).attr("action"),
                    data: $(form).serializeArray(),
                    beforeSend: function beforeSend(e) {
                        submit_btn.prop('disabled', true);
                    },
                    success: function success(return_data) {
                        submit_btn.prop('disabled', false);
                        response_container.replaceWith(create_success_message(return_data.data, false));
                        $(form).trigger("reset");
                    },
                    error: function error(xhr, textStatus, errorThrown) {
                        submit_btn.prop('disabled', false);
                        response_container.replaceWith(create_error_message(xhr.responseJSON.error.message, false));
                        return false;
                    }
                });
            }
        });

        $("#contact-form").validate({
            rules: {
                "email": {
                    required: true,
                    email: true
                }
            },
            messages: {
                "name": "الرجاء تحديد الإسم الكامل",
                "email": {
                    required: "الرجاء تحديد البريد الإلكتروني",
                    email: "الرجاء إدخال بريد الإلكتروني صحيح"
                },
                "message": "الرجاء تحديد الرسالة"
            },
            submitHandler: function submitHandler(form) {
                var submit_btn = $(form).find("input[type=submit]");

                $.ajax({
                    method: $(form).attr("method"),
                    url: $(form).attr("action"),
                    data: $(form).serializeArray(),
                    beforeSend: function beforeSend(e) {
                        if (is_rtl) {
                            submit_btn.val("يبعث...");
                        } else {
                            submit_btn.val("Sending...");
                        }

                        submit_btn.prop('disabled', true);
                    },
                    success: function success(return_data) {
                        var success_message = create_success_message(return_data);
                        $(form).replaceWith(success_message);
                    }
                });
            }
        });

        $("#login-form").validate({
            rules: {
                "email": {
                    required: true,
                    email: true
                }
            },
            messages: {
                "password": "الرجاء تحديد كلمة السر",
                "email": {
                    required: "الرجاء تحديد البريد الإلكتروني",
                    email: "الرجاء إدخال بريد الإلكتروني صحيح"
                }
            },
            submitHandler: function submitHandler(form) {
                var error_container = $(form).parents('.access-container').find('.error-container');
                var submit_btn = $(form).find("input[type=submit]");

                $.ajax({
                    method: $(form).attr("method"),
                    url: $(form).attr("action"),
                    data: $(form).serializeArray(),
                    beforeSend: function beforeSend(e) {
                        submit_btn.prop('disabled', true);
                    },
                    success: function success(return_data) {
                        window.location.reload();
                        // $(form).replaceWith(success_message);
                    },
                    error: function error(xhr, textStatus, errorThrown) {
                        submit_btn.prop('disabled', false);
                        error_container.replaceWith(create_error_message(xhr.responseJSON.errors));
                        return false;
                    }
                });
            }
        });

        // $("#billing-form").validate({
        //     rules: {
        //         'full_address': "required",
        //         'phone_number': "required",
        //         'citu': "required",
        //         'state': "required",
        //         'country': "required",
        //         'postal_code': "required"
        //     },
        //     submitHandler: function (form) {
        //         var error_container = $(form).parents('.billing-form').find('.error-container');
        //         var submit_btn = $(form).find("input[type=submit]");
        //
        //         $.ajax(
        //             {
        //                 method: $(form).attr("method"),
        //                 url: $(form).attr("action"),
        //                 data: $(form).serializeArray(),
        //                 beforeSend: function (e) {
        //                     // submit_btn.prop('disabled', true);
        //                 },
        //                 success: function (return_data) {
        //                     // window.location.reload();
        //                     get_paytabs_data();
        //
        //                     // $(form).replaceWith(success_message);
        //                 },
        //                 error: function (xhr, textStatus, errorThrown) {
        //                     submit_btn.prop('disabled', false);
        //                     error_container.replaceWith(create_error_message(xhr.responseJSON.errors));
        //                     return false;
        //                 }
        //             });
        //     }
        // });

        $("#register-form").validate({
            rules: {
                "email": {
                    required: true,
                    email: true
                },
                "password_confirmation": {
                    required: true,
                    equalTo: "#register-password"
                }
            },
            messages: {
                "name": "الرجاء تحديد الاسم الكامل",
                "password": "الرجاء تحديد كلمة السر",
                "password_confirmation": {
                    required: "الرجاء تحديد كلمة السر",
                    equalTo: "رجاء إدخال نفس القيمة"
                },
                "email": {
                    required: "الرجاء تحديد البريد الإلكتروني",
                    email: "الرجاء إدخال بريد الإلكتروني صحيح"
                }
            },
            submitHandler: function submitHandler(form) {
                var error_container = $(form).parents('.access-container').find('.error-container');
                var submit_btn = $(form).find("input[type=submit]");

                $.ajax({
                    method: $(form).attr("method"),
                    url: $(form).attr("action"),
                    data: $(form).serializeArray(),
                    beforeSend: function beforeSend(e) {
                        submit_btn.prop('disabled', true);
                    },
                    success: function success(return_data) {
                        window.location.reload();
                        // $(form).replaceWith(success_message);
                    },
                    error: function error(xhr, textStatus, errorThrown) {
                        submit_btn.prop('disabled', false);
                        error_container.replaceWith(create_error_message(xhr.responseJSON.errors));
                        return false;
                    }
                });
            }
        });

        $("#search-again-form").validate({
            messages: {
                "q": "الرجاء تحديد كلمة البحث"
            },
            submitHandler: function submitHandler(form) {
                form.submit();
            }
        });

        $("#search-form").validate({
            messages: {
                "q": "الرجاء تحديد كلمة البحث"
            },
            submitHandler: function submitHandler(form) {
                form.submit();
            }
        });

        $("#add-comment-form").validate({
            messages: {
                "comment": "الرجاء تحديد التعليق"
            },
            submitHandler: function submitHandler(form) {
                console.log("submitted");
            }
        });

        $("#change-password-form").validate({
            rules: {
                "confirm-password": {
                    equalTo: "#password"
                }
            },
            messages: {
                "current-password": "الرجاء تحديد الكلمة السرية الحالية",
                "password": "الرجاء تحديد الكلمة السرية الجديدة",
                "confirm-password": {
                    "required": "الرجاء إعادة تحديد الكلمة السرية الجديدة",
                    "equalTo": "الكلمة السرية غير مطابقة للكلمة الجديدة"
                }
            },
            submitHandler: function submitHandler(form) {
                form.submit();
            }
        });

        $("#newsletter-subscription-form").validate({
            rules: {
                "email-address": {
                    "required": true,
                    "email": true
                }
            },
            errorPlacement: function errorPlacement(error, element) {},
            submitHandler: function submitHandler(form) {

                var submit_btn = $(form).find("input[type=submit]");

                $.ajax({
                    method: $(form).attr("method"),
                    url: $(form).attr("action"),
                    data: $(form).serializeArray(),
                    beforeSend: function beforeSend(e) {
                        if (is_rtl) {
                            submit_btn.val("يبعث...");
                        } else {
                            submit_btn.val("Sending...");
                        }

                        submit_btn.prop('disabled', true);
                    },
                    success: function success(return_data) {
                        var success_message = "";
                        if (is_rtl) {
                            success_message = create_success_message("شكراً للاشتراك بصحيفتنا!");
                        } else {
                            success_message = create_success_message("Thank you for subscribing to our newsletter!");
                        }

                        $(form).replaceWith(success_message);
                    },
                    error: function error(xhr, textStatus, errorThrown) {
                        return false;
                    }
                });
            }
        });
    };

    var paytabs_payment = function paytabs_payment() {
        $("#cc-payment").click(function (e) {
            $.ajax({
                url: '/subscribe/' + subscription_id,
                type: 'POST',
                dataType: 'json',
                success: function success(response) {
                    Paytabs("#express_checkout").expresscheckout(response);
                    $(".PT_open_popup").click();
                }
            });
        });
    };

    var login_overlay_register_trigger = function login_overlay_register_trigger() {
        $("#login-overlay-register-trigger").click(function (e) {
            e.preventDefault();

            $("#login-overlay").modal("hide");
            $("#register-overlay").modal("show");
        });
    };

    var header_fixed = function header_fixed() {
        // on load
        var window_top = $(window).scrollTop();
        var bottom_header_top = $(".header-bottom").offset().top;
        var top_header = $(".header-top");

        if (window_top >= bottom_header_top) {
            top_header.addClass("active");
        } else {
            top_header.removeClass("active");
        }

        $(window).scroll(function () {
            window_top = $(window).scrollTop();

            if (window_top >= bottom_header_top) {
                top_header.addClass("active");
            } else {
                top_header.removeClass("active");
            }
        });

        $(window).resize(function () {
            if ($(window).outerWidth() <= 767) {
                if (top_header.hasClass("active")) {
                    top_header.removeClass("active");
                }
            }
        });
    };

    function create_success_message($message) {
        return '<div class="alert alert-success">' + $message + '</div>';
    }

    function create_error_message($message) {
        var $array = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

        var output = '<div class="alert alert-danger">';

        if ($array) {
            $.each($message, function () {
                output += "<div>" + this + "</div>";
            });
        } else {
            output += $message;
        }

        output += '</div>';
        return output;
    }

    var hide_alert = function hide_alert() {
        var alert = $(".alert-hide");

        if (alert.length > 0) {
            var time = alert.data('time');
            setTimeout(function () {
                alert.slideUp(500);
            }, time);
        }
    };

    var dacast_video = function dacast_video() {
        var my_video_el = $("#my-video");

        if (my_video_el.length > 0) {
            // var CONTENT_ID = "121364_c_489112";
            var CONTENT_ID = my_video_el.data('video');

            var myPlayer = dacast(CONTENT_ID, 'my-video', {
                width: 1280,
                height: 720
            });

            // myPlayer.mute(true);
            // myPlayer.play();
        }
    };

    var access_modals = function access_modals() {
        var access_login_modal = $('#access-login-modal');
        var access_register_modal = $('#access-register-modal');
        access_login_modal.on('show.bs.modal', function (e) {
            access_register_modal.modal("hide");
            $(this).modal("show");
        });

        access_register_modal.on('show.bs.modal', function (e) {
            access_login_modal.modal("hide");
            $(this).modal("show");
        });
    };

    var social_share_popup = function social_share_popup() {
        //jQuery
        $("body").on("click", ".js-social-share", function (e) {
            console.log('sharing');
            e.preventDefault();
            var social_type = $(this).data('social');
            console.log(social_type);
            if (social_type === 'facebook') {
                console.log('here');
                var link = $(this).data('link');
                FB.ui({
                    method: 'share',
                    href: link
                }, function (response) {});
            } else if (social_type === 'twitter') {
                var current_text = $(this).attr('href');
                $(this).attr('href', current_text);
                windowPopup($(this).attr("href"), 500, 300);
            } else {
                windowPopup($(this).attr("href"), 500, 300);
            }
        });
    };
    function windowPopup(url, width, height) {
        // Calculate the position of the popup so
        // it’s centered on the screen.
        var left = screen.width / 2 - width / 2,
            top = screen.height / 2 - height / 2;
        window.open(url, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left);
    }

    return {
        //main function to initiate the module
        init: function init() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            language_flag();
            // animate_reveal();
            mobile_toggle();
            open_overlay();
            close_overlay();
            datepicker();
            facebook_share();
            linkedin_share();
            form_validation();
            login_overlay_register_trigger();
            header_fixed();
            hide_alert();
            access_modals();
            social_share_popup();

            $(".selectpicker").selectpicker({
                size: 10
            });

            var video_js = $(".video-js");

            video_js.each(function (i) {
                var player = videojs($(this)[0]);
                player.pause();
            });

            $(".mute-item").click(function (e) {
                e.preventDefault();

                var overlay = $(this).parents('.overlay');

                //Video Js
                // my_video.muted(false);

                //Da Cast
                // myPlayer.play();
                // myPlayer.mute(false);


                $(this).addClass('zoom-animate');
                setTimeout(function () {
                    overlay.hide();
                }, 1000);
            });

            $("#change-plan-link").click(function (e) {
                $("#headingThree").find('.btn').click();
            });

            dacast_video();

            $(".renew-subscription").on("click", function () {
                if ($(this).hasClass('choosable')) {
                    $("#profile-modal").modal("hide");

                    subscription_id = $(this).data('sub-id');

                    $("#step-two").show();
                }
            });

            paytabs_payment();

            $("#phone-payment").click(function () {
                window.location.href = '/subscribe/' + subscription_id;
            });
        }
    };
}();

jQuery(document).ready(function () {
    SmashiTV.init();
});

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);