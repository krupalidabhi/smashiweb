function load_more_scroll(){
var page = 1;
	$(window).scroll(function() {
	    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
	        page++;
	        loadMoreData(page);
	    }
	});


	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
            { 
	            if(data.html == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();
                $("#video-data").append(data.html);
                var video_js = $(".video-js");

                video_js.each(function (i) {
                    var player = videojs($(this)[0]);
                    // player.pause();
                });
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
    }
}
jQuery(document).ready(function() {
	
	window.onscroll = function() {myFunction()};
	var header = document.getElementById("myHeader");
	var sticky = header.offsetTop;

	function myFunction() {
		if (window.pageYOffset > sticky) {
			header.classList.add("sticky");
		} else {
			header.classList.remove("sticky");
		}
	}

	jQuery('footer').before('<div class="ajax-load text-center" style="display:none"><p><img width="46px" src="/assets/img/archive-loader.gif"></p></div>');
   


  // bind resize event
  jQuery(window).resize(toggleNavbarMethod);

		function toggleNavbarMethod() {
			if (jQuery(window).width() > 768) {
				
				jQuery('.navbar .dropdown').on('mouseover', function(){
					jQuery('.dropdown-toggle', this).trigger('click'); 
				}).on('mouseout', function(){
					jQuery('.dropdown-toggle', this).trigger('click').blur();
				});
			}
			else {
				jQuery('.navbar .dropdown').off('mouseover').off('mouseout');
			}
		}
	// toggle navbar hover
		 
	toggleNavbarMethod();

	// FCM push notification

	function sendSubscriptionToServer(token){
		
	  	$.ajax(
	        {
	            url: '/api/postnotification?endpoint=' + token,
	            type: "post",
	        })
	        .done(function(data)
            { 
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	            console.log('server not responding...');
	    	}
	    	);
	}

	/*var pushButton = document.querySelector('.js-push-button');
	if( pushButton !== null ){*/
		// pushButton.addEventListener('click', function() {

	        var config = {
	            apiKey: "AIzaSyCvXsE1zMi2OOlLZIrRi8ACa-fN0aP2bQE",
	            authDomain: "localsmashi.firebaseapp.com",
	            databaseURL: "https://localsmashi.firebaseio.com",
	            projectId: "localsmashi",
	            storageBucket: "localsmashi.appspot.com",
	            messagingSenderId: "514969061533",
	            appId: "1:514969061533:web:9226dc8697567ecbb96c74"
	        };
	        firebase.initializeApp(config);

	        if (firebase.messaging.isSupported()){
				const messaging = firebase.messaging();
		        messaging
		            .requestPermission()
		            .then(function () {
		                return messaging.getToken()
		            })
		            .then(function(token) {
		                sendSubscriptionToServer(token);
		            })
		            .catch(function (err) {
		                // ErrElem.innerHTML =  ErrElem.innerHTML + "; " + err
		                console.log("Unable to get permission to notify.", err);
		            });

		        messaging.onMessage(function(payload) {
		            
		            // NotisElem.innerHTML = NotisElem.innerHTML + JSON.stringify(payload);
		            //kenng - foreground notifications
		            const {title, ...options} = payload.notification;
		            navigator.serviceWorker.ready.then(registration => {
		                registration.showNotification(title, options);
		            });
		        });
	        }
			
		// });
	// }
});

