# smashitv-website
1. Download files from git:-
	Commands:- 1. sudo apt install git
   	           	          2. sudo git clone https://fitzyrichard@bitbucket.org/digitalpandas/smashitv-website.git
			
2. Install apache/Nginx(I used the apache2)
	Commands:- 1. sudo apt install apache2

3. Install and use php 7.3.3
	Commands:- 1.	sudo apt install php7.3-bcmath php7.3-bz2 php7.3-curl php7.3-gd php7.3-intl php7.3-json php7.3-mbstring php7.3-readline php7.3-xml php7.3-zip
		   2.   sudo service apache2 restart

4. Install mysql 5.7
	Commands:- 1.  sudo apt-get install mysql-5.7
		   2.  sudo apt-get install mysql-server
   		   3.  mysql_secure_installation 

5. Make Changes in downloaded laravel fileand upload databse in PHPMyAdmin:-
	1. Change name of .env example filename to only .env and add your database 	credentials(database name, username, etc) over there.
	Note:- You need to add aws access key(everything is mentioned in live .env file) also for making connection with s3.

6. Move the downloaded files to your localhost path mine is /var/www/html. Run below commands in that specific path/area in terminal. 

7. npm install
	Command:- 1.  sudo apt install npm

8. Laravel composer
	Commands:-   1.  sudo composer install
		   	2.  composer dump-autoload 
	Note:- while installing composer one of the file needs nova.laravel.com credentials for 	downloading files
		username:- ayman@strategies-dc.com
		password:- AymanBitar55

9. Setup the laravel environment:-
	Commands:-  	1. php artisan key:generate
			 2. php artisan migrate
		  	 3. php artisan db:seed to run seeders, if any.
		  	 4. php artisan serve/php artisan serve --port=9000
