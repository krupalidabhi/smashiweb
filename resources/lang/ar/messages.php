<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Pagination Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'contact_success' => 'شكرا لتواصلك معنا! فريق سماشي سيتواصل معك قريباً.',
	'by_sign_up' => 'بالتسجيل فأنت توافق على موقعنا',
	'TOU' => 'شروط الخدمة',
	'PP' => 'سياسة الخصوصية',
	'and' => 'و',
	'new_video' => 'تم تحميل فيديو جديد',
	'subscribe_post_notification' => 'اشترك دفع الاخطار',
	'mobile_field' => 'رقم الهاتف المحمول',
	'mobile_field_country_code' => 'الرقم الدولي',
	'mail_field' => 'البريد الإلكتروني',
	'address_field' => 'عنوان',
	'city_field' => 'مدينة',
	'country_field' => 'بلد',
	'state_field' => 'المنطقة',
	'zip_field' => 'الرمز البريدي',
	'personal_info' => 'معلومات شخصية',
	'address_info' => 'عنوان',
	'coupon_info' => 'تطبيق القسيمة',
	'apply_info' => 'تطبيق',
];
