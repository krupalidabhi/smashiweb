<?php return [
    'latest' => 'آخر',
    'categories' => 'الفئات',
    'business'       => 'تجارة وأعمال ',
    'culture'    => 'ثقافة',
    'science' => 'علوم',
    'sports'=> 'رياضة',
    'tech'=> 'تكنولوجيا',
    'shows' => 'برامج',
    'archive' => 'أرشيف',
    'latest'   => 'أحدث الحلقات',
    'mostviewed' => 'الأكثر مشاھدة',
    'lifestyle' => 'لايف ستايل',
    'all_episodes' => 'جميع الحلقات'
];
?>
