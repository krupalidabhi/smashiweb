<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Pagination Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	// 'contact_success' => 'شكرا لتواصلك معنا! فريق خدمة العملاء راح يتواصل معك قريباً.',
	'by_sign_up' => 'By signing up you agree to our',
	'TOU' => 'Terms of Service',
	'PP' => 'Privacy Policy',
	'and' => 'and',
	'new_video' => 'New video uploaded',
	'subscribe_post_notification' => 'Subscribe push notification',
	'mobile_field' => 'Mobile Number:',
	'mobile_field_country_code' => 'Country Code:',
	'mail_field' => 'Email:',
	'address_field' => 'Address:',
	'city_field' => 'City:',
	'country_field' => 'Country:',
	'state_field' => 'State:',
	'zip_field' => 'Zip Code:',
	'personal_info' => 'Personal info',
	'address_info' => 'Address',
	'coupon_info' => 'Apply Coupon',
	'apply_info' => 'Apply',
];
