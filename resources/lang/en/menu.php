<?php return [
    'latest' => 'Latest',
    'categories' => 'Categories',
    'business' => 'Business',
    'culture' => 'Culture',
    'science' => 'Science',
    'sports'=> 'Sports',
    'tech'=>'Technology',
    'shows' => 'Shows',
    'archive' => 'Archive',
    'latest'   => 'Latest',
    'mostviewed' => 'Most Viewed',
    'lifestyle' => 'Lifestyle',
    'all_episodes' => 'All Episodes' ,
];
?>
