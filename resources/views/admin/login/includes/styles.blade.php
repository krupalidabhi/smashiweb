
<!--begin::Base Styles -->
<!--begin::Page Vendors -->
<link href="{{ '/vendors/fullcalendar/fullcalendar.bundle.css' }}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors -->
<link href="{{'/admin/css/vendors.bundle.css'}}" rel="stylesheet" type="text/css" />
<link href="{{'/admin/css/style.bundle.css'}}" rel="stylesheet" type="text/css" />
<link href="{{'/admin/css/admin.css'}}" rel="stylesheet" type="text/css" />
<!--end::Base Styles -->
{{--<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />--}}