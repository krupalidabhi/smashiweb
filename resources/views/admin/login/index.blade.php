@extends('admin.login.master')

@section('content')

    <div class="m-login__logo">
        <a href="#">
            <img src="{{'/admin/img/logo.png'}}" alt="SmashiTV Logo Admin" width="250">
        </a>
    </div>
    <div class="m-login__signin">
        <div class="m-login__head">
            <h3 class="m-login__title">
                Sign In To {{ config('jarvis.project-title') }} Admin
            </h3>
        </div>
        {!! Form::open(['url' => '/' . config('jarvis.admin-path') . '/login', 'method' => 'POST', 'class' => 'm-login__form m-form']) !!}
        @if(Session::has('invalid'))
            <div class="m-alert m-alert--outline alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                <span>Incorrect username or password. Please try again.</span>
            </div>
        @endif
        <div class="form-group m-form__group">
            {!! Form::text('username', null, ['class' => 'form-control m-input', 'placeholder' => 'Username', 'autocomplete' => 'off']) !!}
        </div>
        <div class="form-group m-form__group">
            {!! Form::password('password', ['class' => 'form-control m-input m-login__form-input--last', 'placeholder' => 'Password']) !!}
        </div>
        <div class="row m-login__form-sub">
            <div class="col m--align-left m-login__form-left">
                <label class="m-checkbox  m-checkbox--focus">
                    {!! Form::checkbox('remember') !!}
                    Remember me
                    <span></span>
                </label>
            </div>
        </div>
        <div class="m-login__form-action">
            <button id="m_login" class="btn btn-smashi m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                Sign In
            </button>
        </div>
        {!! Form::close() !!}
    </div>

@endsection