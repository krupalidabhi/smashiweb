<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark" data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item" aria-haspopup="true" >
                <a  href="{{ '/' . config('jarvis.admin-path') . '/' }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Dashboard
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    Components
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <li class="m-menu__item @if(isset($sub_menu) && $sub_menu == 1) m-menu__item--active @endif  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon fa fa-file-video"></i>
                    <span class="m-menu__link-text">
                        Videos
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a  href="{{ '/' . config('jarvis.admin-path') . '/video' }}" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                    View All Videos
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a  href="{{ '/' . config('jarvis.admin-path') . '/video/create' }}" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                    Add New Video
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item @if(isset($sub_menu) && $sub_menu == 2) m-menu__item--active @endif  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon fa fa-file"></i>
                    <span class="m-menu__link-text">
                        Pages
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a  href="{{ '/' . config('jarvis.admin-path') . '/page' }}" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                    View All Pages
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a  href="{{ '/' . config('jarvis.admin-path') . '/page/create' }}" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                    Add New Page
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @if($__user->role->slug == 'super-administrator')
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Settings
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-user-secret"></i>
                        <span class="m-menu__link-text">
                        Admin Role
                    </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{ '/' . config('jarvis.admin-path') . '/admin_role' }}" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                    View All Admin Roles
                                </span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{ '/' . config('jarvis.admin-path') . '/admin_role/create' }}" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                    Add New Admin Role
                                </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-user-secret"></i>
                        <span class="m-menu__link-text">
                        Admin User
                    </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{ '/' . config('jarvis.admin-path') . '/admin_user' }}" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                    View All Admin Users
                                </span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{ '/' . config('jarvis.admin-path') . '/admin_user/create' }}" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                    Add New Admin User
                                </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item" aria-haspopup="true" >
                    <a  href="{{ '/' . config('jarvis.admin-path') . '/settings' }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-cogwheel-2"></i>
                        <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Settings
                            </span>
                        </span>
                    </span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->