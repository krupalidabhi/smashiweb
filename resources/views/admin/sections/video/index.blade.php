@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Video
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin: Search Form -->
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span>
                                                    <i class="la la-search"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                <a href="{{ '/' . config('jarvis.admin-path') . '/video/create' }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="fa fa-file-video"></i>
                                        <span>
                                            New Video
                                        </span>
                                    </span>
                                </a>
                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                            </div>
                        </div>
                    </div>
                    <!--end: Search Form -->
                    <!--begin: Datatable -->
                    <table class="m-datatable" id="html_table" width="100%">
                        <thead>
                        <tr>
                            <th title="Field #1">
                                Title
                            </th>
                            {{--<th title="Field #2">--}}
                                {{--Video--}}
                            {{--</th>--}}
                            <th title="Field #3">
                                Actions
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($videos as $video)
                            <tr>
                                <td>
                                    {{ $video->title }}
                                </td>
                                {{--<td>--}}
                                    {{--<video id="video-{{ $video->id }}" data-id="video-{{ $video->id }}" class="video-js videojs-player" controls preload="auto" width="322" height="133"--}}
                                           {{--poster="" data-setup="{}">--}}
                                        {{--<source src="{{ $video->link }}" type="video/mp4">--}}
                                        {{--<source src="MY_VIDEO.webm" type='video/webm'>--}}
                                        {{--<p class="vjs-no-js">--}}
                                            {{--To view this video please enable JavaScript, and consider upgrading to a web browser that--}}
                                            {{--<a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>--}}
                                        {{--</p>--}}
                                    {{--</video>--}}
                                {{--</td>--}}
                                <td data-field="Actions" class="m-datatable__cell">
                                    <span style="overflow: visible; width: 110px;">
                                        <a href="{{ '/' . config('jarvis.admin-path') . '/video/' . $video->id . '/edit' }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon
                                        m-btn--icon-only m-btn--pill" title="Edit ">
                                            <i class="la la-edit"></i>
                                        </a>

                                        <a href="{{ '/' . config('jarvis.admin-path') . '/video/' . $video->id }}" class="delete-btn m-portlet__nav-link btn m-btn m-btn--hover-danger
                                        m-btn--icon
                                        m-btn--icon-only m-btn--pill" title="Edit ">
                                            <i class="la la-times"></i>
                                        </a>
                                    </span>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
@endsection