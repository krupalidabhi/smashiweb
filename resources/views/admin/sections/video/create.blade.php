@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <!--begin::Form-->
                {!! Form::open(['url' => '/' . config('jarvis.admin-path') . '/video/', 'method' => 'POST', 'class' => 'm-form', 'files' => true]) !!}
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            {{-- Input Box --}}
                            <div class="form-group m-form__group">
                                <label for="example_input_full_name">
                                    Title
                                </label>
                                {!! Form::text('title', null, ['class' => 'form-control m-input', 'placeholder' => '', 'required']) !!}
                                {!! Form::hidden('category_id', 1) !!}
                            </div>
                            {{-- Textarea Box --}}
                            <div class="form-group m-form__group">
                                <label>
                                    Video
                                </label>
                                {!! Form::file('video', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <a href="{{ '/' . config('jarvis.admin-path') . '/video' }}" class="btn btn-secondary">
                                Cancel
                            </a>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

    @endsection