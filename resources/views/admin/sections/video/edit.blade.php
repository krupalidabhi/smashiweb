@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                Edit Page
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                {!! Form::model($video, ['url' => '/' . config('jarvis.admin-path') . '/video/' . $video->id, 'method' => 'PUT', 'class' => 'm-form', 'files' => true]) !!}
                <div class="m-portlet__body">
                    <div class="m-form__section m-form__section--first">
                        {{-- Input Box --}}
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">
                                Title
                            </label>
                            {!! Form::text('title', null, ['class' => 'form-control m-input', 'placeholder' => '', 'required']) !!}
                        </div>
                        {{-- Textarea Box --}}
                        <div class="form-group m-form__group">
                            <label>
                                Video
                            </label>
                            <div class="preview-video clearfix" style="position: relative;">
                                <video id="video-{{ $video->id }}" data-id="video-{{ $video->id }}" class="video-js videojs-player" controls preload="auto" width="322" height="133"
                                       poster="" data-setup="{}">
                                    <source src="{{ $video->link }}" type="video/mp4">
                                    {{--<source src="MY_VIDEO.webm" type='video/webm'>--}}
                                    <p class="vjs-no-js">
                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                        <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                    </p>
                                </video>
                            </div>
                            {!! Form::file('video', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                        <a href="{{ '/' . config('jarvis.admin-path') . '/video' }}" class="btn btn-secondary">
                            Cancel
                        </a>
                    </div>
                </div>
            {!! Form::close() !!}
            <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

@endsection