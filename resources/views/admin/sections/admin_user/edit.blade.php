@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                Edit Admin User
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                {!! Form::model($admin_user,['url' => '/' . config('jarvis.admin-path') . '/admin_user/' . $admin_user->id, 'method' => 'PUT', 'class' => 'm-form']) !!}
                <div class="m-portlet__body">
                    @if(Session::has('errors'))
                        <div class="alert alert-danger" role="alert">
                            {{ Session::get('errors') }}
                        </div>
                    @endif
                    <div class="m-form__section m-form__section--first">
                        {{-- Input Box --}}
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">
                                Username
                            </label>
                            {!! Form::text('username', null, ['class' => 'form-control m-input', 'placeholder' => '', 'required']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">
                                Password
                            </label>
                            {!! Form::password('password', ['class' => 'form-control m-input', 'placeholder' => '', 'required']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">
                                Role Id
                            </label>
                            {!! Form::select('role_id', $admin_roles, null, ['class' => 'form-control m-input m-bootstrap-select m_selectpicker', 'placeholder' => '', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                        <a href="{{ '/' . config('jarvis.admin-path') . '/admin_user' }}" class="btn btn-secondary">
                            Cancel
                        </a>
                    </div>
                </div>
            {!! Form::close() !!}
            <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

@endsection