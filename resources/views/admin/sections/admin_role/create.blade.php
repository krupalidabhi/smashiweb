@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                New Admin Role
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                {!! Form::open(['url' => '/' . config('jarvis.admin-path') . '/admin_role/', 'method' => 'POST', 'class' => 'm-form']) !!}
                <div class="m-portlet__body">
                    @if(Session::has('errors'))
                        <div class="alert alert-danger" role="alert">
                            {{ Session::get('errors') }}
                        </div>
                    @endif
                    <div class="m-form__section m-form__section--first">
                        {{-- Input Box --}}
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">
                                Admin Role Name
                            </label>
                            {!! Form::text('name', null, ['class' => 'form-control m-input', 'placeholder' => '', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                        <a href="{{ '/' . config('jarvis.admin-path') . '/admin_role' }}" class="btn btn-secondary">
                            Cancel
                        </a>
                    </div>
                </div>
            {!! Form::close() !!}
            <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

@endsection