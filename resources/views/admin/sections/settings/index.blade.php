`@extends('admin.master')

@section('page-title')
    Settings
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                @if(session('success'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <strong>Success!</strong> {{ session('success') }}
                    </div>
                @endif
                <!--begin::Form-->
                {!! Form::open(['url' => '/' . config('jarvis.admin-path') . '/settings/', 'method' => 'POST', 'class' => 'm-form']) !!}
                <div class="m-portlet__body">
                    @if(Session::has('errors'))
                        <div class="alert alert-danger" role="alert">
                            {{ Session::get('errors') }}
                        </div>
                    @endif
                    <div class="m-form__section m-form__section--first">
                        @foreach($settings as $setting)
                            @if($setting->type == 1)
                                @php
                                    $name = $setting->key;
                                    $value = $setting->value;
                                    $label =  ucwords(str_replace('_', ' ', $setting->key));
                                    $input_settings = ['class' => 'form-control m-input', 'placeholder' => '', 'required'];
                                @endphp
                                @include('admin.sections.partials.form.text')
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
            <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

@endsection`