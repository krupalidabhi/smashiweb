{{-- Input Box --}}
<div class="form-group m-form__group">
    <label for="example_input_full_name">
        {{ $label }}
    </label>
    {!! Form::text($name, $value, $input_settings) !!}
</div>