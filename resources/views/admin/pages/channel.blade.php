@extends('web.master')

@section('content')

 <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/channel.css') }}" >

<div  class="container">
    <div class="page_title">
       <h1> Start Channel</h1>
       <hr>
    </div>
</div>
<div  class="container">
    <div class="category-videos">
        <div class="row" id="video-data">
         <div class="vod_edit">
            <a href="/adminpage/vodedit">Video Cropping</a>
            <a href="/adminpage/playlist">Playlist</a>
         </div>
       
       
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-centered">
                    <div class="access-container" style="color:white; padding: 30px 0;">
                        {!! Form::open([ 'id' => 'channel-start-form', 'url' => '/adminOnlyPage','method' => 'POST']) !!}
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                       
                        <!-- <div class="sm-form-row">
                            <label for="facebook-URL">Facebook URL</label>
                            {!! Form::text('channel1', null, ['id' => 'fb-url']) !!}
                        </div>

                        <div class="sm-form-row">
                            <label for="facebook-stream">Facebook Stream Name</label>
                            {!! Form::text('channel2', null, ['id' => 'fb-stream']) !!}
                        </div> -->
                        
                        <div class="sm-form-row">
                            <label for="folder-s3">Add new folder name</label>
                            {!! Form::text('folder', null, ['id' => 'folder-s3', 'required' ]) !!}
                        </div>

                        <div class="sm-form-row">
                            <label for="youtube-URL">Restream URL</label>
                            {!! Form::text('channel1', null, ['id' => 'yt-url', 'required']) !!}
                        </div>

                        <div class="sm-form-row">
                            <label for="youtube-stream">Restream Key</label>
                            {!! Form::text('channel2', null, ['id' => 'yt-stream', 'required']) !!}
                        </div>

                        <div class="sm-form-row">
                            <label for="filename-s3">File Name</label>
                            {!! Form::text('filename', null, ['id' => 'filename-s3', '']) !!}
                        </div>

                        <div class="sm-form-row submit">
                            
                            <input type="submit" value="Update Channel" >
                            <div class="api-load-icon text-center" style="display:none">
                                <p><img width="46px" src="/assets/img/archive-loader.gif"></p>
                            </div>
                        </div>
                        {!! Form::close() !!}

                                           
                    </div>
                </div>
            </div>
        <div id="start_stop" >
            <input type="button" value="Start Channel" {{$startButton}} class="start" />
            <input type="button" value="Stop Channel" {{$stopButton}} class="stop" />
            <div class="api-load-icon text-center" style="display:none; margin-top:15px;">
                <p><img width="46px" src="/assets/img/archive-loader.gif"></p>
            </div>
        </div>
        <div class="validation_msg" style="color: White; text-align:center;"><p></p></div>
        </div>

        
<div id="response" style="color: white;"></div>

               
        </div>
    </div>
</div>

<script>
refreshIntervalId = '';
token = $('input[name=_token]').val();
yt_url_aws = $('#yt-url').val();
yt_stream_aws = $('#yt-stream').val();
folder_name_aws = $('#folder-s3').val();

bitrate = '';
aws_height = '';
aws_width = '';
resolution = '';
codec = '';
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

function run_ajax_check_channel($status){

    $.ajax({
        url: "/adminpage/checkchannellive",
        type: "post",
        async: false,
        data: '',
        success: function(data){
            
            if( $status == data.responseObject.state ){

                $('.api-load-icon').hide();
                clearInterval(refreshIntervalId);
                $('input.stop').attr('disabled', data.responseObject.stopButton);
                $('input.start').attr('disabled', data.responseObject.startButton);
                $('form#channel-start-form input[type=submit]').attr('disabled', data.responseObject.formField);
            }

        },
        error:function(data){
            
        }
    });

}

function run_ajax_check_channel_stop_update($status){

    $.ajax({
        url: "/adminpage/checkchannellive",
        type: "post",
        async: false,
        data: '',
        success: function(data){
            if( $status == data.responseObject.state ){

                // $('.api-load-icon').hide();
                clearInterval(refreshIntervalIdStopUpdate);
                $('input.stop').attr('disabled', data.responseObject.stopButton);
                $('input.start').attr('disabled', data.responseObject.startButton);
                $('form#channel-start-form input[type=submit]').attr('disabled', data.responseObject.formField);

                $.ajax({
                    url: "/adminpage/updatechannel",
                    type: "post",
                    data: { yt_url:yt_url_aws, yt_stream:yt_stream_aws, folder_name:folder_name_aws, _token: token, "filename": bitrate, "aws_height": aws_height, "aws_width": aws_width, "resolution": resolution, "codec": codec },
                    success: function(data){
                        serverCheckStatus('IDLE');

                    },
                    error:function(data){
                        $('#channel-start-form .api-load-icon').hide();
                        alert('please try after sometime');
                    }
                });

            }

        },
        error:function(data){
            
        }
    });

}

function serverCheckStatus($status){

    refreshIntervalId = setInterval( function(){ run_ajax_check_channel($status) }, 3000 );

}

function serverCheckStatusStopUpdate($status){
    refreshIntervalIdStopUpdate = setInterval( function(){ run_ajax_check_channel_stop_update($status) }, 3000 );
}

 $( '#channel-start-form' ).on( 'submit', function(e) {
    e.preventDefault();
    $('#channel-start-form .api-load-icon').show();
    token = $('input[name=_token]').val();
    yt_url_aws = $('#yt-url').val();
    yt_stream_aws = $('#yt-stream').val();
    folder_name_aws = $('#folder-s3').val();

    bitrate = $('#filename-s3').val();
    aws_height = $('#h-s3').val();
    aws_width = $('#w-s3').val();
    resolution = $('#resolution-s3').val();
    codec = $('#codec-s3').val();

    $.ajax({
        url: "/adminpage/stopchannel",
        type: "post",

        data: { stop : 'stop'  },
        success: function(data){
            serverCheckStatusStopUpdate('IDLE');
        },
        error:function(data){
            $('.api-load-icon').hide();
            alert('please try after sometime');
        }
    });

});
/**************** start channel ********************/
$( document ).on( 'click', 'input.start:not(:disabled)',function(e) {
        
        e.preventDefault();
        $('#start_stop .api-load-icon').show();
    $.ajax({
        url: "/adminpage/playlist/stop",
        type: "post",

        data: { stop : 'stop'  },
        success: function(data){
            
        },
        error:function(data){
            $('#start_stop .api-load-icon').hide();
            alert('please try after sometime');
        }
    });

    setTimeout(function () {
        
        $.ajax({
            url: "/adminpage/startchannel",
            type: "post",
            data: { start : 'start'  },
            success: function(data){
                
                serverCheckStatus('RUNNING');

            },
            error:function(data){
                $('#start_stop .api-load-icon').hide();
                alert('please try after sometime');
            }
        });

    }, 2000);

});

/**************** Stop channel ********************/
$( document ).on( 'click', 'input.stop:not(:disabled)', function(e) {
    e.preventDefault();
    $('#start_stop .api-load-icon').show();

    $.ajax({
        url: "/adminpage/playlist/start",
        type: "post",

        data: { stop : 'stop'  },
        success: function(data){
            console.log('Vod stop');
        },
        error:function(data){
            $('#start_stop .api-load-icon').hide();
            alert('please try after sometime');
        }
    });

    setTimeout(function () {
        
        $.ajax({
            url: "/adminpage/stopchannel",
            type: "post",

            data: { stop : 'stop'  },
            success: function(data){
                
                serverCheckStatus('IDLE');
            },
            error:function(data){
                $('#start_stop .api-load-icon').hide();
                alert('please try after sometime');
            }
        });

    }, 2000);

});

</script>

@endsection