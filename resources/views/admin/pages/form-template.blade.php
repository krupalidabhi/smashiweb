@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                New {{'RESOURCE_NAME'}}
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                {!! Form::open(['url' => '/' . config('jarvis.admin-path') . '/country/', 'method' => 'POST', 'class' => 'm-form']) !!}
                <div class="m-portlet__body">
                    @if(Session::has('errors'))
                        <div class="alert alert-danger" role="alert">
                            {{ Session::get('errors') }}
                        </div>
                    @endif
                    <div class="m-form__section m-form__section--first">
                        {{-- Input Box --}}
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">
                                Input Box
                            </label>
                            {!! Form::text('name', null, ['class' => 'form-control m-input', 'placeholder' => '', 'required']) !!}
                        </div>
                        {{-- Textarea Box --}}
                        <div class="form-group m-form__group">
                            <label>
                                Textarea Box
                            </label>
                            {!! Form::textarea('name', null, ['class' => 'form-control m-input', 'placeholder' => '', 'required']) !!}
                        </div>

                        {{-- Textarea WYSIWYG Box --}}
                        <div class="form-group m-form__group">
                            <label>
                                Textarea WYSIWYG Box
                            </label>
                            {!! Form::textarea('name', null, ['class' => 'form-control m-input summernote', 'placeholder' => '', 'required']) !!}
                        </div>

                        {{-- Datepicker --}}
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">
                                Datepicker
                            </label>
                            {!! Form::text('date', null, ['class' => 'form-control m_datepicker', 'placeholder' => 'Select Date', 'required', 'data-date-format' => 'dd/mm/yyyy']) !!}
                        </div>

                        {{-- Timepicker --}}
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">
                                Timepicker
                            </label>
                            {!! Form::text('date', null, ['class' => 'form-control m_timepicker', 'placeholder' => 'Select Date', 'required', 'data-date-format' => 'dd/mm/yyyy']) !!}
                        </div>

                        {{-- DateTimePicker --}}
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">
                                DateTimePicker
                            </label>
                            {!! Form::text('date', null, ['class' => 'form-control m_datetimepicker', 'placeholder' => 'Select Date', 'required', 'data-date-format' => 'dd/mm/yyyy hh:ii']) !!}
                        </div>

                        {{-- Select Box (Dropdown) --}}
                        <div class="form-group m-form__group">
                            <label>
                                Select Box (Dropdown)
                            </label>
                            {!! Form::select('items', ['1' => 'First Item', '2' => 'Second Item'], null, ['class' => 'form-control m-bootstrap-select m_selectpicker', 'title' => 'Choose Items']) !!}
                        </div>

                        {{-- Select Box with Search (Dropdown) --}}
                        <div class="form-group m-form__group">
                            <label>
                                Select Box with Search (Dropdown)
                            </label>
                            {!! Form::select('items', ['1' => 'First Item', '2' => 'Second Item'], null, ['class' => 'form-control m-bootstrap-select m_selectpicker', 'title' => 'Choose Items', 'data-live-search' => 'true']) !!}
                        </div>

                        {{-- Checkbox --}}
                        <div class="m-form__group form-group">
                            <label >
                                Checkboxes
                            </label>
                            <div class="m-checkbox-list">
                                <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                    {!! Form::checkbox('checkbox') !!}
                                    Success checked state
                                    <span></span>
                                </label>
                                <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
                                    {!! Form::checkbox('checkbox') !!}
                                    Brand checked state
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        {{-- Radio Buttons --}}
                        <div class="m-form__group form-group">
                            <label >
                                Radios
                            </label>
                            <div class="m-radio-list">
                                <label class="m-radio m-radio--solid m-radio--success">
                                    {!! Form::radio('radio', 5) !!}
                                    Success state
                                    <span></span>
                                </label>
                                <label class="m-radio m-radio--solid m-radio--brand">
                                    {!! Form::radio('radio', 6) !!}
                                    Brand state
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        {{-- Switch --}}
                        <div class="m-form__group form-group ">
                            <label>
                                Switch
                            </label>
                            <div>
                                <span class="m-switch">
                                    <label>
                                        {!! Form::checkbox('name') !!}
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                        <a href="{{ '/' . config('jarvis.admin-path') . '/country' }}" class="btn btn-secondary">
                            Cancel
                        </a>
                    </div>
                </div>
            {!! Form::close() !!}
            <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

@endsection