@extends('web.master')
<style>
h4.title{
    text-align: center;
    font-weight: 600;
    color: white;
    padding-bottom: 20px;
}
.s3_folder{
    border: 2px solid #ed7a3e;
    border-radius: 12px;
    color: white;
    font-size: 19px;
    font-weight: 700;
}
.s3_folder > a {
    display: block;
    text-align: left;
    border-bottom: 1px solid #ed7a3e;
    padding: 11px 21px;
}

.s3_folder > a > i{
    float: left;
    color: #ed7a3e;
    padding-right: 10px;

}
.cnfm_select{
    width: 180px;
    background-color: #f97b3e;
    color: #ffffff;
    font-size: 14px;
    line-height: 24px;
    font-size: 1rem;
    font-weight: 800;
    text-align: center;
    border: 1px solid #f97b3e;
    padding: 8px 10px;
    -webkit-transition: all 0.3s;
    transition: all 0.3s;
    border-radius: 10px;
    cursor: pointer;
    margin-top: 40px;
    display: block;
}
a.cnfm_select:hover{
    background-color: #ffffff;
    color: #f97b3e;
}
.crop_resp{
    margin-top: 20px;
}
.crop_resp > p{
    margin: 0;
    color: white;
    font-weight: 600;
    font-size: 17px;
}
.s3_vod{
    margin-top: 45px;
}
.fa-file-video-o:before{
    content:"\f1c8";
}
a:hover{
    color: #f97b3e !important;
}
</style>
@section('content')
<div  class="container">
    <div class="page_title">
       <h1>Playlist</h1>
       <hr>
    </div>
</div>
<div  class="container">
    <div class="category-videos">
        <div class="row" id="video-data">
            <div class="vod_edit">
                <a href="/adminpage">Live stream</a>
                <a href="/adminpage/vodedit">Video cropping</a>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-centered" style="min-height: 500px;">
                        <?php 
                            $s3_playlist=App\Http\Controllers\Web\Channel\PlaylistController::display_playlist_s3(); 
                        ?>

                        <h4 class="title">S3 list for Playlist</h4>
                        <div id="edit_step1" class="s3_folder">
                            
                            @foreach($s3_playlist as $name)
                                <a href="javascript:void(0)" data-value="<?php echo config('jarvis.aws-playlist-endpoint').$name; ?>" ><i class="fa fa-file-video-o" aria-hidden="true"></i><?php echo $name; ?></a> 
                            @endforeach
                            
                        </div>
                        <!--------- Step 2 code after folder list displlay ---------------->
                        <div id="edit_step2" class="s3_vod" style="display:none;" >
                            <video id="crop_vod" class="overlay-log video-js vjs-default-skin vjs-16-9" controls data-setup="{}" autoplay muted>
                                <source src="https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/part_ali_el_sayed/default/hls/part+ali+el+sayedOutput1.m3u8" type="application/x-mpegURL" label="">
                            </video>
                            <a href="javascript:void(0)" class="cnfm_select">Confirm Select</a>
                            <div class="api-load-icon text-center" style="display:none">
                                <p><img width="46px" src="/assets/img/archive-loader.gif"></p>
                            </div>
                            <div class="crop_resp">
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
refreshIntervalId = '';
CusrefreshIntervalId = '';
refreshIntervalIdStart = '';
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
jQuery(window).load(function(){
    jQuery('.s3_folder > a').on('click',function(){
        jQuery('#edit_step2').show();
        var s3_vod_link=jQuery(this).data('value');
        
        var myPlayer = videojs('crop_vod');
        
        myPlayer.src(s3_vod_link);
    });

});

function run_ajax_check_channel_runnging($status){

    $.ajax({
        url: "/adminpage/checkchannelvod",
        type: "post",
        async: false,
        data: '',
        success: function(data){
            
            if( $status == data.responseObject.state ){

                clearInterval(refreshIntervalIdStart);
                $('.api-load-icon').hide();
                $('.crop_resp > p').html("Video has been selected.");                
            }

        },
        error:function(data){
            
        }
    });

}

function playlist_start(){

    $.ajax({
        url: "/adminpage/playlist/start",
        type: "post",
        data: { start:'start' },
        success: function(data){
            
            refreshIntervalIdStart = setInterval( function(){ run_ajax_check_channel_runnging('RUNNING') }, 3000 );

        },
        error:function(data){
            $('.api-load-icon').hide();
            alert('please try after sometime');
        }
    });  

}

function run_ajax_check_channel_update($status){

    $.ajax({
        url: "/adminpage/checkchannelvod",
        type: "post",
        async: false,
        data: '',
        success: function(data){
            
            if( $status == data.responseObject.state ){

                // $('.api-load-icon').hide();
                clearInterval(CusrefreshIntervalId);
                // $('input.stop').attr('disabled', data.responseObject.stopButton);
                // $('input.start').attr('disabled', data.responseObject.startButton);
                // $('form#channel-start-form input[type=submit]').attr('disabled', data.responseObject.formField);
                
                playlist_start();
            }

        },
        error:function(data){
            
        }
    });

}

function update_channel(){

    $.ajax({
        url: "/adminpage/playlist/update",
        type: "post",
        data: { start:'start' },
        success: function(data){
            
            data = data.replace(/'/g,"");
            var obj = $.parseJSON(data);
            var status = obj['ResponseMetadata']['HTTPStatusCode'];
            
                if(status == 200){
                    
                    CusrefreshIntervalId = setInterval( function(){ run_ajax_check_channel_update('IDLE') }, 3000 );

                } 
                else{
                    
                    alert('please try after sometime');
                }
        },
        error:function(data){
            $('.api-load-icon').hide();
            alert('please try after sometime');
        }
    });  
}

function update_playlist(){

    $.ajax({
        url: "/adminpage/playlist/s3_api",
        type: "post",
        data: { s3_link:$('#crop_vod_html5_api').attr('src') },
        success: function(data){
            
            data = data.replace(/'/g,"");
            var obj = $.parseJSON(data);
            var status = obj['ResponseMetadata']['HTTPStatusCode'];
            if(status == 200){
                
                update_channel();
            } 
            else{
                alert('please try after sometime');
            }
            
        },
        error:function(data){
            $('.api-load-icon').hide();
            alert('please try after sometime');
        }
    });

}

function run_ajax_check_channel($status){

    $.ajax({
        url: "/adminpage/checkchannelvod",
        type: "post",
        async: false,
        data: '',
        success: function(data){
            
            if( $status == data.responseObject.state ){

                // $('.api-load-icon').hide();
                clearInterval(refreshIntervalId);
                // $('input.stop').attr('disabled', data.responseObject.stopButton);
                // $('input.start').attr('disabled', data.responseObject.startButton);
                // $('form#channel-start-form input[type=submit]').attr('disabled', data.responseObject.formField);
                
                update_playlist();
            }

        },
        error:function(data){
            
        }
    });

}

function serverCheckStatus($status){

    refreshIntervalId = setInterval( function(){ run_ajax_check_channel($status) }, 3000 );

}

$( '.cnfm_select' ).on( 'click', function(e) {
    e.preventDefault();
    $('.crop_resp > p').html("");
    $('.api-load-icon').show();
    var vod_link = $('#crop_vod_html5_api').attr('src');
    $('.crop-vod .api-load-icon').show();
    
    $.ajax({
        url: "/adminpage/stopchannel",
        type: "post",

        data: { stop : 'stop'  },
        success: function(data){
            console.log('stop channel'); 
        },
        error:function(data){
            //
        }
    });

    setTimeout(function () {
        
        $.ajax({
            url: "/adminpage/playlist/stop",
            type: "post",
            data: { stop : 'stop'  },
            success: function(data){
                
                serverCheckStatus('IDLE');
            },
            error:function(data){
                $('#start_stop .api-load-icon').hide();
                alert('please try after sometime');
            }
        });        

    }, 2000);

});
</script>
@endsection