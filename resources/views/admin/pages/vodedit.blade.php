@extends('web.master')
<style>
h4.title{
    text-align: center;
    font-weight: 600;
    color: white;
    padding-bottom: 20px;
}
.s3_folder{
    border: 2px solid #ed7a3e;
    border-radius: 12px;
    color: white;
    font-size: 19px;
    font-weight: 700;
}
.s3_folder > a {
    display: block;
    text-align: left;
    border-bottom: 1px solid #ed7a3e;
    padding: 11px 21px;
}

.s3_folder > a > i{
    float: left;
    color: #ed7a3e;
    padding-right: 10px;

}
.form_crop > h5{
    text-align: center;
    margin-top: 30px;
    color: white;
}
#price-range > span{
    background: rgb(237, 122, 62);
    border: rgb(237, 122, 62);
}
#price-range > .ui-slider-range{
    background: #ed7a3ea1;
}
.crop_resp > p{
    margin: 0;
    color: white;
    font-weight: 600;
    font-size: 17px;
}
.crop_time_btn.vod_edit{
    display: block;
    margin: 10px auto 25px;
    text-align: center;
}
.refresh_div{
    margin: 35px 0 25px;
}
.refresh_div > a.refresh{
    color: white;
    font-size: 20px;
    font-weight: 600;
}
.refresh_div > a.refresh:hover , a:hover{
    color: #f97b3e !important;
}
.back_div{
    display: block;
    overflow: hidden;
    padding: 10px 0 20px;
}
.back_div a.back{
    float: left;
    font-size: 20px;
    font-weight: 600;
    color: white;
}
.back_div a.back > i{
    padding-right: 10px;
}
.fa-refresh:before {
    content: "\f021";
}

.folders_list li.folder_name {
    display: block;
    text-align: left;
    border-bottom: 1px solid #ed7a3e;
    padding: 11px 21px;
}

ul.folder_childs {
    text-align: left;
    border-bottom: 1px solid #ed7a3e;
    padding: 11px 21px;
}

.nodisplay{
    display: none;
}

li.folder_name span {
    cursor: pointer;
}

li.childs_name {
    display: block;
    text-align: left;
}

.vod-crop-field a{
    color: blue !important;
}

ul.folder_childs li.childs_name a {
    font-size: initial;
    white-space: nowrap;
}
</style>
@section('content')
<div  class="container">
    <div class="page_title">
       <h1>Video Crop</h1>
       <hr>
    </div>
</div>
<div  class="container">
    <div class="category-videos">
        <div class="row" id="video-data">
            <div class="vod_edit">
                <a href="/adminpage">Live stream</a>
            </div>
       
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-centered" style="min-height: 500px;">


                <?php 
                    $bucket = 'smashi-proudction';
                    $s3_folder=App\Http\Controllers\Web\Channel\VodeditController::display_folder_s3_vodedit($bucket); 
                ?>
                    <h4 class="title">S3 Folder - All live streamed video saved under below folder</h4>
                    <div id="edit_step1" class="s3_folder">
                        
                        <ul class="folders_list">
                            <?php 
                            foreach ($s3_folder as $folderName => $files) {
                            ?>

                                <li class="folder_name"> <span><?php echo $folderName; ?><i class="fa fa-folder" aria-hidden="true"></i></span>
                                    
                                    <?php 
                                    // print_r($files);
                                    if( is_array($files) ){
                                        echo '<ul class="folder_childs nodisplay">';
                                        foreach ($files as $key=>$name) {
                                            ?>
                                            <li class="childs_name">
                                                <a href="javascript:void(0)" data-value="<?php echo 'https://s3.eu-central-1.amazonaws.com/'.$bucket.'/'.$name; ?>" ><?php echo $name; ?>
                                                    <i aria-hidden="true" class="fa fa-file-video"></i>
                                                </a> 
                                            </li>
                                            <?php
                                        }
                                        echo '</ul>';
                                    }
                                    ?>
                                    
                                        <?php /* foreach ($files as $key=>$name) { ?>
                                            <li>
                                                <a href="javascript:void(0)" data-value="<?php echo config('jarvis.aws-stream-endpoint').$name; ?>" ><i class="fa fa-folder" aria-hidden="true"></i><?php echo $name; ?></a> 
                                            </li>
                                        <?php } */ ?>
                                    
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    
                    <!--------- Step 2 code after folder list displlay ---------------->
                    <div id="edit_step2" class="s3_vod" style="display:none;" >
                        <div class="back_div">
                            <a class="back" href="javascript:void(0)">Back to Folders<i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                        </div>
                        <video crossorigin="anonymous" id="crop_vod" class="overlay-log video-js vjs-default-skin vjs-16-9" controls data-setup="{}" autoplay muted>
                            <source crossorigin="anonymous" src="https://s3.eu-central-1.amazonaws.com/smashi2019frank/media-convert/part_ali_el_sayed/default/hls/part+ali+el+sayedOutput1.m3u8" type="application/x-mpegURL" label="">
                        </video>
                        <!-- <video crossorigin="anonymous" id="crop_vod" class="overlay-log video-js vjs-default-skin vjs-16-9" controls
                                   data-setup='{"aspectRatio":"16:9", "fluid": true, "loop": true}' crossorigin="anonymous" autoplay poster="">
                                    <source crossorigin="anonymous" src="" crossorigin="anonymous" type="application/x-mpegURL" label="">
                                </video> -->
                        <div class="api-load-icon text-center" style="display:none">
                                <p><img width="46px" src="/assets/img/archive-loader.gif"></p>
                        </div>
                        <div class="form_crop" style="display:none;">
                            <div class="refresh_div">
                            <a class="refresh" href="javascript:void(0)"><i class="fa fa-refresh" aria-hidden="true">  Reset time</i></a> 
                            </div>
                            <div class="crop_time_btn vod_edit">
                                <a href="javascript:void(0)" class="start_crop">Start cropping</a>   
                            </div>


                            <!-- <h5>Slide left and right controller to adjust the time for cropping.</h5> -->
                            <!-- <div id="price-range" class="slider"></div> -->

                            {!! Form::open([ 'id' => 'vod-crop-field', 'url' => '/adminpage/vodedit','method' => 'POST']) !!}
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                            @endif

                            <div class="sm-form-row ">
                                <label for="vod-s3">Crop Video Name</label>
                                {!! Form::text('Name', null, ['id' => 'crop_name', 'required']) !!}
                            </div>
    
                            <div class="sm-form-row ">
                                <label for="vod-s3">Start Time - Format (HH:MM:SS:FF)</label>
                                {!! Form::text('start', '00:00:00:00', ['id' => 'start-time', 'required']) !!}
                            </div>
                            <div class="sm-form-row">
                                <label for="vod-s3">End Time - Format (HH:MM:SS:FF)</label>
                                {!! Form::text('end', null, ['id' => 'end-time', 'required']) !!}
                            </div>

                            <div class="sm-form-row crop-vod">
                                {!! Form::submit('Crop Video') !!}
                                <div class="api-load-icon text-center" style="display:none">
                                    <p><img width="46px" src="/assets/img/archive-loader.gif"></p>
                                </div>
                            </div>

                            <div class="crop_resp">
                                <p></p>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                   
                </div>
            </div> 
        </div>
    </div>
</div>
<script>
var s3_vod_link;
jQuery(window).load(function(){
    jQuery('#start-time').mask('00:00:00:00');
    jQuery('#end-time').mask('00:00:00:00');
});

jQuery(".folder_name").on('click', 'span', function(){
    jQuery(this).next('ul').toggleClass('nodisplay');
});

jQuery('#edit_step1 a').click(function(){
    
    s3_vod_link=jQuery(this).data('value');
    console.log(s3_vod_link);
    myPlayer = videojs('crop_vod');
    console.log(myPlayer);
    // myPlayer.src(s3_vod_link);
    myPlayer.src({type: 'application/x-mpegURL', src: s3_vod_link});
    console.log('network state',myPlayer.networkState());
     setTimeout(function(){
        //slider jquery
        var lengthOfVideo = myPlayer.duration();
        var slider_val = lengthOfVideo;
        console.log(lengthOfVideo);
        var converted_fomrat=convert_sec_format(lengthOfVideo);
        jQuery("#end-time").val(converted_fomrat);
        jQuery("#end-time").attr('data-val', lengthOfVideo);

        jQuery("#price-range").slider({range: true, min: 0, max: slider_val, values: [0, slider_val], slide: function(event, ui) {
            // jQuery("#priceRange").val("$" + ui.values[0] + " - $" + ui.values[1]);
            jQuery("#start-time").val(convert_sec_format(ui.values[0]));
            jQuery("#end-time").val(convert_sec_format(ui.values[1]));
        }});
       
       //start stop button time jquery
        jQuery(".crop_time_btn ").on('click',' a',function(){
           // get current time of video player
            var whereYouAt_sec = myPlayer.currentTime();
            var whereYouAt = convert_sec_format(whereYouAt_sec);
            if(jQuery(this).hasClass('start_crop')){
                jQuery(this).removeClass('start_crop');
                jQuery("#start-time").val(whereYouAt);
                jQuery("#start-time").attr('data-val', whereYouAt_sec);
                jQuery(this).html("Stop Cropping");
            }
            else{
                jQuery("#end-time").val(whereYouAt);
                jQuery("#end-time").attr('data-val', whereYouAt_sec);
                jQuery(this).addClass('start_crop');
                jQuery(this).html("Start Cropping");
            } 
        });    

        //Refresh button for fields
        jQuery(".refresh_div > a.refresh").click(function(){
            jQuery('#start-time').val('00:00:00:00');
            jQuery("#end-time").val(converted_fomrat);
        });

        jQuery(".form_crop").show();
        jQuery(".api-load-icon").hide();
    }, 5000);
     //Back to folder
     jQuery("#edit_step2 > .back_div > a.back").click(function(){
        location.reload(true);
            myPlayer.pause();
            myPlayer.reset();
            jQuery('#edit_step1').show();
            jQuery("#edit_step2").hide();
            jQuery(".form_crop").hide();
            jQuery(".crop_resp").hide();

            

        });
    // myPlayer.reset();
    jQuery('#edit_step1').hide();
    jQuery("#edit_step2").show();
    jQuery(".api-load-icon").show();
    

    
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$( '#vod-crop-field' ).on( 'submit', function(e) {
    e.preventDefault();
    $('.crop_resp > p').html("");
    var crop_start = $('#start-time').val();

    var st_time = parseFloat( $('#start-time').attr('data-val') );
    var en_time = parseFloat( $('#end-time').attr('data-val') );
    console.log(st_time);
    console.log(en_time);
    var diffDur = (en_time - st_time);
    if( diffDur <= 0 ){
        alert('Invalid start stop duration.');
        return false;
    }
    
    var crop_end = $('#end-time').val();
    var crop_vod_name = $('#crop_name').val();    
    // var vod_link = $('#crop_vod_html5_api').attr('src');\
    var vod_link = s3_vod_link;
    $('.crop-vod .api-load-icon').show();
    // console.log(vod_link);
    $.ajax({
        url: "/adminpage/vodedit/crop",
        type: "post",
        data: { start : crop_start, end:crop_end , s3_link:vod_link, crop_name:crop_vod_name },
        success: function(res){

            /*data = jQuery(data).replace(/'/g,"");
            var obj = $.parseJSON(data);
            var status = obj['HTTPStatusCode'];
            if(status == 201){*/
                $('.crop-vod .api-load-icon').hide();
                $('.crop_resp > p').html("Video has been cropped and saved to s3!, <br/> <p>Open the following link to see the cropped video once aws finished cropping (depends on cropped video size) <br/> <a href='"+res+"' target='_blank'>Browse S3</a> </p>");
            /*} 
            else{
                alert('please try after sometime');
            }*/
            
        },
        error:function(data){
            $('.api-load-icon').hide();
            alert('please try after sometime');
        }
    });
});
function convert_sec_format(lengthOfVideo){

    var h = Math.floor(lengthOfVideo/3600); //Get whole hours
    h = (h==0)?'0'+0:h; 

    h = (h>0 && h<9)?'0'+h:h; 

    lengthOfVideo -= h*3600;
    console.log('houers');
    console.log(h);
    console.log('end houers');
    var m = Math.floor(lengthOfVideo/60); //Get remaining minutes
    lengthOfVideo -= m*60;
    lengthOfVideo = h+":"+(m < 10 ? '0'+m : m)+":"+(lengthOfVideo < 10 ? '0'+lengthOfVideo : lengthOfVideo); //zero padding on minutes and seconds

    lengthOfVideo=lengthOfVideo.split('.')[0];
    lengthOfVideo = lengthOfVideo+':00';
    return lengthOfVideo;
}
 
</script>

@endsection