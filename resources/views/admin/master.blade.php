<!DOCTYPE html>

<html lang="en" >
<head>
    <meta charset="utf-8" />
    <title>
        {{ config('jarvis.admin-site-title') }} | Strategies dC
    </title>
    <meta name="description" content="Strategies dC - Admin Portal, CMS">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    @include('admin.includes.styles')
    <link href="https://vjs.zencdn.net/7.4.1/video-js.css" rel="stylesheet">
</head>
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
@include('admin.partials.header')
<!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        @include('admin.partials.sidemenu')
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">
                            @if(isset($page_title))
                                {{ $page_title }}
                            @else
                                @yield('page-title', 'Dashboard')
                            @endif
                        </h3>
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->
            <div class="m-content">
                <div class="row">
                    <div class="col-md-12">
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                <strong>Well done!</strong> {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('errors'))
                            <div class="alert alert-danger" role="alert">
                                <ul class="errors">
                                    @foreach ($errors->all() as $message)
                                        <li>{{ $message }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                @yield('content')
            </div>
        </div>
    </div>
    <!-- end:: Body -->
    @include('admin.partials.footer')
</div>
<!-- end:: Page -->
<!-- begin::Scroll Top -->
<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->
@include('admin.includes.scripts')
<script src="https://vjs.zencdn.net/7.4.1/video.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=vervmf13agwei7nyfx41w3kjbqg6idmgy8cjiaiv6i1nv2iu"></script>
</body>
</html>
