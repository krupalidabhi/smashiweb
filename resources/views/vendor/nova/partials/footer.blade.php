<p class="mt-8 text-center text-xs text-80">
    <a class="text-primary dim no-underline">SmashiTV</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} SmashiTV
    <span class="px-1">&middot;</span>
</p>
