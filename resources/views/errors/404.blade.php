@extends('web.master')

@section('content')

    <div class="page page-generic">
        <div class="page-content">
            <div class="container">
                <div class="col-12">
                    <div class="section-title">
                        <h1 class="text-center">404 الصفحة غير موجودة</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-text no-top-margin">
                            <p class="text-center">عذراً ولكن الصفحة الذي تبحث عنها غير موجودة</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 text-center mb-30">
                        <a href="/" class="sm-btn orange">العودة إلى الصفحة الرئيسية</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection