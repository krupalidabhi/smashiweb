@extends('web.master')

@section('content')
    <div class="page page-category">
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="section-title">
                            <h1 class="text-line">Blog</h1>
                        </div>
                    </div>
                </div>

                <div class="category-videos">
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-md-4 col-sm-6 text-right">
                                @include('web.partials.blog-post')
                            </div>
                        @endforeach
                    </div>
                </div>

                {{--<div class="load-more-container">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-12 text-center">--}}
                            {{--<a href="javascript:" class="sm-btn orange">المزيد</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>

@endsection