
@extends('web.master')
@section('content')

    @include('web.pages.profile.profile-modal')
    @include('web.pages.profile.subscription.subscription-form-modal')
    @include('web.pages.profile.subscription.subscription-cancel-modal')
    <style type="text/css">
        input#email, input#address {
            background-color: transparent;
            color: white;
            border: 2px solid #ffffff;
        }

        input#email::placeholder, input#address::placeholder{
            color: white;
            opacity: 1;
        }

        #shipping_form input{
            border: 3px solid #447efc !important;
            background: transparent;   
        }

        #shipping_form h4 {
            width: 100%;
            display: block;
            color: white;
            font-size: 54px;
            font-weight: 900;
        }

        #subscription-form-modal .modal-header {
            border: none;
        }

        #subscription-form-modal .form-group {
            width: 100%;
        }

        #subscription-form-modal label {
            color: white;
        }
    </style>
    <div class="page page-account">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title with-line">
                        <h1 class="text-line">الحساب الخاص</h1>
                    </div>
                    <div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="intro">
                            <p>اشترك في سماشي تي في للحصول على:</p>
                            <ul class="sm-ul">
                                <li>مشاهدة جميع البرامج المباشرة</li>
                                <li>مشاهدة جميع البرامج على مدار الساعة</li>
                            </ul>
							<br>
                            <div class="edit-profile" style="position:static;display: inline-block;" >
                                <a href="javascript:" class="btn" data-toggle="modal" data-target="#profile-modal"><i class="fas fa-edit"></i> تعديل البيانات</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if(Auth::user()->has_trial > 0)
                            <p>{{$dayTrialLeft}} أيام محاكمة متبقية</p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h2>المعلومات الشخصية</h2>

                        <div class="row">
                            <div class="col-lg-8">

                                <div class="personal-info">
                                    <div class="personal-info-content">
                                        <div class="personal-info-name">{{ Auth::user()->name }}</div>
                                        <div class="personal-info-email">{{ Auth::user()->email }}</div>
                                        <div class="personal-info-password">&#x25CF;&#x25CF;&#x25CF;&#x25CF;&#x25CF;&#x25CF;&#x25CF;&#x25CF;&#x25CF;&#x25CF;</div>
                                    </div>
                                </div>
                            
								<label class="btn btn-primary" style="background-color:#282828;" >
								{{ Form::checkbox('admin','',  Auth::user()->newsletter,array('id'=>'newsletter')) }} &nbsp; Newsletter
								</label>
							
							
							</div>
                        </div>

                        @if(Auth::user()->has_subscription)
                            <h2>الباقة الحالية</h2>
                        @else
                            <h2>اختر الباقة</h2>
                        @endif

                        <div class="plan-step" id="step-one">
                            <h3>الخطوة الأولى</h3>
                            <div class="sub-title">الرجاء اختيار باقة</div>

                            <div class="subscriptions">
                                @if(Auth::user()->has_subscription)
                                    @foreach($subscriptions as $subscription)
                                        @if($subscription->id == Auth::user()->subscriptions->first()->id)
                                            @php $change_plan = false; $pointer = false; @endphp
                                            @include('web.pages.profile.subscription.partial.subscription-box')
                                        @endif
                                    @endforeach
                                    @if(Auth::user()->subscriptions->count() > 0)
                                        <div class="renew-subscription change-plan">
                                            <div class="square">
                                                <a href="javascript:" data-toggle="modal" data-target="#profile-modal" id="change-plan-link"><span>Change <strong>Plan</strong></span></a>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    @foreach($subscriptions as $subscription)
                                        @php $change_plan = false; $pointer = true @endphp
                                        @include('web.pages.profile.subscription.partial.subscription-box')
                                    @endforeach
                                @endif
                            </div>
                        </div>

                        <div class="plan-step" id="step-two">
                            <?php /* ?><h3>الخطوى الثانية</h3>
                            <div class="sub-title">الرجاء اختيار طريقة الدفع المفضّلة</div>

                            <div class="payment-options">
                                <button type="button" class="btn btn-sm" id="cc-payment"><i class="fas fa-credit-card"></i> Credit Card</button>

                                <div class="PT_express_checkout d-none"></div>
                                <button type="button" class="btn btn-sm" id="phone-payment"><i class="fas fa-mobile"></i> Mobile Payment</button>
                            </div>
                            <?php */ ?>
                        </div>

                        {{--<div class="plan-step collapse" id="step-three">--}}
                        {{--<h3>Step 3</h3>--}}
                        {{--<div class="sub-title">Please fill your billing address</div>--}}

                        {{--<div class="billing-form">--}}
                        {{--<div class="error-container">--}}
                        {{--@if($errors->any())--}}
                        {{--<div class="alert alert-danger">--}}
                        {{--@foreach ($errors->all() as $error)--}}
                        {{--<div>{{ $error }}</div>--}}
                        {{--@endforeach--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-lg-4 col-md-6">--}}
                        {{--{!! Form::open(['method' => 'POST', 'url' => '/account/billing', 'id' => 'billing-form']) !!}--}}
                        {{--<div class="sm-form-row">--}}
                        {{--{!! Form::textarea('full_address', Auth::user()->billing ? Auth::user()->billing->address : null, ['id' => 'billing-full-address', 'required', 'placeholder'--}}
                        {{--=> 'Full Address']) !!}--}}
                        {{--</div>--}}

                        {{--<div class="sm-form-row">--}}
                        {{--{!! Form::text('phone_number', Auth::user()->billing ? Auth::user()->billing->phone_number : null, ['id' => 'billing-phone-number', 'required', 'placeholder'--}}
                        {{--=>--}}
                        {{--'Phone Number']) !!}--}}
                        {{--</div>--}}

                        {{--<div class="sm-form-row">--}}
                        {{--{!! Form::text('city', Auth::user()->billing ? Auth::user()->billing->city : null, ['id' => 'billing-city', 'required', 'placeholder' => 'City']) !!}--}}
                        {{--</div>--}}

                        {{--<div class="sm-form-row">--}}
                        {{--{!! Form::text('state', Auth::user()->billing ? Auth::user()->billing->state : null, ['id' => 'billing-state', 'required', 'placeholder' => 'State']) !!}--}}
                        {{--</div>--}}

                        {{--<div class="sm-form-row">--}}
                        {{--{!! Form::select('country', $countries,  Auth::user()->billing ? Auth::user()->billing->country_id : null, ['id' => 'billing-country', 'class' =>--}}
                        {{--'selectpicker', 'required', 'title' =>--}}
                        {{--'Country',--}}
                        {{--'data-live-search' => true])--}}
                        {{--!!}--}}
                        {{--</div>--}}

                        {{--<div class="sm-form-row">--}}
                        {{--{!! Form::text('postal_code', Auth::user()->billing ? Auth::user()->billing->postal_code : null, ['id' => 'billing-postal-code', 'required', 'placeholder' =>--}}
                        {{--'Postal Code']) !!}--}}
                        {{--</div>--}}

                        {{--<div class="sm-form-row">--}}
                        {{--{!! Form::submit('Subscribe') !!}--}}
                        {{--</div>--}}
                        {{--{!! Form::close() !!}--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--@if(Auth::user()->has_subscription)--}}
                            {{--<div class="cancel-subscription">--}}
                                {{--<a href="javascript:" data-toggle="modal" data-target="#subscription-cancel-modal">إلغاء الاشتراك</a>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    </div>
                </div>
            </div>
        </div>

        <div class="container mt-30">
            <div class="row">
                <div class="col-lg-12">
                    <div class="logout text-right">
                        <a href="/logout" class="btn btn-danger full-width">تسجيل الخروج</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    jQuery('div[data-target="#subscription-form-modal"]').on('click', function(){
        var subId = jQuery(this).parents('.renew-subscription').attr('data-sub-id');
        jQuery("#shipping_form").find('#subscription_id').val(subId);
    });

    jQuery('.cancel_subscription').on('click', function(){

        var comf = confirm('are you sure');
        if( comf ){

            $.ajax({
                url: "/subscription/cancel",
                type: "POST",
                beforeSend: function (request) {
                    request.setRequestHeader("Content-Type", "application/json");
                    request.setRequestHeader("x-li-format", "json");
                },
                data: {},
                success: function (output) {
                    window.location.reload();
                },
                error: function (output) {
                    console.log("error");
                }
            });
        } else {
            console.log('action canceled');
        }
        
    });
</script>
@endsection