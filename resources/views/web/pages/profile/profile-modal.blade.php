<div class="modal fade profile-modal" id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="ProfileModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body m-header">
                <div class="row">
                    <div class="col-3">
                        <img src="{{ '/assets/img/logo-mallet.png' }}" alt="">
                    </div>
                    <div class="col-9">
                        <div class="profile-summary">
                            <p class="smashi">SMASHI</p>
                            @if(Auth::user()->has_subscription)
                                <p class="sub">SUBSCRIPTION</p>
                                <p class="cost">AED {{ Auth::user()->subscriptions->first()->price }} / <span>{{ Auth::user()->subscriptions->first()->name }}</span></p>
                                <hr>
                                <p class="email">{{ Auth::user()->email }}</p>
                            @else
                                <p class="sub">لا يوجد لديك حساب في SMASHI TV</p>
                                <hr>
                                <p class="email">{{ Auth::user()->email }}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="accordion">
                    <div class="info-card">
                        <div class="info-card-header" id="headingOne">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                المعلومات <strong>الشخصية</strong>
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent=".accordion">
                            <div class="info-card-body">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="password-change" data-toggle="tab" href="#password-change-form" role="tab" aria-controls="profile" aria-selected="false">تغيير كلمة السرّ</a>
                                    </li>
                                    {{--<li class="nav-item">--}}
                                    {{--<a class="nav-link" id="email-change" data-toggle="tab" href="#email-change-form" role="tab" aria-controls="home" aria-selected="true">Change Account<br>--}}
                                    {{--<span>Email</span></a>--}}
                                    {{--</li>--}}
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="password-change-form" role="tabpanel" aria-labelledby="password-change">
                                        <div class="response-container"></div>
                                        {!! Form::open(['url' => '/account/update/password', 'id' => 'change-password-form', 'method' => 'POST']) !!}
                                        @if($errors->any())
                                            <div class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                    <div>{{ $error }}</div>
                                                @endforeach
                                            </div>
                                        @endif
                                        <div class="sm-form-row">
                                            {!! Form::password('current-password', ['id' => 'current-password', 'required', 'placeholder' => 'كلمة الدخول*']) !!}
                                            <div class="forgot-password clearfix">
                                                <a href="{{ '/password/reset' }}" class="hover-orange">نسيت كلمة الدخول؟</a>
                                            </div>
                                        </div>

                                        <div class="sm-form-row">
                                            {!! Form::password('password', ['id' => 'password', 'required', 'placeholder' => 'كلمة الدخول*']) !!}
                                        </div>

                                        <div class="sm-form-row">
                                            {!! Form::password('confirm-password', ['id' => 'confirm-password', 'required', 'placeholder' => 'كلمة الدخول*']) !!}
                                        </div>

                                        <div class="text-center">
                                            {!! Form::submit('سجل') !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="tab-pane fade" id="email-change-form" role="tabpanel" aria-labelledby="email-change">...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="info-card">
                        <div class="info-card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    @if(Auth::user()->has_subscription)
                                        <strong>تغيير</strong> الباقة
                                    @else
                                        <strong>أختر</strong> الباقة
                                    @endif
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent=".accordion">
                            <div class="info-card-body">
                                <div class="subscriptions">
                                    <div class="row">
                                        @if(Auth::user()->subscriptions->count() > 0)
                                            @foreach($subscriptions as $subscription)
                                                @if($subscription->id != Auth::user()->subscriptions->first()->id)
                                                    <div class="col-sm-4">
                                                        @php $change_plan = false; $pointer = false @endphp
                                                        @include('web.pages.profile.subscription.partial.subscription-box')
                                                    </div>
                                                @endif
                                            @endforeach
                                        @else
                                            @foreach($subscriptions as $subscription)
                                                <div class="col-sm-4">
                                                    @php $change_plan = false; $pointer = true @endphp
                                                    @include('web.pages.profile.subscription.partial.subscription-box')
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <button class="cancel_subscription btn btn-link collapsed" >
                إلغاء الاشتراك
            </button>
            </div>
        </div>
    </div>
</div>