<!doctype html>
<html lang="en">

@include('web.partials.head')

<script src="{{'/assets/js/main.js'}}" type="application/javascript"></script>

<script src="https://consent-staging.tpay.me/script/ConsentScript?epochTimeSpan={{ $message }}&signature={{ $signature }}"></script>

<body>

<div id="consent-dictionary" data-value="{{ json_encode($consent_dictionary) }}"></div>

<script type="text/javascript">
    var consentDictionary = $("#consent-dictionary").data('value');

    TPay.Subscription.RedirectToConsent(consentDictionary);
</script>


</body>
</html>