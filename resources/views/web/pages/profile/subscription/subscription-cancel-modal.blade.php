<div class="modal fade subscription-cancel-modal" id="subscription-cancel-modal" tabindex="-1" role="dialog" aria-labelledby="SubscriptionCancelModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">إلغاء الاشتراك</h5>
            </div>
            <div class="modal-body m-header">
                هل أنت متأكد من رغبتك في إلغاء الاشتراك؟
            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-6">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق</button>
                        </div>
                        <div class="col-6 text-left">
                            <a href="{{ '/subscription/cancel' }}" class="btn btn-danger">إلغاء</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>