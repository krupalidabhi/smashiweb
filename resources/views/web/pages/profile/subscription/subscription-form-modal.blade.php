<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<div class="modal fade access-modal subscription-form-modal" id="subscription-form-modal" role="dialog" aria-labelledby="SubscriptionCancelModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h5 class="modal-title"></h5> -->
            </div>
            <form id="shipping_form" method="post" action="payment/proceed">
                @csrf
                <div class="modal-body m-header">
                        <div class="container">
                            <div class="row">
                                    
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    
                                    <h4>{{__('messages.personal_info')}}</h4>

                                    <div class="form-group row">
                                        <label for="email" class="col-sm-12 col-form-label">{{__('messages.mail_field')}}</label>
                                        <div class="col-sm-12">
                                            <input required type="email" class="form-control" placeholder="" id="email" name="email" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="tel" class="col-sm-12 col-form-label">{{__('messages.mobile_field')}}</label>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <input required type="tel" class="form-control" placeholder="" id="tel_code" name="tel_code" />
                                                </div>
                                                <div class="col-sm-9" style="padding-right: 0;">
                                                    <input required type="tel" class="form-control" placeholder="" id="tel" name="tel" />
                                                    <input type="hidden" name="subscription_id" id="subscription_id">    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <label>{{__('messages.address_info')}}</label>

                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label for="country" class="col-sm-12 col-form-label">{{__('messages.country_field')}}</label>
                                            
                                            <select class="form-control country-select" id="country" name="country" required>
                                                <option value="">--</option>
                                                <?php

                                                    // foreach ($countrySelectArray as $key => $value) {
                                                    //     echo '<option value="'.$key.'">'.$value.'</option>';
                                                    // }

                                                    foreach ($countries as $key => $value) {
                                                        echo '<option value="'.$key.'">'.$value.'</option>';
                                                    }

                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="state" class="col-sm-12 col-form-label">{{__('messages.state_field')}}</label>
                                            <select class="form-control country-select" id="state" name="state" required>
                                                <option value="">--</option>
                                            </select>
                                        </div>
                                        
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label for="city" class="col-sm-12 col-form-label">{{__('messages.city_field')}}</label>
                                            <select class="form-control country-select" id="city" name="city" required>
                                                <option value="">--</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-6">

                                            <?php

                                            /*$result = json_decode(file_get_contents('http://ip-api.io/json/157.36.173.144'));
                                            echo '<pre>';
                                            print_r($result);
                                            die;*/
                                            ?>

                                            <div class="input-group mb-3">
                                                <label for="email" class="col-sm-12 col-form-label">
                                                    {{__('messages.zip_field')}}
                                                </label>
                                                <input required readonly type="text" class="form-control" placeholder="" id="zip_code" name="zip_code">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text get-current-location" style="cursor:pointer;">
                                                        <!-- <img src="{{'/assets/img/gps.png'}}" width="20px" alt="gps"> -->
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                                
                                            </div>

                                            <!-- <label for="email" class="col-sm-12 col-form-label">{{__('messages.zip_field')}}</label>
                                            <input required type="text" class="form-control" placeholder="" id="zip_code" name="zip_code"> -->
                                        </div>
                                    </div>
                                    <?php /* <h4>{{__('messages.coupon_info')}}</h4>

                                    <div class="form-group row">
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" placeholder="" id="coupon" />    
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="button" class="btn" value="{{__('messages.apply_info')}}" />
                                        </div>
                                    </div> */ ?>
                                
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-6">
                                <button type="submit" class="btn btn-sucess">تقدم</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق</button>
                            </div>
                            <div class="col-6 text-left">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<video
id="examplePlayer"
class="video-js vjs-default-skin"
width="640"
height="264"
poster="http://vjs.zencdn.net/v/oceans.png"
preload="auto"
controls>
<source src="/demo/a.mp4" type='video/mp4' />
<source src="http://vjs.zencdn.net/v/oceans.webm" type='video/webm' />
<source src="http://vjs.zencdn.net/v/oceans.ogv" type='video/ogg' />
</video>

<script src="https://cdnjs.cloudflare.com/ajax/libs/piwik/3.14.1/piwik.js" integrity="sha512-Zz4l73Tbiz2Xaq4FDYbFJHzCoIwOvAZ1grzHzYV4O64DrNHy7+v4wjZ7nlgMuKktsgB1a9Oq95AFdIvF8MNfbw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-ga/0.4.2/videojs.ga.min.js" integrity="sha512-30ALoOs6XlINShUAhrRuVULWNK/FBV7dzLbc41Ozexlrl9CF0TmwQVReMkE3mBiDF6JpWZF/uygxZ3KemX8Mew==" crossorigin="anonymous"></script>

<script>
    jQuery('#country').select2();
    jQuery('#state').select2();
    jQuery('#city').select2();

    function get_location_using_lat_long(latitude = null, longitude = null){
        if (latitude == null || longitude == null) {
            return;
        }

        $.ajax({
            url: '/location/current/' + latitude + '/' + longitude,
            success: function (data) {

                jQuery('body #subscription-form-modal #zip_code').val(data);

            }
        });
    }

    function showPosition(position){

        lat = position.coords.latitude;
        long = position.coords.longitude;
        google_current_loc = position.coords;

        get_location_using_lat_long(lat, long);
    }

    jQuery('#subscription-form-modal').on('click', '.get-current-location', function(){

        navigator.permissions.query({
            name: 'geolocation'
        }).then(function (result) {
            if (result.state == 'granted') {                
                    navigator.geolocation.getCurrentPosition(showPosition);

            } else if (result.state == 'prompt') {
                
                navigator.geolocation.getCurrentPosition(showPosition);
                
            } else if (result.state == 'denied') {

            }
            result.onchange = function () {

                msg = result.state;

                if (msg == 'granted') {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else if (msg == 'denied') {
                    
                }
            }
        });
    });

    $('#country').on('select2:select', function (e) {

        var value = e.params.data;

        $.ajax({
            url: '/country/state/get',
            method: 'post',
            data: {'country': e.params.data.id},
            success: function (data) {           
               
                
                $("#state").select2({
                    data : data
                });
            }
        });
    });

     var state = $('#state').val();
     if(state != ""){
        $('#state').trigger('change');
     }

    $('#state').on('select2:select', function (e) {
        var value = e.params.data;

        $.ajax({
            url: '/state/city/get',
            method: 'post',
            data: {'state': e.params.data.id},
            success: function (data) {
                $("#city option").remove();
                $("#city").select2({
                    data : data
                });               
            }
        });
    });

    var player = videojs('examplePlayer');
    player.ga();
    
    /** load GoogleAnalytics */
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-XXXX-Y', { 'cookieDomain': 'none' });
    ga('send', 'pageview');

    // player.analytics();
</script>