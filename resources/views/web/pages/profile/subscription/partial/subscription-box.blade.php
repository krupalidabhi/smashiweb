<div class="renew-subscription @if(isset($change_plan) && $change_plan) change-plan @endif @if(isset($pointer) && $pointer) choosable @endif" data-sub-id="{{ $subscription->id }}">
    <div class="square">
        @if(isset($pointer) && $pointer)
            <div class="cursor-pointer" data-toggle="modal" data-target="#subscription-form-modal">
                <span>{{ $subscription->name }}<br><strong>{{ __('currency.AED') }} {{ __( \App\Helpers\Minion::changeNumberFormat( $subscription->price ) )}}</strong></span>
            </div>
        @else
            <div class="cursor-auto">
                <span>{{ $subscription->name }}<br><strong>{{ __('currency.AED') }} {{ \App\Helpers\Minion::changeNumberFormat( $subscription->price ) }}</strong></span>
            </div>
        @endif
    </div>
</div>