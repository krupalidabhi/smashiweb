@extends('web.master')

@section('content')
<?php
$poster_url='';
if(empty($main_video->poster_url))
{
$poster_url=$main_video->poster;
}
else
{
$poster_url=$main_video->poster_url;
}
 $hide_tag=0;
?>

<style type="text/css">
.hover_date {
    display: inline-block;
    position: absolute;
    z-index: 2;
    padding: 10px;
    background: #437dfa61;
}

.hover_date:hover{
    background: #437dfa;
}

.hover_image i.live_popup {
    cursor: pointer;
    position: absolute;
    font-size: 100%;
    left: 0.4%;
    top: 0.4%;
}
@media screen and (min-width: 1200px){
    .hover_date, .hover_image i.live_popup {
        font-size: 1.6em;
    }
}

body.dark .page.page-article .article-meta .published-date span{
    margin: 0;
}
</style>
<?php

function url_exists($url) {
    // if (!$fp = curl_init($url)) return false;
    // return true;

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($code == 200) {
        $status = true;
    } else {
        $status = false;
    }
    curl_close($ch);
    return $status;

    /*$allHeaders = get_headers($url);

    if( $allHeaders[0] == 'HTTP/1.1 200 OK' ){
      return true;
    } else {
      return false;
    }*/
}

?>
    <div class="page page-article">


        <div class="top-video-container">
            <div class="container">
                <div class="row">
                    <div class="col-11 col-centered">
                        <div class="top-video video-hover-parent">
                            <?php if( !empty($main_video->published_on) ){ ?>
                                <div class="hover_date">
                                    <span title="Published On"><?php echo $main_video->published_on; ?></span>
                                </div>
                            <?php } ?>

                            <?php if( $main_video->under_payment == '1' ){ ?>
                                <div class="hover_image" style="display: none;">
                                    
                                    <img calss="fade" src="{{'/assets/img/sign_up_paywall.png'}}" alt="">
                                    <?php
                                        if( "{$UserType}" == 'guest' ){
                                            ?>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#access-login-modal"></a>
                                            <?php
                                        } else {
                                        ?>
                                        <a href="/account/#step-one"></a>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <video crossOrigin='true' id="video-{{ $main_video->id }}" data-setup='{"loop": true}' class="overlay-log video-js vjs-default-skin vjs-16-9" controls
                                   data-setup="{}" autoplay poster="{{ $poster_url }}">
                                <source src="{{ $main_video->link }}" type="application/x-mpegURL" label="">

                                  <?php

                                  $langSub = ['ar' => 'Arabic', 'en' => 'English'];
                                  $arr_videoname = explode('/', $main_video->link);

                                  foreach ($langSub as $key => $value) {
                                    $vodName = $arr_videoname[5];
                                    $vttUrl = "https://s3.eu-central-1.amazonaws.com/smashitranslate/$vodName/$vodName$key.vtt";

                                    if( url_exists($vttUrl) ){
                                      echo "<track kind='captions' src='".$vttUrl."' srclang='".$key."' label='".$value."' />";
                                    }
                                  }

                                  ?>

                                <!-- <source src=" https://528dc4ef17d725ed.mediapackage.eu-central-1.amazonaws.com/out/v1/a60d84a4018f462389336c917a34fd14/index.m3u8" type="application/x-mpegURL" label=""> -->
                            </video>
                        </div>
<span id="duration"></span>

            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8 col-12" style="padding: 0;">
                        <div class="video-title">
                            <h1>{{ $main_video->title }} </h1>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-12" style="padding: 0;">
                        <div class="article-meta mt-20 mb-0 text-left">
                            <div class="share-on-social">
                                <ul class="inline-list">
                                    <li><a href="https://twitter.com/share/?text={{ $main_video->title }}&url={{ URL::current() }}"class="hover-black js-social-share" data-social="twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{ URL::current() }}&title={{ $main_video->title }}&source=SMASHITV" class="hover-black js-social-share"><i class="fab fa-linkedin-in" data-social="linkedin"></i></a>
                                    </li>
                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}&display=popup" class="hover-black js-social-share"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="mailto:example@hello.com?body={{ $main_video->title }} {{ URL::current() }}"
                                           class="hover-black"><i class="fas fa-envelope"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 text-right" style="padding: 0px;">
                        <div class="article-meta">
                            <div class="published-date">
                                <!-- <i class="far fa-clock"></i> -->
                                <!-- <span>{{ $main_video->created_at->format('d F, Y') }}</span> -->
                                <span> {{ App\Helpers\Minion::arabic_date_format($main_video->created_at)}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                @if($tags->count() > 0 && $hide_tag!=0)
            <div class="row">
                <div class="col-12 text-right">
                    <div class="video-tags">
                   <?php foreach($tags as $tag){
                       $tag_arr = json_decode( $tag->name);
                       foreach($tag_arr as $key => $value){
                            if(strlen($value) != mb_strlen($value, 'utf-8')){
                                $val_arr[]=$value;
                            }
                       }
                    }
                    if(!empty($val_arr)){
                        // echo '<a>'.implode(", ",$val_arr).'</a>';
                        foreach($val_arr as $tag){ // iterate
                            $arr[] = '<a href=/search?q='.$tag.'>'.$tag.'</a>'; //turn it into link
                        }
                        $final = implode(", ",$arr); //turn it back to string
                        echo $final;
                    } ?>
                    </div>
                </div>
            </div>
            @endif


                <div class="row">
                    <div class="col-12" style="padding: 0;">
                        <div class="video-body">
                            {{ $main_video->body }}
                        </div>
                    </div>
                </div>

            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-content">
            
            @if($videos->count() > 0)
                <div class="container mt-30">
                    <div class="row">
                        <div class="col-lg-12 text-center relate-vod">
                            <h2 class="text-line">{{ __('other.relatedvid') }}</h2>
                            <!-- <hr class="sm-hr"> -->
                        </div>
                    </div>
                    <div id="video-data">
                         @include('web.partials.single-video-card')
                    </div>
                </div>
            @endif
        </div>

    </div>
    <script type="text/javascript">
        load_more_scroll();

        function popup_image_toggle($show = false){
        if( !$show ){
            jQuery('.hover_image').hide();
        } else {
            // jQuery('.hover_image').removeClass('hidden');
            var video_js = $(".video-js");
            video_js.each(function (i) {
                var player = videojs($(this)[0]);
                //player.pause();
            });

            jQuery('.hover_image').show("scale",{}, 200);
        }
    }

    jQuery(document).ready(function(){
        $inc = 0;
        // Seconds
        $popupInterval = 60;
        popupStart = true;
        userType = "{{$UserType}}";

        jQuery('.live_popup').on('click', function(){
            $inc = 0;
            popup_image_toggle(false);
        });

        switch(userType){
            case 'guest':        
                popupStart = true;
                break;
            case 'login':
                popupStart = true;
                break;
            default:
                popupStart = false;
                break;
        }

        setInterval(function(){

            if( popupStart ){
                $inc++;
                if($inc%$popupInterval == 0){
                    if( !$("p").is(":visible") ){
                        popup_image_toggle(true);
                    }
                    
                }
            }

        }, 1000);
    });

    var id = "video-{{ $main_video->id }}";
    var player = videojs(id);
    var options = {
        id: 'content_video',
        adTagUrl: 'https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator=[timestamp]'
    };
    player.ima(options);
    </script>
@endsection
