@extends('web.master')

@section('content')

<?php
$poster_url='';
if(empty($main_video->poster_url))
{
$poster_url=$main_video->poster;	
}	
else
{
$poster_url=$main_video->poster_url;		
}	

?>



    <div class="page page-article">


        <div class="top-video-container">
            <div class="container">
                <div class="row">
                    <div class="col-11 col-centered">
                        <div class="top-video">
                            <video id="video-{{ $main_video->id }}" class="video-js vjs-default-skin vjs-16-9" controls
                                   data-setup="{}" autoplay muted poster="{{ $poster_url }}">
                                <source src="{{ $main_video->link }}" type="application/x-mpegURL" label="">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8 col-12">
                        <div class="video-title">
                            <h1>{{ $main_video->title }}</h1>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-12">
                        <div class="article-meta mt-20 mb-0 text-left">
                            <div class="share-on-social">
                                <ul class="inline-list">
                                    <li>
                                        <a href="https://twitter.com/intent/tweet/?url={{ URL::current() }}&text={{ $main_video->title }}"
                                           class="hover-black js-social-share" data-social="twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{ urlencode(URL::current()) }}&source=SMASHITV" class="hover-black js-social-share"><i
                                                    class="fab fa-linkedin-in" data-social="linkedin"></i></a>
                                    </li>
                                    <li><a href="javascript:" class="hover-black js-social-share"
                                           data-link="{{ URL::current() }}" data-social="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="mailto:example@hello.com?body={{ $main_video->title }} {{ URL::current() }}"
                                           class="hover-black"><i class="fas fa-envelope"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 text-right">
                        <div class="article-meta">
                            <div class="published-date">
                                <i class="far fa-clock"></i>
                                <span>{{ $main_video->created_at->format('d F, Y') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="video-body">
                            {{ $main_video->body }}
                        </div>
                    </div>
                </div>

            </div>

            @if($videos->count() > 0)
                <div class="container mt-30">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <hr class="sm-hr">
                        </div>
                    </div>
                    @foreach($videos as $video_chunks)
                        <div class="category-videos">
                            <div class="row">
                                @foreach($video_chunks as $video)
                                    <div class="col-md-3 col-sm-6 text-right">
                                        @include('web.partials.video-card')
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>

    </div>

@endsection