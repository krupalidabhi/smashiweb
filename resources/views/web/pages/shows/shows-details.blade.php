@extends('web.master')

@section('content')
<div  class="container">
    <div class="page_title">
       <h1> {{__($ar_slug)}}</h1>
       <hr>
    </div>
</div>
<div  class="container">
    <div class="parent-presenter-grid">
        @include('web.partials.shows-details')
    </div>
  
</div>

<!-- load more gif and script -->
<!-- <script type="text/javascript">
	load_more_scroll();
</script> -->

@endsection