@extends('web.master')

@section('content')
<div class="page page-article">

    <div class="container">
        <div class="top-video-container">
            <div class="container">
                <div class="row">
                    <div class="col-11 col-centered">
                        <div class="top-video">
                            <video id="video-{{ $show_video->id }}" class="overlay-log video-js vjs-default-skin vjs-16-9" controls
                                   data-setup="{}" autoplay poster="">
                                <source src="{{ $show_video->show_link }}" type="application/x-mpegURL" label="">
                                <!-- <source src=" https://528dc4ef17d725ed.mediapackage.eu-central-1.amazonaws.com/out/v1/a60d84a4018f462389336c917a34fd14/index.m3u8" type="application/x-mpegURL" label=""> -->
                            </video>
                        </div>

                        <span id="duration"></span> 

                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-sm-8 col-12" style="padding: 0;">
                                    <div class="video-title">
                                        <h1>{{ $main_video->title }} </h1>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 col-12" style="padding: 0;">
                                    <div class="article-meta mt-20 mb-0 text-left">
                                        <div class="share-on-social">
                                            <ul class="inline-list">
                                                <li>
                                                    <a href="https://twitter.com/intent/tweet/?url={{ URL::current() }}&text={{ $main_video->title }}"
                                                       class="hover-black js-social-share" data-social="twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>


                                                <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{ URL::current() }}&source=SMASHITV" class="hover-black js-social-share"><i
                                                                class="fab fa-linkedin-in" data-social="linkedin"></i></a>
                                                </li>


                                                <li><a href="javascript:" class="hover-black js-social-share"
                                                       data-link="{{ URL::current() }}" data-social="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="mailto:example@hello.com?body={{ $main_video->title }} {{ URL::current() }}"
                                                       class="hover-black"><i class="fas fa-envelope"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 text-right" style="padding: 0px;">
                                    <div class="article-meta">
                                        <div class="published-date">
                                            <!-- <i class="far fa-clock"></i> -->
                                            <!-- <span>{{ $main_video->created_at->format('d F, Y') }}</span> -->
                                            <span> {{ App\Helpers\Minion::arabic_date_format($main_video->created_at)}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr/>
                        
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <div class="container">
        <div class="top-video-container">
            <div class="container">
                <div class="row">
                    <div class="col-11 col-centered">
                            <div class="category-videos">
                                <div class="row" id="video-data">
                                 
                                    @include('web.partials.archive-video-card')
                                       
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection