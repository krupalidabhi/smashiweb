@extends('web.master')

@section('content')
<div  class="container">
    <div class="page_title">
       <h1> {{__($ar_slug)}}</h1>
       <hr>
    </div>
</div>
<div  class="container">
    <div class="category-videos">
        <div class="row" id="video-data">
         
            @include('web.partials.archive-video-card')
               
        </div>
    </div>
</div>

<?php
    
    if( $ar_slug != 'menu.latest' ){
      ?>
      <!-- load more gif and script -->
      <script type="text/javascript">
        load_more_scroll();
      </script>
      <?php
    }

?>
@endsection