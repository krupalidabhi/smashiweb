@extends('web.master')

@section('content')
<div class="page page-search no-search-results">
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-8 col-centered">
                    <div class="section-title">
                        <h1>نتائج البحث عن: <span>{{ Request::get('q') }}</span></h1>
                    </div>

                    <p class="number-of-results">عدد النتائج: <span>0</span></p>

                    <p class="apology-text">عذراً ولكن لم نجد نتائج تلائم بحثك. الرجاء المحاولة مرة أخرى.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-7 col-md-8 col-sm-10 col-centered">
                    {!! Form::open(['url' => '/search', 'method' => 'GET', 'id' => 'search-again-form']) !!}
                        <div class="sm-form-row">
                            {!! Form::text('q', null, ['placeholder' => 'ابحث...', 'required']) !!}
                        </div>

                        <div class="sm-form-row text-center">
                            {!! Form::submit('ابحث', ['class' => 'sm-btn orange small-btn']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</div>

@endsection