@extends('web.master')

@section('content')
    <div class="page page-search">
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-right">
                        <div class="section-title">
                            <div class="row">
                                <div class="col-md-6">
                                    <h1>نتائج البحث عن: <span>{{ Request::get('q') }}</span></h1>
                                </div>

                                <div class="col-md-6">
                                    <p class="number-of-results">عدد النتائج: <span>{{ $total_videos }}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="category-videos">
                    @foreach($videos as $videos_chunk)
                        <div class="row">
                            @foreach($videos_chunk as $video)
                                <div class="col-md-4 col-sm-6 text-right">
                                @include('web.partials.video-card')
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>

                <div class="load-more-container">
                    <div class="row">
                        {{--<div class="col-12 text-center">--}}
                            {{--<a href="javascript:" class="sm-btn orange">المزيد</a>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection