@extends('web.master')

@section('content')
    <div class="page page-contact">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!--                    <div class="breadcrumbs-container text-right">-->
                    <!--                        <div class="title-holder">-->
                    <!--                            <h1></h1>-->
                    <!--                        </div>-->
                    <!--                        <div class="breadcrumbs">-->
                    <!--                            <ul class="inline-list">-->
                    <!--                                <li><a href="/" class="hover-black">الصفحة الرئيسية</a></li>-->
                    <!--                                <li><span>الحساب الخاص</span></li>-->
                    <!--                            </ul>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <div class="section-title">
                        <div class="row">
                            <div class="col-6 col-centered">
                                <h1 class="text-line">اتصل بنا</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-centered">
                    {{--<p class="text-right">أم يتم تونس التجارية, هو مكن أسابيع الثقيلة, في العالم، وهولندا، فعل. قدما الطريق غير هو, وصل مئات أراض مع. تحت تم مئات السبب ارتكبها, خيار فقامت ان جهة, الا--}}
                        {{--عل تُصب الأرواح. الا وبدون وصافرات الأراضي إذ. قام لدحر وحلفاؤها قد, عل على تسبب وجهان الثقيلة. سكان الأوروبية كان ماية بال هو.</p>--}}
                        {!! Form::open(['url' => '/contact', 'method' => 'POST', 'id' => 'contact-form']) !!}
                        <div class="sm-form-row">
                            {!! Form::label('contact-name', 'الاسم*', ['class' => 'orange','style'=>'color:#FFF;']) !!}
                            {!! Form::text('name', null, ['id' => 'contact-name', 'required']) !!}
                        </div>

                        <div class="sm-form-row">
                            {!! Form::label('contact-email', 'البريد الالكتروني*', ['class' => 'orange','style'=>'color:#FFF;']) !!}
                            {!! Form::email('email', null, ['id' => 'contact-email', 'required']) !!}
                        </div>

                        <div class="sm-form-row">
                            {!! Form::label('contact-message', 'الرسالة*', ['class' => 'orange','style'=>'color:#FFF;']) !!}
                            {!! Form::textarea('message', null, ['id' => 'contact-message', 'required', 'rows' => 10]) !!}
                        </div>

                        <div class="sm-form-row text-center">
                            {!! Form::submit('أرسل', ['class' => 'sm-btn small-btn orange']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection