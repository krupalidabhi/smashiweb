@extends('web.master')

@section('content')

    <div class="page page-home">
        <div class="top-video-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 col-centered">
                        <div class="top-video">
                            <div class="embed-container">
                                <div id="my-video" data-video="{{ $live_video->video }}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content mt-30">
            <div class="container">
{{--                @foreach($videos as $video_chunks)--}}
                    <div class="category-videos">
                        <div class="row">
                            @foreach($videos as $video)
                                <div class="col-md-4 col-6 text-right">
                                    @include('web.partials.video-card')
                                </div>
                            @endforeach
                        </div>
                    </div>
                {{--@endforeach--}}
            </div>
        </div>
    </div>

@endsection