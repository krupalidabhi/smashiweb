@extends('web.master')

@section('content')
    <div class="page page-access">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <div class="row">
                            <div class="col-6 col-centered">
                                 @if(isset($_COOKIE['message']))
                                    <div class="alert alert-success">  {{$_COOKIE['message']}} </div>
                                    @endif
                                <h1 class="text-line">تسجيل الدخول</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-centered">
                    <div class="access-container">
                        {!! Form::open(['url' => '/login', 'id' => 'login-form', 'method' => 'POST']) !!}
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <div class="sm-form-row">
                            <label for="login-email">البريد الالكتروني*</label>
                            {!! Form::email('email', null, ['id' => 'login-email', 'required']) !!}
                            @csrf
                        </div>

                        <div class="sm-form-row">
                            <label for="login-password">كلمة الدخول*</label>
                            {!! Form::password('password', ['id' => 'login-password', 'required']) !!}
                        </div>

                        <div class="sm-form-row">
                            {!! Form::submit('سجل') !!}
                        </div>
                        {!! Form::close() !!}

                        <div class="forgot-password clearfix">
                            <div class="left-element">
                                <a href="{{ '/password/reset' }}" class="hover-orange">نسيت كلمة الدخول؟</a>
                            </div>

                            <div class="right-element">
                                ليس لديك حساب؟ <a href="{{ '/register' }}" class="hover-black">التسجيل</a>
                            </div>
                        </div>

                        <h4>أو تسجيل الدخول باستخدام</h4>

                        <div class="social-media-login">
                            <a href="{{ '/auth/facebook' }}"><span>Facebook</span> <i class="fab fa-facebook-f"></i></a>
                            <a href="{{ '/auth/google' }}"><span>Google</span>
                                <i class="fab
                        fa-google"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection