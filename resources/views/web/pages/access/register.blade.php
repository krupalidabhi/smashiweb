@extends('web.master')

@section('content')
    <div class="page page-access">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <div class="row">
                            <div class="col-6 col-centered">
                                <h1 class="text-line">التسجيل</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if (session('warning'))
                    <div class="alert alert-warning">
                        {!! session('warning') !!}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {!! session('error') !!}
                    </div>
                @endif
                <div class="col-lg-6 col-centered">
                    {!! Form::open(['method' => 'POST', 'url' => '/register', 'id' => 'register-form']) !!}
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
                    <div class="sm-form-row">
                        {!! Form::label('register-full-name', 'الاسم الكامل*') !!}
                        {!! Form::text('name', null, ['id' => 'register-full-name', 'required']) !!}
                    </div>

                    <div class="sm-form-row">
                        {!! Form::label('register-email', 'البريد الالكتروني*') !!}
                        {!! Form::email('email', null, ['id' => 'register-email', 'required']) !!}
                    </div>

                    <div class="sm-form-row">
                        {!! Form::label('register-password', 'كلمة الدخول*') !!}
                        {!! Form::password('password', ['id' => 'register-password', 'required']) !!}
                    </div>

                    <div class="sm-form-row">
                        {!! Form::label('register-password-confirmation', 'تأكيد كلمة المرور*') !!}
                        {!! Form::password('password_confirmation', ['id' => 'register-password-confirmation', 'required']) !!}
                    </div>

                    <div class="sm-form-row">
                        {!! Form::submit('سجل') !!}
                    </div>
                    {!! Form::close() !!}

                    <div class="t-and-c-notice">
                        عن طريق إنشاء هذا الحساب فإنك توافق على شروطنا
                    </div>

                    <h4>أو التسجيل باستخدام</h4>

                    <div class="social-media-login">
                        <a href="{{ '/auth/facebook' }}"><span>Facebook</span> <i class="fab fa-facebook-f"></i></a> <a href="{{ '/auth/google' }}"><span>Google</span> <i class="fab
                        fa-google"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection