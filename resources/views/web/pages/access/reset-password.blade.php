@extends('web.master')

@section('content')
    <div class="page page-access">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <div class="row">
                            <div class="col-6 col-centered">
                                <h1 class="text-line">نسيت كلمة الدخول؟</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-centered">
                    {!! Form::open(['url' => '/password/reset', 'id' => 'reset-password-form', 'method' => 'POST']) !!}
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
                    {!! Form::hidden('token', $token) !!}

                    <div class="sm-form-row">
                        {!! Form::label('reset-email', 'البريد الالكتروني*') !!}
                        {!! Form::email('email', null, ['id' => 'reset-email', 'required']) !!}
                    </div>

                    <div class="sm-form-row">
                        {!! Form::label('reset-password', 'كلمة الدخول*') !!}
                        {!! Form::password('password', ['id' => 'reset-password', 'required']) !!}
                    </div>

                    <div class="sm-form-row">
                        {!! Form::label('reset-password-confirmation', 'تأكيد كلمة المرور*') !!}
                        {!! Form::password('password_confirmation', ['id' => 'reset-password-confirmation', 'required']) !!}
                    </div>

                    <div class="sm-form-row">
                        {!! Form::submit('سجل') !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection