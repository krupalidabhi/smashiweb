@extends('web.master')

@section('content')
    <div class="page page-access">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <div class="row">
                            <div class="col-6 col-centered">
                                <h1 class="text-line">نسيت كلمة الدخول؟</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-centered">
                    {!! Form::open(['url' => '/password/email', 'id' => 'forgot-password-form', 'method' => 'POST']) !!}
                    @if(session('status'))
                        <div class="alert-success alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if($errors->has('email'))
                        <div class="alert alert-danger">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                    <div class="sm-form-row">
                        {!! Form::label('forgot-email', 'البريد الالكتروني*') !!}
                        {!! Form::email('email', null, ['id' => 'forgot-email', 'required']) !!}
                    </div>

                    <div class="sm-form-row">
                        {!! Form::submit('سجل') !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection