<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<style type="text/css">
	.loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('assets/img/tenor.gif') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}

</style>

@extends('web.master')

@section('content')

    <div class="page page-home">
	<div class="loader"></div>
        <div class="top-video-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 col-centered">
                        <div class="top-video">
                            <div class="embed-container">
                                <div id="my-video" data-video="{{ $live_video->video }}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content mt-30">
            <div class="container">
{{--                @foreach($videos as $video_chunks)--}}
                    <div class="category-videos">
                        <div class="row">
                            @foreach($videos as $video)
                                <div class="col-md-4 col-6 text-right">
                                    @include('web.partials.video-card')
                                </div>
                            @endforeach
                        </div>
                    </div>
                {{--@endforeach--}}
            </div>
        </div>
    </div>

<script type="text/javascript">
$(window).load(function() {
    $(".loader").fadeOut("slow");
});
</script>

@endsection