<style type="text/css">
    .video-js .vjs-tech{
        object-fit: fill;
    }

    .vjs-big-play-button{
        display: none;
    }

    .share-on-social ul li:not(:last-child) {
        margin-left: 15px;
    }

    .share-on-social i {
        color: #f97b3e;
    }

    .showreplay {
      display: inline-block;
      position: absolute;
      z-index: 2;
      left: 0;
      padding: 5px;
    }

.hover_date {
    /*display: inline-block;*/
    display: none;
    position: absolute;
    z-index: 2;
    background: white;
    width: 14.8%;
    height: 5.9%;
    line-height: 1.7;
    text-align: center;
    font-size: 1.6vw;
    color: #555455;
    right: 7.8%;
    font-weight: 800;
    direction: ltr;
}

.hover_date img {
    position: absolute;
    right: -53%;
    width: 53%;
    top: -1px;
}

.hover_image a {
    width: 29%;
    height: 14%;
    background: transparent;
    z-index: 5;
    left: 2%;
    position: absolute;
    top: 40%;
}

.hidden{
    display: none !important;
}

.hover_image img {
    width: 100%;
    background: #d89c85c4;
}

.hover_image {
    position: absolute;
    z-index: 2;
}

.hover_image i.live_popup {
    cursor: pointer;
    position: absolute;
    font-size: 100%;
    left: 0.4%;
    top: 0.4%;
}
@media screen and (min-width: 1200px){
    .hover_date, .hover_image i.live_popup {
        font-size: 1.6em;
    }
}

.not-logged-in {
    text-align: center;
    position: absolute;
    top: 46%;
    left: 0;
    right: 0;
}

.not-logged-in a:first-child {
    margin-left: 3px;
}

.not-logged-in a:nth-child(2) {
    height: 34px !important;
    display: inline-block;
    border-right: 1px solid;
    padding-right: 9px;
}
</style>

@extends('web.master')

@section('content')

@if (session()->has('payment_errors'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <ul>
            {{session('payment_errors')}}
        </ul>
    </div>
@endif

    <div class="page page-home">
	<div class="loader"></div>
        <div class="top-video-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 col-centered">
                        <div class="top-video" data-attr="{{$frontEndChannel}}">
                            <div class="embed-container">
                                <?php if( $frontEndChannel == 'LIVE' ) { ?>
                                    <div class="hover_date">
                                        <img src="{{'/assets/img/video_time.png'}}" alt="">
                                        <span id="clock" ><?php echo " ".Date('H:i:s') . " " .Date('A'); ?></span>
                                    </div>
                                    <div class="hover_image" style="display: none;">
                                        <i class="fa fa-times live_popup"></i>
                                        <img calss="fade" src="{{'/assets/img/sign_up_paywall.png'}}" alt="">
                                        <?php 
                                            if( "{$UserType}" == 'guest' ){
                                                ?>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#access-login-modal"></a>
                                                <?php
                                            } else {
                                            ?>
                                            <a href="/account/#step-one"></a>
                                        <?php } ?>
                                    </div>
                                <?php } else { ?>    

                                <?php } ?>
                                <video id="video" class="overlay-log video-js vjs-default-skin vjs-16-9" controls
                                   data-setup='{"aspectRatio":"16:9", "fluid": true, "loop": true}' crossorigin="anonymous" autoplay poster="">
                                    <source src="{{$LiveVideoUrl}}" crossorigin="anonymous" type="application/x-mpegURL" label="">
                                </video>
                            </div>
                        </div>
                        <br/>

                        <div class="share-on-social">
                            <ul class="inline-list" style="text-align: left;">
                                <li>
                                    <a href="https://twitter.com/intent/tweet/?url={{ URL::current() }}&text=SMASHITV"
                                       class="hover-black js-social-share" data-social="twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>


                                <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{ URL::current() }}&source=SMASHITV" class="hover-black js-social-share"><i
                                                class="fab fa-linkedin-in" data-social="linkedin"></i></a>
                                </li>


                                <li><a href="javascript:" class="hover-black js-social-share"
                                       data-link="{{ URL::current() }}" data-social="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="mailto:example@hello.com?body=SMASHITV {{ URL::current() }}"
                                       class="hover-black"><i class="fas fa-envelope"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>




        <div class="page-content top-gap">

            <div  class="container "><br>

                    <div class="category-videos">
                        <div class="row">
                            @foreach($archive_videos as $video)
                                <div class="col-md-4 col-6 text-right">
                                    @include('web.partials.video-card')
                                </div>
                            @endforeach
                        </div>
                    </div>

            </div>
          </div>




            </div>


<script>
    function updateClock ( )
    {
        var currentTime = new Date ( );
        var currentHours = currentTime.getHours ( );
        var currentMinutes = currentTime.getMinutes ( );
        var currentSeconds = currentTime.getSeconds ( );

        // Pad the minutes and seconds with leading zeros, if required
        currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
        currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

        // Choose either "AM" or "PM" as appropriate
        var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

        // Convert the hours component to 12-hour format if needed
        currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

        // Convert an hours component of "0" to "12"
        currentHours = ( currentHours == 0 ) ? 12 : currentHours;

        // Compose the string for display
        var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
        
        $("#clock").html(currentTimeString);
            
    }

    function popup_image_toggle($show = false){
        if( !$show ){
            jQuery('.hover_image').hide();
        } else {
            // jQuery('.hover_image').removeClass('hidden');
            jQuery('.hover_image').show("scale",{}, 200);
        }
    }

    jQuery(document).ready(function(){
        $inc = 0;
        // Seconds
        $popupInterval = 60;
        popupStart = true;
        userType = "{{$UserType}}";

        jQuery('.live_popup').on('click', function(){
            $inc = 0;
            popup_image_toggle(false);
        });

        switch(userType){
            case 'guest':        
                popupStart = true;
                break;
            case 'login':
                popupStart = true;
                break;
            default:
                popupStart = false;
                break;
        }

        setInterval(function(){

            if( popupStart ){
                $inc++;
                if($inc%$popupInterval == 0){
                    if( !$("p").is(":visible") ){
                        popup_image_toggle(true);
                    }
                    
                }
            }

            updateClock();
        }, 1000);

        // if( ChannelState != 'LIVE' ){

            MyChannelInterval = setInterval( function(){
                var video_js = $(".top-video .video-js");

                video_js.each(function (i) {

                    let player = videojs($(this)[0]);
                     if( player.duration() == 'Infinity'){
                        ChannelState = 'LIVE';
                     } else {
                        ChannelState = 'NOLIVE';
                     }
                });

                if( ChannelState != 'LIVE'){
                    $.ajax({
                        url: "/api/adminpage/checkchannelstate",
                        type: "POST",
                        async: false,
                        data: '',
                        success: function(data){
                            // var ChannelState = jQuery('.top-video').attr('data-attr');

                            if( data != ChannelState ){
                                // clearInterval(MyChannelInterval);
                                jQuery('.top-video').attr('data-attr', data)
                                //window.location.reload();
                            }
                        },
                        error:function(data){

                        }
                    });
                }
            }, 15000 )

        // }

    });
</script>

<?php 
    if( $UserType == 'guest' ){
?>
    <style>
        #access-login-modal .modal-dialog{
            min-height: calc(100vh - 60px);
            display: flex;
            flex-direction: column;
            justify-content: center;
            overflow: auto;
        }
    </style>
    <script type="text/javascript">

    $(function() {
        jQuery('#access-login-modal').modal('show');
    });
    </script>
<?php } ?>

@endsection
