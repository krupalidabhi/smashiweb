@extends('web.master')

@section('content')
    <div class="page page-generic">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title with-line">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="text-line">{{ $page->title }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-text no-top-margin">
                            {!! $page->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection