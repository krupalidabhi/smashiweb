<!DOCTYPE html>
<html dir="rtl" lang="{{ str_replace('_', '-', App::getLocale()) }}">

@include('web.partials.head')

<body dir="rtl" class="rtl dark">

<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: "<?php echo  env("FB_APP_ID", "232306637449432") ?>",
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v3.0'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Document Wrapper
============================================= -->
@include('web.partials.header')

<div id="wrapper" class="clearfix">

    @include('web.partials.overlay.search-overlay')
    @include('web.partials.overlay.access-overlay')

    @if(session('reset-status'))
        <div class="alert alert-success alert-dismissible show main-alert alert-hide" data-time="1500" role="alert">
            {{ session('reset-status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @yield('content')

</div><!-- #wrapper end -->

@include('web.partials.footer')

<!-- <link href="https://vjs.zencdn.net/5.19.2/video-js.css" rel="stylesheet">

<script src="https://vjs.zencdn.net/5.19.2/video.js"></script> -->

<!-- already commented -->
<!-- <script src="{{'/assets/js/hls.min.js?v=v0.9.1'}}"></script> -->


<!-- <script src="{{'/assets/js/videojs5-hlsjs-source-handler.min.js?v=0.3.1'}}"></script>

<script src="{{'/assets/js/vjs-quality-picker.js?v=v0.0.2'}}"></script> -->

<script>
// jQuery(document).ready(function () {
    var video_js = $(".video-js");
    video_js.each(function (i) {
        var player = videojs($(this)[0]);

        player.qualityPickerPlugin();

        player.volume(0);
        player.muted(true);
        player.play();

    });
// });
</script>

<script src="{{'/assets/js/main.js'}}" type="application/javascript"></script>
<script src="https://www.paytabs.com/express/express_checkout_v3.js"></script>

<script>
$("#newsletter").change(function(){
	
    $.ajax({
        url: "/account/newsletter_permation",
        type: "post",
        data: { newsletter :  $(this).prop('checked')  },
        success: function(data){
            $("#employees").html(data);
        }
    });
});
</script>


</body>
</html>