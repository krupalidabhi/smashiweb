<div class="blog-post animate-element">
    <div class="post-image hover-opacity-low">
        <a href="{{ '/blog/' . $post->slug }}" class="d-block">
            <img src="{{ $post->image }}" alt="">
        </a>
    </div>

    <a href="{{ '/blog/' . $post->slug }}" class="title d-block hover-black">{{ $post->title }}</a>
    <span class="published-date d-block"> <i
                class="far fa-clock"></i>{{ App\Helpers\Minion::human_date_format($post->created_at) }}</span>
    <div class="link-overlay">
        <a href="{{ '/blog/' . $post->slug }}"></a>
    </div>
</div>