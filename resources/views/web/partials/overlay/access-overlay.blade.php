<div class="modal fade access-modal" id="access-login-modal" tabindex="-1" role="dialog"
     aria-labelledby="AccessLoginModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                <div class="section-title">
                    <div class="row">
                        <div class="col-12 col-centered">
                            <h2>تسجيل دخول‬ ‫إلى‬ سماشي‬</h2>
                        </div>
                    </div>
                </div>
                <div class="intro">
                    <div class="row">
                        <div class="col-sm-10 col-12 col-centered">
                            <div class="row">
                                <div class="col-12">
                                    <p>
                                    ‫‫ ‫‫‫‫ ‫‫‫‫ ‫‫‪أهلاً بك في سماشي تي في
قم بتسجيل الدخول للاستمتاع بمشاهدة آخر الأخبار والبرامج فيما يتعلق بإدارة الأعمال والتكنولوجيا واللايف ستايل
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="access-container">
                    <div class="row">
                        <div class="col-sm-10 col-12 col-centered">
                            <div class="error-container"></div>
                            {!! Form::open(['url' => '/login', 'id' => 'login-form', 'method' => 'POST']) !!}
                            @if($errors->any() && !$errors->has('emailForget') )
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                            @endif
                            <div class="sm-form-row">
                                {!! Form::email('email', null, ['id' => 'login-email', 'required', 'placeholder' => 'البريد الالكتروني*']) !!}
                            </div>

                            <div class="sm-form-row">
                                {!! Form::password('password', ['id' => 'login-password', 'required', 'placeholder' => '‫‫كلمة‬ السر‬*']) !!}

                                <div class="forgot-password clearfix">
                                    <div class="left-element">
                                        <a href="{{ '/password/reset' }}" class="hover-orange">‫‫‫نسیت‬ كلمة‬ السر؟‬</a>
                                    </div>

                                    <div class="right-element">
                                        <input type="checkbox" name="remember_me">
                                        تخزين معلوماتي
                                    </div>
                                </div>
                            </div>

                            <div class="sm-form-row text-center">
                                {!! Form::submit('‫‫سجل‬ الآن‬') !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="separator">
                        <hr>
                    </div>

                    <div class="social-media-login">
                        <a class="a-l-fb" href="{{ '/auth/facebook' }}"><span>تسجيل الدخول باستخدام الفيسبوك
                                </span> <i class="fab fa-facebook-f"></i></a>
                        <a class="a-l-g" href="{{ '/auth/google' }}"><span>تسجيل الدخول عبر جوجل</span> <i
                                    class="fab fa-google"></i></a>
                    </div>

                    <div class="access-swap text-center">
                        <p>إذا لم يكن لديك حساب في سماشي، <a href="javascript:" data-toggle="modal" data-target="#access-register-modal">قم بإنشاء حساب جديد الآن</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade access-modal" id="access-register-modal" tabindex="-1" role="dialog"
     aria-labelledby="AccessRegisterModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="section-title">
                    <div class="row">
                        <div class="col-12 col-centered">
                            <h2> ‫‫إنشاء‬ ‫حساب‬ جدید‬ ‫على‬ ‫سماشي‬‬</h2>
                        </div>
                    </div>
                </div>
                <div class="intro">
                    <div class="row">
                        <div class="col-sm-10 col-12 col-centered">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="logo">
                                        <img src="{{ '/assets/img/logo-mallet.png' }}" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <ul class="sm-ul">
                                        <li>فترة تجريبية مجانية لمدة 7 أيام</li>
                                        <li>قناة إخبارية مباشرة</li>
                                        <li>مقاطع فيديو غير محدودة عند الطّلب</li>
                                        <li>لا يتطلّب التسجيل بطاقة إئتمان</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="access-container">
                    <div class="row">
                        <div class="col-sm-10 col-12 col-centered">
                            <div class="error-container"></div>
                            {!! Form::open(['method' => 'POST', 'url' => '/register', 'id' => 'register-form']) !!}
                            @if($errors->any() && !$errors->has('emailForget') )
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                            @endif
                            <div class="sm-form-row">
                                {!! Form::text('name', null, ['id' => 'register-full-name', 'required', 'placeholder' => 'الاسم الكامل*']) !!}
                            </div>

                            <div class="sm-form-row">
                                {!! Form::email('email', null, ['id' => 'register-email', 'required', 'placeholder' => 'البريد الالكتروني*']) !!}
                            </div>

                            <div class="sm-form-row">
                                {!! Form::password('password', ['id' => 'register-password', 'required', 'placeholder' => '‫‫كلمة‬ السر‬‬*']) !!}
                            </div>

                            <div class="sm-form-row">
                                {!! Form::password('password_confirmation', ['id' => 'register-password-confirmation', 'required', 'placeholder' => 'تأكيد كلمة السر*']) !!}

                                <div class="checkbox mt-1">
                                    {!! Form::checkbox('newsletter', 1) !!} <span>  أرغب في الاشتراك في آخر المستجدات والأخبار من سماشي </span>
                                </div>
                            </div>

                            <div class="sm-form-row">
                                <div class="g-recaptcha recaptcha_customcss" data-sitekey="<?php echo env('GOOGLE_recaptcha_public', '6Lc8b_AUAAAAADPugNOgW5cSaqFGCWaP-iAhbP65'); ?>"></div>
                            </div>
                            
                            <div class="t-and-c-notice text-center">
                                <p><a href="{{ '/terms' }}" target="_blank">‫‫‫‫‫‫  عند إنشاء حساب فإنك توافق على الشروط والأحكام‬ </a></p>
                            </div>

                            <div class="sm-form-row text-center">
                                {!! Form::submit('سجل') !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="separator">
                        <p class="text-lines">أو</p>
                    </div>

                    <div class="social-media-login">
                        <a class="a-l-fb" href="{{ '/auth/facebook' }}"><span>تسجيل الدخول باستخدام الفيسبوك</span> <i class="fab fa-facebook-f"></i></a>
                        <a class="a-l-g" href="{{ '/auth/google' }}"><span>تسجيل الدخول عبر جوجل</span> <i
                                    class="fab fa-google"></i></a>
                    </div>

                    <div class="t-and-c-notice text-center">
                        <p>{{ __('messages.by_sign_up') }} <a href="{{ '/terms' }}" target="_blank">{{ __('messages.TOU') }}</a>
                            {{ __('messages.and')}}<a href="{{ '/privacy' }}" target="_blank">{{ __('messages.PP') }}</a></p>
                    </div>
                    <div class="access-swap text-center">
                        <p>‫‫‫‫‫ھل‬ لدیك‬ ‫‫حساب‬ في‬ سماشي؟‬ ‬ <a href="javascript:" data-toggle="modal" data-target="#access-login-modal">تسجيل دخول</a></p>
                    </div>
                    {{--<div class="t-and-c-notice">--}}
                    {{--عن طريق إنشاء هذا الحساب فإنك توافق على شروطنا--}}
                    {{--</div>--}}

                    {{--<h4>أو التسجيل باستخدام</h4>--}}

                    {{--<div class="social-media-login">--}}
                    {{--<a href="{{ '/auth/facebook' }}"><span>Facebook</span> <i class="fab fa-facebook-f"></i></a> <a--}}
                    {{--href="{{ '/auth/google' }}"><span>Google</span> <i class="fab--}}
                    {{--fa-google"></i></a>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
