<div class="sm-overlay search-overlay closed" id="search-overlay">
    <div class="sm-overlay-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-centered text-right overlay-column-container">
                    <a href="javascript:" class="close-overlay-btn hover-orange" data-overlay="search-overlay"><i class="fal fa-times"></i></a>

                    {!! Form::open(['url' => '/search', 'method' => 'GET', 'id' => 'search-form']) !!}
                    {!! Form::text('q', null, ['placeholder' => 'ابحث...', 'required']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>