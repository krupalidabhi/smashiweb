<?php foreach($shows as $show){  ?>
    
    <div class="presenter-grid">
        
        <a href="javascript:" class="hover-orange" data-toggle="modal" data-target="#access-login-modal">
            <img class="img-responsive" src="<?php echo asset("storage/$show->presenter_image"); ?>"/>
        </a>

        <div class="presenter-grid-right">
            <a href="<?php echo '/shows/'.$show->slug; ?>"><h3><?php echo $show->title; ?></h3></a>
            <p>‬<?php echo $show->body; ?></p>
            <img class="img-responsive" src="<?php echo asset("storage/$show->presenter_logo"); ?>"/>      
        </div>

    </div>  

    <hr>

<?php } ?>


