<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136114794-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-125335704');
    </script>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="author" content="Strategies dC"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('web.partials.social-meta-tags')

    <link rel="apple-touch-icon" sizes="180x180" href="{{'/assets/favicon/apple-touch-icon.png'}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{'/assets/favicon/favicon-32x32.png'}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{'/assets/favicon/favicon-16x16.png'}}">
    <link rel="manifest" href="{{'/assets/favicon/site.webmanifest'}}">
    <link rel="mask-icon" href="{{'/assets/favicon/safari-pinned-tab.svg'}}" color="#f97b3e">
    <link rel="shortcut icon" href="{{'/assets/favicon/favicon.ico'}}">
    <meta name="msapplication-TileColor" content="#437efb">
    <meta name="msapplication-config" content="{{'/assets/favicon/browserconfig.xml'}}">
    <meta name="theme-color" content="#437efb">

    <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
    <script type="IN/Share" data-url="{{ URL::to('/') }}" data-counter="top"></script>

    <link rel="stylesheet" href="{{'/assets/css/style-ar.css'}}"/>
</head>