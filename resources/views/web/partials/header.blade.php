
<header id="myHeader">

	<div class="header header-top">
        <div class="top d-md-block">
            <div class="container">
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-2">
                        <div class="logo-container small text-right">
                            <a href="/" class="d-inline-block text-right"><img src="{{'/assets/img/Smashi-logo-arabic.png'}}" alt="SMASHITV logo"></a>
                        </div>
                    </div>
                    <div class="col-xl-10 col-lg-10 col-md-10 d-md-block">

                        <nav class="navbar navbar-expand-md custom-menu">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                            <div class="collapse-button" >
                                <span class="navbar-toggler-icon"></span>
                                <span class="navbar-toggler-icon"></span>
                                <span class="navbar-toggler-icon"></span>
                            </div>
                        </button>
                        <div class="collapse navbar-collapse" id="collapsibleNavbar">
                            <ul class="navbar-nav menu-center">

                            @if (Auth::guard('admin')->check())
                                <li class="nav-item">
                                    <a class="nav-link" href="/adminpage">Live Stream</a>
                                </li>
                            @endif

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                    {{ __('menu.categories') }}
                                    </a>
                                    <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/latest">{{ __('menu.latest') }}</a>
																		<a class="dropdown-item" href="/all_episodes">{{ __('menu.all_episodes') }}</a>   
                                    <a class="dropdown-item" href="/most_viewed">{{ __('menu.mostviewed') }}</a>
                                    <a class="dropdown-item" href="/category/business">{{ __('menu.business') }}</a>

                                    <a class="dropdown-item" href="/category/tech">{{ __('menu.tech') }}</a>
                                    <a class="dropdown-item" href="/category/lifestyle">{{ __('menu.lifestyle') }}</a>


                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/shows">{{ __('menu.shows') }}</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="/archive">{{ __('menu.archive') }}</a>
                                </li>                 -->
                            </ul>
                            <ul class="inline-list secondary-navigation">
                                <li><a href="#" class="hover-blue open-overlay-btn" data-overlay="search-overlay"><i class="far fa-search"></i></a></li>
                                @if(Auth::user())
                                    <li><a href="{{ '/account' }}" class="hover-blue"><i class="fas fa-user"></i></a></li>
                                @else
                                    <li><a href="javascript:" class="hover-blue regstr-btn" data-toggle="modal" data-target="#access-register-modal">‫إنشاء‬ حساب‬ </a>
                                        <a href="javascript:" class="hover-blue login-icon" data-toggle="modal" data-target="#access-login-modal"><img width="33px" src="/assets/img/login-icon.png"></i></a></li>
                                @endif
                            </ul>
                        </div>
                        </nav>
                    </div>

                    <!--div class="col-xl-3 col-lg-3 col-md-3 d-none d-md-block text-left">
                        <ul class="inline-list secondary-navigation">
                            <li><a href="#" class="hover-blue open-overlay-btn" data-overlay="search-overlay"><i class="far fa-search"></i></a></li>
                            @if(Auth::user())
                                <li><a href="{{ '/account' }}" class="hover-blue"><i class="fas fa-user"></i></a></li>
                            @else
                                <li><a href="javascript:" class="hover-blue" data-toggle="modal" data-target="#access-login-modal">تسجيل الدخول</a>
                                    او <a href="javascript:" class="hover-blue" data-toggle="modal" data-target="#access-register-modal">التسجيل</a></li>
                            @endif
                        </ul>
                    </div-->
                </div>
            </div>
        </div>
    </div>
    <!--div class="header header-bottom">
        <div class="bottom">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-6 text-right">
                        <div class="logo-container">
                            <a href="/" class="d-inline-block"><img src="{{'/assets/img/logo.png'}}" alt="SMASHITV logo"></a>
                        </div>
                    </div>

                    <div class="col-6 d-block d-md-none">
                        <div class="mobile-menu-toggle-container text-left">
                            <a href="javascript:" id="toggle-mobile-menu"><i class="far fa-bars"></i></a>
                        </div>
                    </div>

                    <div class="col-12 d-block d-md-none">
                        <ul class="mobile-navigation closed" id="mobile-menu">
                            <li>
                                <a href="#" class="hover-orange open-overlay-btn" data-overlay="search-overlay"><i class="far fa-search"></i></a> |

                            @if(Auth::user())
                                {{--<li><a href="" class="hover-blue"><i class="fas fa-user"></i></a></li>--}}
                                <a href="{{ '/account' }}" class="hover-orange" ><i class="fas fa-user-alt"></i></a>
                            @else
                                <a href="javascript:" class="hover-orange" data-toggle="modal" data-target="#access-login-modal">تسجيل الدخول</a>
                                    او <a href="javascript:" class="hover-orange" data-toggle="modal" data-target="#access-register-modal">التسجيل</a>
                            @endif
                                {{--<a href="#" class="hover-orange" data-toggle="modal" data-target="#login-overlay"><i class="fas fa-user-alt"></i></a>--}}

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div-->
</header>
