<?php
$poster_url='';
if(empty($video->poster_url))
{
$poster_url=$video->poster;	
}	
else
{
$poster_url=$video->poster_url;		
}	

?>
<div class="video-card animate-element">
    <div class="video-container">
        <a href="{{ '/video/' . $video->slug }}" class="d-block">
            <div class="vid-box">
                 <video id="video-{{ $video->id }}" data-id="video-{{ $video->id }}" class="video-js videojs-player vjs-16-9" poster="{{ $poster_url }}">   
					<!-- <source src="{{ $poster_url }}" type="application/x-mpegURL"> -->
                </video>
            </div>
            <div class="icon-play">
                <i class="far fa-play-circle"></i>
            </div>
        </a>
    </div>
    <div class="date-display">
        <?php 
            if( !empty( $video->duration ) ){
                $returnValue = preg_replace('/(.00)/', '', $video->duration, -1);

                $totalCount = substr_count($returnValue, ':');

                /*$explode_dur = explode(':', $returnValue);
                $explode_dur = array_filter($explode_dur, function($v, $i){
                    if( $v != 00 ){
                        return true;
                    }
                }, ARRAY_FILTER_USE_BOTH);*/
                
                // $new_duration =  implode(':', $explode_dur);

                $new_duration = ($totalCount > 1) ? preg_replace( '/^00:/', '' ,$returnValue ) : $returnValue;

                // $new_duration = preg_replace( '/^00:/', '' ,$returnValue );

            } else {
                $new_duration = '';
            }

        ?>
        <?php if( !empty($new_duration) ) { ?>
            <span class="vod_dur">{{ $new_duration }}<i class="fa fa-play" aria-hidden="true"></i></span>
        <?php } ?>
        <span class="published-date date-three"> {{ App\Helpers\Minion::arabic_date_format($video->created_at)}}</span>
        <span class="published-date date-center"> {{ App\Helpers\Minion::human_date_format($video->created_at) }}</span>
        
    </div>
    <a href="{{ '/video/' . $video->slug }}" class="title d-block hover-black">{{ $video->title }}</a>
    
    <div class="link-overlay">
        <a href="{{ '/video/' . $video->slug }}"></a>
    </div>
</div>