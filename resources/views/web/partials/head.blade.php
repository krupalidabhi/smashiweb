<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125335704-1"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
	 function gtag(){dataLayer.push(arguments);}
	 gtag('js', new Date());  gtag('config', 'UA-125335704-1');
	</script>


    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="author" content="Strategies dC"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('web.partials.social-meta-tags')

    <link rel="apple-touch-icon" sizes="180x180" href="{{'/assets/favicon/apple-touch-icon.png'}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{'/assets/favicon/favicon-32x32.png'}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{'/assets/favicon/favicon-16x16.png'}}">
    <link rel="manifest" href="{{'/assets/favicon/site.webmanifest'}}">
    <link rel="mask-icon" href="{{'/assets/favicon/safari-pinned-tab.svg'}}" color="#f97b3e">
    <link rel="shortcut icon" href="{{'/assets/favicon/favicon.ico'}}">
    <!--link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"-->
    <meta name="msapplication-TileColor" content="#437efb">
    <meta name="msapplication-config" content="{{'/assets/favicon/browserconfig.xml'}}">
    <meta name="theme-color" content="#437efb">

    <script src="https://www.gstatic.com/firebasejs/7.2.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.2.1/firebase-messaging.js"></script>
    <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
    <script type="IN/Share" data-url="{{ URL::to('/') }}" data-counter="top"></script>
    <script src="{{'/assets/js/jquery.min.js'}}" ></script>
    <script src="{{'/assets/js/jquery.mask.min.js'}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" integrity="sha256-p6xU9YulB7E2Ic62/PX+h59ayb3PBJ0WFTEQxq0EjHw=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
    <!--script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script-->
    <script src="{{'/assets/js/dacast.js'}}" ></script>
    <script src="{{'/assets/js/custom.js'}}" ></script>
    <link rel="stylesheet" href="{{'/assets/css/style-ar.css'}}"/>
    <link rel="stylesheet" href="{{'/assets/css/custom.css'}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-ads/6.7.0/videojs-contrib-ads.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/videojs-ima/1.8.3/videojs.ima.css" integrity="sha512-vvsEsf+dZDp6wbommO1Jbb2bpFhVQjw6pIzfE5fUY5Fgkmsgn/16sQWegqrd236T69kK5F1SbGZ+yK46a9il5A==" crossorigin="anonymous" />

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '357791278380483');
      fbq('track', 'PageView');
    </script>

    <link href="https://vjs.zencdn.net/5.19.2/video-js.css" rel="stylesheet">

    <script src="https://vjs.zencdn.net/5.19.2/video.js"></script>
    <!-- <script src="{{'/assets/js/hls.min.js?v=v0.9.1'}}"></script> -->
    <script src="{{'/assets/js/videojs5-hlsjs-source-handler.min.js?v=0.3.1'}}"></script>

    <script src="{{'/assets/js/vjs-quality-picker.js?v=v0.0.2'}}"></script>

    <script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-ads/6.7.0/videojs-contrib-ads.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-ima/1.8.3/videojs.ima.min.js" integrity="sha512-JU4ZhJAvKzfjuu/qzmDqZ70oLSHm5Fu7VP4jzlrEEYCaq3uWJghvNsmbHURk7+ekZPq+DzigABvf8UduAstWvA==" crossorigin="anonymous"></script>

    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=357791278380483&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->  
</head>