@if(isset($meta_tags) )
    @if($meta_tags == 'page')
        <title>{{ @$page->title }} - SMASHITV</title>
        <meta name="description" content="{{ substr(strip_tags(@$page->body), 0, 300) }}"/>
        <meta property="og:title" content="{{ @$page->title }}"/>
        <meta property="og:description" content="{{ substr(strip_tags(@$page->body), 0, 300) }}"/>
        <meta property="og:image" content="{{ url('/assets/img/social-share-img.jpg') }}"/>
        <meta name="twitter:description" content="{{ substr(strip_tags(@$page->body), 0, 300) }}"/>
        <meta name="twitter:title" content="{{ @$page->title }}"/>
        <meta name="twitter:image" content="{{ url('/assets/img/social-share-img.jpg') }}"/>
    @elseif($meta_tags == 'post')
        <title>{{ $post->title }} - SMASHITV</title>
        <meta name="description" content="{{ substr(strip_tags($post->body), 0, 300) }}"/>
        <meta property="og:title" content="{{ $post->title }}"/>
        <meta property="og:description" content="{{ substr(strip_tags($post->body), 0, 300) }}"/>
        @if($post->image)
            <meta property="og:image" content="{{ $post->image }}"/>
        @else
            <meta property="og:image" content="{{ url('/assets/img/social-share-img.jpg') }}"/>
        @endif
        <meta name="twitter:description" content="{{ substr(strip_tags($post->body), 0, 300) }}"/>
        <meta name="twitter:title" content="{{ $post->title }}"/>
        <meta name="twitter:image" content="{{ $post->image }}"/>
    @elseif($meta_tags == 'video')
        <title>{{ $main_video->title }} - SMASHITV</title>
        <meta name="description" content="{{ substr(strip_tags($main_video->body), 0, 300) }}"/>
        <meta property="og:title" content="{{ $main_video->title }}"/>
        <meta property="og:description" content="{{ substr(strip_tags($main_video->body), 0, 300) }}"/>
        @if($main_video->poster_url)
            <meta property="og:image:url" content="{{ rtrim(preg_replace ('/https/','https',$main_video->poster_url,1),'/') }}"/>
            <meta property="og:image" content="{{ rtrim(preg_replace ('/https/','https',$main_video->poster_url,1),'/') }}"/>

            <meta name="twitter:image" content="{{ rtrim(preg_replace ('/https/','https',$main_video->poster_url,1),'/') }}"/>
        @elseif($main_video->poster)    
            <meta property="og:image:url" content="{{ rtrim(preg_replace ('/https/','https',$main_video->poster,1),'/') }}"/>
            <meta property="og:image" content="{{ rtrim(preg_replace ('/https/','https',$main_video->poster,1),'/') }}"/> 

            <meta name="twitter:image" content="{{ rtrim(preg_replace ('/https/','https',$main_video->poster,1),'/') }}"/>
        @else
            <meta property="og:image:url" content="{{ url('/assets/img/social-share-img.jpg') }}"/>
            <meta name="twitter:image" content="{{ rtrim(preg_replace ('/https/','https',$main_video->poster,1),'/') }}"/>
        @endif
        <meta property="og:url" content="{{ Url::current() }}"/>
        <meta name="twitter:description" content="{{ substr(strip_tags($main_video->body), 0, 300) }}"/>
        <meta name="twitter:title" content="{{ $main_video->title }}"/>
        <meta name="url" content="{{ Url::current() }}">
    @else
        <title>SMASHITV</title>
        <meta name="description" content="SMASHITV"/>
        <meta property="og:title" content="SMASHITV"/>
        <meta property="og:description" content="SMASHITV"/>
        <meta property="og:image" content="{{ url('/assets/img/social-share-img.jpg') }}"/>
        <meta name="twitter:description" content="SMASHITV"/>
        <meta name="twitter:title" content="SMASHITV"/>
        <meta name="twitter:image" content="{{ url('/assets/img/social-share-img.jpg') }}"/>
    @endif
@else
    <title>SMASHITV</title>
    <meta name="description" content="SMASHITV"/>
    <meta property="og:title" content="SMASHITV"/>
    <meta property="og:description" content="SMASHITV"/>
    <meta property="og:image" content="{{ url('/assets/img/social-share-img.jpg') }}"/>
    <meta name="twitter:description" content="SMASHITV"/>
    <meta name="twitter:title" content="SMASHITV"/>
    <meta name="twitter:image" content="{{ url('/assets/img/social-share-img.jpg') }}"/>
@endif


<meta property="og:locale" content="ar_AR"/>
<meta property="og:type" content="website"/>
{{--<meta property="og:url" content="{{ Url::current() }}"/>--}}
<meta property="og:site_name" content="SMASHITV"/>
<meta property="twitter:card" content="summary_large_image"/> 
<meta property="fb:app_id" content="{{env('FB_APP_ID', '232306637449432')}}"/> 
<meta property="og:image:width" content="450"/> 
<meta property="og:image:height" content="300"/> 