<footer>
    <div class="top-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6 order-xl-2 order-lg-1 col-md-12 col-sm-12 order-sm-2">

                    {!! menu_builder('footer-menu', 'inline-list footer-navigation', 'child') !!}
                </div>
                <div class="col-xl-4 col-lg-6 order-xl-3 order-lg-2 col-md-12 col-sm-12 order-sm-3 text-center">
                    <div class="copy-desktop" >
                        <img class="aug_logo" src="/assets/img/Augustus-logo-arabic.png">&nbsp;&nbsp;
	                    <span style="    font-size: 15px;color: white;" >&copy; سماشي 2019 جميع الحقوق محفوظة </span>
                    </div>
                    {{--<div class="download-mobile-app text-center">--}}
                    {{--<span>حمّل التطبيق الآن</span>--}}
                    {{--<a href="#" class="hover-opacity-low"><img src="{{'/assets/img/app_store_android.png'}}" alt="Download the SmashiTV app on your android device"></a>--}}
                    {{--<a href="#" class="hover-opacity-low"><img src="{{'/assets/img/app_store_ios.png'}}" alt="Download the SmashiTV app on your iOS device"></a>--}}
                    {{--</div>--}}

                    {{--<div class="payment-methods">--}}
                        {{--<i class="fab fa-cc-visa hover-opacity-low"></i>--}}
                        {{--<i class="fab fa-cc-mastercard hover-opacity-low"></i>--}}
                    {{--</div>--}}
                </div>
                <div class="col-xl-3 col-lg-2 order-xl-4 order-lg-3  col-sm-12 order-sm-4">
                    <ul class="inline-list social-media-links">
                    <li><a href="https://www.tiktok.com/@smashitv?language=en&sec_uid=MS4wLjABAAAAabsg-5nzdMPEk8orZrZWzYOScoEWHSPW45XoF3K
vaoSBA1xOVqhNLhdRSq7e6iCm&u_code=d7el00dmkmh57c&timestamp=1564644330&user_id=6720084417199113221&enter_
from=h5_m" class="hover-orange" target="_blank"><img style="width:16px;vertical-align: sub;" src="/assets/img/tiktok.png" class="img-responsive tik-icon"/></a></li>
                        @foreach($social_links as $social_link)
                            <li><a href="{{ $social_link->link }}" class="hover-orange" target="_blank"><i class="{{ $social_link->icon }}"></i></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        
		
		
		<!--
		<div class="row"  >
		<form > 
			 
			
		<div style="padding-top:20px;" >
			
			<div class="sm-form-row" >
			<span style="float:left;" >Newsletter</span>&nbsp;&nbsp;<input type="email" id="cd-email" style="background:#FFF;width:200px;"  name="cd-email" placeholder="Enter your email address" required>
			</div>
			<div style="margin-top:-50px;margin-right: -20px;" >
			<input type="submit" value="OK" style="width:40px;" >
			</div>
		</div>	
		</form>

	
			
    </div>
	-->	
		
		
		
		
	</div>
	</div>
    
	
	<!-- <div class="bottom-section copy-mobile" style="padding-right:18%">
	    <img src="/assets/img/logo_footer.png" height="30px" width="40px" >&nbsp;&nbsp;
	    <span style="font-size:11px;" >&copy; 2018 سماشي جميع الحقوق محفوظة </span>
	</div> -->
</footer>