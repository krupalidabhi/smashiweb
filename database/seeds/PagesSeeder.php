<?php

use App\Models\Page;
use App\Models\Slug;
use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages_to_have = [
			'about',
	        'advertise',
	        'terms',
	        'privacy'
        ];

	    foreach ( $pages_to_have as $item ) {
		    $page_exist = Page::where('title', $item)->first();

		    if(!$page_exist) {
				$page = new Page();
				$page->title = $item;
				$page->content = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pharetra magna eget justo varius dictum. Morbi sagittis a libero id ultricies. Nullam ornare nunc leo, in luctus nulla congue non. Vivamus fermentum vulputate ligula vitae vestibulum. Suspendisse potenti. Phasellus imperdiet, quam id tempor fringilla, turpis nisl fermentum ante, et lobortis massa urna a magna. Fusce congue fringilla ipsum, ut ultricies nisi auctor sed.</p>
<p>&nbsp;</p>
<p>Vestibulum sollicitudin feugiat massa, sit amet vehicula ligula rutrum quis. Etiam placerat odio velit, nec blandit sapien feugiat vel. Aenean volutpat, diam id pretium dignissim, orci sapien venenatis sem, ac maximus sem diam vitae sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi auctor nulla odio, sed condimentum risus sollicitudin id. Proin elementum vel purus at porta. Quisque mollis venenatis eros, ultrices ullamcorper odio venenatis eget. Morbi ut convallis augue. Duis eget ex et tortor vehicula mollis et ac lectus. Nullam gravida, sem nec mollis scelerisque, turpis turpis auctor velit, vitae vestibulum urna lectus ut lorem. Phasellus porttitor condimentum mi, sit amet imperdiet tortor lacinia eget. Pellentesque finibus eros orci. Aliquam non fringilla lorem. Nullam et efficitur tortor.</p>";
				$page->slug = str_slug($item);
				$page->save();
		    }
        }
    }
}
