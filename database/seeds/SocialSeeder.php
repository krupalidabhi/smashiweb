<?php

use App\Models\Social;
use Illuminate\Database\Seeder;

class SocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $social_exist = Social::all();

        if($social_exist->count() == 0) {
            $social_accounts = [
              'facebook' => 'fab fa-facebook-f',
              'youtube' => 'fab fa-youtube',
              'linkedin' => 'fab fa-linkedin-in',
              'instagram' => 'fab fa-instagram',
              'snapchat' => 'fab fa-snapchat-ghost'
            ];

            foreach ($social_accounts as $social_account => $social_icon) {
                $social = new Social();
                $social->name = $social_account;
                $social->icon = $social_icon;
                $social->save();
            }
        }
    }
}
