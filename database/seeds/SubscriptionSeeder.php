<?php

use App\Models\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscriptions = Subscription::all();
        if($subscriptions->count() == 0) {
        	$subscriptions = [
        	    [
        	    	'name' => 'يوميّاً',
                    'product_id' => 29,
                    'pricing_id' => 64,
                    'price' => 5
	            ],
        	    [
        	    	'name' => 'أسبوعياً',
                    'product_id' => 30,
                    'pricing_id' => 65,
                    'price' => 12
	            ],
        	    [
        	    	'name' => 'شهرياً',
                    'product_id' => 31,
                    'pricing_id' => 66,
                    'price' => 25
	            ]
	        ];

	        foreach ( $subscriptions as $subscription ) {
		        $sub = new Subscription();
		        $sub->name = $subscription['name'];
		        $sub->price = $subscription['price'];
		        $sub->product_id= $subscription['product_id'];
		        $sub->pricing_id = $subscription['pricing_id'];
		        $sub->save();
        	}
        }
    }
}
