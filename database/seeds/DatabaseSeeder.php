<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(AdminTableSeeder::class);
		$this->call(CountriesSeeder::class);
		$this->call(PagesSeeder::class);
		$this->call(SubscriptionSeeder::class);
    }
}
