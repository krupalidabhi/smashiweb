<?php

use App\Helpers\Minion;
use App\Models\AdminUser;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $admin_user_exist = AdminUser::where('email', '=', 'ayman@digitalpandas.io')->first();

    	if(!$admin_user_exist) {
			$admin_user = new AdminUser();
			$admin_user->name = 'Ayman Bitar';
			$admin_user->email = 'ayman@digitalpandas.io';
			$admin_user->password = Hash::make('123');
			$admin_user->slug = Minion::create_slug('Ayman Bitar', AdminUser::class);
			$admin_user->save();
	    }

	    $admin_user_exist = AdminUser::where('email', '=', 'giles@weareaugustus.com')->first();

    	if(!$admin_user_exist) {
			$admin_user = new AdminUser();
			$admin_user->name = 'Giles Wright';
			$admin_user->email = 'giles@weareaugustus.com';
			$admin_user->password = Hash::make('123');
			$admin_user->slug = Minion::create_slug('Giles Wright', AdminUser::class);
			$admin_user->save();
	    }
    }
}