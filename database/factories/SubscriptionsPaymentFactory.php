<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SubscriptionsPayment;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(SubscriptionsPayment::class, function (Faker $faker) {
    return [
      'user_id' => $faker->sentence(5),
      'order_id' => random_int(1,3),
      'name' => random_int(1,3),
      'email' => $faker->unique()->safeEmail,
      'mobile' => random_int(1,9),
      'billing_address' => $faker->sentence(5),
      'billing_city' => $faker->sentence(5),
      'billing_country' => $faker->sentence(5),
      'billing_state' => $faker->sentence(5),
      'billing_zip_code' => random_int(1,3),
      'token' => Str::random(10),
      'plan_selected' => random_int(1,3),
      'amount' => random_int(1,3),
      'tax_amount' => random_int(1,3),
      'currency_code' => $faker->sentence(3),
  ];
});
