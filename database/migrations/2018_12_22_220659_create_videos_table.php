<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('categories', function(Blueprint $table) {
			$table->increments('id');

			$table->string('name');

			$table->integer('order')->default(0);

			$table->timestamps();
	    });

        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');

	        $table->string('link');

            $table->string('raw_link')->nullable();

            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->text('body')->nullable();

            $table->boolean('is_featured')->default(0);

            $table->string('slug');

            $table->timestamps();
        });

        Schema::create('videos_comments', function(Blueprint $table) {
			$table->increments('id');

	        $table->unsignedInteger('video_id');
	        $table->foreign('video_id')->references('id')->on('videos')->onDelete('cascade');

	        $table->text('comment');

	        $table->timestamps();
        });

        Schema::create('videos_likes', function(Blueprint $table) {
        	$table->increments('id');

	        $table->unsignedInteger('video_id');
	        $table->foreign('video_id')->references('id')->on('videos')->onDelete('cascade');

	        $table->enum('enum', [1, 2])->comment('1 = Like, 2 = Dislike');

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('videos_likes', function(Blueprint $table) {
			$table->dropForeign('videos_likes_video_id_foreign');
	    });
        Schema::dropIfExists('videos_likes');

	    Schema::table('videos_comments', function(Blueprint $table) {
		    $table->dropForeign('videos_comments_video_id_foreign');
	    });
	    Schema::dropIfExists('videos_comments');

	    Schema::table('videos', function(Blueprint $table) {
		    $table->dropForeign('videos_category_id_foreign');
	    });
        Schema::dropIfExists('videos');

        Schema::dropIfExists('categories');
    }
}
