<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionPaytabsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'transactions_paytabs', function ( Blueprint $table ) {
			$table->increments( 'id' );

			$table->unsignedInteger( 'transaction_id' );
			$table->foreign( 'transaction_id' )->references( 'id' )->on( 'transactions' )->onDelete( 'cascade' );

			$table->string( 'paytab_transaction_id' );

			$table->string( 'order_id' );

			$table->string( 'response_code' );

			$table->string( 'response_message' );

			$table->string( 'customer_name' );

			$table->string( 'customer_email' );

			$table->string( 'transaction_currency' );

			$table->string( 'customer_phone' );

			$table->string( 'last_4_digits' );

			$table->string( 'first_4_digits' );

			$table->string( 'card_brand' );

			$table->string( 'trans_date' );

			$table->string( 'pt_customer_email' )->nullable();

			$table->string( 'pt_customer_password' )->nullable();

			$table->string( 'pt_token' )->nullable();

			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'transactions_paytabs' );
	}
}
