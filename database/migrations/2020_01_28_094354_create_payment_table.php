<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->string('initial_amount')->nullable();
            $table->date('valid_date')->nullable();
            $table->string('transaction_id',255)->nullable();
            $table->integer('panding_cycle')->nullable();
            $table->integer('completed_cycle')->nullable();
            $table->string('status')->nullable();
            $table->string('payment_status')->nullable();
            $table->decimal('total_billing_amount')->nullable();
            $table->decimal('total_billed_amount')->nullable();
            $table->decimal('billing_amount')->nullable();
            $table->string('billing_address')->nullable();
            $table->string('payment_from')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('currency')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment');
    }
}
