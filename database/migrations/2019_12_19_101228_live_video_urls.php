<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LiveVideoUrls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('live_video_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('web_url')->nullable();
            $table->string('android_url')->nullable();
            $table->string('ios_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('live_video_urls');
    }
}
