<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->string('sku');

            $table->string('price');

            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');

            $table->string('product_name');

            $table->string('operator');

            $table->string('subscription_plan_id');

            $table->string('catalog_name');

            $table->timestamps();
        });

        Schema::create('transactions', function(Blueprint $table) {
			$table->increments('id');

			$table->tinyInteger('response_status')->nullable();

	        $table->integer('user_id')->unsigned();
	        $table->foreign('user_id')->references('id')->on('countries')->onDelete('cascade');

	        $table->integer('subscription_id')->unsigned();
	        $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');

	        $table->text('request_raw')->nullable();

	        $table->string('subscription_contract_id')->nullable();

	        $table->string('payment_transaction_status_code')->nullable();

	        $table->string('tpay_transaction_id')->nullable();

	        $table->dateTime('next_payment_date')->nullable();

	        $table->text('error_message')->nullable();

	        $table->integer('subscription_contract_status')->nullable();

	        $table->string('msisdn')->nullable();

			$table->timestamps();
        });

        Schema::create('users_subscriptions', function(Blueprint $table) {
			$table->increments('id');

	        $table->integer('transaction_id')->unsigned();
	        $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');

	        $table->integer('user_id')->unsigned();
	        $table->foreign('user_id')->references('id')->on('countries')->onDelete('cascade');

	        $table->integer('subscription_id')->unsigned();
	        $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');

			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_subscriptions');
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('subscriptions');
    }
}
