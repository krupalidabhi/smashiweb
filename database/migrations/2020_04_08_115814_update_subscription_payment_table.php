<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSubscriptionPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('order_id');
        });

        Schema::create('recurrence_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->text('order_id');
            $table->text('payment_reference');
            $table->text('pt_customer_email');
            $table->text('pt_customer_password');
            $table->text('pt_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
