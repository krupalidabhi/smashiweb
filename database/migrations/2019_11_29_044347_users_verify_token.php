<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersVerifyToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_verify_token', function(Blueprint $table) {
			$table->increments('id');

			$table->text('token');
            
            $table->text('user_email');

			$table->integer('user_id')->nullable();

            $table->text('timestamp');

            $table->text('reset_password')->nullable();

			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
