<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->increments('id');

            $table->string('name');

            $table->string('email');

            $table->string('password');

            $table->string('slug')->nullable();

	        $table->rememberToken();

            $table->timestamps();
        });

	    Schema::create('admin_user_role', function (Blueprint $table) {
		    $table->increments('id');

		    $table->integer('admin_user_id');

		    $table->integer('role_id');

		    $table->timestamps();
	    });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('admin_user_role');
		Schema::dropIfExists('admin_users');
	}
}
