<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LogSaveInDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $tabel) {
            $tabel->increments('id');
            $tabel->string('api_name')->nullable();
            $tabel->text('description')->nullable();
            $tabel->string('file_Name')->nullable();
            $tabel->string('type')->nullable();
            $tabel->string('code')->nullable();
            $tabel->string('line')->nullable();;
            $tabel->ipAddress('ip');
            $tabel->string('user_agent', 200)->nullable();
            $tabel->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            //
        });
    }
}
