<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBillingTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'users_billing', function ( Blueprint $table ) {
			$table->increments( 'id' );

			$table->unsignedInteger( 'user_id' );
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );

			$table->text( 'address' );

			$table->string( 'phone_number' );

			$table->string( 'city' );

			$table->string( 'state' );

			$table->unsignedInteger( 'country_id' );
			$table->foreign( 'country_id' )->references( 'id' )->on( 'countries' )->onDelete( 'cascade' );

			$table->string( 'postal_code' );

			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'users_billing' );
	}
}
