Nova.booting((Vue, router, store) => {
  Vue.component('index-popup-field', require('./components/IndexField'))
  Vue.component('detail-popup-field', require('./components/DetailField'))
  Vue.component('form-popup-field', require('./components/FormField'))
})
