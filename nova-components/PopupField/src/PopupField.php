<?php

namespace Amazon\PopupField;

use Laravel\Nova\Fields\Field;

class PopupField extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'popup-field';
}
